import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class OTPRequest {
  int? otpId;
  String? otp;

  OTPRequest({this.otpId, this.otp});

  OTPRequest.fromJson(Map<String, dynamic> json) {
    otpId = json['otpId'];
    otp = json['otp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['otpId'] = otpId;
    data['otp'] = otp;
    return data;
  }
}
