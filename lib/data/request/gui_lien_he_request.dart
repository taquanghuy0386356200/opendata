class GuiLienHeRequest {
  String? name;
  String? email;
  String? title;
  String? content;

  GuiLienHeRequest({this.name, this.email, this.title, this.content});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['name'] = this.name;
    data['email'] = this.email;
    data['title'] = this.title;
    data['content'] = this.content;
    return data;
  }
}
