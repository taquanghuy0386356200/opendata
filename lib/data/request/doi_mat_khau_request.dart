import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class DoiMatKhauRequest {
  String? oldPassword;
  String? newPassword;
  String? confirmPassword;
  String? email;

  DoiMatKhauRequest(
      {this.oldPassword, this.newPassword, this.confirmPassword, this.email});

  DoiMatKhauRequest.fromJson(Map<String, dynamic> json) {
    oldPassword = json['oldPassword'];
    newPassword = json['newPassword'];
    confirmPassword = json['confirmPassword'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['oldPassword'] = oldPassword;
    data['newPassword'] = newPassword;
    data['confirmPassword'] = confirmPassword;
    data['email'] = email;
    return data;
  }
}
