import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class XacThucTKRequest {
  String? token;

  XacThucTKRequest({this.token});

  XacThucTKRequest.fromJson(Map<String, dynamic> json) {
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['token'] = token;
    return data;
  }
}
