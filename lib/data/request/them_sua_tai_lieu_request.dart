import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class ThemSuaTaiLieuRequest {
  int? id;
  String? title;
  String? categoryID;
  String? categoryName;
  String? subcategoryID;
  String? subcategoryName;
  String? description;
  String? fileDinhKem;
  String? status;
  String? nguoiDangTai;

  ThemSuaTaiLieuRequest({
    this.id,
    this.title,
    this.categoryID,
    this.categoryName,
    this.subcategoryID,
    this.subcategoryName,
    this.description,
    this.fileDinhKem,
    this.status,
    this.nguoiDangTai,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['categoryID'] = categoryID;
    data['categoryName'] = categoryName;
    data['subcategoryID'] = subcategoryID;
    data['subcategoryName'] = subcategoryName;
    data['description'] = description;
    data['fileDinhKem'] = fileDinhKem;
    data['status'] = status;
    data['nguoiDangTai'] = nguoiDangTai;
    return data;
  }

  @override
  String toString() {
    return 'ThemSuaTaiLieuRequest{id: $id, title: $title, categoryID: $categoryID, categoryName: $categoryName, subcategoryID: $subcategoryID, subcategoryName: $subcategoryName, description: $description, fileDinhKem: $fileDinhKem, status: $status, nguoiDangTai: $nguoiDangTai}';
  }
}
