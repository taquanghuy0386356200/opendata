import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class RefreshTokenRequest {
  String? grant_type;
  String? refresh_token;

  RefreshTokenRequest({
    this.grant_type,
    this.refresh_token,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['grant_type'] = grant_type;
    data['refresh_token'] = refresh_token;
    return data;
  }
}
