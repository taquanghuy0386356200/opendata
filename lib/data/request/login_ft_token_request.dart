import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class LoginFeatTokenRequest {

  String? grant_type;
  String? password;
  String? username;

  LoginFeatTokenRequest({this.grant_type, this.password, this.username});

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['grant_type'] = grant_type;
    data['password'] = password;
    data['username'] = username;
    return data;
  }
}
