import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class RegisterRequest {
  String? email;
  String? name;
  String? password;
  String? confirmPassword;
  String? username;
  bool? usernameIsEmail;
  String? captchaCode;
  String? capchaId;

  RegisterRequest({
    this.email,
    this.name,
    this.password,
    this.confirmPassword,
    this.username,
    this.usernameIsEmail,
    this.captchaCode,
    this.capchaId,
  });

  RegisterRequest.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    name = json['name'];
    password = json['password'];
    confirmPassword = json['confirmPassword'];
    username = json['username'];
    usernameIsEmail = json['usernameIsEmail'];
    captchaCode = json['captchaCode'];
    capchaId = json['capchaId'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['name'] = name;
    data['password'] = password;
    data['confirmPassword'] = confirmPassword;
    data['username'] = username;
    data['usernameIsEmail'] = usernameIsEmail;
    data['captchaCode'] = captchaCode;
    data['capchaId'] = capchaId;
    return data;
  }
}

class QuenMatKhauRequest {
  String email;

  QuenMatKhauRequest(this.email);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    return data;
  }
}

class ThietLapMatKhauRequest {
  String? newPassword;
  String? confirmPassword;
  String? email;
  int? otpId;
  String? token;

  ThietLapMatKhauRequest({
    this.newPassword,
    this.confirmPassword,
    this.email,
    this.otpId,
    this.token,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['newPassword'] = newPassword;
    data['confirmPassword'] = confirmPassword;
    data['email'] = email;
    data['otpId'] = otpId;
    data['token'] = token;
    return data;
  }
}
