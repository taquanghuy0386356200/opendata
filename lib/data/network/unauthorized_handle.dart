import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/data/request/refreshTokenRequest.dart';
import 'package:bnv_opendata/data/response/authentication/get_token_login_response.dart';
import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_domain_am_repo.dart';
import 'package:get/get.dart';

class HandleUnauthorized {
  static final List<_ResultRefreshTokenCallBack> _callBackUnauthorized = [];

  static void resignRefreshToken({
    required Function(String) onRefreshToken,
    required Function(AppException) onError,
  }) {
    if (_callBackUnauthorized.isEmpty) {
      _handleUnauthorized().then((value) {
        value.when(
          success: (res) {
            PrefsService.saveToken(res.accessToken ?? '');
            PrefsService.saveRefreshToken(
              res.refreshToken ?? '',
            );
            for (final element in _callBackUnauthorized) {
              element.onRefreshToken.call(res.accessToken ?? '');
            }
            _callBackUnauthorized.clear();
          },
          error: (error) {
            for (final element in _callBackUnauthorized) {
              element.onError.call(error);
            }
            _callBackUnauthorized.clear();
          },
        );
      });
    }
    _callBackUnauthorized
        .add(_ResultRefreshTokenCallBack(onRefreshToken, onError));
  }

  static Future<Result<GetTokenLoginResponse>> _handleUnauthorized() async {
    final AuthenticationDomainAmRepo _loginRepo = Get.find();

    return _loginRepo.refreshToken(
      RefreshTokenRequest(
        grant_type: 'refresh_token',
        refresh_token: PrefsService.getRefreshToken(),
      ),
    );
  }
}

class _ResultRefreshTokenCallBack {
  final Function(String) onRefreshToken;
  final Function(AppException) onError;

  _ResultRefreshTokenCallBack(this.onRefreshToken, this.onError);
}
