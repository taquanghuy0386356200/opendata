import 'package:bnv_opendata/data/response/home/categories_response.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

import '../../../utils/constants/api_constants.dart';

part 'home_service.g.dart';

@RestApi()
abstract class HomeService {
  @factoryMethod
  factory HomeService(Dio dio, {String baseUrl}) = _HomeService;

  @GET(ApiConstants.LIST_DANH_SACH_LINH_VUC)
  Future<CategoriesTotalResponse> getListCategories();
}
