import 'package:bnv_opendata/data/response/home/document_response.dart';
import 'package:bnv_opendata/data/response/thong_bao/chi_tiet_thong_bao_response.dart';
import 'package:bnv_opendata/data/response/thong_bao/thong_bao_response.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/http.dart';
import 'package:retrofit/retrofit.dart';

part 'thong_bao_service.g.dart';

@RestApi()
abstract class ThongBaoService {
  @factoryMethod
  factory ThongBaoService(Dio dio, {String baseUrl}) = _ThongBaoService;

  @GET(ApiConstants.LIST_THONG_BAO)
  Future<ThongBaoResponse> getListThongBao(
    @Query('keySearch') String? keySearch,
    @Query('pageSize') int? pageSize,
    @Query('pageIndex') int? pageIndex,
  );

  @GET(ApiConstants.LIST_VAN_BAN)
  Future<DocumentTotalResponse> getListVanBan(
    @Query('keySearch') String? keySearch,
    @Query('pageSize') int? pageSize,
    @Query('pageIndex') int? pageIndex,
  );

  @GET(ApiConstants.CHI_TIET_THONG_BAO)
  Future<DetailThongBaoResponse> getDetailThongBao(
    @Path('id') String? id,
  );
}
