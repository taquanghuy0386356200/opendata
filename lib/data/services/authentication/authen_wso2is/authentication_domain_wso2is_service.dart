// ignore: depend_on_referenced_packages
import 'package:bnv_opendata/data/request/login_ft_token_request.dart';

import 'package:bnv_opendata/data/response/authentication/get_token_login_response.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'authentication_domain_wso2is_service.g.dart';

@RestApi()
abstract class AuthenticationWso2isService {
  @factoryMethod
  factory AuthenticationWso2isService(Dio dio, {String baseUrl}) =
      _AuthenticationWso2isService;

  @POST(ApiConstants.LOGIN_OPEN_DATA)
  Future<GetTokenLoginResponse> loginGetToken(
    @Body() LoginFeatTokenRequest request,
  );

  // @POST(ApiConstants.REFRESH_TOKEN)
  // Future<GetTokenLoginResponse> refreshToken(
  //   @Body() LoginFeatTokenRequest request,
  // );
}
