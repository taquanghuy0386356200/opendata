import 'package:bnv_opendata/data/request/login_ft_token_request.dart';
import 'package:bnv_opendata/data/request/refreshTokenRequest.dart';
import 'package:bnv_opendata/data/response/authentication/get_token_login_response.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/retrofit.dart';

part 'authen_wso2is_refresh_token_service.g.dart';

@RestApi()
abstract class AuthenWso2isRefreshTokenService {
  @factoryMethod
  factory AuthenWso2isRefreshTokenService(Dio dio, {String baseUrl}) =
      _AuthenWso2isRefreshTokenService;

  @POST(ApiConstants.REFRESH_TOKEN)
  Future<GetTokenLoginResponse> refreshToken(
    @Body() RefreshTokenRequest request,
  );
}
