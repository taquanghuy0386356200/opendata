import 'package:bnv_opendata/data/request/doi_mat_khau_request.dart';
import 'package:bnv_opendata/data/request/otp_request.dart';
import 'package:bnv_opendata/data/request/register_request.dart';
import 'package:bnv_opendata/data/request/xac_thuc_tai_khoan_request.dart';
import 'package:bnv_opendata/data/response/authentication/capcha_response.dart';
import 'package:bnv_opendata/data/response/authentication/doi_mat_khau_response.dart';
import 'package:bnv_opendata/data/response/authentication/lost_pw_response.dart';
import 'package:bnv_opendata/data/response/authentication/otp_response.dart';
import 'package:bnv_opendata/data/response/authentication/register_reponse.dart';
import 'package:bnv_opendata/data/response/authentication/thiet_lap_mat_khau_response.dart';
import 'package:bnv_opendata/data/response/authentication/xac_thuc_tk_response.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/http.dart';
import 'package:retrofit/retrofit.dart';

part 'authentication_service.g.dart';

@RestApi()
abstract class AuthenticationService {
  @factoryMethod
  factory AuthenticationService(Dio dio, {String baseUrl}) =
      _AuthenticationService;

  @POST(ApiConstants.REGISTER_OPEN_DATA)
  Future<RegisterTotalReponse> registerUser(
    @Body() RegisterRequest request,
  );

  @POST(ApiConstants.LOST_PASSWORD)
  Future<LostPasswordRespone> quenMatKhau(
    @Body() QuenMatKhauRequest request,
  );

  @POST(ApiConstants.THIET_LAP_MAT_KHAU)
  Future<ThietLapMatKhauResponse> thietLapMatKhau(
    @Body() ThietLapMatKhauRequest request,
  );

  @POST(ApiConstants.OTP)
  Future<OTPResponse> handleOTP(
    @Body() OTPRequest request,
  );

  @POST(ApiConstants.DOI_MAT_KHAU)
  Future<DoiMatKhauResponse> doiMatKhau(
    @Body() DoiMatKhauRequest request,
  );

  @POST(ApiConstants.XAC_THUC_TAI_KHOAN)
  Future<XacThucTKResponse> xacThucTaiKhoan(
    @Body() XacThucTKRequest request,
  );

  @GET(ApiConstants.GET_CAPCHAT)
  Future<CapchaResponse> getCapchat();
}
