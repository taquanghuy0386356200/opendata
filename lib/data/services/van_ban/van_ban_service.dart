import 'package:bnv_opendata/data/response/home/document_response.dart';
import 'package:bnv_opendata/data/response/thong_bao/chi_tiet_thong_bao_response.dart';
import 'package:bnv_opendata/data/response/van_ban/van_ban_response.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/http.dart';
import 'package:retrofit/retrofit.dart';

part 'van_ban_service.g.dart';

@RestApi()
abstract class VanBanService {
  @factoryMethod
  factory VanBanService(Dio dio, {String baseUrl}) = _VanBanService;

  @GET(ApiConstants.LIST_VAN_BAN)
  Future<VanBanResponse> getListVanBan(
    @Query('keySearch') String? keySearch,
    @Query('pageSize') int? pageSize,
    @Query('pageIndex') int? pageIndex,
  );

  @GET(ApiConstants.CHI_TIET_VAN_BAN)
  Future<DetailThongBaoResponse> getDetailVanBan(
    @Path('id') String id,
  );
}
