import 'dart:io';

import 'package:bnv_opendata/data/request/gui_lien_he_request.dart';
import 'package:bnv_opendata/data/request/them_sua_tai_lieu_request.dart';
import 'package:bnv_opendata/data/response/tien_ich/chi_tiet_huong_dan_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/chi_tiet_tai_lieu_dang_tai_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/danh_dau_du_lieu_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/gui_lien_he_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/huong_dan_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/lien_he_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/post_file_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/tai_lieu_dang_tai_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/them_sua_tai_lieu_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/xoa_tai_lieu_response.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';

// ignore: depend_on_referenced_packages
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/http.dart';

part 'tien_ich_service.g.dart';

@RestApi()
abstract class TienIchService {
  @factoryMethod
  factory TienIchService(Dio dio, {String baseUrl}) = _TienIchService;

  @GET(ApiConstants.LIST_HUONG_DAN)
  Future<HuongDanResponse> getListHuongDan(
    @Query('keySearch') String? keySearch,
    @Query('pageSize') int? pageSize,
    @Query('pageIndex') int? pageIndex,
  );

  @GET(ApiConstants.CHI_TIET_HUONG_DAN)
  Future<DetailHuongDanResponse> getDetailHuongDan(
    @Path('id') String? id,
  );

  @GET(ApiConstants.LIEN_HE)
  Future<LienHeResponse> getLienHe();

  @POST(ApiConstants.GUI_LIEN_HE)
  Future<SentLienHeResponse> guiLienHe(
    @Body() GuiLienHeRequest guiLienHeRequest,
  );

  @POST(ApiConstants.UPLOAD_FILE)
  @MultiPart()
  Future<PostFileTotalResponse> uploadFile(
    @Part() List<File> file,
  );

  @POST(ApiConstants.THEM_SUA_TAI_LIEU)
  Future<ThemSuaTaiLieuTotalResponse> themSuaTaiLieu(
    @Body() ThemSuaTaiLieuRequest request,
  );

  @POST(ApiConstants.XOA_TAI_LIEU)
  Future<XoaTaiLieuResponse> xoaTaiLieu(
    @Query('id') int idTaiLieu,
  );

  @GET(ApiConstants.CHI_TIET_TAI_LIEU_DANG_TAI)
  Future<ChiTietTaiLieuDangTaiResponse> getChiTietTaiLieuDangTai(
    @Path('id') int idTaiLieu,
  );

  @POST(ApiConstants.DANH_DAU_DU_LIEU)
  Future<DanhDauDuLieuResponse> danhDauDuLieu(
    @Query('id') int id,
    @Query('nguoiDanhDau') String nguoiDanhDau,
  );

  @POST(ApiConstants.BO_DANH_DAU_DU_LIEU)
  Future<DanhDauDuLieuResponse> boDanhDauDuLieu(
    @Query('id') int id,
    @Query('nguoiDanhDau') String nguoiDanhDau,
  );

  @GET(ApiConstants.DANH_SACH_DANG_TAI_CA_NHAN)
  Future<TaiLieuDangTaiTotalResponse> getTaiLieuDangTaiCaNhan(
    @Query('pageSize') int pageSize,
    @Query('pageIndex') int pageIndex,
    @Query('nguoiDangTai') String nguoiDangTai,
    @Query('keySearch') String keySearch,
    @Query('status') String status,
    @Query('fileType') String fileType,
    @Query('categoryID') String categoryID,
    @Query('subcategoryID') String subcategoryID,
    @Query('fromDate') int fromDate,
    @Query('toDate') int toDate,
  );
}
