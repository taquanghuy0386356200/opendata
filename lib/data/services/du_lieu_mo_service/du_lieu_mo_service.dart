import 'package:bnv_opendata/data/response/du_lieu_mo_response/detail_opendata_response.dart';
import 'package:bnv_opendata/data/response/du_lieu_mo_response/open_data_list_response.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:retrofit/http.dart';
import 'package:retrofit/retrofit.dart';

part 'du_lieu_mo_service.g.dart';

@RestApi()
abstract class DuLieuMoService {
  @factoryMethod
  factory DuLieuMoService(Dio dio, {String baseUrl}) = _DuLieuMoService;

  @GET(ApiConstants.LIST_OPEN_DATA)
  Future<OpenDataListResponse> getListOpenData({
    @Query('pageSize') int? pageSize,
    @Query('pageIndex') int? pageIndex,
    @Query('keySearch') String? keySearch,
    @Query('nguoiDung') String nguoiDung = '',
    @Query('categoryID') int? categoryID,
    @Query('subCategoryID') int? subCategoryID,
    @Query('dataMark') bool? bookmark,
  });

  @GET(ApiConstants.DETAIL_OPEN_DATA)
  Future<DetailOpenDataResponse> getDtailOpenData(
    @Path('id') int id,
    @Query('nguoiDung') String nguoiDung,
  );

  @GET(ApiConstants.DANH_SACH_BAI_VIET)
  Future<OpenDataListResponse> getListBaiViet({
    @Query('pageSize') int? pageSize,
    @Query('pageIndex') int? pageIndex,
    @Query('keySearch') String? keySearch,
    @Query('nguoiDung') String nguoiDung = '',
    @Query('categoryID') int? categoryID,
    @Query('subCategoryID') int? subCategoryID,
    @Query('dataMark') bool? bookmark,
  });

  @GET(ApiConstants.DETAIL_BAI_VIET)
  Future<DetailOpenDataResponse> getDetailBaiViet(
    @Path('id') int id,
    @Query('nguoiDung') String nguoiDung,
  );
}
