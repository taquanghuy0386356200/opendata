import 'package:bnv_opendata/data/response/home/categories_response.dart';
import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/data/services/home/home_service.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/domain/repository/home/home_repository.dart';

class HomeImplement implements HomeRepository {
  final HomeService _service;

  HomeImplement(this._service);

  @override
  Future<Result<List<CategoryModel>>> getListCategoryHome() {
    return runCatchingAsync<CategoriesTotalResponse, List<CategoryModel>>(
      () => _service.getListCategories(),
      (response) =>
          response.body?.items?.map((e) => e.toModel()).toList() ?? [],
    );
  }
}
