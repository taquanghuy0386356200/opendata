import 'package:bnv_opendata/data/response/du_lieu_mo_response/detail_opendata_response.dart';
import 'package:bnv_opendata/data/response/du_lieu_mo_response/open_data_list_response.dart';
import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/data/services/du_lieu_mo_service/du_lieu_mo_service.dart';
import 'package:bnv_opendata/domain/models/open_data_model/detail_open_data_model.dart';
import 'package:bnv_opendata/domain/models/open_data_model/open_data_model.dart';
import 'package:bnv_opendata/domain/repository/du_lieu_mo/du_lieu_mo_repository.dart';

class DuLieuMoImpl implements DuLieuMoRepository {
  final DuLieuMoService _service;

  DuLieuMoImpl(this._service);

  @override
  Future<Result<DataDuLieuMoModel>> getListOpenData({
    int? pageSize,
    int? pageIndex,
    String? keySearch,
    String nguoiDung = '',
    int? categoryID,
    int? subCategoryID,
    bool? bookmark,
  }) {
    return runCatchingAsync<OpenDataListResponse, DataDuLieuMoModel>(
      () => _service.getListOpenData(
        pageIndex: pageIndex,
        pageSize: pageSize,
        keySearch: keySearch,
        nguoiDung: nguoiDung,
        categoryID: categoryID,
        subCategoryID: subCategoryID,
        bookmark: bookmark,
      ),
      (response) => response.body?.toModel() ?? DataDuLieuMoModel(),
    );
  }

  @override
  Future<Result<DetailOpenDataModel>> getDtailOpenData(
    int id,
    String nguoiDung,
  ) {
    return runCatchingAsync<DetailOpenDataResponse, DetailOpenDataModel>(
      () => _service.getDtailOpenData(id, nguoiDung),
      (response) => response.toModel(),
    );
  }

  @override
  Future<Result<DataDuLieuMoModel>> getListBaiViet({
    int? pageSize,
    int? pageIndex,
    String? keySearch,
    String nguoiDung = '',
    int? categoryID,
    int? subCategoryID,
    bool? bookmark,
  }) {
    return runCatchingAsync<OpenDataListResponse, DataDuLieuMoModel>(
      () => _service.getListBaiViet(
        pageIndex: pageIndex,
        pageSize: pageSize,
        keySearch: keySearch,
        nguoiDung: nguoiDung,
        categoryID: categoryID,
        subCategoryID: subCategoryID,
        bookmark: bookmark,
      ),
      (response) => response.body?.toModel() ?? DataDuLieuMoModel(),
    );
  }

  @override
  Future<Result<DetailOpenDataModel>> getDetailBaiViet(
    int id,
    String nguoiDung,
  ) {
    return runCatchingAsync<DetailOpenDataResponse, DetailOpenDataModel>(
      () => _service.getDetailBaiViet(id, nguoiDung),
      (response) => response.toModel(),
    );
  }
}
