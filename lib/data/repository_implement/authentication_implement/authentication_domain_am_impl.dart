import 'package:bnv_opendata/data/request/login_ft_token_request.dart';
import 'package:bnv_opendata/data/request/refreshTokenRequest.dart';
import 'package:bnv_opendata/data/response/authentication/get_token_login_response.dart';
import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/data/services/authentication/authen_wso2is/authen_wso2is_refresh_token_service.dart';
import 'package:bnv_opendata/data/services/authentication/authen_wso2is/authentication_domain_wso2is_service.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_domain_am_repo.dart';

class AuthenticationDomainAMImpl implements AuthenticationDomainAmRepo {
  final AuthenticationWso2isService _service;
  final AuthenWso2isRefreshTokenService _serviceRefresh;

  AuthenticationDomainAMImpl(
    this._service,
    this._serviceRefresh,
  );

  @override
  Future<Result<GetTokenLoginResponse>> getTokenLogin(
    LoginFeatTokenRequest request,
  ) {
    return runCatchingAsync<GetTokenLoginResponse, GetTokenLoginResponse>(
      () => _service.loginGetToken(request),
      (response) => response,
    );
  }

  @override
  Future<Result<GetTokenLoginResponse>> refreshToken(
    RefreshTokenRequest request,
  ) {
    return runCatchingAsync<GetTokenLoginResponse, GetTokenLoginResponse>(
      () => _serviceRefresh.refreshToken(request),
      (response) => response,
    );
  }
}
