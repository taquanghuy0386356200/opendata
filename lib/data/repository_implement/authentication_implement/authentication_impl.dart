import 'package:bnv_opendata/data/request/doi_mat_khau_request.dart';
import 'package:bnv_opendata/data/request/otp_request.dart';
import 'package:bnv_opendata/data/request/register_request.dart';
import 'package:bnv_opendata/data/request/xac_thuc_tai_khoan_request.dart';
import 'package:bnv_opendata/data/response/authentication/capcha_response.dart';
import 'package:bnv_opendata/data/response/authentication/doi_mat_khau_response.dart';
import 'package:bnv_opendata/data/response/authentication/lost_pw_response.dart';
import 'package:bnv_opendata/data/response/authentication/otp_response.dart';
import 'package:bnv_opendata/data/response/authentication/register_reponse.dart';
import 'package:bnv_opendata/data/response/authentication/thiet_lap_mat_khau_response.dart';
import 'package:bnv_opendata/data/response/authentication/xac_thuc_tk_response.dart';
import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/data/services/authentication/authentication_service.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_repository.dart';

class AuthenticationImpl implements AuthenticationRepository {
  final AuthenticationService _service;

  AuthenticationImpl(this._service);

  @override
  Future<Result<RegisterTotalReponse>> register({
    required String email,
    required String fullName,
    required String password,
    required String capchaCode,
    required String capchaID,
  }) {
    return runCatchingAsync<RegisterTotalReponse, RegisterTotalReponse>(
      () => _service.registerUser(
        RegisterRequest(
          email: email,
          name: fullName,
          username: fullName,
          confirmPassword: password,
          password: password,
          usernameIsEmail: true,
          capchaId: capchaID,
          captchaCode: capchaCode,
        ),
      ),
      (response) => response,
    );
  }

  @override
  Future<Result<LostPasswordRespone>> quenMatKhau(
      {required QuenMatKhauRequest request}) {
    return runCatchingAsync<LostPasswordRespone, LostPasswordRespone>(
      () => _service.quenMatKhau(request),
      (response) => response,
    );
  }

  @override
  Future<Result<OTPResponse>> handleOTP({
    required OTPRequest request,
  }) {
    return runCatchingAsync<OTPResponse, OTPResponse>(
      () => _service.handleOTP(
        request,
      ),
      (response) => response,
    );
  }

  @override
  Future<Result<ThietLapMatKhauResponse>> thietLapMatKhau({
    required String newPassword,
    required String confirmPassword,
    required String email,
    required int otpId,
    required String token,
  }) {
    return runCatchingAsync<ThietLapMatKhauResponse, ThietLapMatKhauResponse>(
      () => _service.thietLapMatKhau(
        ThietLapMatKhauRequest(
          email: email,
          newPassword: newPassword,
          confirmPassword: confirmPassword,
          otpId: otpId,
          token: token,
        ),
      ),
      (response) => response,
    );
  }

  @override
  Future<Result<DoiMatKhauResponse>> doiMatKhau(
      {required DoiMatKhauRequest request}) {
    return runCatchingAsync<DoiMatKhauResponse, DoiMatKhauResponse>(
      () => _service.doiMatKhau(request),
      (response) => response,
    );
  }

  @override
  Future<Result<XacThucTKResponse>> xacThucTaiKhoan(
      {required XacThucTKRequest request}) {
    return runCatchingAsync<XacThucTKResponse, XacThucTKResponse>(
      () => _service.xacThucTaiKhoan(request),
      (response) => response,
    );
  }

  @override
  Future<Result<CapchaResponse>> getCapchaCode() {
    return runCatchingAsync<CapchaResponse, CapchaResponse>(
      () => _service.getCapchat(),
      (response) => response,
    );
  }

// @override
// Future<Result<AuthenticationLoginTotalModel>> login({
//   required String email,
//   required String password,
// }) {
//   return runCatchingAsync<AuthenticationLoginResponse,
//       AuthenticationLoginTotalModel>(
//     () => _service.postToLogin(),
//     (response) => response.toModel(),
//   );
// }
//
// @override
// Future<Result<AuthenticationLoginTotalModel>> register({
//   required String email,
//   required String fullName,
//   required String password,
// }) {
//   return runCatchingAsync<AuthenticationLoginResponse,
//       AuthenticationLoginTotalModel>(
//     () => _service.postToRegister(),
//     (response) => response.toModel(),
//   );
// }
//
// @override
// Future<Result<AuthenticationLoginTotalModel>> resetPassword({
//   required String email,
//   required String password,
//   String key = 'key',
// }) {
//   return runCatchingAsync<AuthenticationLoginResponse,
//       AuthenticationLoginTotalModel>(
//     () => _service.postToResetPassword(),
//     (response) => response.toModel(),
//   );
// }
//
// @override
// Future<Result<ResultPopularModel>> confirmEmailToResetPassword({
//   required String email,
// }) {
//   return runCatchingAsync<ResultPopularResponse, ResultPopularModel>(
//     () => _service.confirmEmailToResetPassword(),
//     (response) => response.toModel(),
//   );
// }
}
