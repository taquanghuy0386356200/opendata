import 'dart:io';

import 'package:bnv_opendata/data/request/gui_lien_he_request.dart';
import 'package:bnv_opendata/data/request/them_sua_tai_lieu_request.dart';
import 'package:bnv_opendata/data/response/tien_ich/chi_tiet_huong_dan_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/chi_tiet_tai_lieu_dang_tai_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/danh_dau_du_lieu_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/gui_lien_he_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/huong_dan_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/lien_he_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/post_file_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/tai_lieu_dang_tai_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/them_sua_tai_lieu_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/xoa_tai_lieu_response.dart';
import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/data/services/tien_ich/tien_ich_service.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/domain/models/tien_ich/chi_tiet_tai_lieu_dang_tai_model.dart';
import 'package:bnv_opendata/domain/models/tien_ich/lien_he_model.dart';
import 'package:bnv_opendata/domain/models/tien_ich/tai_lieu_dang_tai_model.dart';
import 'package:bnv_opendata/domain/repository/tien_ich/tien_ich_respository.dart';

class TienIchImpl implements TienIchRepository {
  final TienIchService _service;

  TienIchImpl(this._service);

  @override
  Future<Result<DataDuThongBaoModel>> getListHuongDan({
    String? keySearch,
    int? pageSize,
    int? pageIndex,
  }) {
    return runCatchingAsync<HuongDanResponse, DataDuThongBaoModel>(
      () => _service.getListHuongDan(
        keySearch,
        pageSize,
        pageIndex,
      ),
      (response) => response.body?.toModel() ?? DataDuThongBaoModel(),
    );
  }

  @override
  Future<Result<ItemThongBaoModel>> getDetailHuongDan({
    String? id,
  }) {
    return runCatchingAsync<DetailHuongDanResponse, ItemThongBaoModel>(
      () => _service.getDetailHuongDan(
        id,
      ),
      (response) => response.body?.toData() ?? ItemThongBaoModel(),
    );
  }

  @override
  Future<Result<LienHeModel>> getDetailLienHe() {
    return runCatchingAsync<LienHeResponse, LienHeModel>(
      () => _service.getLienHe(),
      (response) => response.body?.toModel() ?? LienHeModel(),
    );
  }

  @override
  Future<Result<LienHeModel>> guiLienHe(
    GuiLienHeRequest guiLienHeRequest,
  ) {
    return runCatchingAsync<SentLienHeResponse, LienHeModel>(
      () => _service.guiLienHe(guiLienHeRequest),
      (response) => response.body?.toModel() ?? LienHeModel(),
    );
  }

  @override
  Future<Result<PostFileTotalResponse>> postFile(List<File> file) {
    return runCatchingAsync<PostFileTotalResponse, PostFileTotalResponse>(
      () => _service.uploadFile(file),
      (response) => response,
    );
  }

  @override
  Future<Result<ThemSuaTaiLieuTotalResponse>> themSuaTaiLieu(
    ThemSuaTaiLieuRequest request,
  ) {
    return runCatchingAsync<ThemSuaTaiLieuTotalResponse,
        ThemSuaTaiLieuTotalResponse>(
      () => _service.themSuaTaiLieu(request),
      (response) => response,
    );
  }

  @override
  Future<Result<XoaTaiLieuResponse>> xoaTaiLieu(int idTailIEU) {
    return runCatchingAsync<XoaTaiLieuResponse, XoaTaiLieuResponse>(
      () => _service.xoaTaiLieu(idTailIEU),
      (response) => response,
    );
  }

  @override
  Future<Result<DanhDauDuLieuResponse>> danhDauDuLieu(
      int id, String nguoiDanhDau) {
    return runCatchingAsync<DanhDauDuLieuResponse, DanhDauDuLieuResponse>(
      () => _service.danhDauDuLieu(id, nguoiDanhDau),
      (response) => response,
    );
  }

  @override
  Future<Result<DanhDauDuLieuResponse>> boDanhDauDuLieu(
    int id,
    String nguoiDanhDau,
  ) {
    return runCatchingAsync<DanhDauDuLieuResponse, DanhDauDuLieuResponse>(
      () => _service.boDanhDauDuLieu(id, nguoiDanhDau),
      (response) => response,
    );
  }

  @override
  Future<Result<List<TaiLieuDangTaiModel>>> getListTaiLieuDangTai(
    int pageSize,
    int pageIndex,
    String nguoiDangTai,
    String keySearch,
    String status,
    String fileType, {
    String categoryID = '',
    String subCategoryID = '',
    int fromDate = 0,
    int toDate = 0,
  }) {
    return runCatchingAsync<TaiLieuDangTaiTotalResponse,
            List<TaiLieuDangTaiModel>>(
        () => _service.getTaiLieuDangTaiCaNhan(
              pageSize,
              pageIndex,
              nguoiDangTai,
              keySearch,
              status,
              fileType,
              categoryID,
              subCategoryID,
              fromDate,
              toDate,
            ),
        (response) =>
            response.body?.items?.map((e) => e.toModel()).toList() ?? []);
  }

  @override
  Future<Result<ChiTietTaiLieuDangTaiModel>> getChiTietTaiLieuDangTai(
      {required int id}) {
    return runCatchingAsync<ChiTietTaiLieuDangTaiResponse,
        ChiTietTaiLieuDangTaiModel>(
      () => _service.getChiTietTaiLieuDangTai(id),
      (response) =>
          response.body?.toModel() ??
          ChiTietTaiLieuDangTaiModel(
              0, '', '', '', '', 0, '', '', 0, '', 0, '', 0),
    );
  }
}
