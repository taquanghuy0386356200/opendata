import 'package:bnv_opendata/data/response/thong_bao/chi_tiet_thong_bao_response.dart';
import 'package:bnv_opendata/data/response/van_ban/van_ban_response.dart';
import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/data/services/van_ban/van_ban_service.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/domain/repository/van_ban/van_ban_respository.dart';

class VanBanImpl implements VanBanRepository {
  final VanBanService _service;

  VanBanImpl(this._service);

  @override
  Future<Result<List<ItemThongBaoModel>>> getListVanBan({
    String? keySearch,
    int? pageSize,
    int? pageIndex,
  }) {
    return runCatchingAsync<VanBanResponse, List<ItemThongBaoModel>>(
      () => _service.getListVanBan(keySearch, pageSize, pageIndex),
      (response) => response.body?.items?.map((e) => e.toData()).toList() ?? [],
    );
  }

  @override
  Future<Result<ItemThongBaoModel>> getDetailVanBan(
    String id,
  ) {
    return runCatchingAsync<DetailThongBaoResponse, ItemThongBaoModel>(
      () => _service.getDetailVanBan(
        id,
      ),
      (response) => response.body?.toData() ?? ItemThongBaoModel(),
    );
  }
}
