import 'package:bnv_opendata/data/response/home/document_response.dart';
import 'package:bnv_opendata/data/response/thong_bao/chi_tiet_thong_bao_response.dart';
import 'package:bnv_opendata/data/response/thong_bao/thong_bao_response.dart';
import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/data/services/thong_bao/thong_bao_service.dart';
import 'package:bnv_opendata/domain/model/document_model.dart';
import 'package:bnv_opendata/domain/model/notification_model.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/domain/repository/thong_bao/thong_bao_respository.dart';

class ThongBaoImpl implements ThongBaoRepository {
  final ThongBaoService _service;

  ThongBaoImpl(this._service);

  @override
  Future<Result<List<DocumentModel>>> getListVanBan(
      {String? keySearch, int? pageSize, int? pageIndex}) {
    return runCatchingAsync<DocumentTotalResponse, List<DocumentModel>>(
      () => _service.getListVanBan(keySearch, pageSize, pageIndex),
      (response) =>
          response.body?.items?.map((e) => e.toModel()).toList() ?? [],
    );
  }

  @override
  Future<Result<ItemThongBaoModel>> getDetailThongBao({String? id}) {
    return runCatchingAsync<DetailThongBaoResponse, ItemThongBaoModel>(
      () => _service.getDetailThongBao(id),
      (response) => response.body?.toData() ?? ItemThongBaoModel(),
    );
  }

  @override
  Future<Result<ThongBaoTotalModel>> getListThongBao({
    String? keySearch,
    int? pageSize,
    int? pageIndex,
  }) {
    return runCatchingAsync<ThongBaoResponse, ThongBaoTotalModel>(
      () => _service.getListThongBao(keySearch, pageSize, pageIndex),
      (response) => response.body?.toModel() ?? ThongBaoTotalModel(0, 0, 0, []),
    );
  }

  @override
  Future<Result<List<ThongBaoModel>>> getListThongBaoMoiNhat({
    String? keySearch,
    int? pageSize,
    int? pageIndex,
  }) {
    return runCatchingAsync<ThongBaoResponse, List<ThongBaoModel>>(
      () => _service.getListThongBao(keySearch, pageSize, pageIndex),
      (response) =>
          response.body?.items?.map((e) => e.toModel()).toList() ?? [],
    );
  }
}
