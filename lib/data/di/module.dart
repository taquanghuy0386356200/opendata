import 'package:bnv_opendata/data/di/flutter_transformer.dart';
import 'package:bnv_opendata/data/network/unauthorized_handle.dart';
import 'package:bnv_opendata/data/repository_implement/authentication_implement/authentication_domain_am_impl.dart';
import 'package:bnv_opendata/data/repository_implement/authentication_implement/authentication_impl.dart';
import 'package:bnv_opendata/data/repository_implement/du_lieu_mo/du_lieu_mo_impl.dart';
import 'package:bnv_opendata/data/repository_implement/home/home_impl.dart';
import 'package:bnv_opendata/data/repository_implement/thong_bao/thong_bao_impl.dart';
import 'package:bnv_opendata/data/repository_implement/tien_ich/tien_ich_impl.dart';
import 'package:bnv_opendata/data/repository_implement/van_ban/van_ban_impl.dart';
import 'package:bnv_opendata/data/services/authentication/authen_wso2is/authen_wso2is_refresh_token_service.dart';
import 'package:bnv_opendata/data/services/authentication/authen_wso2is/authentication_domain_wso2is_service.dart';
import 'package:bnv_opendata/data/services/authentication/authentication_service.dart';
import 'package:bnv_opendata/data/services/du_lieu_mo_service/du_lieu_mo_service.dart';
import 'package:bnv_opendata/data/services/home/home_service.dart';
import 'package:bnv_opendata/data/services/thong_bao/thong_bao_service.dart';
import 'package:bnv_opendata/data/services/tien_ich/tien_ich_service.dart';
import 'package:bnv_opendata/data/services/van_ban/van_ban_service.dart';
import 'package:bnv_opendata/domain/env/model/app_constants.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_domain_am_repo.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_repository.dart';
import 'package:bnv_opendata/domain/repository/du_lieu_mo/du_lieu_mo_repository.dart';
import 'package:bnv_opendata/domain/repository/home/home_repository.dart';
import 'package:bnv_opendata/domain/repository/thong_bao/thong_bao_respository.dart';
import 'package:bnv_opendata/domain/repository/tien_ich/tien_ich_respository.dart';
import 'package:bnv_opendata/domain/repository/van_ban/van_ban_respository.dart';

// ignore: depend_on_referenced_packages
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart' as foundation;
import 'package:get/get.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';

enum BaseURLOption {
  LGSP_OPENDATA,
  WSO2IS_OPENDATA,
}

String getUrlDomain({
  BaseURLOption baseURLOption = BaseURLOption.LGSP_OPENDATA,
}) {
  final appConstants = Get.find<AppConstants>();
  switch (baseURLOption) {
    case BaseURLOption.LGSP_OPENDATA:
      return appConstants.baseUrl;
    case BaseURLOption.WSO2IS_OPENDATA:
      return appConstants.baseUrlWso2is;
    default:
      return appConstants.baseUrl;
  }
}

void configureDependencies() {
  /// dang nhap, dang ky, quen mat khau
  Get.put(
    AuthenticationService(
      provideDio(),
    ),
  );
  Get.put<AuthenticationRepository>(
    AuthenticationImpl(Get.find()),
  );

  Get.put(
    AuthenticationWso2isService(
      provideDioLogin(),
    ),
  );

  Get.put(
    AuthenWso2isRefreshTokenService(
      provideDio(),
    ),
  );

  Get.put<AuthenticationDomainAmRepo>(
    AuthenticationDomainAMImpl(Get.find(), Get.find()),
  );

  /// home
  Get.put(
    HomeService(
      provideDio(),
    ),
  );
  Get.put<HomeRepository>(
    HomeImplement(Get.find()),
  );

  /// thong bao
  Get.put(
    ThongBaoService(
      provideDio(),
    ),
  );
  Get.put<ThongBaoRepository>(
    ThongBaoImpl(Get.find()),
  );

  /// dữ liệu mở
  Get.put(
    DuLieuMoService(
      provideDio(),
    ),
  );

  Get.put<DuLieuMoRepository>(
    DuLieuMoImpl(Get.find()),
  );

  /// liên hệ
  Get.put(
    TienIchService(
      provideDio(),
    ),
  );

  Get.put<TienIchRepository>(
    TienIchImpl(Get.find()),
  );

  ///Van ban
  Get.put(
    VanBanService(
      provideDio(),
    ),
  );

  Get.put<VanBanRepository>(
    VanBanImpl(Get.find()),
  );
}

int _connectTimeOut = 60000;

//todo handle lúc lỗi refreshToken

Dio provideDio() {
  final appConstants = Get.find<AppConstants>();
  final options = BaseOptions(
    baseUrl: appConstants.baseUrl,
    receiveTimeout: _connectTimeOut,
    connectTimeout: _connectTimeOut,
    followRedirects: false,
  );
  final dio = Dio(options);
  // ignore: no_leading_underscores_for_local_identifiers
  void _onReFreshToken(DioError e, ErrorInterceptorHandler handler) {
    HandleUnauthorized.resignRefreshToken(
      onRefreshToken: (token) async {
        if (token.isNotEmpty) {
          options.headers['Authorization'] = 'Bearer $token';
        }
        final opts = Options(
          method: e.requestOptions.method,
          headers: e.requestOptions.headers,
        );
        final cloneReq = await dio.request(
          e.requestOptions.path,
          options: opts,
          data: e.requestOptions.data,
          queryParameters: e.requestOptions.queryParameters,
        );

        return handler.resolve(cloneReq);
      },
      onError: (error) {
        // MessageConfig.show(
        //   title: S.current.het_han_token,
        //   messState: MessState.error,
        //   onDismiss: () {
        //     if (MessageConfig.contextConfig != null) {
        //       AppStateCt.of(MessageConfig.contextConfig!).appState.setToken('');
        //     }
        //     FirebaseMessaging.instance.deleteToken();
        //     HiveLocal.clearData();
        //     PrefsService.saveLoginUserName('');
        //   },
        // );
        return handler.next(e);
      },
    );
  }

  dio.transformer = FlutterTransformer();
  dio.interceptors.add(
    InterceptorsWrapper(
      onRequest:
          (RequestOptions options, RequestInterceptorHandler handler) async {
        final String accessToken = PrefsService.getToken();
        options.baseUrl = appConstants.baseUrl;
        options.headers['Content-Type'] = 'application/json';
        options.headers['X-Client-Version'] = '1.0';
        if (accessToken.isNotEmpty) {
          options.headers.remove('Authorization');
          options.headers['Authorization'] = 'Bearer $accessToken';
        }
        return handler.next(options);
      },
      onResponse: (response, handler) {
        return handler.next(response); // continue
      },
      onError: (DioError e, handler) async {
        if (e.response?.statusCode == 401) {
          return _onReFreshToken(e, handler);
        }
        return handler.next(e); //continue
      },
    ),
  );
  if (foundation.kDebugMode) {
    dio.interceptors.add(dioLogger());
  }
  return dio;
}

Dio provideDioLogin() {
  final appConstants = Get.find<AppConstants>();
  final options = BaseOptions(
    baseUrl: appConstants.baseUrlWso2is,
    receiveTimeout: _connectTimeOut,
    connectTimeout: _connectTimeOut,
    followRedirects: false,
  );
  final dio = Dio(options);

  dio.transformer = FlutterTransformer();
  dio.interceptors.add(
    InterceptorsWrapper(
      onRequest:
          (RequestOptions options, RequestInterceptorHandler handler) async {
        options.baseUrl = appConstants.baseUrlWso2is;
        options.headers['Content-Type'] = 'application/json';
        options.headers['X-Client-Version'] = '1.0';
        options.headers.remove('Authorization');
        options.headers['Authorization'] = appConstants.hardTokenAPILogin;
        return handler.next(options);
      },
      onResponse: (response, handler) {
        return handler.next(response); // continue
      },
      onError: (DioError e, handler) async {
        return handler.next(e);
      },
    ),
  );
  if (foundation.kDebugMode) {
    dio.interceptors.add(dioLogger());
  }
  return dio;
}

PrettyDioLogger dioLogger() {
  return PrettyDioLogger(
    requestHeader: true,
    requestBody: true,
    maxWidth: 100,
  );
}
