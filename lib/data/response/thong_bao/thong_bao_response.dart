import 'package:bnv_opendata/domain/model/notification_model.dart';

class ThongBaoResponse {
  int? errorCode;
  String? errorMessage;
  Body? body;

  ThongBaoResponse({this.errorCode, this.errorMessage, this.body});

  ThongBaoResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
    body = json['body'] != null ? Body.fromJson(json['body']) : null;
  }
}

class Body {
  List<Items>? items;
  int? totalItem;
  int? pageSize;
  int? pageIndex;

  Body({this.items, this.totalItem, this.pageSize, this.pageIndex});

  Body.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      items = <Items>[];
      // ignore: avoid_dynamic_calls
      json['items'].forEach((v) {
        items!.add(Items.fromJson(v));
      });
    }
    totalItem = json['totalItem'];
    pageSize = json['pageSize'];
    pageIndex = json['pageIndex'];
  }

  ThongBaoTotalModel toModel() => ThongBaoTotalModel(
        pageIndex ?? 0,
        pageSize ?? 0,
        totalItem ?? 0,
        items?.map((e) => e.toModel()).toList() ?? [],
      );
}

class Items {
  int? id;
  String? title;
  String? banner;
  bool? isSaved;
  int? releaseAt;
  int? affectedAt;

  Items({
    this.id,
    this.title,
    this.banner,
    this.isSaved,
    this.releaseAt,
    this.affectedAt,
  });

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    banner = json['banner'];
    isSaved = json['isRead'];
    releaseAt = json['createdAt'];
    affectedAt = json['affectedAt'];
  }

  ThongBaoModel toModel() => ThongBaoModel(
        id: id ?? 0,
        title: title ?? '',
        banner: banner ?? '',
        isSaved: isSaved ?? false,
        releaseAt: releaseAt ?? 0,
        affectedAt: affectedAt ?? 0,
      );
}
