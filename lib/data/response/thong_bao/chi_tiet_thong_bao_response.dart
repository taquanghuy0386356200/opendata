import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';

class DetailThongBaoResponse {
  int? errorCode;
  String? errorMessage;
  Body? body;

  DetailThongBaoResponse({this.errorCode, this.errorMessage, this.body});

  DetailThongBaoResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
    body = json['body'] != null ? Body.fromJson(json['body']) : null;
  }
}

class Body {
  int? id;
  String? title;
  String? banner;
  String? content;
  bool? isSaved;
  int? releaseAt;
  int? affectedAt;
  int? createdAt;

  Body({
    this.id,
    this.title,
    this.banner,
    this.content,
    this.isSaved,
    this.releaseAt,
    this.affectedAt,
    this.createdAt,
  });

  Body.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    banner = json['banner'];
    content = json['content'];
    isSaved = json['isRead'];
    releaseAt = json['releaseAt'];
    affectedAt = json['affectedAt'];
    createdAt = json['createdAt'];
  }

  ItemThongBaoModel toData() => ItemThongBaoModel(
        id: id.toString(),
        title: title,
        banner: banner,
        content: content,
        releaseAt: releaseAt,
        affectedAt: affectedAt,
        createdAt: createdAt,
      );
}
