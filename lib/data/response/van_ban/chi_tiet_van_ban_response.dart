import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';

class DetailVanBanResponse {
  int? errorCode;
  String? errorMessage;
  Body? body;

  DetailVanBanResponse({this.errorCode, this.errorMessage, this.body});

  DetailVanBanResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
    body = json['body'] != null ? Body.fromJson(json['body']) : null;
  }
}

class Body {
  String? id;
  String? title;
  String? banner;
  String? content;
  bool? isRead;
  int? createdAt;

  Body(
      {this.id,
      this.title,
      this.banner,
      this.content,
      this.isRead,
      this.createdAt});

  Body.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    banner = json['banner'];
    content = json['content'];
    isRead = json['isRead'];
    createdAt = json['createdAt'];
  }

  ItemThongBaoModel toData() => ItemThongBaoModel(
        id: id,
        title: title,
        banner: banner,
        isSaved: isRead,
        releaseAt: createdAt,
        content: content,
      );
}
