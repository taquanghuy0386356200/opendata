import 'package:bnv_opendata/domain/models/open_data_model/open_data_model.dart';

class OpenDataListResponse {
  int? errorCode;
  String? errorMessage;
  Body? body;

  OpenDataListResponse({this.errorCode, this.errorMessage, this.body});

  OpenDataListResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
    body = json['body'] != null ? Body.fromJson(json['body']) : null;
  }
}

class Body {
  List<Item>? item;
  int? totalItem;
  int? pageSize;
  int? pageIndex;

  Body({this.item, this.totalItem, this.pageSize, this.pageIndex});

  Body.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      item = <Item>[];
      // ignore: avoid_dynamic_calls
      json['items'].forEach((v) {
        item!.add(Item.fromJson(v));
      });
    }
    totalItem = json['totalItem'];
    pageSize = json['pageSize'];
    pageIndex = json['pageIndex'];
  }

  DataDuLieuMoModel toModel() {
    return DataDuLieuMoModel(
      item: item?.map((e) => e.toDomain()).toList() ?? [],
      totalItem: totalItem,
      pageSize: pageSize,
      pageIndex: pageIndex,
    );
  }
}

class Item {
  int? id;
  String? title;
  int? updatedAt;
  int? categoryId;
  String? categoryName;
  int? subcategoryID;
  String? subcategoryName;
  String? description;
  bool? danhDau;
  String? fileDinhKem;

  Item({
    this.id,
    this.title,
    this.updatedAt,
    this.categoryId,
    this.categoryName,
    this.subcategoryID,
    this.subcategoryName,
    this.description,
    this.danhDau,
    this.fileDinhKem,
  });

  Item.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    updatedAt = json['updatedAt'];
    categoryId = json['categoryId'];
    categoryName = json['categoryName'];
    subcategoryID = json['subcategoryID'];
    subcategoryName = json['subcategoryName'];
    description = json['description'];
    danhDau = json['danhDau'];
    fileDinhKem = json['fileDinhKem'];
  }

  OpenDataModel toDomain() => OpenDataModel(
        id: id,
        title: title,
        updatedAt: updatedAt,
        categoryId: categoryId,
        categoryName: categoryName,
        subCategoryId: subcategoryID,
        subCategoryName: subcategoryName,
        description: description,
        fileDinhKem: fileDinhKem,
      );
}
