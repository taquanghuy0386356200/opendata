import 'dart:convert';

import 'package:bnv_opendata/domain/models/open_data_model/detail_open_data_model.dart';

class DetailOpenDataResponse {
  Body? body;
  int? errorCode;
  String? errorMessage;

  DetailOpenDataResponse({this.body, this.errorCode, this.errorMessage});

  DetailOpenDataResponse.fromJson(Map<String, dynamic> json) {
    body = json['body'] != null ? Body.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    return data;
  }

  DetailOpenDataModel toModel() => DetailOpenDataModel(
    body: body?.toData() ?? BodyDetailOpenDataModel(),
    errorCode: errorCode,
    errorMessage: errorMessage,
  );
}

class Body {
  int? id;
  String? title;
  String? description;
  String? content;
  String? fileDinhKem;
  int? categoryID;
  String? categoryName;
  int? subcategoryID;
  String? subcategoryName;
  int? updatedAt;
  bool? danhDau;
  String? chartType;
  String? dataTable;
  String? link;

  Body({
    this.id,
    this.title,
    this.description,
    this.content,
    this.fileDinhKem,
    this.categoryID,
    this.categoryName,
    this.subcategoryID,
    this.subcategoryName,
    this.updatedAt,
    this.danhDau,
    this.chartType,
    this.dataTable,
    this.link,
  });

  Body.fromJson(Map<String, dynamic> data) {
    id = data['id'];
    title = data['title'];
    description = data['description'];
    content = data['content'];
    fileDinhKem = data['fileDinhKem'];
    categoryID = data['categoryID'];
    categoryName = data['categoryName'];
    subcategoryID = data['subcategoryID'];
    subcategoryName = data['subcategoryName'];
    updatedAt = data['updatedAt'];
    danhDau = data['danhDau'];
    chartType = data['chartType'];
    dataTable = json.decode(data['dataTable']);
    link = data['link'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['description'] = description;
    data['content'] = content;
    data['fileDinhKem'] = fileDinhKem;
    data['categoryID'] = categoryID;
    data['categoryName'] = categoryName;
    data['subcategoryID'] = subcategoryID;
    data['subcategoryName'] = subcategoryName;
    data['updatedAt'] = updatedAt;
    data['danhDau'] = danhDau;
    data['chartType'] = chartType;
    data['dataTable'] = dataTable;
    data['link'] = link;
    return data;
  }

  BodyDetailOpenDataModel toData() {
    DataTable dataChartAfterDecode = DataTable();
    String dataTableJsonAfterRelaced = '';
    if ((dataTable ?? '').isNotEmpty) {
      try {
        final data1 = (dataTable ?? '').toString().replaceAll('"{', '{');
        final data2 = data1.replaceAll('}"', '}');
        final data3 = data2.replaceAll('\\', '');
        dataTableJsonAfterRelaced = data3;
        final decode = json.decode(data3);
        final data = ChartTableValue.fromJson(decode);
        dataChartAfterDecode = data.toDomain();
      } catch (e) {
        rethrow;
      }
    }

    return BodyDetailOpenDataModel(
      id: id,
      title: title,
      description: description,
      content: content,
      fileDinhKem: fileDinhKem,
      categoryID: categoryID,
      categoryName: categoryName,
      subcategoryID: subcategoryID,
      subcategoryName: subcategoryName,
      updatedAt: updatedAt,
      danhDau: danhDau,
      chartType: chartType,
      dataTable: dataTableJsonAfterRelaced,
      link: link,
      dataTableDeCode: dataChartAfterDecode,
    );
  }
}

class ChartTableValue {
  List<ColumnsData>? columns;
  List<RowsData>? rows;
  int? rowCount;

  ChartTableValue({this.columns, this.rows, this.rowCount});

  ChartTableValue.fromJson(Map<String, dynamic> json) {
    if (json['columns'] != null) {
      columns = <ColumnsData>[];
      json['columns'].forEach((v) {
        columns!.add(ColumnsData.fromJson(v));
      });
    }
    if (json['rows'] != null) {
      rows = <RowsData>[];
      json['rows'].forEach((v) {
        rows!.add(RowsData.fromJson(v));
      });
    }
    rowCount = json['rowCount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (columns != null) {
      data['columns'] = columns!.map((v) => v.toJson()).toList();
    }
    if (rows != null) {
      data['rows'] = rows!.map((v) => v.toJson()).toList();
    }
    data['rowCount'] = rowCount;
    return data;
  }

  DataTable toDomain() => DataTable(
    columns: columns?.map((e) => e.toDomain()).toList() ?? [],
    rows: rows?.map((e) => e.toDomain()).toList() ?? [],
    rowCount: rowCount,
  );
}

class ColumnsData {
  String? columnName;
  String? displayColumnName;
  bool? hasFilter;
  String? dataType;

  // ColumnPropsData? columnProps;

  ColumnsData({
    this.columnName,
    this.displayColumnName,
    this.hasFilter,
    this.dataType,
    // this.columnProps,
  });

  ColumnsData.fromJson(Map<String, dynamic> json) {
    columnName = json['columnName'];
    displayColumnName = json['displayColumnName'];
    hasFilter = json['hasFilter'];
    dataType = json['dataType'];
    // columnProps = json['columnProps'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['columnName'] = columnName;
    data['displayColumnName'] = displayColumnName;
    data['hasFilter'] = hasFilter;
    data['dataType'] = dataType;
    // data['columnProps'] = columnProps;
    return data;
  }

  Columns toDomain() => Columns(
    columnName: columnName,
    // columnProps: columnProps?.toData(),
    dataType: dataType,
    displayColumnName: displayColumnName,
    hasFilter: hasFilter,
  );
}

class RowsData {
  String? kyhieu;
  String? loaivanban;
  String? trichyeu;
  String? nguoitao;
  String? strNgaytao;
  String? nguoixuly;
  String? strNgayxuly;
  String? tinhtrangxuly;
  String? trangthai;

  RowsData({
    this.kyhieu,
    this.loaivanban,
    this.trichyeu,
    this.nguoitao,
    this.strNgaytao,
    this.nguoixuly,
    this.strNgayxuly,
    this.tinhtrangxuly,
    this.trangthai,
  });

  RowsData.fromJson(Map<String, dynamic> json) {
    kyhieu = json['kyhieu'];
    loaivanban = json['loaivanban'];
    trichyeu = json['trichyeu'];
    nguoitao = json['nguoitao'];
    strNgaytao = json['str_ngaytao'];
    nguoixuly = json['nguoixuly'];
    strNgayxuly = json['str_ngayxuly'];
    tinhtrangxuly = json['tinhtrangxuly'];
    trangthai = json['trangthai'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['kyhieu'] = kyhieu;
    data['loaivanban'] = loaivanban;
    data['trichyeu'] = trichyeu;
    data['nguoitao'] = nguoitao;
    data['str_ngaytao'] = strNgaytao;
    data['nguoixuly'] = nguoixuly;
    data['str_ngayxuly'] = strNgayxuly;
    data['tinhtrangxuly'] = tinhtrangxuly;
    data['trangthai'] = trangthai;
    return data;
  }

  Rows toDomain() => Rows(
    kyhieu: kyhieu,
    loaivanban: loaivanban,
    nguoitao: nguoitao,
    nguoixuly: nguoixuly,
    strNgaytao: strNgaytao,
    strNgayxuly: strNgayxuly,
    tinhtrangxuly: tinhtrangxuly,
    trangthai: trangthai,
    trichyeu: trichyeu,
  );
}

class ColumnPropsData {
  String? id;

  ColumnPropsData({this.id});

  ColumnPropsData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    return data;
  }

  ColumnProps toData() => ColumnProps(
    id: id,
  );
}
