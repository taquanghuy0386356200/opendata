class LostPasswordRespone {
  LostPWBody? body;
  int? errorCode;
  String? message;

  LostPasswordRespone({this.body, this.errorCode, this.message});

  LostPasswordRespone.fromJson(Map<String, dynamic> json) {
    body = json['body'] != null ? LostPWBody.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['message'] = message;
    return data;
  }
}

class LostPWBody {
  int? id;
  String? nameotp;

  LostPWBody({this.id, this.nameotp});

  LostPWBody.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameotp = json['nameotp'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['nameotp'] = nameotp;
    return data;
  }
}
