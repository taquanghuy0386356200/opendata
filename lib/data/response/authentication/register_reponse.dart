import 'package:bnv_opendata/domain/model/authentication/authentication_login.dart';

class RegisterTotalReponse {
  Data? body;
  int? errorCode;
  String? message;

  RegisterTotalReponse({this.body, this.errorCode, this.message});

  RegisterTotalReponse.fromJson(Map<String, dynamic> json) {
    body = json['body'] != null ? Data.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['message'] = message;
    return data;
  }
}

class Data {
  String? refreshToken;
  int? expiredTime;
  UserInfo? userInfo;

  Data({this.refreshToken, this.expiredTime, this.userInfo});

  Data.fromJson(Map<String, dynamic> json) {
    refreshToken = json['refreshToken'];
    expiredTime = json['expiredTime'];
    userInfo =
        json['userInfo'] != null ? UserInfo.fromJson(json['userInfo']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['refreshToken'] = refreshToken;
    data['expiredTime'] = expiredTime;
    if (userInfo != null) {
      data['userInfo'] = userInfo!.toJson();
    }
    return data;
  }
}

class UserInfo {
  String? email;
  String? fullName;
  String? avatar;

  UserInfo({this.email, this.fullName, this.avatar});

  UserInfoModel toModel() => UserInfoModel(
        email: email,
        fullName: fullName,
        avatar: avatar,
      );

  UserInfo.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    fullName = json['fullName'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['fullName'] = fullName;
    data['avatar'] = avatar;
    return data;
  }
}
