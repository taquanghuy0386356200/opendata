class CapchaResponse {
  CapchaResponseBody? body;
  int? errorCode;
  String? message;

  CapchaResponse({this.body, this.errorCode, this.message});

  CapchaResponse.fromJson(Map<String, dynamic> json) {
    body =
        json['body'] != null ? CapchaResponseBody.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['message'] = message;
    return data;
  }
}

class CapchaResponseBody {
  String? id;
  String? capcha;

  CapchaResponseBody({this.id, this.capcha});

  CapchaResponseBody.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    capcha = json['capcha'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['capcha'] = capcha;
    return data;
  }
}
