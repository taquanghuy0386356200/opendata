class ThietLapMatKhauResponse {
  ThietLapMkBody? body;
  int? errorCode;
  String? message;

  ThietLapMatKhauResponse({this.body, this.errorCode, this.message});

  ThietLapMatKhauResponse.fromJson(Map<String, dynamic> json) {
    body = json['body'] != null ? ThietLapMkBody.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['message'] = message;
    return data;
  }
}

class ThietLapMkBody {
  String? newPassword;
  String? confirmPassword;
  String? email;
  int? otpId;
  String? token;

  ThietLapMkBody(
      {this.newPassword,
      this.confirmPassword,
      this.email,
      this.otpId,
      this.token});

  ThietLapMkBody.fromJson(Map<String, dynamic> json) {
    newPassword = json['newPassword'];
    confirmPassword = json['confirmPassword'];
    email = json['email'];
    otpId = json['otpId'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['newPassword'] = newPassword;
    data['confirmPassword'] = confirmPassword;
    data['email'] = email;
    data['otpId'] = otpId;
    data['token'] = token;
    return data;
  }
}
