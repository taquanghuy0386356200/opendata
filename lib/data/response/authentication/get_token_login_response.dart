class GetTokenLoginResponse {
  String? accessToken;
  String? refreshToken;
  String? scope;
  String? tokenType;
  int? expiresIn;

  GetTokenLoginResponse({
    this.accessToken,
    this.refreshToken,
    this.scope,
    this.tokenType,
    this.expiresIn,
  });

  GetTokenLoginResponse.fromJson(Map<String, dynamic> json) {
    accessToken = json['access_token'];
    refreshToken = json['refresh_token'];
    scope = json['scope'];
    tokenType = json['token_type'];
    expiresIn = json['expires_in'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['access_token'] = accessToken;
    data['refresh_token'] = refreshToken;
    data['scope'] = scope;
    data['token_type'] = tokenType;
    data['expires_in'] = expiresIn;
    return data;
  }
}

class GetTokenLoginResponseError {
  String? errorDescription;
  String? error;

  GetTokenLoginResponseError({this.errorDescription, this.error});

  GetTokenLoginResponseError.fromJson(Map<String, dynamic> json) {
    errorDescription = json['error_description'];
    error = json['error'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['error_description'] = errorDescription;
    data['error'] = error;
    return data;
  }
}
