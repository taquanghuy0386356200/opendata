import 'package:bnv_opendata/domain/model/authentication/authentication_login.dart';

class RefreshTokenResponse {
  int? errorCode;
  String? errorMessage;
  Body? body;

  RefreshTokenResponse({this.errorCode, this.errorMessage, this.body});

  RefreshTokenResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
    body = json['body'] != null ? Body.fromJson(json['body']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    if (body != null) {
      data['body'] = body!.toJson();
    }
    return data;
  }
}

class Body {
  String? accesToken;
  String? refreshToken;
  int? expiredTime;
  UserInfo? userInfo;

  Body({this.accesToken, this.refreshToken, this.expiredTime, this.userInfo});

  Body.fromJson(Map<String, dynamic> json) {
    accesToken = json['accesToken'];
    refreshToken = json['refreshToken'];
    expiredTime = json['expiredTime'];
    userInfo = json['userInfo'] != null
        ? UserInfo.fromJson(json['userInfo'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['accesToken'] = accesToken;
    data['refreshToken'] = refreshToken;
    data['expiredTime'] = expiredTime;
    if (userInfo != null) {
      data['userInfo'] = userInfo!.toJson();
    }
    return data;
  }
}

class UserInfo {
  String? email;
  String? fullName;
  String? avatar;

  UserInfo({this.email, this.fullName, this.avatar});

  UserInfoModel toModel() => UserInfoModel(
        email: email,
        fullName: fullName,
        avatar: avatar,
      );

  UserInfo.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    fullName = json['fullName'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['fullName'] = fullName;
    data['avatar'] = avatar;
    return data;
  }
}
