import 'package:bnv_opendata/domain/model/authentication/authentication_login.dart';
import 'package:json_annotation/json_annotation.dart';

part 'authentication_login_response.g.dart';

@JsonSerializable()
class AuthenticationLoginResponse {
  @JsonKey(name: 'errorCode')
  int? errorCode;
  @JsonKey(name: 'errorMessage')
  String? errorMessage;
  @JsonKey(name: 'body')
  BodyLoginAuthenticationResponse bodyLoginRes;

  AuthenticationLoginResponse(
    this.errorCode,
    this.errorMessage,
    this.bodyLoginRes,
  );

  AuthenticationLoginTotalModel toModel() => AuthenticationLoginTotalModel(
        errorCode,
        errorMessage,
        bodyLoginRes.toModel(),
      );

  factory AuthenticationLoginResponse.fromJson(Map<String, dynamic> json) =>
      _$AuthenticationLoginResponseFromJson(json);

  Map<String, dynamic> toJson() => _$AuthenticationLoginResponseToJson(this);
}

@JsonSerializable()
class BodyLoginAuthenticationResponse {
  @JsonKey(name: 'accessToken')
  String? accessToken;
  @JsonKey(name: 'refreshToken')
  String? refreshToken;
  @JsonKey(name: 'expiredTime')
  int? expiredTime;
  @JsonKey(name: 'userInfo')
  UserInfoResponse userInfo;

  BodyLoginAuthenticationResponse(
    this.accessToken,
    this.refreshToken,
    this.expiredTime,
    this.userInfo,
  );

  AuthenticationLoginModel toModel() => AuthenticationLoginModel(
        accessToken ?? '',
        refreshToken ?? '',
        expiredTime,
        UserInfoModel(),
      );

  factory BodyLoginAuthenticationResponse.fromJson(Map<String, dynamic> json) =>
      _$BodyLoginAuthenticationResponseFromJson(json);

  Map<String, dynamic> toJson() =>
      _$BodyLoginAuthenticationResponseToJson(this);
}

@JsonSerializable()
class UserInfoResponse {
  @JsonKey(name: 'email')
  String? email;
  @JsonKey(name: 'fullName')
  String? fullName;
  @JsonKey(name: 'avatar')
  String? avatar;

  UserInfoResponse(this.email, this.fullName, this.avatar);

  UserInfoModel toModel() => UserInfoModel(
        email: email ?? '',
        fullName: fullName ?? '',
        avatar: avatar ?? '',
      );

  factory UserInfoResponse.fromJson(Map<String, dynamic> json) =>
      _$UserInfoResponseFromJson(json);

  Map<String, dynamic> toJson() => _$UserInfoResponseToJson(this);
}
