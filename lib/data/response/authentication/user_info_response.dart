class UserInfoResponse {
  int? errorCode;
  String? errorMessage;
  UserInfoBody? body;

  UserInfoResponse({this.errorCode, this.errorMessage, this.body});

  UserInfoResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
    body = json['body'] != null ? UserInfoBody.fromJson(json['body']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    if (body != null) {
      data['body'] = body!.toJson();
    }
    return data;
  }
}

class UserInfoBody {
  String? email;
  String? fullName;
  String? avatar;

  UserInfoBody({this.email, this.fullName, this.avatar});

  UserInfoBody.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    fullName = json['name'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['name'] = fullName;
    data['avatar'] = avatar;
    return data;
  }
}
