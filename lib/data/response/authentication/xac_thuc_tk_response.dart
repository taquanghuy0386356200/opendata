class XacThucTKResponse {
  String? message;
  int? errorCode;
  XacThucTKBodyResponse? body;

  XacThucTKResponse({this.message, this.errorCode, this.body});

  XacThucTKResponse.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    errorCode = json['errorCode'];
    body = json['body'] != null
        ? XacThucTKBodyResponse.fromJson(json['body'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['errorCode'] = message;
    data['errorCode'] = errorCode;
    if (body != null) {
      data['body'] = body!.toJson();
    }
    return data;
  }
}

class XacThucTKBodyResponse {
  String? email;
  String? fullName;
  String? avatar;

  XacThucTKBodyResponse({this.email, this.fullName, this.avatar});

  XacThucTKBodyResponse.fromJson(Map<String, dynamic> json) {
    email = json['email'];
    fullName = json['name'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['email'] = email;
    data['name'] = fullName;
    data['avatar'] = avatar;
    return data;
  }
}
