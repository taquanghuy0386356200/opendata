class DoiMatKhauResponse {
  int? errorCode;
  String? message;

  DoiMatKhauResponse({this.errorCode, this.message});

  DoiMatKhauResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['errorCode'] = errorCode;
    data['message'] = message;
    return data;
  }
}