class OTPResponse {
  OTPBodyResponse? body;
  int? errorCode;
  String? errorMessage;

  OTPResponse({this.body, this.errorCode, this.errorMessage});

  OTPResponse.fromJson(Map<String, dynamic> json) {
    body = json['body'] != null ? OTPBodyResponse.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    errorMessage = json['message'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['message'] = errorMessage;
    return data;
  }
}

class OTPBodyResponse {
  int? thanhCong;
  int? id;
  String? email;
  String? otp;
  String? token;

  OTPBodyResponse({this.thanhCong, this.id, this.email, this.otp, this.token});

  OTPBodyResponse.fromJson(Map<String, dynamic> json) {
    thanhCong = json['thanhCong'];
    id = json['id'];
    email = json['email'];
    otp = json['otp'];
    token = json['token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['thanhCong'] = thanhCong;
    data['id'] = id;
    data['email'] = email;
    data['otp'] = otp;
    data['token'] = token;
    return data;
  }
}
