import 'package:bnv_opendata/domain/models/tien_ich/chi_tiet_tai_lieu_dang_tai_model.dart';

class ChiTietTaiLieuDangTaiResponse {
  Data? body;
  int? errorCode;
  String? errorMessage;

  ChiTietTaiLieuDangTaiResponse({this.body, this.errorCode, this.errorMessage});

  ChiTietTaiLieuDangTaiResponse.fromJson(Map<String, dynamic> json) {
    body = json['body'] != null ? Data.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    return data;
  }
}

class Data {
  int? id;
  String? title;
  String? description;
  String? fileDinhKem;
  String? fileName;
  int? fileSize;
  String? fileType;
  String? status;
  int? categoryID;
  String? categoryName;
  int? subcategoryID;
  String? subcategoryName;
  int? createdAt;

  Data({
    this.id,
    this.title,
    this.description,
    this.fileDinhKem,
    this.fileName,
    this.fileSize,
    this.fileType,
    this.status,
    this.categoryID,
    this.categoryName,
    this.subcategoryID,
    this.subcategoryName,
    this.createdAt,
  });

  ChiTietTaiLieuDangTaiModel toModel() => ChiTietTaiLieuDangTaiModel(
        id ?? 0,
        title ?? '',
        description ?? '',
        fileDinhKem ?? '',
        fileName ?? '',
        fileSize ?? 0,
        fileType ?? '',
        status ?? '',
        categoryID ?? 0,
        categoryName ?? '',
        subcategoryID ?? 0,
        subcategoryName ?? '',
        createdAt ?? 0,
      );

  Data.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    fileDinhKem = json['fileDinhKem'];
    fileName = json['fileName'];
    fileSize = json['fileSize'];
    fileType = json['fileType'];
    status = json['status'];
    categoryID = json['categoryID'];
    categoryName = json['categoryName'];
    subcategoryID = json['subcategoryID'];
    subcategoryName = json['subcategoryName'];
    createdAt = json['createdAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['description'] = description;
    data['fileDinhKem'] = fileDinhKem;
    data['fileName'] = fileName;
    data['fileSize'] = fileSize;
    data['fileType'] = fileType;
    data['status'] = status;
    data['categoryID'] = categoryID;
    data['categoryName'] = categoryName;
    data['subcategoryID'] = subcategoryID;
    data['subcategoryName'] = subcategoryName;
    data['createdAt'] = createdAt;
    return data;
  }
}
