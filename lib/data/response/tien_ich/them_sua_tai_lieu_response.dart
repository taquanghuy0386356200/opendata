class ThemSuaTaiLieuTotalResponse {
  BodyData? body;
  int? errorCode;
  String? errorMessage;

  ThemSuaTaiLieuTotalResponse({this.body, this.errorCode, this.errorMessage});

  ThemSuaTaiLieuTotalResponse.fromJson(Map<String, dynamic> json) {
    body = json['body'] != null ? BodyData.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    return data;
  }
}

class BodyData {
  int? id;
  String? title;
  String? categoryID;
  String? categoryName;
  String? subcategoryID;
  String? subcategoryName;
  String? description;
  String? fileDinhKem;
  String? status;
  String? nguoiDangTai;

  BodyData(
      {this.id,
      this.title,
      this.categoryID,
      this.categoryName,
      this.subcategoryID,
      this.subcategoryName,
      this.description,
      this.fileDinhKem,
      this.status,
      this.nguoiDangTai});

  BodyData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    categoryID = json['categoryID'];
    categoryName = json['categoryName'];
    subcategoryID = json['subcategoryID'];
    subcategoryName = json['subcategoryName'];
    description = json['description'];
    fileDinhKem = json['fileDinhKem'];
    status = json['status'];
    nguoiDangTai = json['nguoiDangTai'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['categoryID'] = categoryID;
    data['categoryName'] = categoryName;
    data['subcategoryID'] = subcategoryID;
    data['subcategoryName'] = subcategoryName;
    data['description'] = description;
    data['fileDinhKem'] = fileDinhKem;
    data['status'] = status;
    data['nguoiDangTai'] = nguoiDangTai;
    return data;
  }
}
