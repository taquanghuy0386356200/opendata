import 'package:bnv_opendata/domain/models/tien_ich/tai_lieu_dang_tai_model.dart';

class TaiLieuDangTaiTotalResponse {
  Data? body;
  int? errorCode;
  String? errorMessage;

  TaiLieuDangTaiTotalResponse({this.body, this.errorCode, this.errorMessage});

  TaiLieuDangTaiTotalResponse.fromJson(Map<String, dynamic> json) {
    body = json['body'] != null ? Data.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    return data;
  }
}

class Data {
  List<Items>? items;
  int? pageIndex;
  int? pageSize;
  int? totalItem;

  Data({this.items, this.pageIndex, this.pageSize, this.totalItem});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(Items.fromJson(v));
      });
    }
    pageIndex = json['pageIndex'];
    pageSize = json['pageSize'];
    totalItem = json['totalItem'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    data['pageIndex'] = pageIndex;
    data['pageSize'] = pageSize;
    data['totalItem'] = totalItem;
    return data;
  }
}

class Items {
  int? id;
  String? title;
  String? description;
  String? nguoiDangTai;
  String? fileDinhKem;
  int? fileSize;
  String? fileType;
  String? status;
  String? categoryID;
  String? categoryName;
  String? subcategoryID;
  String? subcategoryName;
  int? createdAt;
  int? thoiGianGuiDuyet;
  int? thoiGianDuyet;
  String? lyDoTraLai;

  Items({
    this.id,
    this.title,
    this.description,
    this.nguoiDangTai,
    this.fileDinhKem,
    this.fileSize,
    this.fileType,
    this.status,
    this.categoryID,
    this.categoryName,
    this.subcategoryID,
    this.subcategoryName,
    this.createdAt,
    this.thoiGianGuiDuyet,
    this.thoiGianDuyet,
    this.lyDoTraLai,
  });

  TaiLieuDangTaiModel toModel() => TaiLieuDangTaiModel(
        id ?? 0,
        title ?? '',
        description ?? '',
        nguoiDangTai ?? '',
        fileDinhKem ?? '',
        fileSize ?? 0,
        status ?? '',
        categoryID ?? '',
        categoryName ?? '',
        subcategoryID ?? '',
        subcategoryName ?? '',
        createdAt ?? 0,
        fileType ?? '',
        thoiGianGuiDuyet ?? 0,
        thoiGianDuyet ?? 0,
        lyDoTraLai ?? '',
      );

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    description = json['description'];
    nguoiDangTai = json['nguoiDangTai'];
    fileDinhKem = json['fileDinhKem'];
    fileSize = json['fileSize'];
    fileType = json['fileType'];
    status = json['status'];
    categoryID = json['categoryID'];
    categoryName = json['categoryName'];
    subcategoryID = json['subcategoryID'];
    subcategoryName = json['subcategoryName'];
    createdAt = json['createdAt'];
    thoiGianGuiDuyet = json['thoiGianGuiDuyet'];
    thoiGianDuyet = json['thoiGianDuyet'];
    lyDoTraLai = json['lyDoTraLai'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['description'] = description;
    data['nguoiDangTai'] = nguoiDangTai;
    data['fileDinhKem'] = fileDinhKem;
    data['fileSize'] = fileSize;
    data['fileType'] = fileType;
    data['status'] = status;
    data['categoryID'] = categoryID;
    data['categoryName'] = categoryName;
    data['subcategoryID'] = subcategoryID;
    data['subcategoryName'] = subcategoryName;
    data['createdAt'] = createdAt;
    data['thoiGianGuiDuyet'] = thoiGianGuiDuyet;
    data['thoiGianDuyet'] = thoiGianDuyet;
    data['lyDoTraLai'] = lyDoTraLai;
    return data;
  }
}
