import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';

class DetailHuongDanResponse {
  int? errorCode;
  String? errorMessage;
  BodyData? body;

  DetailHuongDanResponse({this.errorCode, this.errorMessage, this.body});

  DetailHuongDanResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
    body = json['body'] != null ? BodyData.fromJson(json['body']) : null;
  }
}

class BodyData {
  int? id;
  String? title;
  String? banner;
  String? content;
  int? createdAt;

  BodyData({
    this.id,
    this.title,
    this.banner,
    this.content,
    this.createdAt,
  });

  BodyData.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    banner = json['banner'];
    content = json['content'];
    createdAt = json['createdAt'];
  }

  ItemThongBaoModel toData() => ItemThongBaoModel(
        id: id.toString(),
        title: title,
        banner: banner,
        releaseAt: createdAt,
        content: content,
      );
}
