class XoaTaiLieuResponse {
  int? body;
  int? errorCode;
  String? errorMessage;

  XoaTaiLieuResponse({this.body, this.errorCode, this.errorMessage});

  XoaTaiLieuResponse.fromJson(Map<String, dynamic> json) {
    body = json['body'];
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['body'] = body;
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    return data;
  }
}
