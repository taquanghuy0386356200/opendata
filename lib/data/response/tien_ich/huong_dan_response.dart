import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';

class HuongDanResponse {
  int? errorCode;
  String? errorMessage;
  BodyData? body;

  HuongDanResponse({this.errorCode, this.errorMessage, this.body});

  HuongDanResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
    body = json['body'] != null ? BodyData.fromJson(json['body']) : null;
  }
}

class BodyData {
  List<Items>? items;
  int? totalItem;
  int? pageSize;
  int? pageIndex;

  BodyData({this.items, this.totalItem, this.pageSize, this.pageIndex});

  BodyData.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      items = <Items>[];
      // ignore: avoid_dynamic_calls
      json['items'].forEach((v) {
        items!.add(Items.fromJson(v));
      });
    }
    totalItem = json['totalItem'];
    pageSize = json['pageSize'];
    pageIndex = json['pageIndex'];
  }

  DataDuThongBaoModel toModel() => DataDuThongBaoModel(
        item: items?.map((e) => e.toData()).toList(),
        totalItem: totalItem,
        pageIndex: pageIndex,
        pageSize: pageSize,
      );
}

class Items {
  int? id;
  String? title;
  String? banner;
  int? createdAt;

  Items({this.id, this.title, this.banner, this.createdAt});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    banner = json['banner'];
    createdAt = json['createdAt'];
  }

  ItemThongBaoModel toData() => ItemThongBaoModel(
        id: id.toString(),
        title: title,
        banner: banner,
        releaseAt: createdAt,
      );
}
