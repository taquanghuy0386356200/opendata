import 'package:bnv_opendata/domain/models/tien_ich/lien_he_model.dart';

class LienHeResponse {
  int? errorCode;
  String? errorMessage;
  BodyData? body;

  LienHeResponse({this.errorCode, this.errorMessage, this.body});

  LienHeResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
    body = json['body'] != null ? BodyData.fromJson(json['body']) : null;
  }
}

class BodyData {
  double? latitude;
  double? longtitude;
  String? address;
  String? phoneNumber;
  String? faxNumber;
  String? email;
  String? title;

  BodyData({
    this.latitude,
    this.longtitude,
    this.address,
    this.phoneNumber,
    this.faxNumber,
    this.email,
    this.title,
  });

  BodyData.fromJson(Map<String, dynamic> json) {
    latitude = json['latitude'];
    longtitude = json['longtitude'];
    address = json['address'];
    phoneNumber = json['phoneNumber'];
    faxNumber = json['faxNumber'];
    email = json['email'];
    title = json['title'];
  }

  LienHeModel toModel() => LienHeModel(
        latitude: latitude,
        longtitude: longtitude,
        address: address,
        phoneNumber: phoneNumber,
        faxNumber: faxNumber,
        email: email,
        title: title,
      );
}
