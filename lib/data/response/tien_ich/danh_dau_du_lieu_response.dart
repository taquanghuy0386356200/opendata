class DanhDauDuLieuResponse {
  int? body;
  int? errorCode;
  String? errorMessage;

  DanhDauDuLieuResponse({this.body, this.errorCode, this.errorMessage});

  DanhDauDuLieuResponse.fromJson(Map<String, dynamic> json) {
    body = json['body'];
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['body'] = body;
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    return data;
  }
}
