import 'package:bnv_opendata/domain/models/tien_ich/lien_he_model.dart';

class SentLienHeResponse {
  int? errorCode;
  String? errorMessage;
  BodyData? body;

  SentLienHeResponse({this.errorCode, this.errorMessage, this.body});

  SentLienHeResponse.fromJson(Map<String, dynamic> json) {
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
    body = json['body'] != null ? BodyData.fromJson(json['body']) : null;
  }
}

class BodyData {
  String? name;
  String? email;
  String? title;
  String? content;

  BodyData({this.name, this.email, this.title, this.content});

  BodyData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    email = json['email'];
    title = json['title'];
    content = json['content'];
  }

  LienHeModel toModel() => LienHeModel();
}
