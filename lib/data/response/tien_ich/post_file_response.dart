class PostFileTotalResponse {
  int? errorCode;
  String? errorMessage;
  String? body;

  PostFileTotalResponse({this.errorCode, this.errorMessage, this.body});

  PostFileTotalResponse.fromJson(Map<String, dynamic> json) {
    body = json['body'];
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
  }
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    data['body'] = body;
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    return data;
  }

}
