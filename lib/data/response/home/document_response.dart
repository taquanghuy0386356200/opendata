import 'package:bnv_opendata/domain/model/document_model.dart';

class DocumentTotalResponse {
  DocumentResponse? body;
  int? errorCode;
  String? errorMessage;

  DocumentTotalResponse({this.body, this.errorCode, this.errorMessage});

  DocumentTotalResponse.fromJson(Map<String, dynamic> json) {
    body =
        json['body'] != null ? DocumentResponse.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    return data;
  }
}

class DocumentResponse {
  List<DocumentItemResponse>? items;
  int? pageIndex;
  int? pageSize;
  int? totalItem;

  DocumentResponse({this.items, this.pageIndex, this.pageSize, this.totalItem});

  DocumentResponse.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      items = <DocumentItemResponse>[];
      // ignore: avoid_dynamic_calls
      json['items'].forEach((v) {
        items!.add(DocumentItemResponse.fromJson(v));
      });
    }
    pageIndex = json['pageIndex'];
    pageSize = json['pageSize'];
    totalItem = json['totalItem'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    data['pageIndex'] = pageIndex;
    data['pageSize'] = pageSize;
    data['totalItem'] = totalItem;
    return data;
  }
}

class DocumentItemResponse {
  int? id;
  String? title;
  String? banner;
  bool? isSaved;
  int? releaseAt;
  int? affectedAt;

  DocumentItemResponse({
    this.id,
    this.title,
    this.banner,
    this.isSaved,
    this.releaseAt,
    this.affectedAt,
  });

  DocumentModel toModel() => DocumentModel(
        id ?? 0,
        title ?? '',
        banner ?? '',
        releaseAt ?? 0,
        affectedAt ?? 0,
      );

  DocumentItemResponse.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    title = json['title'];
    banner = json['banner'];
    isSaved = json['isSaved'];
    releaseAt = json['releaseAt'];
    affectedAt = json['affectedAt'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['title'] = title;
    data['banner'] = banner;
    data['isSaved'] = isSaved;
    data['releaseAt'] = releaseAt;
    data['affectedAt'] = affectedAt;
    return data;
  }
}
