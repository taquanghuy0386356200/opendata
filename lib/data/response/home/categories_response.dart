import 'package:bnv_opendata/domain/model/category_model.dart';

class CategoriesTotalResponse {
  CategoriesResponse? body;
  int? errorCode;
  String? errorMessage;

  CategoriesTotalResponse({this.body, this.errorCode, this.errorMessage});

  CategoriesTotalResponse.fromJson(Map<String, dynamic> json) {
    body =
        json['body'] != null ? CategoriesResponse.fromJson(json['body']) : null;
    errorCode = json['errorCode'];
    errorMessage = json['errorMessage'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (body != null) {
      data['body'] = body!.toJson();
    }
    data['errorCode'] = errorCode;
    data['errorMessage'] = errorMessage;
    return data;
  }
}

class CategoriesResponse {
  List<Items>? items;
  int? pageIndex;
  int? pageSize;
  int? totalItem;

  CategoriesResponse(
      {this.items, this.pageIndex, this.pageSize, this.totalItem});

  CategoriesResponse.fromJson(Map<String, dynamic> json) {
    if (json['items'] != null) {
      items = <Items>[];
      json['items'].forEach((v) {
        items!.add(Items.fromJson(v));
      });
    }
    pageIndex = json['pageIndex'];
    pageSize = json['pageSize'];
    totalItem = json['totalItem'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    if (items != null) {
      data['items'] = items!.map((v) => v.toJson()).toList();
    }
    data['pageIndex'] = pageIndex;
    data['pageSize'] = pageSize;
    data['totalItem'] = totalItem;
    return data;
  }
}

class Items {
  int? id;
  String? name;
  String? icon;
  List<SubCategories>? subCategories;

  Items({this.id, this.name, this.icon, this.subCategories});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    icon = json['icon'];
    if (json['subCategories'] != null) {
      subCategories = <SubCategories>[];
      json['subCategories'].forEach((v) {
        subCategories!.add(SubCategories.fromJson(v));
      });
    }
  }

  CategoryModel toModel() => CategoryModel(
        id ?? 0,
        name ?? '',
        icon ?? '',
        subCategories?.map((e) => e.toModel()).toList(),
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['icon'] = icon;
    if (subCategories != null) {
      data['subCategories'] = subCategories!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class SubCategories {
  int? id;
  String? name;

  SubCategories({this.id, this.name});

  SubCategories.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
  }

  SubCategoriesModel toModel() => SubCategoriesModel(id ?? 0, name ?? '');

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    return data;
  }
}
