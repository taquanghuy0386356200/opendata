import 'package:bnv_opendata/domain/model/popular_model/result_popular.dart';
import 'package:json_annotation/json_annotation.dart';

part 'result_popular_response.g.dart';

@JsonSerializable()
class ResultPopularResponse {
  @JsonKey(name: 'errorCode')
  int? errorCode;
  @JsonKey(name: 'errorMessage')
  String? errorMessage;
  @JsonKey(name: 'body')
  dynamic body;

  ResultPopularResponse(this.errorCode, this.errorMessage, this.body);

  ResultPopularModel toModel() => ResultPopularModel(
        errorCode ?? 0,
        errorMessage ?? '',
        body,
      );

  factory ResultPopularResponse.fromJson(Map<String, dynamic> json) =>
      _$ResultPopularResponseFromJson(json);

  Map<String, dynamic> toJson() => _$ResultPopularResponseToJson(this);
}
