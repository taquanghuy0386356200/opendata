import 'package:bnv_opendata/utils/constants/app_constants.dart';
import 'package:intl/intl.dart';

enum TimeRange { HOM_NAY, TUAN_NAY, THANG_NAY, NAM_NAY }

extension DateFormatString on DateTime {
  String get toStringDDMMYYYY {
    final dateString = DateFormat(DateTimeFormat.DAY_MONTH_YEAR).format(this);
    return dateString;
  }
}

extension FormatDateTime on int {
  String getDateTimeFromMil({required int milisecondsTime}) {
    final DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
      (milisecondsTime - (3600 * 7)) * 1000,
    );
    final String result = DateFormat('dd/MM/yyyy').format(dateTime);
    return result;
  }

  String getTimeFromMil({required int milisecondsTime}) {
    final DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
      (milisecondsTime - (3600 * 7)) * 1000,
    );
    final String result = DateFormat('HH:mm').format(dateTime);
    return result;
  }

  String fomatFullDateFromMili(int milisecondsTime) {
    final DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
      (milisecondsTime - (3600 * 7)) * 1000,
    );
    final String result = DateFormat('dd/MM/yyyy HH:mm').format(dateTime);
    return result;
  }
}

extension FormatString on String {
  DateTime convertStringToDate({
    String formatPattern = DateTimeFormat.DAY_MONTH_YEAR,
  }) {
    try {
      return DateFormat(formatPattern).parse(this);
    } catch (_) {
      return DateTime.now();
    }
  }
}

extension StringParse on String {
  String convertNameFile() {
    final document = this;

    final parts = document.split('/');

    final lastName = parts.last;

    final partsNameFile = lastName.split('.');

    if (partsNameFile[0].length > 30) {
      partsNameFile[0] = '${partsNameFile[0].substring(0, 10)}... ';
    }
    final fileName = '${partsNameFile[0]}.${partsNameFile[1]}';

    return fileName;
  }
}
