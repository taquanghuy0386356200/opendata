import 'dart:async';

import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/presentation/authentication/login/ui/login_screen.dart';
import 'package:flutter/material.dart';

void openScreen(BuildContext context, String screenName, {dynamic args}) {
  Navigator.of(context).pushNamed(screenName, arguments: args);
  // Get.toNamed(screenName, arguments: args);
}

void openScreenWithData(BuildContext context, String screenName, Object args) {
  Navigator.of(context).pushNamed(screenName, arguments: args);
}

void openScreenWithDataForResult(
  BuildContext context,
  String screenName,
  Object args,
  Function action,
) {
  Navigator.of(context).pushNamed(screenName, arguments: args).then((result) {
    if (result != null) {
      // ignore: avoid_dynamic_calls
      action(result);
    }
  });
}

void replaceScreen(BuildContext context, String screenName) {
  Navigator.of(context).pushReplacementNamed(screenName);
}

void pushAndReplacement(BuildContext context, Widget screen) {
  Navigator.pushReplacement(
    context,
    MaterialPageRoute(builder: (context) => screen),
  );
}

Future<void> gotoLoginAndDirectScreen(
  BuildContext context, {
  required String nameDirect,
  String? id,
}) async {
  unawaited(
    PrefsService.saveNameScreen(
      screenName: (id ?? '').isNotEmpty ? '$nameDirect/${id ?? ''}' : nameDirect,
    ),
  );
  await Navigator.pushReplacement(
    context,
    MaterialPageRoute(builder: (context) => const LoginScreen()),
  );
}

void openScreenAndRemoveUtil(
  BuildContext context,
  String screenName, {
  dynamic args,
}) {
  Navigator.of(context).pushNamedAndRemoveUntil(
    screenName,
    (Route<dynamic> route) => false,
    arguments: args,
  );
}

void popUntilName(BuildContext context, String screenName) {
  Navigator.of(context).popUntil(ModalRoute.withName(screenName));
}

void popToFirst(BuildContext context) {
  Navigator.of(context).popUntil((route) => route.isFirst);
}

void closeScreen(BuildContext context, {dynamic args}) {
  Navigator.of(context).pop(args);
}

void closeScreenWithData(BuildContext context, Object? args) {
  Navigator.of(context).pop(args);
}

void goTo(BuildContext context, Widget screen) {
  Navigator.of(context).push(MaterialPageRoute(builder: (context) => screen));
}
