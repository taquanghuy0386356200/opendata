import 'dart:io';
import 'dart:math';

import 'package:bnv_opendata/generated/l10n.dart';

import 'package:bnv_opendata/utils/constants/app_constants.dart';

extension FileSizeHelper on int {
  String convertToMb() {
    final String fileSizeMB = '${(this / 1000).toStringAsFixed(2)} Mb';
    return fileSizeMB;
  }

  String formatBytes() {
    if (this <= 0) return "0 B";
    const suffixes = ["B", "KB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB"];
    var i = (log(this) / log(1024)).floor();
    return '${(this / pow(1024, i)).toStringAsFixed(2)} ${suffixes[i]}';
  }
}

extension StringExtension on String {
  String capitalize() {
    // ignore: prefer_single_quotes
    return "${this[0].toUpperCase()}${substring(1).toLowerCase()}";
  }

  String handleStringTooLong() {
    return length > 35 ? '${substring(0, 30)}...' : this;
  }

  String trimLeftRight() {
    return trimLeftRight();
  }

  String phaiNhapDungDinhDang({required String name}) {
    return '${S.current.phai_nhap_dung_dinh_dang} $name';
  }

  String? checkTruongNull(
    String name,
  ) {
    if (trim().isEmpty) {
      return '${S.current.vui_long_nhap}$name';
    }
    return null;
  }

  String? validateEmail({String? errMessage}) {
    // final checkEmail = RegExp(
    //         r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
    //     .hasMatch(this);
    // if (!checkEmail) {
    //   return S.current.email_khong_dung_dinh_dang;
    // } else {
    //   return null;
    // }

    String pattern = r'^[a-zA-Z0-9_]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]'
        r'{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]'
        r'{0,253}[a-zA-Z0-9])?)*$';
    RegExp regex = RegExp(pattern);
    if (isEmpty || !regex.hasMatch(this)) {
      return S.current.email_khong_dung_dinh_dang;
    } else {
      return null;
    }
  }

  String? checkPassWord() {
    final isCheck = RegExp(
      r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&|*~.:=_;({})%^+,-/?><`"]).{8,15}$',
    ).hasMatch(this);
    if (isCheck) {
      return null;
    } else {
      return S.current.mat_khau_8_15_yeu;
    }
  }

  String? checkEmailBoolean({String? errMessage}) {
    final isCheck = RegExp(
      EMAIL_REGEX,
    ).hasMatch(this);
    if (isCheck) {
      if ((indexOf('@')) > 64 || (length - indexOf('.') - 1) > 254) {
        return errMessage ?? S.current.nhap_sai_dinh_dang;
      } else {
        return null;
      }
    } else {
      return errMessage ?? S.current.nhap_sai_dinh_dang;
    }
  }

  String convertNameFile() {
    final document = this;

    final parts = document.split('/');

    final lastName = parts.last;

    final partsNameFile = lastName.split('.');

    if (partsNameFile[0].length > 30) {
      partsNameFile[0] = '${partsNameFile[0].substring(0, 10)}... ';
    }
    final fileName = '${partsNameFile[0]}.${partsNameFile[1]}';

    return fileName;
  }
}

extension FileUtils on File {
  get size {
    int sizeInBytes = this.lengthSync();
    double sizeInMb = sizeInBytes / (1024 * 1024);
    return sizeInMb;
  }
}
