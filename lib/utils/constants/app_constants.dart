enum AppMode { LIGHT, DARK }

enum ServerType { DEV, QA, STAGING, PRODUCT }

enum LoadingType { REFRESH, LOAD_MORE }

enum CompleteType { SUCCESS, ERROR }

enum MenuType { FEED, NOTIFICATIONS, POLICY, LOGOUT }

enum AuthMode { LOGIN, REGISTER }

enum AuthType { ACCOUNT, PHONE }

enum PageTransitionType {
  FADE,
  RIGHT_TO_LEFT,
  BOTTOM_TO_TOP,
  RIGHT_TO_LEFT_WITH_FADE,
}

const String CALENDAR_TYPE_DAY = 'Day';
const String CALENDAR_TYPE_MONTH = 'Month';
const String CALENDAR_TYPE_YEAR = 'Year';
const String LOGGED_IN = 'logged in';

const EN_CODE = 'en';
const VI_CODE = 'vi';
const VI_LANG = 'vn';

const EMAIL_REGEX =
    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+";
const VN_PHONE = r'(84|0[3|5|7|8|9])+([0-9]{8})\b';

const OTP = r'^[a-zA-Z0-9]+$';

//2021-06-18 04:24:27
const _dtFormat1 = 'yyyy-MM-dd HH:mm:ss';
const _dtFormat2 = 'hh:mm a';
const _dtFormat3 = 'dd/MM hh:mm a';
const _dtFormat4 = 'yyyy-MM-dd';
const _dtFormat5 = 'MMM dd, yyyy';
const _dtFormat10 = 'dd/MM/yyyy';
const String dtLowCase = 'dd/mm/yyyy';

class DateTimeFormat {
  static const DEFAULT_FORMAT = _dtFormat1;
  static const HOUR_FORMAT = _dtFormat2;
  static const CREATE_FORMAT = _dtFormat3;
  static const DOB_FORMAT = _dtFormat4;
  static const CREATE_BLOG_FORMAT = _dtFormat5;
  static const DAY_MONTH_YEAR = _dtFormat10;
}

class FileExtensions {
  static const String DOC = 'doc';
  static const String DOCX = 'docx';
  static const String PDF = 'pdf';
  static const String PNG = 'png';
  static const String JPEG = 'jpeg';
  static const String JPG = 'jpg';
  static const String XLSX = 'xlsx';
  static const String PPTX = 'pptx';
  static const String HEIC = 'heic';
}

class MaxSizeFile {
  static const MAX_SIZE_30MB = 30000000;
  static const MAX_SIZE_20MB = 20000000;
  static const MAX_SIZE_10MB = 10000000;
}

class TypeChartFormat {
  static const FILE = 1;
  static const PIE = 2;
  static const COLUMN = 3;
  static const LINE = 4;
  static const LIST = 5;
  static const HorizontalColum = 6;
  static const StackedBarChart = 7;
  static const StatisticalReports = 8;
}

class MainScreens {
  static const HomeScreen = 0;
  static const DuLieuMoScreen = 1;
  static const ThongBaoScreen = 2;
  static const VanBanScreen = 3;
  static const TienIchScreen = 4;
}
