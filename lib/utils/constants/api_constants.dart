import 'package:bnv_opendata/domain/env/model/app_constants.dart';
import 'package:get/get.dart';

final appConstants = Get.find<AppConstants>();

class ApiConstants {
  static const LOGIN = '/example_view';

  static const int DEFAULT_PAGE_SIZE = 10;
  static const int PAGE_BEGIN = 1;
  static const int NOT_SAVED_ID = -1;
  static const int TIME_OUT = 30;

  /// base img
  static String baseUrl = appConstants.baseUrl;

  ///Authentication
  static const String LOGIN_OPEN_DATA = '/oauth2/token';
  static const String REGISTER_OPEN_DATA = '/auth/register';
  static const String LOST_PASSWORD = '/User/ForgotPassword';
  static const String OTP = '/User/HandleOTP';
  static const String THIET_LAP_MAT_KHAU = '/User/HandleChangeNewPassForgot';
  static const String RESET_PASSWORD = '/auth/resetPassword';
  static const String REFRESH_TOKEN = '/oauth2/token';
  static const String XAC_THUC_TAI_KHOAN = '/LoginToken';

  static const String GET_CAPCHAT = '/capcha/get';
  static const String DOI_MAT_KHAU = '/User/HandleChangePassword';

  ///HOME
  static const String LIST_DANH_SACH_LINH_VUC = '/categories';

  /// open data
  static const String LIST_OPEN_DATA = '/content/list';
  static const String DETAIL_OPEN_DATA = '/content/{id}';

  /// thong bao
  static const String LIST_THONG_BAO = '/notification/list';
  static const String CHI_TIET_THONG_BAO = '/notification/{id}';

  /// tien ich
  static const String LIST_HUONG_DAN = '/guide/list';
  static const String CHI_TIET_HUONG_DAN = '/guide/{id}';
  static const String LIEN_HE = '/contact';
  static const String GUI_LIEN_HE = '/contact/send';
  static const String DANH_SACH_DANG_TAI_CA_NHAN = '/personalData/list';
  static const String UPLOAD_FILE = '/uploadFileMobile';
  static const String THEM_SUA_TAI_LIEU = '/personalData/create';
  static const String CHI_TIET_TAI_LIEU_DANG_TAI = '/personalData/{id}';
  static const String XOA_TAI_LIEU = '/personalData/delete';
  static const String DANH_DAU_DU_LIEU = '/data/bookmark';
  static const String BO_DANH_DAU_DU_LIEU = '/data/unbookmark';
  static const String DANH_SACH_BAI_VIET = '/data/list';
  static const String DETAIL_BAI_VIET = '/data/{id}';

  /// Văn bản
  static const String LIST_VAN_BAN = '/document/list';
  static const String CHI_TIET_VAN_BAN = '/document/{id}';
}
