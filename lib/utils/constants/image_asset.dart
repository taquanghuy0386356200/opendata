import 'package:bnv_opendata/config/resources/images.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ImageAssets {
  ///Svg path
  static const String icBack = '$baseImg/ic_back.svg';
  static const String icMenu = '$baseImg/ic_menu.svg';

  ///Png path
  //icon bottom bar
  static const String icHomeSelect = '$baseImg/icon_home_select.png';
  static const String icHomeUnSelect = '$baseImg/icon_home_un_select.png';
  static const String icOpenDataSelect = '$baseImg/ic_open_data_select.png';
  static const String icOpenDataUnSelect =
      '$baseImg/ic_open_data_un_select.png';
  static const String icNotificationSelect =
      '$baseImg/icon_notification_select.png';
  static const String icNotificationUnSelect =
      '$baseImg/icon_notification_un_select.png';
  static const String icDocumentSelect = '$baseImg/icon_document_select.png';
  static const String icDocumentUnSelect =
      '$baseImg/icon_document_un_select.png';
  static const String icTienIchSelect = '$baseImg/icon_tien_ich_select.png';
  static const String icTienIchUnSelect =
      '$baseImg/icon_tien_ich_Un_select.png';

  ///icon open data
  static const String iconFilterOpendata = '$baseImg/icon_filter_opendata.png';
  static const String icSearchOpenData = '$baseImg/icon_search.png';
  static const String icPinedOpenData = '$baseImg/icon_pined.png';
  static const String icUnPinOpenData = '$baseImg/icon_unpin.png';
  static const String icWarningOpenData = '$baseImg/icon_warning.png';
  static const String imgKhongCoDuLieu = '$baseImg/img_khong_co_du_lieu.png';
  static const String imgExemple = '$baseImg/img_example.png';
  static const String icClearFile = '$baseImg/icClearFile.png';

  ///icon thong bao
  static const String icSearchThongBao = '$baseImg/icon_search_thong_bao.png';
  static const String icSuccess = '$baseImg/ic_success.png';
  static const String bgLogin = '$baseImg/bg_login.png';
  static const String bgRegister = '$baseImg/bg_register.png';
  static const String bgLostPassWord = '$baseImg/bg_lost_password.png';
  static const String bgCreatePassWord = '$baseImg/bg_create_password.png';
  static const String bgChangePassWord = '$baseImg/bg_change_pw.png';
  static const String icBackBtn = '$baseImg/ic_back_btn.png';
  static const String bgSplash = '$baseImg/bg_splash.png';
  static const String icQuocHuy = '$baseImg/ic_quoc_huy.png';
  static const String icUser = '$baseImg/ic_user.png';
  static const String icPassword = '$baseImg/ic_password.png';
  static const String icEye = '$baseImg/ic_eye.png';
  static const String icHideEye = '$baseImg/ic_hide_eye.png';
  static const String icClear = '$baseImg/ic_clear.png';
  static const String icEmail = '$baseImg/ic_email.png';
  static const String icOTP = '$baseImg/ic_otp.png';
  static const String icChangePWSuccess = '$baseImg/ic_success_change_pw.png';
  static const String icEmailGoogle = '$baseImg/ic_email_gg.png';
  static const String icShutDown = '$baseImg/ic_shutdown.png';
  static const String icTickOrange = '$baseImg/ic_tick_orange.png';
  static const String icWarningOrange = '$baseImg/ic_warning_orange.png';
  static const String icSearchOrange = '$baseImg/ic_search_orange.png';
  static const String icWarning = '$baseImg/ic_warning.png';
  static const String icUserHome = '$baseImg/ic_user_home.png';
  static const String icCapcha = '$baseImg/icon_capcha.png';
  static const String icChangeCapcha = '$baseImg/reload_capcha.png';
  static const String icErrorLogin = '$baseImg/ic_error_login.png';

  ///Home
  static const String icSearch = '$baseImg/ic_search.png';
  static const String icCaiCachHanhChinh =
      '$baseImg/ic_cai_cach_hanh_chinh.png';
  static const String icToChucHanhChinh = '$baseImg/ic_to_chuc_hanh_chinh.png';
  static const String icCanBoCCVC = '$baseImg/ic_can_bo_ccvc.png';
  static const String icThanhTraNoiVu = '$baseImg/ic_thanh_tra_nganh_nv.png';
  static const String icHopTacQuocTe = '$baseImg/ic_hop_tac_qt.png';
  static const String icThiDuaKhenThuong = '$baseImg/ic_thi_dua.png';
  static const String icBienChe = '$baseImg/ic_bien_che.png';
  static const String icHoiToChucPhiChinhPhu =
      '$baseImg/ic_hoi_tc_phi_chinh_phu.png';
  static const String icTonGiao = '$baseImg/ic_ton_giao.png';
  static const String icDaoTaoBoiDuongCbo = '$baseImg/ic_dao_tao_can_bo.png';
  static const String icVanThuLuuTru = '$baseImg/ic_van_thu_luu_tru.png';
  static const String icCongTacThanhNien =
      '$baseImg/ic_cong_tac_thanh_nien.png';
  static const String icCalender12 = '$baseImg/ic_calender12.png';
  static const String icCheck = '$baseImg/check.png';

  ///Tien ich
  static const String icDuLieuDanhDau = '$baseImg/ic_bookmark.png';
  static const String icTaiLieuDangTai = '$baseImg/ic_bag_violent.png';
  static const String icLienHe = '$baseImg/ic_message_green.png';
  static const String icHuongDan = '$baseImg/ic_shield_yellow.png';
  static const String icArrowRight = '$baseImg/ic_arrow_right.png';
  static const String icAddCircleOrange = '$baseImg/ic_add_orange.png';
  static const String icEdit = '$baseImg/ic_edit_tai_lieu.png';
  static const String icDeleteDoc = '$baseImg/ic_remove_document.png';
  static const String icXoaDialog = '$baseImg/ic_xoa_dialog.png';
  static const String icDinhFile = '$baseImg/attach_file.png';
  static const String icSaveAndSend = '$baseImg/ic_save_send.png';
  static const String icSaveDraft = '$baseImg/ic_save_draft.png';
  static const String icDocumentTienIch = '$baseImg/ic_document_tien_ich.png';
  static const String icDropDownArror = '$baseImg/arrow_down.png';

  ///liên hệ
  static const String icSendLienHe = '$baseImg/ic_send_lien_he.png';

  ///Popular image
  static const String emptyData = '$baseImg/no_data.png';

  /// default img
  static const String default_img = '$baseImg/default_image.png';
  static const String default_avatar = '$baseImg/default_avatar.png';

  static SvgPicture svgAssets(
    String name, {
    Color? color,
    double? width,
    double? height,
    BoxFit? fit,
    BlendMode? blendMode,
  }) {
    final size = _svgImageSize[name];
    var w = width;
    var h = height;
    if (size != null) {
      w = width ?? size[0];
      h = height ?? size[1];
    }
    return SvgPicture.asset(
      name,
      colorBlendMode: blendMode ?? BlendMode.srcIn,
      color: color,
      width: w,
      height: h,
      fit: fit ?? BoxFit.none,
    );
  }

  static const Map<String, List<double>> _svgImageSize = {
    icMenu: [18, 16],
    icBack: [6, 12.25],
  };
}
