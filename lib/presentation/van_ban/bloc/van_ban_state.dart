part of 'van_ban_cubit.dart';

@immutable
abstract class VanBanState extends BaseState {
  const VanBanState();
}

class VanBanInitial extends VanBanState {
  @override
  List<Object?> get props => [];
}
