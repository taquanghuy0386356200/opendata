import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/config/base/base_state.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/domain/repository/van_ban/van_ban_respository.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:bnv_opendata/utils/constants/app_constants.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/subjects.dart';

part 'van_ban_state.dart';

class VanBanCubit extends BaseCubit<BaseState> {
  VanBanCubit() : super(VanBanInitial()) {
    showContent();
  }

  String keySearch = '';

  VanBanRepository get _authenRepo => Get.find();

  BehaviorSubject<List<ItemThongBaoModel>> listVanBanSubject =
      BehaviorSubject();
  BehaviorSubject<ItemThongBaoModel> vanBanSubject = BehaviorSubject();

  Future<void> getListDocument() async {
    if (!getInLoadMore) {
      showLoading();
    }
    inSearching.sink.add(keySearch.isNotEmpty);
    final result = await _authenRepo.getListVanBan(
      keySearch: keySearch,
      pageSize: ApiConstants.DEFAULT_PAGE_SIZE,
      pageIndex: loadMorePage,
    );
    result.when(
      success: (response) {
        if (loadMorePage == ApiConstants.PAGE_BEGIN) {
          if (response.isEmpty) {
            showContent();
            emit(const CompletedLoadMore(CompleteType.SUCCESS, posts: []));
          } else {
            showContent();
            emit(CompletedLoadMore(CompleteType.SUCCESS, posts: response));
          }
        } else {
          showContent();
          emit(CompletedLoadMore(CompleteType.SUCCESS, posts: response));
        }
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> getDetailDocument({
    required String id,
  }) async {
    showLoading();
    final result = await _authenRepo.getDetailVanBan(
      id,
    );
    result.when(
      success: (response) {
        showContent();
        vanBanSubject.sink.add(response);
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> onSearching(String value) async {
    canMoveToTop = true;
    keySearch = value;
    loadMorePage = ApiConstants.PAGE_BEGIN;
  }
}
