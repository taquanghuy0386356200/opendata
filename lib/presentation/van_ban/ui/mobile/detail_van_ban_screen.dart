import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/van_ban/bloc/van_ban_cubit.dart';
import 'package:bnv_opendata/presentation/webview/webview_from_html.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/html_edit/edit_html.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class DetailVanBanScreen extends StatefulWidget {
  final String id;

  const DetailVanBanScreen({
    Key? key,
    required this.id,
  }) : super(key: key);

  @override
  State<DetailVanBanScreen> createState() => _DetailVanBanScreenState();
}

class _DetailVanBanScreenState extends State<DetailVanBanScreen> {
  VanBanCubit cubit = VanBanCubit();

  @override
  void initState() {
    super.initState();
    cubit.getDetailDocument(id: widget.id);
  }

  @override
  void dispose() {
    cubit.vanBanSubject.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: S.current.chi_tiet_van_ban,
        isShowBack: true,
      ),
      body: StateStreamLayout(
        retry: () {
          cubit.getDetailDocument(id: widget.id);
        },
        error: AppException('', S.current.something_went_wrong),
        textEmpty: '',
        stream: cubit.stateStream,
        child: StreamBuilder<ItemThongBaoModel>(
          stream: cubit.vanBanSubject,
          builder: (context, snapshot) {
            final data = snapshot.data ?? ItemThongBaoModel();
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                spaceH16,
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data.title ?? '',
                        style: XelaTextStyle.Xela18Bold.copyWith(
                          color: color364564,
                        ),
                      ),
                      spaceH8,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          timeWidget(
                            title: S.current.ban_hanh,
                            value: data.releaseAt ?? 0,
                          ),
                          timeWidget(
                            title: S.current.hieu_luc,
                            value: data.affectedAt ?? 0,
                            colorValue: color20C997,
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                spaceH16,
                Container(
                  color: colorF9FAFF,
                  height: 8,
                ),
                Expanded(
                  child: WebViewWithHtml(
                    htmlData: data.content ?? '',
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }

  Widget timeWidget({
    required String title,
    required int value,
    Color? colorValue,
  }) =>
      Expanded(
        child: Row(
          children: [
            Container(
              height: 20,
              width: 20,
              padding: const EdgeInsets.only(right: 5.5),
              child: Image.asset(ImageAssets.icCalender12),
            ),
            Text(
              '$title: ',
              style: XelaTextStyle.Xela14Regular.copyWith(
                color: color667793,
              ),
            ),
            Expanded(
              child: Text(
                value.getDateTimeFromMil(milisecondsTime: value),
                style: XelaTextStyle.Xela14Regular.copyWith(
                  color: colorValue ?? color667793,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      );
}
