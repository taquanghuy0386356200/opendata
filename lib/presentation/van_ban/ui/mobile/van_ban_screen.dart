import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/van_ban/bloc/van_ban_cubit.dart';
import 'package:bnv_opendata/presentation/van_ban/ui/mobile/detail_van_ban_screen.dart';
import 'package:bnv_opendata/presentation/van_ban/ui/widget/cell_van_ban.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/widgets/appbar/app_bar_search.dart';
import 'package:bnv_opendata/widgets/listview/loadmore_list.dart';
import 'package:flutter/material.dart';

class VanBanScreen extends StatefulWidget {
  final bool showBack;

  const VanBanScreen({Key? key, this.showBack = false}) : super(key: key);

  @override
  State<VanBanScreen> createState() => _VanBanScreenState();
}

class _VanBanScreenState extends State<VanBanScreen>
    with AutomaticKeepAliveClientMixin<VanBanScreen> {
  @override
  bool get wantKeepAlive => true;
  VanBanCubit cubit = VanBanCubit();
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AppBarSearch(
            title: S.current.van_ban,
            isCanBack: widget.showBack,
            onchange: (value) {
              cubit.onSearching(value).then((value) => cubit.getListDocument());
            }, controller: _controller,
          ),
          Expanded(
            child: ListViewLoadMoreWidget(
              cubit: cubit,
              isListView: true,
              callApi: (page) => cubit.getListDocument(),
              viewItem: (value, index) {
                value as ItemThongBaoModel;
                return CellVanBan(
                  image: '${ApiConstants.baseUrl}${value.banner}',
                  title: value.title ?? '',
                  releaseAt: (value.releaseAt ?? 0).getDateTimeFromMil(
                    milisecondsTime: value.releaseAt ?? 0,
                  ),
                  affectedAt: (value.affectedAt ?? 0).getDateTimeFromMil(
                    milisecondsTime: value.affectedAt ?? 0,
                  ),
                  ontap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => DetailVanBanScreen(
                          id: value.id ?? '',
                        ),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
