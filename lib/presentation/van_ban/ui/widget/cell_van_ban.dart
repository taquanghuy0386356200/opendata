import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/image_network/image_network.dart';
import 'package:bnv_opendata/widgets/text/ellipsis_character_text.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class CellVanBan extends StatelessWidget {
  final String image;
  final String title;
  final String releaseAt;
  final String affectedAt;
  final Function() ontap;

  const CellVanBan({
    Key? key,
    required this.image,
    required this.title,
    required this.releaseAt,
    required this.affectedAt,
    required this.ontap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        ontap();
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 8),
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(16),
          ),
          color: colorF9FAFF,
        ),
        child: Column(
          children: [
            Row(
              children: [
                Expanded(
                  flex: 3,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(8.0),
                    child:
                        SizedBox(height: 65, child: imageNetwork(image: image)),
                  ),
                ),
                spaceW16,
                Expanded(
                  flex: 7,
                  child: EllipsisDoubleLineText(
                    title,
                    style: XelaTextStyle.Xela14Medium.copyWith(
                      color: color364564,
                    ),
                  ),
                )
              ],
            ),
            spaceH8,
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                timeWidget(
                  title: S.current.ban_hanh,
                  value: releaseAt,
                ),
                timeWidget(
                  title: S.current.hieu_luc,
                  value: affectedAt,
                  colorValue: color20C997,
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget timeWidget({
    required String title,
    required String value,
    Color? colorValue,
  }) =>
      Expanded(
        child: Row(
          children: [
            Container(
              height: 20,
              width: 20,
              padding: const EdgeInsets.only(right: 5.5),
              child: Image.asset(ImageAssets.icCalender12),
            ),
            Text(
              '$title: ',
              style: XelaTextStyle.Xela14Regular.copyWith(
                color: color667793,
              ),
            ),
            Expanded(
              child: Text(
                value,
                style: XelaTextStyle.Xela14Regular.copyWith(
                  color: colorValue ?? color667793,
                ),
                overflow: TextOverflow.ellipsis,
              ),
            )
          ],
        ),
      );
}
