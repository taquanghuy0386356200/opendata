import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/config/base/base_state.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/domain/models/open_data_model/chart_model.dart';
import 'package:bnv_opendata/domain/models/open_data_model/detail_open_data_model.dart';
import 'package:bnv_opendata/domain/models/open_data_model/open_data_model.dart';
import 'package:bnv_opendata/domain/repository/du_lieu_mo/du_lieu_mo_repository.dart';
import 'package:bnv_opendata/domain/repository/tien_ich/tien_ich_respository.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:bnv_opendata/utils/constants/app_constants.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'du_lieu_mo_state.dart';

class DuLieuMoCubit extends BaseCubit<BaseState> {
  DuLieuMoCubit() : super(DuLieuMoInitial()) {
    showContent();
  }

  DuLieuMoRepository get _openDataRepo => Get.find();

  TienIchRepository get _tienIchRepo => Get.find();

  List<SubCategoriesModel> subCategories = [];

  String nguoiDung = PrefsService.getUserNameSameGmail();

  ///data filtter
  SubCategoriesModel? dataSubCategoriesFilter;
  CategoryModel? dataCategoryFilter;
  bool? isDanhDauScreen;
  String? keySearch;

  List<Map<String, dynamic>> dataListChartTable = [];

  BehaviorSubject<int> countDataSubject = BehaviorSubject.seeded(0);

  BehaviorSubject<OpenDataModel> openDataSubject = BehaviorSubject();
  BehaviorSubject<DetailOpenDataModel> detailOpenDataSubject =
      BehaviorSubject();

  BehaviorSubject<List<Map<String, dynamic>>> dataChartTableListSubject =
      BehaviorSubject();

  Future<void> getListOpenData({
    int? id,
  }) async {
    dynamic convertIntToNull(int data) {
      if (data == 0) {
        return null;
      }
      return data;
    }

    if (!getInLoadMore) {
      showLoading();
    }
    final result = await _openDataRepo.getListOpenData(
      keySearch: keySearch,
      categoryID: convertIntToNull(dataCategoryFilter?.id ?? 0),
      subCategoryID: convertIntToNull(dataSubCategoriesFilter?.id ?? 0),
      nguoiDung: nguoiDung,
      pageSize: ApiConstants.DEFAULT_PAGE_SIZE,
      pageIndex: loadMorePage,
      bookmark: isDanhDauScreen == false ? null : true,
    );
    result.when(
      success: (response) {
        showContent();
        countDataSubject.sink.add(response.totalItem ?? 0);

        emit(
          CompletedLoadMore(
            CompleteType.SUCCESS,
            posts: response.item ?? [],
          ),
        );
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> getDetailOpenData(
    int id,
  ) async {
    showLoading();
    final result = await _openDataRepo.getDtailOpenData(
      id,
      nguoiDung,
    );
    result.when(
      success: (response) {
        showContent();
        detailOpenDataSubject.sink.add(response);
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> danhDauDuLieu({
    required int id,
    required String nguoiDanhDau,
  }) async {
    showLoading();
    final result = await _tienIchRepo.danhDauDuLieu(
      id,
      nguoiDanhDau,
    );
    result.when(
      success: (response) {
        showContent();
      },
      error: (error) {
        showContent();
      },
    );
  }

  Future<void> boDanhDauDuLieu({
    required int id,
    required String nguoiDanhDau,
  }) async {
    showLoading();
    final result = await _tienIchRepo.boDanhDauDuLieu(
      id,
      nguoiDanhDau,
    );
    result.when(
      success: (response) {
        showContent();
      },
      error: (error) {
        showContent();
      },
    );
  }

  Future<void> checkToPinData(int id) async {
    if (!(detailOpenDataSubject.value.body?.danhDau ?? true)) {
      await danhDauDuLieu(
        id: id,
        nguoiDanhDau: nguoiDung,
      ).then((value) => getDetailOpenData(id));
    } else {
      await boDanhDauDuLieu(
        id: id,
        nguoiDanhDau: nguoiDung,
      ).then((value) => getDetailOpenData(id));
    }
  }

  bool checkLoggedIn() {
    return PrefsService.getToken().isNotEmpty;
  }

  void onDispose() {
    subCategories = [];
    dataSubCategoriesFilter = SubCategoriesModel(0, '');
    dataCategoryFilter = CategoryModel(0, '', '', []);
    isDanhDauScreen = null;
    keySearch = '';
    countDataSubject.close();
  }

  void onReset() {
    dataSubCategoriesFilter = SubCategoriesModel(0, '');
    dataCategoryFilter = CategoryModel(0, '', '', []);
    subCategories = [];
  }

  void onInitState({
    required String keySearch,
    required bool isDanhDauScreen,
    SubCategoriesModel? dataSubCategoriesFilter,
    CategoryModel? dataCategoryFilter,
  }) {
    this.keySearch = keySearch;
    this.isDanhDauScreen = isDanhDauScreen;
    this.dataSubCategoriesFilter = dataSubCategoriesFilter;
    this.dataCategoryFilter = dataCategoryFilter;
  }

  ///bai viet du lieu
  Future<void> getListBaiVietDuLieu({
    int? id,
  }) async {
    dynamic convertIntToNull(int data) {
      if (data == 0) {
        return null;
      }
      return data;
    }

    if (!getInLoadMore) {
      showLoading();
    }
    final result = await _openDataRepo.getListBaiViet(
      keySearch: keySearch,
      categoryID: convertIntToNull(dataCategoryFilter?.id ?? 0),
      subCategoryID: convertIntToNull(dataSubCategoriesFilter?.id ?? 0),
      nguoiDung: nguoiDung,
      pageSize: ApiConstants.DEFAULT_PAGE_SIZE,
      pageIndex: loadMorePage,
      bookmark: isDanhDauScreen == false ? null : true,
    );
    result.when(
      success: (response) {
        showContent();
        countDataSubject.sink.add(response.totalItem ?? 0);

        if (loadMorePage == ApiConstants.PAGE_BEGIN) {
          if ((response.item ?? []).isEmpty) {
            showContent();
            emit(const CompletedLoadMore(CompleteType.SUCCESS, posts: []));
          } else {
            showContent();
            emit(
              CompletedLoadMore(
                CompleteType.SUCCESS,
                posts: response.item ?? [],
              ),
            );
          }
        } else {
          showContent();
          emit(
            CompletedLoadMore(
              CompleteType.SUCCESS,
              posts: response.item ?? [],
            ),
          );
        }
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> getDetailBaiViet(
    int id,
  ) async {
    showLoading();
    final result = await _openDataRepo.getDetailBaiViet(
      id,
      nguoiDung,
    );
    result.when(
      success: (response) {
        showContent();
        detailOpenDataSubject.sink.add(response);
      },
      error: (error) {
        showError();
      },
    );
  }

  bool checkNonFilter() =>
      (dataCategoryFilter?.name ?? '').isEmpty &&
      (dataSubCategoriesFilter?.name ?? '').isEmpty;

  Future<void> getListData({
    required bool isBaiVietDuLieuScreen,
    bool? isDanhDauScreen,
  }) {
    inSearching.sink.add((keySearch ?? '').isNotEmpty || !checkNonFilter());
    if (isBaiVietDuLieuScreen && (isDanhDauScreen ?? false)) {
      /// get list data bai viet du lieu voi danh giau
      getListBaiVietDuLieu();
    }
    if (isBaiVietDuLieuScreen) {
      return getListBaiVietDuLieu();
    }
    return getListOpenData();
  }

  Future<void> onSearching(String value) async {
    canMoveToTop = true;
    keySearch = value;
    loadMorePage = ApiConstants.PAGE_BEGIN;
  }

  Future<void> onFilter() async {
    canMoveToTop = true;
    loadMorePage = ApiConstants.PAGE_BEGIN;
  }

  Future<void> getDetailData({
    required int id,
    required bool isBaiVietDuLieu,
  }) async {
    isBaiVietDuLieu ? await getDetailBaiViet(id) : await getDetailOpenData(id);
    await createdDataListChartTable();
  }

  Future<void> createdDataListChartTable() async {
    final DetailOpenDataModel dataDetail =
        detailOpenDataSubject.valueOrNull ?? DetailOpenDataModel();
    final data = dataDetail.body?.getListMaps() ?? [];
    if (data.isNotEmpty) {
      dataListChartTable = dataDetail.body?.getListMaps() ?? [];
      dataChartTableListSubject.sink.add(dataListChartTable);
    }
  }

  void searchListChartTable(String keySearch) {
    final data = dataListChartTable;
    final keySearchFormat = keySearch.trim();
    if (keySearchFormat.isEmpty) {
      dataChartTableListSubject.sink.add(dataListChartTable);
      return;
    }
    final result = data.where((item) {
      final firthValueItem = item.values.first.toString().trim();
      print('------------------------------------------------------');
      print('keySearch:$keySearchFormat}');
      print('value:$firthValueItem');
      return firthValueItem.contains(keySearchFormat);
    }).toList();

    print('result:$result');
    dataChartTableListSubject.sink.add(result);
  }
}
