part of 'du_lieu_mo_cubit.dart';

@immutable
abstract class DuLieuMoState extends BaseState {}

class DuLieuMoInitial extends DuLieuMoState {
  @override
  List<Object?> get props => [];
}
