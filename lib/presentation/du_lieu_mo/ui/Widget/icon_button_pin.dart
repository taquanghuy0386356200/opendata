import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:flutter/cupertino.dart';

class IconButtonPin extends StatefulWidget {
  final bool? initData;
  final Function(bool) onChange;
  final bool? disible;

  const IconButtonPin({
    Key? key,
    this.initData,
    required this.onChange,
    this.disible,
  }) : super(key: key);

  @override
  State<IconButtonPin> createState() => _IconButtonPinState();
}

class _IconButtonPinState extends State<IconButtonPin> {
  bool isPined = false;

  @override
  void didUpdateWidget(covariant IconButtonPin oldWidget) {
    isPined = widget.initData ?? false;
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    Widget icon() {
      if (isPined) {
        return GestureDetector(
          onTap: () {
            onTap();
          },
          child: Image.asset(ImageAssets.icPinedOpenData),
        );
      }
      return GestureDetector(
        onTap: () {
          onTap();
        },
        child: Image.asset(ImageAssets.icUnPinOpenData),
      );
    }

    return SizedBox(
      height: 20,
      width: 16,
      child: icon(),
    );
  }

  void onTap() {
    if (!(widget.disible ?? false)) {
      setState(() {
        isPined = !isPined;
      });
    }
    widget.onChange(isPined);
  }
}
