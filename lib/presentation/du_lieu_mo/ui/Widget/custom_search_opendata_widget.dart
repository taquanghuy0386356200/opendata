import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class CustomSearchOpenData extends StatefulWidget {
  final String hintText;
  final Function(String) textChange;
  final TextEditingController controller;

  const CustomSearchOpenData({
    Key? key,
    required this.hintText,
    required this.textChange,
    required this.controller,
  }) : super(key: key);

  @override
  State<CustomSearchOpenData> createState() => _CustomSearchOpenDataState();
}

class _CustomSearchOpenDataState extends State<CustomSearchOpenData> {
  late String textSearch;
  late FocusNode myFocusNode;
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    myFocusNode = FocusNode();
    super.initState();
    textSearch = widget.controller.text;
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: Container(
        padding: const EdgeInsets.only(left: 16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8),
          border: Border.all(color: colorE2E8F0),
        ),
        child: Row(
          children: [
            SizedBox(
              width: 20,
              height: 20,
              child: GestureDetector(
                onTap: () {
                  myFocusNode.unfocus();
                  widget.textChange(textSearch);
                },
                child: Image.asset(ImageAssets.icSearch),
              ),
            ),
            const SizedBox(
              width: 12,
            ),
            Flexible(
              child: TextField(
                controller: _controller,
                focusNode: myFocusNode,
                onChanged: (text) {
                  widget.textChange(text);
                  textSearch = text;
                },
                onSubmitted: (value) {
                  widget.textChange(value);
                },
                textAlignVertical: TextAlignVertical.center,
                keyboardType: TextInputType.text,
                textInputAction: TextInputAction.search,
                style: XelaTextStyle.Xela18Medium.copyWith(color: color364564),
                decoration: InputDecoration(
                  border: InputBorder.none,
                  focusedBorder: InputBorder.none,
                  enabledBorder: InputBorder.none,
                  errorBorder: InputBorder.none,
                  disabledBorder: InputBorder.none,
                  hintText: widget.hintText,
                  hintStyle:
                      XelaTextStyle.Xela16Regular.copyWith(color: colorA2AEBD),
                  counterText: '',
                ),
                maxLength: 255,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
