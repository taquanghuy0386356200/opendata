import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/domain/models/open_data_model/open_data_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/widgets/text/ellipsis_character_text.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/cupertino.dart';

class CellListOpenData extends StatelessWidget {
  final OpenDataModel openDataMode;
  final Function() onTap;

  const CellListOpenData({
    Key? key,
    required this.openDataMode,
    required this.onTap,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        onTap();
      },
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          border: Border.all(color: colorE2E8F0),
        ),
        padding: const EdgeInsets.all(16),
        margin: const EdgeInsets.only(bottom: 20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 16),
              child: EllipsisDoubleLineText(
                (openDataMode.title ?? '').trim(),
                style: XelaTextStyle.Xela16Medium.copyWith(color: color364564),
              ),
            ),
            rowData(
              key: S.current.thoi_gian_cap_nhat,
              value: (openDataMode.updatedAt ?? 0)
                  .fomatFullDateFromMili(openDataMode.updatedAt ?? 0),
            ),
            rowData(
              key: S.current.linh_vuc,
              value: openDataMode.categoryName ?? '',
            ),
            rowData(
              key: S.current.chuyen_muc,
              value: openDataMode.subCategoryName ?? '',
            ),
            rowData(
              key: S.current.mo_ta,
              value: openDataMode.description ?? '',
            ),
            if (openDataMode.getNameFile().isNotEmpty)
              rowData(
                key: S.current.file_dinh_kem,
                value: openDataMode.getNameFile(),
                colorValue: color20C997,
              )
          ],
        ),
      ),
    );
  }

  Widget rowData({
    required String key,
    required String value,
    Color? colorValue,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 3,
            child: Text(
              key,
              style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
            ),
          ),
          Expanded(
            flex: 7,
            child: EllipsisDoubleLineText(
              value,
              style: XelaTextStyle.Xela14Regular.copyWith(
                color: colorValue ?? color364564,
              ),
            ),
          )
        ],
      ),
    );
  }
}
