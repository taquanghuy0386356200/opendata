import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/xela_buttom_custom.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/download_file.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/dialog/show_toast.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cached_pdfview/flutter_cached_pdfview.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PreviewFile extends StatefulWidget {
  final String title;
  final String filePath;
  final String fileDoubleMain;

  const PreviewFile({
    Key? key,
    required this.filePath,
    required this.title,
    required this.fileDoubleMain,
  }) : super(key: key);

  @override
  _PreviewFileState createState() => _PreviewFileState();
}

class _PreviewFileState extends State<PreviewFile> {
  int pagePdf = 1;
  int pageTotal = 0;
  final FToast toast = FToast();

  @override
  void initState() {
    toast.init(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: colorDDDDE6,
      appBar: BaseAppBar(
        title: widget.title,
        isShowBack: true,
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          Stack(
            children: [
              Container(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height * 0.75,
                ),
                padding: const EdgeInsets.all(16),
                child: viewPdfFromUrlCache(widget.filePath),
              ),
              Positioned(
                top: 20,
                right: 25,
                child: Container(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                  color: Colors.orangeAccent,
                  child: Row(
                    children: [
                      Text(
                        pagePdf.toString(),
                        style: XelaTextStyle.Xela14Medium.copyWith(
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        '/${pageTotal.toString()}',
                        style: XelaTextStyle.Xela14Medium.copyWith(
                          color: Colors.white,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          Container(
            color: Colors.white,
            padding:
                const EdgeInsets.only(left: 16, right: 16, bottom: 30, top: 16),
            child: XelaButtonCustom(
              text: S.current.tai_du_lieu,
              autoResize: false,
              onPressed: () {
                saveFile(
                  fileName: widget.title,
                  url: widget.fileDoubleMain,
                  contextConfig: context,
                );
              },
            ),
          )
        ],
      ),
    );
  }

  Widget viewPdfFromUrl(String url) {
    return const PDF().fromUrl(
      url,
      placeholder: (double progress) => Center(child: Text('$progress %')),
      errorWidget: (dynamic error) => const Center(
        child: Text(
          'file đính kèm bị lỗi',
        ),
      ),
    );
  }

  Widget viewPdfFromUrlCache(String url) {
    return PDF(
      onPageChanged: (page, total) {
        setState(() {
          pagePdf = (page ?? 0) + 1;
          pageTotal = total ?? 0;
        });
      },
    ).cachedFromUrl(
      url,
      placeholder: (double progress) => Center(child: Text('$progress %')),
      errorWidget: (dynamic error) => Center(
        child: Text(
          'file đính kèm bị lỗi',
          style: XelaTextStyle.Xela18Medium.copyWith(color: Colors.black),
        ),
      ),
    );
  }
}
