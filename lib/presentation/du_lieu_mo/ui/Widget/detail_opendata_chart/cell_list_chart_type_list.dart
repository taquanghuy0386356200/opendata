import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/cupertino.dart';

class CellChartTypeList extends StatefulWidget {
  final Map<String, dynamic> data;

  const CellChartTypeList({
    Key? key,
    required this.data,
  }) : super(key: key);

  @override
  State<CellChartTypeList> createState() => _CellChartTypeListState();
}

class _CellChartTypeListState extends State<CellChartTypeList> {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        border: Border.all(color: colorE2E8F0),
      ),
      padding: const EdgeInsets.all(16),
      margin: const EdgeInsets.only(bottom: 20, left: 16, right: 16),
      child: Column(
        children: List.generate(
          widget.data.length,
          (index) {
            final data = widget.data;
            return rowData(
              key: data.keys.elementAt(index),
              value: data.values.elementAt(index),
            );
          },
        ),
      ),
    );
  }

  Widget rowData({
    required String key,
    required String value,
    Color? colorValue,
  }) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 8),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 4,
            child: Text(
              key,
              style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
            ),
          ),
          Expanded(
            flex: 6,
            child: Text(
              value,
              style: XelaTextStyle.Xela14Regular.copyWith(
                color: colorValue ?? color364564,
              ),
            ),
          )
        ],
      ),
    );
  }
}
