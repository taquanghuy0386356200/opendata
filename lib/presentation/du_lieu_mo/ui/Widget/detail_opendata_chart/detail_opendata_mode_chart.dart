import 'package:bnv_opendata/domain/models/open_data_model/chart_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/bloc/du_lieu_mo_cubit.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/type_chart/chart_horizontal_column.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/type_chart/column_chart_widget.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/type_chart/detail_type_file_view.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/type_chart/line_chart.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/type_chart/list_chart.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/type_chart/pie_chart_widget.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/type_chart/stacked_bar_chart.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/type_chart/statistical_reports_widget.dart';
import 'package:flutter/cupertino.dart';

enum DetailOpenDataModeChart {
  FILE,
  PIE,
  COLUMN,
  LINE,
  LIST,
  HORIZONTAL_COLUMN,
  STASKED_BAR_CHART,
  STATISTICAL_REPORTS,
}

extension TypeVBDen on DetailOpenDataModeChart {
  Widget getWidget({required List<ChartDataModel> dataChart}) {
    switch (this) {
      case DetailOpenDataModeChart.FILE:
        return const DetailOpenDataTypeFileWidget(file: '');
      case DetailOpenDataModeChart.PIE:
        return PieChartWidget(
          listDataChart: dataChart,
        );
      case DetailOpenDataModeChart.COLUMN:
        return ColumnChartWidget(
          listDataChart: dataChart,
        );
      case DetailOpenDataModeChart.LINE:
        return LineChartWidget(
          chartData: dataChart,
          title: S.current.bao_cao_tinh_hinh_hop_tac_quoc_te,
        );
      case DetailOpenDataModeChart.LIST:
        final DuLieuMoCubit cubit = DuLieuMoCubit();
        return ListChartWidget(
          title: S.current.bao_cao_tinh_hinh_hop_tac_quoc_te, cubit: cubit,
        );
      case DetailOpenDataModeChart.HORIZONTAL_COLUMN:
        return ChartHorizontalColumn(
          chartData: dataChart,
          title: S.current.bao_cao_tinh_hinh_hop_tac_quoc_te,
        );
      case DetailOpenDataModeChart.STASKED_BAR_CHART:
        return StackedBarChart(
          chartData: dataChart,
          title: S.current.bao_cao_tinh_hinh_hop_tac_quoc_te,
        );
      case DetailOpenDataModeChart.STATISTICAL_REPORTS:
        return StatisticalReportsChart(
          chartData: dataChart,
          title: S.current.bao_cao_tinh_hinh_hop_tac_quoc_te,
        );
    }
  }
}
