import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/domain/models/open_data_model/chart_model.dart';
import 'package:bnv_opendata/domain/models/xela_chart_models.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_chart.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class PieChartWidget extends StatelessWidget {
  final List<ChartDataModel> listDataChart;

  const PieChartWidget({Key? key, required this.listDataChart})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Color> colorsThis = [
      colorE26F2C,
      color667793,
      colorPrimary,
      colorPrimaryTransparent,
    ];
    const double Size = 250;

    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 20),
          height: Size,
          child: Stack(
            alignment: Alignment.center,
            children: [
              XelaChart(
                height: Size,
                type: XelaChartType.PIE,
                pieBackgroundColor: XelaColor.Gray12,
                datasetPieChart: XelaPieChartDataset(
                  label: listDataChart.first.label ?? '',
                  data: listDataChart.map((e) => e.number ?? 0).toList(),
                  fillColors: colorsThis,
                ),
              ),
              Container(
                height: Size / 2.5,
                decoration: const BoxDecoration(
                  shape: BoxShape.circle,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
        GridView.count(
          childAspectRatio: 4,
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          primary: true,
          padding: const EdgeInsets.all(20),
          crossAxisCount: 2,
          children: List.generate(
            listDataChart.length,
            (index) => describeCellPie(
              chartModel: listDataChart[index],
              color: colorsThis[index],
            ),
          ),
        ),
        Text(
          S.current.bao_cao_tinh_hinh_hop_tac_quoc_te,
          style: XelaTextStyle.Xela16Medium.copyWith(color: color364564),
        )
      ],
    );
  }

  Widget describeCellPie({
    required ChartDataModel chartModel,
    required Color color,
  }) {
    return Row(
      children: [
        Container(
          height: 14,
          width: 14,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: color,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            '${chartModel.label} (${chartModel.number}%)',
            style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
          ),
        )
      ],
    );
  }
}
