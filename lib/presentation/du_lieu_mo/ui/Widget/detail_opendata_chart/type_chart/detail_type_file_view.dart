import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/view_file_web_view.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/widgets/button/double_buttom.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class DetailOpenDataTypeFileWidget extends StatelessWidget {
  final String file;

  const DetailOpenDataTypeFileWidget({Key? key, required this.file})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        bottom: 44,
        top: MediaQuery.of(context).size.height * 0.45,
      ),
      child: DoubleButtonBottom(
        titleLeft: S.current.xem_truoc,
        titleRight: S.current.tai_du_lieu,
        onClickLeft: () {
          // goTo(context, const ViewFileWebView());
          _launchInBrowser('http://www.africau.edu/images/default/sample.pdf');
        },
        onClickRight: () {},
      ),
    );
  }

  Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(url, forceSafariVC: false, forceWebView: false);
    } else {
      throw 'Could not launch $url';
    }
  }

  Future<void> _launchUrl(String urlFile) async {
    final Uri url = Uri.parse(urlFile);
    if (!await launchUrl(url)) {
      throw 'Could not launch $url';
    }
  }
}
