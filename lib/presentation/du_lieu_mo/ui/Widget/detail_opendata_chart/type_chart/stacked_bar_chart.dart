import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/domain/models/open_data_model/chart_model.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class StackedBarChart extends StatelessWidget {
  final String title;
  final List<ChartDataModel> chartData;

  const StackedBarChart({
    Key? key,
    required this.title,
    required this.chartData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Color> colorsThis = [
      colorE26F2C,
      color667793,
      colorPrimary,
      colorPrimaryTransparent,
      colorE2AEBD,
    ];

    List<ChartSeries<ChartDataModel, String>> listWidget() {
      final List<ChartSeries<ChartDataModel, String>> data = [];
      for (int i = 0; i < chartData.length; i++) {
        data.add(
          StackedBarSeries<ChartDataModel, String>(
            color: colorsThis[i],
            dataLabelSettings: DataLabelSettings(
              isVisible: true,
              textStyle:
                  XelaTextStyle.Xela12Regular.copyWith(color: colorF9FAFF),
              labelAlignment: ChartDataLabelAlignment.outer,
              labelPosition: ChartDataLabelPosition.outside,
            ),
            dataSource: chartData,
            xValueMapper: (ChartDataModel data, _) => data.label,
            yValueMapper: (ChartDataModel data, _) => data.number,
          ),
        );
      }

      return data;
    }

    return Column(
      children: [
        spaceH24,
        SizedBox(
          height: 70.0 * chartData.length,
          child: SfCartesianChart(
            isTransposed: true,
            primaryXAxis: CategoryAxis(
              placeLabelsNearAxisLine: true,
              labelStyle:
                  XelaTextStyle.Xela12Regular.copyWith(color: colorA2AEBD),
              maximumLabelWidth: 60,
              majorGridLines: const MajorGridLines(width: 0),
            ),
            primaryYAxis: CategoryAxis(
              labelStyle: XelaTextStyle.Xela14Regular,
              placeLabelsNearAxisLine: true,
              axisLine: const AxisLine(
                color: color364564,
                width: 0.41,
              ),
              interval: 5,
              minimum: 0,
              majorGridLines: const MajorGridLines(
                width: 0.34,
                color: colorA2AEBD,
                dashArray: [5, 5],
              ),
            ),
            series: listWidget(),
          ),
        ),
        GridView.count(
          childAspectRatio: 6,
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          primary: true,
          padding: const EdgeInsets.all(20),
          crossAxisCount: 2,
          children: List.generate(
            chartData.length,
            (index) => describeCellPie(
              chartModel: chartData[index],
              color: colorsThis[index],
            ),
          ),
        ),
        spaceH20,
        Text(
          title,
          style: XelaTextStyle.Xela16Medium.copyWith(color: color364564),
        ),
        spaceH38
      ],
    );
  }

  Widget describeCellPie({
    required ChartDataModel chartModel,
    required Color color,
  }) {
    return Row(
      children: [
        Container(
          height: 14,
          width: 14,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: color,
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            '${chartModel.label} (${chartModel.number})',
            style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
          ),
        )
      ],
    );
  }
}
