import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/domain/models/open_data_model/chart_model.dart';
import 'package:bnv_opendata/domain/models/xela_chart_models.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_chart.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ColumnChartWidget extends StatelessWidget {
  final List<ChartDataModel> listDataChart;

  const ColumnChartWidget({Key? key, required this.listDataChart})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    // final List<Color> colorsThis = [
    //   colorE26F2C,
    //   color667793,
    //   colorPrimary,
    //   colorPrimaryTransparent,
    //   colorE2AEBD,
    // ];

    double heightChart() {
      double numberMax = 0;
      for (int i = 0; i < listDataChart.length; i++) {
        if (i < (listDataChart[i].number ?? 0)) {
          numberMax = listDataChart[i].number ?? 0;
        }
      }

      if (numberMax <= 60) {
        return 200;
      }

      return 400;
    }

    List<String> listLabel() {
      final List<String> data = [];
      for (int i = 0; i < listDataChart.length; i++) {
        data.add('');
      }
      return data;
    }

    return Column(
      children: [
        spaceH25,
        XelaChart(
          height: heightChart(),
          dataStep: listDataChart.length * 1.0,
          type: XelaChartType.BAR,
          datasetsBarChart: [
            XelaBarChartDataset(
              label: 'acvb',
              data: listDataChart.map((e) => (e.number ?? 0) * 0.1).toList(),
            ),
          ],
          labels: listLabel(),
        ),
        Text(
          S.current.bao_cao_tinh_hinh_hop_tac_quoc_te,
          style: XelaTextStyle.Xela16Medium.copyWith(color: color364564),
        )
      ],
    );
  }
}
