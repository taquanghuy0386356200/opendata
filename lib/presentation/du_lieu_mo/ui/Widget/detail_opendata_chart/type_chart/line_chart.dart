import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/domain/models/open_data_model/chart_model.dart';
import 'package:bnv_opendata/domain/models/xela_chart_models.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_chart.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class LineChartWidget extends StatelessWidget {
  final String title;
  final List<ChartDataModel> chartData;

  const LineChartWidget({
    Key? key,
    required this.chartData,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final List<Color> colorsThis = [
      colorE26F2C,
      color667793,
      colorPrimary,
      colorPrimaryTransparent,
      colorE2AEBD
    ];

    List<String> listDataMonth() {
      final List<String> data = [];
      for (int i = 0; i <= 12; i++) {
        if (i < 10) {
          data.add('T0$i');
        } else {
          data.add('T$i');
        }
      }
      return data;
    }

    List<XelaLineChartDataset> listDataLine() {
      final List<XelaLineChartDataset> data = [];

      for (int i = 0; i < chartData.length; i++) {
        data.add(
          XelaLineChartDataset(
            label: 'First',
            data: chartData[i].listdata ?? [],
            lineColor: colorsThis[i],
            pointColor: colorsThis[i],
            tension: 0.5,
          ),
        );
      }
      return data;
    }

    double heightChart() {
      double numberMax = 0;
      for (int i = 0; i < chartData.length; i++) {
        if (i < (chartData[i].number ?? 0)) {
          numberMax = chartData[i].number ?? 0;
        }
      }

      if (numberMax <= 60) {
        return 200;
      }

      return 400;
    }

    return Column(
      children: [
        GridView.count(
          childAspectRatio: 6,
          physics: const NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          primary: true,
          padding: const EdgeInsets.all(20),
          crossAxisCount: 2,
          children: List.generate(
            chartData.length,
            (index) => describeCellLine(
              chartModel: chartData[index],
              color: colorsThis[index],
            ),
          ),
        ),
        SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: SizedBox(
            width: 1000,
            child: XelaChart(
              height: heightChart(),
              type: XelaChartType.LINE,
              labels: listDataMonth(),
              datasetsLineChart: listDataLine(),
            ),
          ),
        ),
        spaceH25,
        Text(
          title,
          style: XelaTextStyle.Xela16Medium.copyWith(color: color364564),
        )
      ],
    );
  }

  Widget describeCellLine({
    required ChartDataModel chartModel,
    required Color color,
  }) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Stack(
          alignment: Alignment.center,
          children: [
            Container(
              height: 2,
              width: 30,
              decoration: BoxDecoration(
                color: color,
                borderRadius: const BorderRadius.all(
                  Radius.circular(2),
                ),
                border: Border.all(color: color),
              ),
            ),
            Container(
              height: 14,
              width: 14,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: color.withOpacity(0.5),
              ),
            ),
            Container(
              height: 7,
              width: 7,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: color,
              ),
            )
          ],
        ),
        Padding(
          padding: const EdgeInsets.only(left: 12),
          child: Text(
            '${chartModel.label}',
            style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
          ),
        )
      ],
    );
  }
}
