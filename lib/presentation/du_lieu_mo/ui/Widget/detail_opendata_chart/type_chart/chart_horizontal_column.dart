import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/domain/models/open_data_model/chart_model.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class ChartHorizontalColumn extends StatelessWidget {
  final String title;
  final List<ChartDataModel> chartData;

  const ChartHorizontalColumn({
    Key? key,
    required this.title,
    required this.chartData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        spaceH38,
        SizedBox(
          height: 70.0 * chartData.length,
          child: SfCartesianChart(
            primaryXAxis: CategoryAxis(
              placeLabelsNearAxisLine: true,
              labelStyle:
                  XelaTextStyle.Xela12Regular.copyWith(color: colorA2AEBD),
              maximumLabelWidth: 60,
              majorGridLines: const MajorGridLines(width: 0),
            ),
            primaryYAxis: CategoryAxis(
              labelStyle: XelaTextStyle.Xela14Regular,
              placeLabelsNearAxisLine: true,
              axisLine: const AxisLine(
                color: color364564,
                width: 0.41,
              ),
              interval: 5,
              minimum: 0,
              majorGridLines: const MajorGridLines(
                width: 0.34,
                color: colorA2AEBD,
                dashArray: [5, 5],
              ),
            ),
            series: <ChartSeries<ChartDataModel, String>>[
              BarSeries<ChartDataModel, String>(
                color: color20C997,
                dataLabelSettings: DataLabelSettings(
                  isVisible: true,
                  textStyle:
                      XelaTextStyle.Xela12Regular.copyWith(color: color364564),
                  labelAlignment: ChartDataLabelAlignment.outer,
                  labelPosition: ChartDataLabelPosition.outside,
                ),
                dataSource: chartData,
                xValueMapper: (ChartDataModel data, _) => data.label,
                yValueMapper: (ChartDataModel data, _) => data.number,
              ),
            ],
          ),
        ),
        spaceH24,
        Text(
          title,
          style: XelaTextStyle.Xela16Medium.copyWith(color: color364564),
        ),
      ],
    );
  }
}
