import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/domain/models/open_data_model/chart_model.dart';
import 'package:bnv_opendata/domain/models/open_data_model/detail_open_data_model.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/bloc/du_lieu_mo_cubit.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/cell_list_chart_type_list.dart';
import 'package:bnv_opendata/widgets/appbar/app_bar_search.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class ListChartWidget extends StatefulWidget {
  final String? title;
  final DuLieuMoCubit cubit;

  const ListChartWidget({
    Key? key,
    this.title,
    required this.cubit,
  }) : super(key: key);

  @override
  State<ListChartWidget> createState() => _ListChartWidgetState();
}

class _ListChartWidgetState extends State<ListChartWidget> {
  TextEditingController controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<Map<String, dynamic>>>(
      stream: widget.cubit.dataChartTableListSubject,
      builder: (context, snapshot) {
        final data = snapshot.data ?? [];
        if (snapshot.hasData) {
          return Column(
            children: [
              spaceH24,
              AppBarSearch(
                isTypeOther: true,
                title: widget.title ?? 'Biểu đồ',
                controller: controller,
                onchange: (value) {
                  widget.cubit.searchListChartTable(value);
                },
              ),
              ListView.builder(
                primary: true,
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: data.length,
                itemBuilder: (context, index) {
                  return CellChartTypeList(
                    data: data[index],
                  );
                },
              ),
            ],
          );
        }
        return const SizedBox.shrink();
      },
    );
  }
}
