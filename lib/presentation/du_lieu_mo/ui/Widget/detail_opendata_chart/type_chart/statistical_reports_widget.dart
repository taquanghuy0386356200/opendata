import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/domain/models/open_data_model/chart_model.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class StatisticalReportsChart extends StatelessWidget {
  final String title;
  final List<ChartDataModel> chartData;

  const StatisticalReportsChart({
    Key? key,
    required this.title,
    required this.chartData,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        spaceH24,
        Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 54,
            vertical: 16,
          ),
          decoration: BoxDecoration(
            color: colorBgHome,
            borderRadius: BorderRadius.circular(8),
            border: Border.all(color: colorE2E8F0),
          ),
          child: Column(
            children: [
              const Text(
                'Doanh Nghiệp',
                style: XelaTextStyle.Xela14Medium,
              ),
              spaceH8,
              Text(
                '3.215',
                style: XelaTextStyle.Xela28Bold.copyWith(color: colorE26F2C),
              ),
              spaceH8,
              Text(
                'năm trước: 2.704 (+8.66%)',
                style: XelaTextStyle.Xela12Regular.copyWith(color: color667793),
              ),
            ],
          ),
        ),
        spaceH20,
        Text(
          title,
          style: XelaTextStyle.Xela16Medium.copyWith(color: color364564),
        ),
        spaceH38
      ],
    );
  }
}
