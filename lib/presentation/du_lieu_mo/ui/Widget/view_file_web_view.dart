import 'dart:async';
import 'dart:io';

import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/domain/locals/logger.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/xela_buttom_custom.dart';
import 'package:bnv_opendata/presentation/thong_bao/bloc/thong_bao_cubit.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/get_ext.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/views/state_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class ViewFileWebView extends StatefulWidget {
  const ViewFileWebView({
    Key? key,
  }) : super(key: key);

  @override
  State<ViewFileWebView> createState() => _ViewFileWebViewState();
}

class _ViewFileWebViewState extends State<ViewFileWebView> {
  ThongBaoCubit cubit = ThongBaoCubit();
  String url = 'http://www.africau.edu/images/default/sample.pdf';

  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  StateLayout _stateLayout = StateLayout.showLoading;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: 'file',
        isShowBack: true,
      ),
      body: StateFullLayout(
        stateLayout: _stateLayout,
        retry: () {
          reload();
        },
        error: AppException('', S.current.something_went_wrong),
        textEmpty: '',
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            WebView(
              zoomEnabled: false,
              initialUrl: url,
              javascriptMode: JavascriptMode.unrestricted,
              onWebViewCreated: (WebViewController webViewController) {
                _controller.complete(webViewController);
              },
              onProgress: (int progress) {
                logger.d('WebView is loading (progress : $progress%)');
              },
              javascriptChannels: const <JavascriptChannel>{},
              navigationDelegate: (NavigationRequest request) {
                return NavigationDecision.navigate;
              },
              onPageStarted: (String url) {
                showLoading();
              },
              onPageFinished: (String url) {
                hideLoading();
              },
              onWebResourceError: (error) {
                showError();
              },
              gestureNavigationEnabled: true,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16, bottom: 40),
              child: XelaButtonCustom(
                text: S.current.tai_du_lieu,
                autoResize: false,
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }

  void showLoading() {
    if (_stateLayout != StateLayout.showLoading) {
      _stateLayout = StateLayout.showLoading;
    }
    setState(() {});
  }

  void hideLoading() {
    if (_stateLayout == StateLayout.showLoading) {
      _stateLayout = StateLayout.showContent;
    }
    setState(() {});
  }

  void showError() {
    _stateLayout = StateLayout.showError;
    setState(() {});
  }

  Future<void> reload() async {
    final WebViewController controller = await _controller.future;
    await controller.loadUrl(url);
  }

  Future<void> backToPreScreen() async => finish();
}
