import 'dart:io';

import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/domain/models/open_data_model/detail_open_data_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/authentication/login/bloc/derect_screen_extension.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/bloc/du_lieu_mo_cubit.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/type_chart/list_chart.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/icon_button_pin.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/view_detail_file.dart';
import 'package:bnv_opendata/presentation/webview/webview_from_html.dart';
import 'package:bnv_opendata/utils/app_utils.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/download_file.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/utils/style_utils.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/button/only_button_widget.dart';
import 'package:bnv_opendata/widgets/dialog/dialog_popular.dart';
import 'package:bnv_opendata/widgets/html_edit/edit_html.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DetailOpendataScreen extends StatefulWidget {
  final int id;
  final bool isDanhDauScreen;
  final bool isBaiVietDuLieu;

  const DetailOpendataScreen({
    Key? key,
    required this.id,
    this.isDanhDauScreen = false,
    this.isBaiVietDuLieu = false,
  }) : super(key: key);

  @override
  State<DetailOpendataScreen> createState() => _DetailOpendataScreenState();
}

class _DetailOpendataScreenState extends State<DetailOpendataScreen> {
  final FToast toast = FToast();
  DuLieuMoCubit cubit = DuLieuMoCubit();
  bool onZoom = false;

  @override
  void initState() {
    cubit.checkLoggedIn();
    cubit.getDetailData(id: widget.id, isBaiVietDuLieu: widget.isBaiVietDuLieu);
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    toast.init(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        hideKeyboard(context);
      },
      child: Scaffold(
        appBar: BaseAppBar(
          refreshData: widget.isDanhDauScreen,
          title: titleDetail(),
          isShowBack: true,
          actions: [
            StreamBuilder<DetailOpenDataModel>(
              stream: cubit.detailOpenDataSubject.stream,
              builder: (context, snapshot) {
                final bool data = snapshot.data?.body?.danhDau ?? false;
                return Padding(
                  padding: const EdgeInsets.only(right: 20),
                  child: IconButtonPin(
                    initData: data,
                    disible: !cubit.checkLoggedIn(),
                    onChange: (value) {
                      if (cubit.checkLoggedIn()) {
                        return cubit.checkToPinData(widget.id);
                      }
                      showThongBaoLogin();
                    },
                  ),
                );
              },
            )
          ],
        ),
        body: RefreshIndicator(
          onRefresh: () async {
            await cubit.getDetailData(
              id: widget.id,
              isBaiVietDuLieu: widget.isBaiVietDuLieu,
            );
          },
          child: StateStreamLayout(
            textEmpty: S.current.error,
            retry: () {
              cubit.getDetailOpenData(widget.id);
            },
            error: AppException(
              S.current.error,
              S.current.error,
            ),
            stream: cubit.stateStream,
            child: StreamBuilder<DetailOpenDataModel>(
              stream: cubit.detailOpenDataSubject,
              builder: (context, snapshot) {
                final data = snapshot.data?.body ?? BodyDetailOpenDataModel();

                ///main UI: chi co hai loai chart table list va other chart

                if ((data.link ?? '').isEmpty) {
                  return Column(
                    children: [
                      /// chart content
                      Expanded(child: mainBody(data)),

                      /// double button
                      dobuleButton(data)
                    ],
                  );
                }
                return Column(
                  children: [
                    /// web view
                    webView(data: data),

                    /// double button
                    dobuleButton(data)
                  ],
                );
              },
            ),
          ),
        ),
      ),
    );
  }

  Widget mainBody(BodyDetailOpenDataModel data) {
    return SingleChildScrollView(
      physics: const AlwaysScrollableScrollPhysics(),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          spaceH20,
          rowData(
            key: S.current.tieu_de,
            value: data.title ?? '',
          ),
          rowData(
            key: S.current.thoi_gian_cap_nhat,
            value: (data.updatedAt ?? 0) != 0
                ? (data.updatedAt ?? 0)
                    .fomatFullDateFromMili(data.updatedAt ?? 0)
                : '',
          ),
          rowData(
            key: S.current.linh_vuc,
            value: data.categoryName ?? '',
          ),
          rowData(
            key: S.current.chuyen_muc,
            value: data.subcategoryName ?? '',
          ),
          rowData(
            key: S.current.mo_ta,
            value: data.description?.trim() ?? '',
          ),
          rowData(
            key: S.current.file_dinh_kem,
            value: data.getNameFile(),
            colorValue: color20C997,
          ),

          /// chart type list
          if (data.getListMaps().isNotEmpty)
            ListChartWidget(
              cubit: cubit,
            )
        ],
      ),
    );
  }

  Widget dobuleButton(
    BodyDetailOpenDataModel data,
  ) {
    final String filePath =
        '${appConstants.baseUrl}/umbraco/${data.fileDinhKem}';
    final String fileDoumain = '/umbraco/${data.fileDinhKem}';
    if ((data.fileDinhKem ?? '').isNotEmpty) {
      return Padding(
        padding: EdgeInsets.only(
          left: 16,
          right: 16,
          top: 16,
          bottom: MediaQuery.of(context).size.height * 0.04,
        ),
        child: Row(
          children: [
            if (data.getTypeFile() == 'pdf') ...[
              Expanded(
                child: onlyButtonWidget(
                  onTap: () {
                    goTo(
                      context,
                      PreviewFile(
                        title: data.getNameFile(),
                        filePath: filePath,
                        fileDoubleMain: fileDoumain,
                      ),
                    );
                  },
                  title: S.current.xem_truoc,
                  colors: colorFEF2E2,
                  textColors: color364564,
                ),
              ),
              const SizedBox(width: 16.0),
            ],
            Expanded(
              child: onlyButtonWidget(
                onTap: () {
                  saveFile(
                    fileName: data.getNameFile(),
                    url: fileDoumain,
                    contextConfig: context,
                  );
                },
                title: S.current.tai_du_lieu,
                colors: colorE26F2C,
                textColors: colorFFFFFF,
              ),
            ),
          ],
        ),
      );
    }
    return const SizedBox.shrink();
  }

  Widget rowData({
    required String key,
    required String value,
    Color? colorValue,
  }) {
    if (value.isEmpty) {
      return const SizedBox.shrink();
    }
    return Padding(
      padding: const EdgeInsets.only(bottom: 10, left: 16, right: 16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 3,
            child: Padding(
              padding: const EdgeInsets.only(right: 12),
              child: Text(
                key,
                style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
              ),
            ),
          ),
          Expanded(
            flex: 7,
            child: GestureDetector(
              onTap: () {},
              child: Text(
                value,
                style: XelaTextStyle.Xela14Regular.copyWith(
                  color: colorValue ?? color364564,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget textWarning() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 20, bottom: 14),
          child: Text(
            S.current.thong_bao,
            style: XelaTextStyle.Xela18Medium.copyWith(color: color364564),
          ),
        ),
        Text(
          S.current.vui_long_dang_nhap_de_su_dung_chuc_nang_nay,
          style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
          textAlign: TextAlign.center,
        )
      ],
    );
  }

  void showThongBaoLogin() {
    showDialogPopular(
      context,
      isDoubleBtn: true,
      imageContent: ImageAssets.icWarningOpenData,
      contentDialog: textWarning(),
      titleDefaultBtn: S.current.dang_nhap,
      titleLeftBtn: S.current.dong,
      leftPress: () {
        Navigator.of(context).pop();
      },
      defaultPress: () {
        gotoLoginAndDirectScreen(
          context,
          nameDirect: NameScreenDirect.detail_open_data,
          id: widget.id.toString(),
        );
      },
    );
  }

  String titleDetail() {
    /// bai viet danh dau
    if (widget.isDanhDauScreen && widget.isBaiVietDuLieu) {
      return 'Xem chi tiết dữ liệu đánh dấu';
    }

    /// du lieu mo danh dau
    if (widget.isDanhDauScreen && !widget.isBaiVietDuLieu) {
      return 'Chi tiết dữ liệu đánh dấu';
    }

    /// bai viet du lieu
    if (widget.isBaiVietDuLieu) {
      return S.current.xem_bai_viet;
    }

    /// du lieu mơ
    return S.current.du_lieu_mo;
  }

  Widget webView({required BodyDetailOpenDataModel data}) {
    return Expanded(
      child: WebViewWithHtml(
        htmlData: viewHtmlDetailOpendata(data: data),
      ),
    );
  }
}
