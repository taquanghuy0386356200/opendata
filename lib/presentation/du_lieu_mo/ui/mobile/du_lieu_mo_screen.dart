import 'package:bnv_opendata/config/resources/color.dart';

import 'package:bnv_opendata/domain/model/category_model.dart';

import 'package:bnv_opendata/domain/models/open_data_model/open_data_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/bloc/du_lieu_mo_cubit.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/cell_list_opendata.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/xela_buttom_custom.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/mobile/detail_opendata_screen.dart';

import 'package:bnv_opendata/utils/constants/image_asset.dart';

import 'package:bnv_opendata/utils/style_utils.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/bottomShet/show_bottom_shet_custom.dart';
import 'package:bnv_opendata/widgets/category_widget/category_bottomshet_widget.dart';

import 'package:bnv_opendata/widgets/listview/loadmore_list.dart';

import 'package:bnv_opendata/widgets/search_popular/search_popular.dart';

import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class DuLieuMoScreen extends StatefulWidget {
  final SubCategoriesModel? dataSubCategoriesFilter;
  final CategoryModel? dataCategoryFilter;
  final bool showBack;
  final String? keySearch;
  final bool isDanhDauScreen;
  final bool isBaiVietDuLieu;

  const DuLieuMoScreen({
    Key? key,
    this.keySearch,
    this.showBack = false,
    this.isDanhDauScreen = false,
    this.dataSubCategoriesFilter,
    this.dataCategoryFilter,
    this.isBaiVietDuLieu = false,
  }) : super(key: key);

  @override
  State<DuLieuMoScreen> createState() => _DuLieuMoScreenState();
}

class _DuLieuMoScreenState extends State<DuLieuMoScreen>
    with AutomaticKeepAliveClientMixin<DuLieuMoScreen> {
  @override
  bool get wantKeepAlive => true;
  DuLieuMoCubit cubit = DuLieuMoCubit();
  TextEditingController controller = TextEditingController();

  @override
  void initState() {
    super.initState();
    controller.text = widget.keySearch ?? '';
    cubit.onInitState(
      keySearch: widget.keySearch ?? '',
      isDanhDauScreen: widget.isDanhDauScreen,
      dataSubCategoriesFilter: widget.dataSubCategoriesFilter,
      dataCategoryFilter: widget.dataCategoryFilter,
    );
  }

  @override
  void dispose() {
    super.dispose();
    cubit.onDispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: getTitle(),
        isShowBack: widget.showBack,
      ),
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: WidgetSearchPopular(
              filterCallBack: () {
                showFilter(
                  cubit: cubit,
                );
              },
              searchController: controller,
              onChange: (value) {
                cubit.onSearching(value).then((value) => getListData());
              },
              onClear: () {},
            ),
          ),
          spaceH25,
          StreamBuilder<int>(
            stream: cubit.countDataSubject.stream,
            builder: (context, snapshot) {
              return textFilter(cubit);
            },
          ),
          Expanded(
            child: ListViewLoadMoreWidget(
              cubit: cubit,
              isListView: true,
              callApi: (page) => getListData(),
              onReload: () {
                cubit.onReset();
              },
              viewItem: (value, index) {
                value as OpenDataModel;
                return CellListOpenData(
                  openDataMode: value,
                  onTap: () {
                    Navigator.of(context)
                        .push(
                      MaterialPageRoute(
                        builder: (context) => DetailOpendataScreen(
                          id: value.id ?? 0,
                          isBaiVietDuLieu: widget.isBaiVietDuLieu,
                          isDanhDauScreen: widget.isDanhDauScreen,
                        ),
                      ),
                    )
                        .then(
                          (value) {
                        value as bool;
                        if (value) {
                          getListData();
                        }
                      },
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget textFilter(DuLieuMoCubit cubit) {
    final String category = cubit.dataCategoryFilter?.name ?? '';
    final String subCategories = cubit.dataSubCategoriesFilter?.name ?? '';
    if (cubit.checkNonFilter()) {
      return const SizedBox.shrink();
    }
    return Padding(
      padding: const EdgeInsets.only(left: 16, right: 16, bottom: 16),
      child: Row(
        children: [
          Text(
            '${S.current.bo_loc}: ',
            style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
          ),
          Text(
            '$category${subCategories.isEmpty ? '' : ' / '}',
            style: XelaTextStyle.Xela14Medium.copyWith(color: color667793),
          ),
          Text(
            subCategories,
            style: XelaTextStyle.Xela14Medium.copyWith(color: color364564),
          ),
          Text(
            ' (${cubit.countDataSubject.value})',
            style: XelaTextStyle.Xela14Medium.copyWith(color: color364564),
          ),
        ],
      ),
    );
  }

  void showFilter({
    CategoryModel? initCategory,
    SubCategoriesModel? initSubCategories,
    required DuLieuMoCubit cubit,
  }) {
    showBottomSheetCustom(
      context,
      title: S.current.loc,
      child: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: CategoryBottomShetWidget(
              initCategory: cubit.dataCategoryFilter,
              initSubCategories: cubit.dataSubCategoriesFilter,
              onchangeCategoryData: (value) {
                cubit.dataCategoryFilter = value;
              },
              onchangeSubCategoriesData: (value) {
                cubit.dataSubCategoriesFilter = value;
              },
            ),
          ),
          const SizedBox(height: 20),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: XelaButtonCustom(
              autoResize: false,
              text: S.current.search,
              leftIcon: SizedBox(
                height: 20,
                width: 20,
                child: Image.asset(
                  ImageAssets.icSearchOpenData,
                  color: Colors.white,
                ),
              ),
              onPressed: () {
                cubit.onFilter().then((value) => getListData());
                Navigator.pop(context);
              },
            ),
          ),
          const SizedBox(height: 40),
        ],
      ),
    );
  }

  String getTitle() {
    /// bai viet danh dau
    if (widget.isDanhDauScreen && widget.isBaiVietDuLieu) {
      return S.current.bai_viet_danh_dau;
    }

    /// du lieu mo danh dau
    if (widget.isDanhDauScreen && !widget.isBaiVietDuLieu) {
      return S.current.du_lieu_danh_dau;
    }

    /// bai viet du lieu
    if (widget.isBaiVietDuLieu) {
      return S.current.bai_viet_du_lieu;
    }

    /// du lieu mơ
    return S.current.du_lieu_mo;
  }

  void getListData() =>
      cubit.getListData(isBaiVietDuLieuScreen: widget.isBaiVietDuLieu);
}