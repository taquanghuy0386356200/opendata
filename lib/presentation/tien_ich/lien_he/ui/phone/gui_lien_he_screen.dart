import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/xela_buttom_custom.dart';
import 'package:bnv_opendata/presentation/tien_ich/lien_he/bloc/lien_he_cubit.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/string_ext.dart';
import 'package:bnv_opendata/utils/style_utils.dart';
import 'package:bnv_opendata/widgets/dialog/dialog_popular.dart';
import 'package:bnv_opendata/widgets/follow_keyboard/follow_keyboard.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class GuiLienHeScreen extends StatefulWidget {
  final LienHeCubit cubit;

  const GuiLienHeScreen({
    Key? key,
    required this.cubit,
  }) : super(key: key);

  @override
  State<GuiLienHeScreen> createState() => _GuiLienHeScreenState();
}

class _GuiLienHeScreenState extends State<GuiLienHeScreen> {
  final _keyForm = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController mailController = TextEditingController();
  TextEditingController tieuDeController = TextEditingController();
  TextEditingController noiDungController = TextEditingController();
  late FocusNode myFocusNode;

  @override
  void initState() {
    myFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    widget.cubit.onDispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16),
      constraints: BoxConstraints(
        maxHeight: (MediaQuery.of(context).size.height - 50) * (2 / 3),
      ),
      child: Padding(
        padding: const EdgeInsets.only(bottom: 16),
        child: FollowKeyBoardWidget(
          bottomWidget: XelaButtonCustom(
            text: S.current.gui,
            leftIcon: SizedBox(
              height: 18,
              width: 18,
              child: Image.asset(ImageAssets.icSendLienHe),
            ),
            autoResize: false,
            onPressed: () {
              if (_keyForm.currentState?.validate() ?? false) {
                widget.cubit.guiLienHe().then(
                      (value) => {
                        if (value)
                          {
                            showPopup().then((value) => Navigator.pop(context)),
                          }
                        else
                          {Navigator.pop(context)}
                      },
                    );
              }
            },
          ),
          child: SingleChildScrollView(
            child: Form(
              key: _keyForm,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  spaceH20,
                  textInput(
                    title: 'Tên của bạn',
                    placeholder: 'Nhập họ và tên của bạn',
                    onChnage: (value) {
                      widget.cubit.tenLienHe = value;
                    },
                    controller: nameController,
                    validate: (value) {
                      return (value ?? '').checkTruongNull(' tên liên hệ!');
                    },
                  ),
                  textInput(
                    validate: (value) {
                      if ((value ?? '').isNotEmpty) {
                        return (value ?? '').trim().validateEmail();
                      } else {
                        return (value ?? '').checkTruongNull(' email!');
                      }
                    },
                    title: 'Email',
                    placeholder: 'Nhập địa chỉ email',
                    onChnage: (value) {
                      widget.cubit.email = value;
                    },
                    controller: mailController,
                  ),
                  textInput(
                    validate: (value) {
                      return (value ?? '').checkTruongNull(' tiêu đề!');
                    },
                    title: 'Tiêu đề',
                    placeholder: 'Nhập tiêu đề',
                    onChnage: (value) {
                      widget.cubit.tieuDe = value;
                    },
                    controller: tieuDeController,
                  ),
                  textInput(
                    minHeight: 115,
                    maxHeight: MediaQuery.of(context).size.height / 5,
                    validate: (value) {
                      return (value ?? '').checkTruongNull(' nội dung!');
                    },
                    title: 'Nội dung',
                    placeholder: 'Nội dung liên hệ',
                    maxLength: 500,
                    onChnage: (value) {
                      widget.cubit.noiDung = value;
                    },
                    controller: noiDungController,
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget textInput({
    required String title,
    required String placeholder,
    int? maxLength,
    required Function(String) onChnage,
    required TextEditingController controller,
    Function(String?)? validate,
    double? minHeight,
    double? maxHeight,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            Text(
              title,
              style: XelaTextStyle.Xela14Regular.copyWith(
                color: color586B8B,
              ),
            ),
            Text(
              ' *',
              style: XelaTextStyle.Xela14Regular.copyWith(color: Colors.red),
            ),
          ],
        ),
        spaceH8,
        XelaTextFieldCustom(
          minHeight: minHeight,
          maxHeight: maxHeight,
          validate: validate,
          maxLength: maxLength ?? 255,
          placeholder: placeholder,
          onChange: (value) {
            onChnage(value);
          },
        ),
        spaceH20
      ],
    );
  }

  Future<void> showPopup() => showDialogPopular(
        context,
        defaultPress: () {
          Navigator.pop(context);
        },
        imageContent: ImageAssets.icTickOrange,
        contentDialog: Column(
          children: [
            spaceH25,
            Text(
              'Gửi thành công',
              style: XelaTextStyle.Xela18Medium.copyWith(color: color364564),
            ),
            spaceH15,
            Text(
              'Thông tin liên hệ của bạn đã được gửi tới Bộ nội vụ thành công!',
              style: XelaTextStyle.Xela14Regular.copyWith(
                color: color667793,
              ),
              textAlign: TextAlign.center,
            ),
          ],
        ),
        titleDefaultBtn: S.current.dong,
      );
}
