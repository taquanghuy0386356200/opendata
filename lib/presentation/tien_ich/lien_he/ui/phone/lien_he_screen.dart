import 'dart:async';

import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/domain/models/tien_ich/lien_he_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/xela_buttom_custom.dart';
import 'package:bnv_opendata/presentation/tien_ich/lien_he/bloc/lien_he_cubit.dart';
import 'package:bnv_opendata/presentation/tien_ich/lien_he/ui/phone/gui_lien_he_screen.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/bottomShet/show_bottom_shet_custom.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class LienHeScreen extends StatefulWidget {
  const LienHeScreen({Key? key}) : super(key: key);

  @override
  State<LienHeScreen> createState() => _LienHeScreenState();
}

class _LienHeScreenState extends State<LienHeScreen> {
  LienHeCubit cubit = LienHeCubit();
  final Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    getValueLienHe();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: S.current.lien_he,
        isShowBack: true,
      ),
      body: StateStreamLayout(
        retry: () {
          getValueLienHe();
        },
        textEmpty: S.current.something_went_wrong,
        error: AppException(
          S.current.error,
          S.current.something_went_wrong,
        ),

        stream: cubit.stateStream,
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.4,
                width: MediaQuery.of(context).size.width,
                child: StreamBuilder<LienHeModel>(
                  stream: cubit.lienHeSubject.stream,
                  builder: (context, snapshot) {
                    if ((snapshot.data?.latitude ?? 0) != 0) {
                      final data = snapshot.data ?? LienHeModel();
                      return GoogleMap(
                        initialCameraPosition: CameraPosition(
                          target: LatLng(
                            data.latitude ?? 0,
                            data.longtitude ?? 0,
                          ),
                          zoom: 4.5,
                        ),
                        onMapCreated: (controler) {
                          setState(() {
                            cubit.markers.add(
                              Marker(
                                markerId: const MarkerId('id-1'),
                                position: LatLng(
                                  snapshot.data?.latitude ?? 0,
                                  snapshot.data?.longtitude ?? 0,
                                ),
                                infoWindow: InfoWindow(
                                  title: snapshot.data?.address ?? '',
                                ),
                              ),
                            );
                          });
                        },
                        markers: cubit.markers,
                      );
                    }

                    return const SizedBox();
                  },
                ),
              ),
              spaceH32,
              StreamBuilder<LienHeModel>(
                stream: cubit.lienHeSubject.stream,
                builder: (context, snapshot) {
                  final data = snapshot.data ?? LienHeModel();
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Text(
                          data.title ?? '',
                          style: XelaTextStyle.Xela24Bold.copyWith(
                            color: color364564,
                          ),
                        ),
                      ),
                      spaceH24,
                      rowDataWidget(text: 'Địa chỉ', value: data.address ?? ''),
                      rowDataWidget(
                        text: 'Số điện thoại',
                        value: data.phoneNumber ?? '',
                      ),
                      rowDataWidget(
                          text: 'Số fax', value: data.faxNumber ?? ''),
                      rowDataWidget(text: 'Email', value: data.email ?? ''),
                      spaceH40,
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: XelaButtonCustom(
                          text: S.current.lien_he,
                          autoResize: false,
                          onPressed: () {
                            showBottomSheetCustom(
                              context,
                              child: GuiLienHeScreen(
                                cubit: cubit,
                              ),
                              title: S.current.lien_he,
                            );
                          },
                        ),
                      )
                    ],
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget rowDataWidget({
    required String text,
    required String value,
  }) =>
      Padding(
        padding: const EdgeInsets.only(bottom: 10, left: 16, right: 16),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              flex: 3,
              child: Text(
                text,
                style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
              ),
            ),
            Expanded(
              flex: 7,
              child: Text(
                value,
                style: XelaTextStyle.Xela16Regular.copyWith(color: color364564),
                overflow: TextOverflow.ellipsis,
                maxLines: 3,
              ),
            )
          ],
        ),
      );

  void getValueLienHe() {
    cubit.getLienHe().then(
          (value) => {
            cubit.markers.add(
              Marker(
                markerId: const MarkerId('id-1'),
                position: LatLng(
                  cubit.lienHeData.latitude ?? 0,
                  cubit.lienHeData.latitude ?? 0,
                ),
                infoWindow: const InfoWindow(
                  title: 'Synodus',
                ),
              ),
            ),
            setState(() {}),
          },
        );
  }
}
