import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/data/request/gui_lien_he_request.dart';
import 'package:bnv_opendata/domain/models/tien_ich/lien_he_model.dart';
import 'package:bnv_opendata/domain/repository/tien_ich/tien_ich_respository.dart';
import 'package:get/get.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'lien_he_state.dart';

class LienHeCubit extends BaseCubit<LienHeState> {
  LienHeCubit() : super(LienHeInitial()) {
    showContent();
  }

  TienIchRepository get _tienIchRepo => Get.find();

  final BehaviorSubject<LienHeModel> lienHeSubject = BehaviorSubject();

  LienHeModel get lienHeData => lienHeSubject.valueOrNull ?? LienHeModel();

  /// gửi liên hệ
  String tenLienHe = '';
  String email = '';
  String tieuDe = '';
  String noiDung = '';

  final Set<Marker> markers = {};

  Future<void> getLienHe() async {
    showLoading();
    final result = await _tienIchRepo.getDetailLienHe();
    result.when(
      success: (response) {
        showContent();
        final LienHeModel data = response;
        lienHeSubject.sink.add(data);
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<bool> guiLienHe() async {
    bool success = false;
    showLoading();
    final result = await _tienIchRepo.guiLienHe(
      GuiLienHeRequest(
        name: tenLienHe,
        email: email,
        title: tieuDe,
        content: noiDung,
      ),
    );
    result.when(
      success: (response) {
        success = true;
        showContent();
      },
      error: (error) {
        success = false;
        showError();
      },
    );
    return success;
  }

  void onDispose() {
    tenLienHe = '';
    email = '';
    tieuDe = '';
    noiDung = '';
  }
}
