part of 'huong_dan_cubit.dart';

@immutable
abstract class HuongDanState extends BaseState{}

class HuongDanInitial extends HuongDanState {
  @override
  List<Object?> get props => [];
}
