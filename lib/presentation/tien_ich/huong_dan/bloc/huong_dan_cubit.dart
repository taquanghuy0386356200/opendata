import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/config/base/base_state.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/domain/repository/tien_ich/tien_ich_respository.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:bnv_opendata/utils/constants/app_constants.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/subjects.dart';

part 'huong_dan_state.dart';

class HuongDanCubit extends BaseCubit<BaseState> {
  HuongDanCubit() : super(HuongDanInitial()) {
    showContent();
  }

  String keySearch = '';

  TienIchRepository get _tienIchRepo => Get.find();

  BehaviorSubject<ItemThongBaoModel> huongDanSubject = BehaviorSubject();

  Future<void> getListHuongDan() async {
    if (!getInLoadMore) {
      showLoading();
    }
    inSearching.sink.add(keySearch.isNotEmpty);
    final result = await _tienIchRepo.getListHuongDan(
      keySearch: keySearch,
      pageSize: ApiConstants.DEFAULT_PAGE_SIZE,
      pageIndex: loadMorePage,
    );
    result.when(
      success: (response) {
        showContent();
        if (loadMorePage == ApiConstants.PAGE_BEGIN) {
          if ((response.item ?? []).isEmpty) {
            showContent();
            emit(const CompletedLoadMore(CompleteType.SUCCESS, posts: []));
          } else {
            showContent();
            emit(
              CompletedLoadMore(
                CompleteType.SUCCESS,
                posts: response.item ?? [],
              ),
            );
          }
        } else {
          showContent();
          emit(
            CompletedLoadMore(
              CompleteType.SUCCESS,
              posts: response.item ?? [],
            ),
          );
        }
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> getDetailHuongDan({
    required String id,
  }) async {
    showLoading();
    final result = await _tienIchRepo.getDetailHuongDan(
      id: id,
    );
    result.when(
      success: (response) {
        showContent();
        huongDanSubject.sink.add(response);
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> onSearching(String value) async {
    canMoveToTop = true;
    keySearch = value;
    loadMorePage = ApiConstants.PAGE_BEGIN;
  }
}
