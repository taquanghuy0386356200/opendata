import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/tien_ich/huong_dan/bloc/huong_dan_cubit.dart';
import 'package:bnv_opendata/presentation/tien_ich/huong_dan/ui/mobile/detail_huong_dan_screen.dart';
import 'package:bnv_opendata/presentation/tien_ich/huong_dan/ui/widget/cell_huong_dan.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/widgets/appbar/app_bar_search.dart';
import 'package:bnv_opendata/widgets/listview/loadmore_list.dart';
import 'package:flutter/material.dart';

class HuongDanScreen extends StatefulWidget {
  const HuongDanScreen({Key? key}) : super(key: key);

  @override
  State<HuongDanScreen> createState() => _HuongDanScreenState();
}

class _HuongDanScreenState extends State<HuongDanScreen> {
  HuongDanCubit cubit = HuongDanCubit();
  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AppBarSearch(
            title: S.current.huong_dan,
            isCanBack: true,
            onchange: (value) {
              cubit.onSearching(value).then((value) => cubit.getListHuongDan());
            }, controller: _controller,
          ),
          Expanded(
            child: ListViewLoadMoreWidget(
              cubit: cubit,
              isListView: true,
              callApi: (page) => cubit.getListHuongDan(),
              viewItem: (value, index) {
                value as ItemThongBaoModel;
                return CellHuongDan(
                  image: '${ApiConstants.baseUrl}${value.banner}',
                  title: value.title ?? '',
                  time: (value.releaseAt ?? 0)
                      .fomatFullDateFromMili(value.releaseAt ?? 0),
                  ontap: () {
                    goTo(
                      context,
                      DetailHuongDanScreen(
                        id: value.id ?? '',
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
