import 'dart:async';
import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/tien_ich/huong_dan/bloc/huong_dan_cubit.dart';
import 'package:bnv_opendata/presentation/webview/webview_from_html.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/html_edit/edit_html.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class DetailHuongDanScreen extends StatefulWidget {
  final String id;

  const DetailHuongDanScreen({
    Key? key,
    required this.id,
  }) : super(key: key);

  @override
  State<DetailHuongDanScreen> createState() => _DetailHuongDanScreenState();
}

class _DetailHuongDanScreenState extends State<DetailHuongDanScreen> {
  HuongDanCubit cubit = HuongDanCubit();

  @override
  void initState() {
    super.initState();
    cubit.getDetailHuongDan(id: widget.id);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: S.current.chi_tiet_huong_dan,
        isShowBack: true,
      ),
      body: StateStreamLayout(
        retry: () {
          cubit.getDetailHuongDan(id: widget.id);
        },
        error: AppException('', S.current.something_went_wrong),
        textEmpty: '',
        stream: cubit.stateStream,
        child: StreamBuilder<ItemThongBaoModel>(
          stream: cubit.huongDanSubject,
          builder: (context, snapshot) {
            final data = snapshot.data ?? ItemThongBaoModel();
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                spaceH16,
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        data.title ?? '',
                        style: XelaTextStyle.Xela18Bold.copyWith(
                          color: color364564,
                        ),
                      ),
                      spaceH8,
                      Row(
                        children: [
                          Container(
                            height: 20,
                            width: 20,
                            padding: const EdgeInsets.only(right: 5.5),
                            child: Image.asset(ImageAssets.icCalender12),
                          ),
                          Text(
                            (data.releaseAt ?? 0)
                                .fomatFullDateFromMili(data.releaseAt ?? 0),
                            style: XelaTextStyle.Xela14Regular.copyWith(
                              color: color667793,
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ),
                spaceH16,
                Container(
                  color: colorF9FAFF,
                  height: 8,
                ),
                Expanded(
                  child: WebViewWithHtml(
                    htmlData: data.content ?? '',
                  ),
                )
              ],
            );
          },
        ),
      ),
    );
  }
}
