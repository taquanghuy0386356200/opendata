import 'dart:io';

import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/data/request/them_sua_tai_lieu_request.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/domain/models/tien_ich/tai_lieu_dang_tai_model.dart';
import 'package:bnv_opendata/domain/repository/tien_ich/tien_ich_respository.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart';

part 'chi_tiet_tai_lieu_state.dart';

class ChiTietTaiLieuCubit extends BaseCubit<ChiTietTaiLieuState> {
  ChiTietTaiLieuCubit() : super(ChiTietTaiLieuInitial()) {
    showContent();
  }

  TienIchRepository get _repo => Get.find();

  CategoryModel categoryModelEdit = CategoryModel(0, '', '', []);
  SubCategoriesModel subCategoriesModel = SubCategoriesModel(0, '');

  final ThemSuaTaiLieuRequest request = ThemSuaTaiLieuRequest();

  Future<void> initApi({required int idChiTiet}) async {}

  void getDefaultModelChiTiet(TaiLieuDangTaiModel model) {
    categoryModelEdit.name = model.categoryName;
    subCategoriesModel.name = model.subcategoryName;
    categoryModelEdit.subCategories = [subCategoriesModel];
    request.subcategoryName = model.subcategoryName;
    request.categoryID = model.categoryID;
    request.categoryName = model.categoryName;
    request.subcategoryID = model.subcategoryID;
    request.subcategoryName = model.subcategoryName;
    request.title = model.title;
    request.description = model.description;
    request.fileDinhKem = model.fileDinhKem;
    request.status = model.status;
    request.id = model.id;
    request.nguoiDangTai = PrefsService.getNameUser();
    fileThemMoiTaiLieu = File(request.fileDinhKem ?? '');

    ///end
  }

  bool checkValidateThemMoiTaiLieu() {
    if ((request.categoryID ?? '') == ',' ||
        ((request.subcategoryID ?? '') == ',')) {
      return false;
    } else {
      return true;
    }
  }

  String getNameFile(String originalName) {
    final List<String> result = originalName.split('\\');
    return result.last;
  }

  void convertLinhVucToRequest(List<CategoryModel> value) {
    if (value.length == 1) {
      request.categoryName = '${value[0].name};';
      request.categoryID = '${value[0].id},';
    } else {
      final listCategoryId =
          value.map((e) => e.id.toString()).toList().join(',');
      request.categoryID = '$listCategoryId,';
      final listNameCategory = value.map((e) => e.name).join(';');
      request.categoryName = '$listNameCategory;';
    }
  }

  void convertChuyenMucToRequest(List<SubCategoriesModel> value) {
    if (value.length == 1) {
      request.subcategoryName = '${value[0].name};';
      request.subcategoryID = '${value[0].id},';
    } else {
      final listCategoryId =
          '${value.map((e) => e.id.toString()).toList().join(',')},';
      request.subcategoryID = listCategoryId;
      final listNameCategory = '${value.map((e) => e.name).join(';')};';
      request.subcategoryName = listNameCategory;
    }
  }

  final List<File> filePost = [];
  File fileThemMoiTaiLieu = File('');
  bool validateFile = true;

  void checkFileInit() {
    fileThemMoiTaiLieu = File(request.fileDinhKem ?? '');
    errorFileAddChiTietTailieu.add('');
    validateFile = true;
  }

  void checkValidateHaveFile() {
    if (fileThemMoiTaiLieu.path.isEmpty) {
      errorFileAddChiTietTailieu.add(S.current.vui_long_tai_file);
      validateFile = false;
    } else {
      errorFileAddChiTietTailieu.add('');
      validateFile = true;
    }
  }

  BehaviorSubject<bool> donePickFile = BehaviorSubject();

  Future<void> postFile(File file) async {
    donePickFile.sink.add(true);
    filePost.clear();
    filePost.add(file);
    final result = await _repo.postFile(filePost);
    result.when(
      success: (response) {
        showContent();
        request.fileDinhKem = response.body;
        donePickFile.sink.add(false);
      },
      error: (error) {
        showContent();
        donePickFile.sink.add(false);
      },
    );
  }

  static const String StatusReviewingFile = 'REVIEWING';
  static const String StatusDraftFile = 'DRAFT';

  Future<void> suaTaiLieu({
    bool isDraft = false,
  }) async {
    request.status = isDraft ? StatusDraftFile : StatusReviewingFile;
    request.nguoiDangTai = PrefsService.getNameUser();
    showLoading();
    final result = await _repo.themSuaTaiLieu(request);
    result.when(
      success: (response) {
        showContent();
      },
      error: (error) {
        errorFileAddChiTietTailieu.sink.add('Lỗi tải tài liệu, thử lại');
        showError();
      },
    );
  }

  BehaviorSubject<String> errorFileAddChiTietTailieu =
      BehaviorSubject.seeded('');

  Future<void> xoaTaiLieu({required int idTaiLieu}) async {
    showLoading();
    final result = await _repo.xoaTaiLieu(idTaiLieu);
    result.when(
      success: (response) {
        showContent();
      },
      error: (error) {
        showError();
      },
    );
  }

  Widget getStatusTaiLieuDangTai(String value) {
    switch (value) {
      case 'DRAFT':
        return Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 23,
            vertical: 3,
          ),
          decoration: BoxDecoration(
            color: colorE2E8F0,
            borderRadius: BorderRadius.circular(30),
          ),
          child: Text(
            S.current.nhap,
            style: XelaTextStyle.Xela12w700.copyWith(
              color: colorBgHome,
            ),
          ),
        );
      case 'REVIEWING':
        return Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 23,
            vertical: 3,
          ),
          decoration: BoxDecoration(
            color: colorFF9F43,
            borderRadius: BorderRadius.circular(30),
          ),
          child: Text(
            S.current.cho_duyet,
            style: XelaTextStyle.Xela12w700.copyWith(
              color: colorBgHome,
            ),
          ),
        );
      case 'APPROVED':
        return Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 23,
            vertical: 3,
          ),
          decoration: BoxDecoration(
            color: color20C997,
            borderRadius: BorderRadius.circular(30),
          ),
          child: Text(
            S.current.da_duyet,
            style: XelaTextStyle.Xela12w700.copyWith(
              color: colorBgHome,
            ),
          ),
        );
      default:
        return Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 23,
            vertical: 3,
          ),
          decoration: BoxDecoration(
            color: colorFF4F50,
            borderRadius: BorderRadius.circular(30),
          ),
          child: Text(
            S.current.da_tra_lai,
            style: XelaTextStyle.Xela12w700.copyWith(
              color: colorBgHome,
            ),
          ),
        );
    }
  }
}
