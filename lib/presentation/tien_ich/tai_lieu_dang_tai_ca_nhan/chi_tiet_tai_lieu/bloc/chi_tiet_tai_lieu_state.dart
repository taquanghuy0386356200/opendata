part of 'chi_tiet_tai_lieu_cubit.dart';

abstract class ChiTietTaiLieuState extends Equatable {
  const ChiTietTaiLieuState();
}

class ChiTietTaiLieuInitial extends ChiTietTaiLieuState {
  @override
  List<Object> get props => [];
}
