import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/domain/models/tien_ich/tai_lieu_dang_tai_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/chi_tiet_tai_lieu/bloc/chi_tiet_tai_lieu_cubit.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/chi_tiet_tai_lieu/ui/chinh_sua_tai_lieu.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/components/row_widget_detail.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/utils/string_ext.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/bottomShet/show_bottom_shet_custom.dart';
import 'package:bnv_opendata/widgets/dialog/dialog_popular.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

enum TypesScreenChiTietTaiLieu {
  DRAFT,
  POPULAR,
}

class ChiTietTaiLieuScreen extends StatefulWidget {
  const ChiTietTaiLieuScreen({
    Key? key,
    required this.model,
  }) : super(key: key);
  final TaiLieuDangTaiModel model;

  @override
  State<ChiTietTaiLieuScreen> createState() => _ChiTietTaiLieuScreenState();
}

class _ChiTietTaiLieuScreenState extends State<ChiTietTaiLieuScreen> {
  late ChiTietTaiLieuCubit cubit;

  @override
  void initState() {
    super.initState();
    cubit = ChiTietTaiLieuCubit();
    cubit.getDefaultModelChiTiet(widget.model);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: widget.model.title,
        isShowBack: true,
        actions: widget.model.status == 'REVIEWING'
            ? [
                GestureDetector(
                  onTap: () {
                    showBottomSheetCustom(
                      context,
                      child: ChinhSuaTaiLieuBTS(
                        model: widget.model,
                        cubit: cubit,
                      ),
                      title: S.current.chinh_sua_tai_lieu,
                    );
                  },
                  child: SizedBox(
                    height: 24,
                    width: 24,
                    child: Image.asset(ImageAssets.icEdit),
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
                GestureDetector(
                  onTap: () {
                    showDialogPopular(
                      context,
                      contentDialog: Column(
                        children: [
                          spaceH20,
                          Text(
                            S.current.xoa_tai_lieu_ca_nhan,
                            style: XelaTextStyle.Xela18Medium.copyWith(
                              color: color364564,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          spaceH14,
                          Text(
                            S.current.ban_co_chac_chan_xoa_tai_lieu_nay_khong,
                            style: XelaTextStyle.Xela14Regular.copyWith(
                              color: color667793,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ],
                      ),
                      leftPress: () {
                        Navigator.pop(context);
                      },
                      isDoubleBtn: true,
                      titleLeftBtn: S.current.dong,
                      titleDefaultBtn: S.current.dong_y,
                      defaultPress: () {
                        cubit
                            .xoaTaiLieu(idTaiLieu: widget.model.id)
                            .then((value) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                        });
                      },
                      imageContent: ImageAssets.icXoaDialog,
                    );
                  },
                  child: SizedBox(
                    width: 24,
                    height: 24,
                    child: Image.asset(ImageAssets.icDeleteDoc),
                  ),
                ),
                const SizedBox(
                  width: 16,
                ),
              ]
            : null,
      ),
      body: StateStreamLayout(
        stream: cubit.stateStream,
        error: AppException('', S.current.something_went_wrong),
        retry: () {},
        textEmpty: '',
        child: Stack(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 27),
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  rowWidgetDetail(
                    S.current.linh_vuc,
                    widget.model.categoryName,
                  ),
                  spaceH10,
                  rowWidgetDetail(
                    S.current.chuyen_muc,
                    widget.model.subcategoryName,
                  ),
                  spaceH10,
                  rowWidgetDetail(
                    S.current.mo_ta_tai_lieu,
                    widget.model.description,
                  ),
                  spaceH10,
                  rowWidgetDetail(
                    S.current.file_du_lieu,
                    cubit.getNameFile(widget.model.fileDinhKem),
                    colorContent: color20C997,
                  ),
                  spaceH10,
                  rowWidgetDetail(
                    S.current.dung_luong,
                    widget.model.fileSize.formatBytes(),
                  ),
                  spaceH10,
                  widgetRowTrangThai(
                    widget.model.status,
                  ),
                  if (widget.model.status == 'REJECTED')
                    Column(
                      children: [
                        spaceH10,
                        rowWidgetDetail(
                          S.current.thoi_gian_gui_duyet,
                          '${widget.model.thoiGianGuiDuyet.getDateTimeFromMil(
                            milisecondsTime: widget.model.thoiGianGuiDuyet,
                          )}  ${widget.model.thoiGianGuiDuyet.getTimeFromMil(
                            milisecondsTime: widget.model.thoiGianGuiDuyet,
                          )}',
                        ),
                        spaceH10,
                        rowWidgetDetail(
                          S.current.thoi_gian_tra_lai,
                          '${widget.model.thoiGianDuyet.getDateTimeFromMil(
                            milisecondsTime: widget.model.thoiGianDuyet,
                          )}  ${widget.model.thoiGianDuyet.getTimeFromMil(
                            milisecondsTime: widget.model.thoiGianDuyet,
                          )}',
                        ),
                        spaceH10,
                        rowWidgetDetail(
                          S.current.ly_do_tra_lai,
                          widget.model.lyDoTraLai,
                        ),
                      ],
                    )
                  else if (widget.model.status == 'APPROVED')
                    Column(
                      children: [
                        spaceH10,
                        rowWidgetDetail(
                          S.current.thoi_gian_gui_duyet,
                          '${widget.model.thoiGianGuiDuyet.getDateTimeFromMil(
                            milisecondsTime: widget.model.thoiGianGuiDuyet,
                          )}  ${widget.model.thoiGianGuiDuyet.getTimeFromMil(
                            milisecondsTime: widget.model.thoiGianGuiDuyet,
                          )}',
                        ),
                        spaceH10,
                        rowWidgetDetail(
                          S.current.thoi_gian_duyet,
                          '${widget.model.thoiGianDuyet.getDateTimeFromMil(
                            milisecondsTime: widget.model.thoiGianDuyet,
                          )}  ${widget.model.thoiGianDuyet.getTimeFromMil(
                            milisecondsTime: widget.model.thoiGianDuyet,
                          )}',
                        ),
                      ],
                    )
                  else if (widget.model.status == 'REVIEWING')
                    Column(
                      children: [
                        spaceH10,
                        rowWidgetDetail(
                          S.current.thoi_gian_gui_duyet,
                          '${widget.model.thoiGianGuiDuyet.getDateTimeFromMil(
                            milisecondsTime: widget.model.thoiGianGuiDuyet,
                          )}  ${widget.model.thoiGianGuiDuyet.getTimeFromMil(
                            milisecondsTime: widget.model.thoiGianGuiDuyet,
                          )}',
                        ),
                      ],
                    )
                  else
                    const SizedBox.shrink()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget widgetRowTrangThai(String status) {
    return Row(
      children: [
        Text(
          S.current.trang_thai,
          style: XelaTextStyle.Xela14Regular.copyWith(
            color: color667793,
          ),
        ),
        spaceW40,
        cubit.getStatusTaiLieuDangTai(status)
      ],
    );
  }
}
