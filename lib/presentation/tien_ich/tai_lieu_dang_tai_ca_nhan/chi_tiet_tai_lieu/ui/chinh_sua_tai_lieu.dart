import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/domain/models/tien_ich/tai_lieu_dang_tai_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/chi_tiet_tai_lieu/bloc/chi_tiet_tai_lieu_cubit.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/string_ext.dart';
import 'package:bnv_opendata/widgets/button/button_select_file/button_select_file.dart';
import 'package:bnv_opendata/widgets/dropdown/multi_choice/multichoi_tai_lieu_dang_tai.dart';
import 'package:bnv_opendata/widgets/follow_keyboard/follow_keyboard.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class ChinhSuaTaiLieuBTS extends StatefulWidget {
  const ChinhSuaTaiLieuBTS({
    Key? key,
    required this.model,
    required this.cubit,
  }) : super(key: key);
  final TaiLieuDangTaiModel model;
  final ChiTietTaiLieuCubit cubit;

  @override
  State<ChinhSuaTaiLieuBTS> createState() => _ChinhSuaTaiLieuBTSState();
}

class _ChinhSuaTaiLieuBTSState extends State<ChinhSuaTaiLieuBTS> {
  final _controllerTieuDe = TextEditingController();
  final _controllerMoTaTaiLieu = TextEditingController();
  final _key = GlobalKey<FormState>();
  final _keyMultiForm = GlobalKey<MultiChoiceTaiLieuDangTaiState>();

  @override
  void initState() {
    super.initState();
    _controllerTieuDe.text = widget.model.title;
    _controllerMoTaTaiLieu.text = widget.model.description;
    widget.cubit.checkFileInit();
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomLeft,
      child: FollowKeyBoardWidget(
        child: Form(
          key: _key,
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                spaceH20,
                RichText(
                  textScaleFactor: MediaQuery.of(context).textScaleFactor,
                  text: TextSpan(
                    text: S.current.tieu_de,
                    style: XelaTextStyle.Xela14Regular.copyWith(
                      color: color586B8B,
                    ),
                    children: [
                      TextSpan(
                        text: ' *',
                        style: XelaTextStyle.Xela16Regular.copyWith(
                          color: Colors.red,
                        ),
                      )
                    ],
                  ),
                ),
                spaceH8,
                XelaTextFieldCustom(
                  textEditingController: _controllerTieuDe,
                  placeholder: S.current.vui_long_nhap_tieu_de,
                  validate: (value) {
                    if ((value ?? '').isEmpty) {
                      return S.current.vui_long_nhap_tieu_de;
                    } else {
                      return null;
                    }
                  },
                  onChange: (value) {
                    widget.cubit.request.title = value;
                  },
                ),
                spaceH20,
                MultiChoiceTaiLieuDangTai(
                  key: _keyMultiForm,
                  categoryIDSuaTaiLieu: widget.model.categoryID
                      .substring(0, widget.model.categoryID.length - 1),
                  subCategoryIDSuaTaiLieu: widget.model.subcategoryID
                      .substring(0, widget.model.subcategoryID.length - 1),
                  getCategoriesSelected: (categories) {
                    widget.cubit.convertLinhVucToRequest(categories);
                  },
                  getSubCategories: (subCategories) {
                    widget.cubit.convertChuyenMucToRequest(subCategories);
                  },
                ),
                spaceH20,
                RichText(
                  textScaleFactor: MediaQuery.of(context).textScaleFactor,
                  text: TextSpan(
                    text: S.current.mo_ta_tai_lieu,
                    style: XelaTextStyle.Xela14Regular.copyWith(
                      color: color586B8B,
                    ),
                    children: [
                      TextSpan(
                        text: ' *',
                        style: XelaTextStyle.Xela16Regular.copyWith(
                          color: Colors.red,
                        ),
                      )
                    ],
                  ),
                ),
                spaceH8,
                XelaTextFieldCustom(
                  placeholder: S.current.nhap_mo_ta_tai_lieu,
                  textEditingController: _controllerMoTaTaiLieu,
                  isCanMultiLine: true,
                  keyboardType: TextInputType.multiline,
                  maxLength: 500,
                  maxLine: 5,
                  onChange: (value) {
                    widget.cubit.request.description = value;
                  },
                  validate: (value) {
                    if ((value ?? '').trim().isEmpty) {
                      return S.current.vui_long_nhap_mo_ta_tai_lieu;
                    }
                    return null;
                  },
                ),
                spaceH20,
                ButtonSelectFile(
                  onChange: (file) async {
                    widget.cubit.errorFileAddChiTietTailieu.sink.add('');
                    widget.cubit.fileThemMoiTaiLieu = file;
                    if (file.path.isNotEmpty) {
                      await widget.cubit.postFile(
                        widget.cubit.fileThemMoiTaiLieu,
                      );
                    }
                    widget.cubit.checkValidateHaveFile();
                  },
                  initFileFromApi:
                      widget.cubit.getNameFile(widget.model.fileDinhKem),
                  initFileSizeApi: widget.model.fileSize.formatBytes(),
                ),
                _errorTextFile(),
                spaceH20,
                XelaButton(
                  onPressed: () {
                    _keyMultiForm.currentState?.cubit.checkAllValidate();
                    widget.cubit.checkValidateHaveFile();
                    if ((_key.currentState?.validate() ?? false) &&
                        widget.cubit.validateFile &&
                        widget.cubit.checkValidateThemMoiTaiLieu()) {
                      widget.cubit.suaTaiLieu().then((value) {
                        Navigator.pop(context);
                        Navigator.pop(context);
                      });
                    } else {}
                  },
                  text: S.current.luu,
                  leftIcon: SizedBox(
                    height: 24,
                    width: 24,
                    child: Image.asset(
                      ImageAssets.icSaveAndSend,
                      color: colorBgHome,
                    ),
                  ),
                  background: colorE26F2C,
                  textStyle: XelaTextStyle.Xela16Medium.copyWith(
                    color: Colors.white,
                  ),
                  autoResize: false,
                ),
                spaceH40,
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _errorTextFile() {
    return StreamBuilder<String>(
      stream: widget.cubit.errorFileAddChiTietTailieu.stream,
      builder: (context, snapshot) {
        return ((snapshot.data ?? '').isNotEmpty)
            ? Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                child: Text(
                  snapshot.data ?? '',
                  style: XelaTextStyle.Xela14Regular.copyWith(
                    color: Colors.red,
                  ),
                ),
              )
            : const SizedBox.shrink();
      },
    );
  }

  Widget dinhKemFile() => Container(
        padding: const EdgeInsets.symmetric(
          vertical: 4,
          horizontal: 10,
        ),
        decoration: BoxDecoration(
          color: colorFEF2E2,
          borderRadius: BorderRadius.circular(4),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 24,
              width: 24,
              child: Image.asset(ImageAssets.icDinhFile),
            ),
            spaceW6,
            Text(
              S.current.tai_lieu_dinh_kem,
              style: XelaTextStyle.Xela14Medium.copyWith(
                color: colorE26F2C,
              ),
            )
          ],
        ),
      );
}
