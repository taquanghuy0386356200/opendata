import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

Widget widgetRowTrangThai({required String status}) {
  return Row(
    children: [
      Text(
        S.current.trang_thai,
        style: XelaTextStyle.Xela14Regular.copyWith(
          color: color667793,
        ),
      ),
      spaceW40,
      Container(
        padding: const EdgeInsets.symmetric(
          horizontal: 23,
          vertical: 3,
        ),
        decoration: BoxDecoration(
          color: colorFF9F43,
          borderRadius: BorderRadius.circular(30),
        ),
        child: Text(
          S.current.cho_duyet,
          style: XelaTextStyle.Xela12w700.copyWith(
            color: colorBgHome,
          ),
        ),
      ),
    ],
  );
}
