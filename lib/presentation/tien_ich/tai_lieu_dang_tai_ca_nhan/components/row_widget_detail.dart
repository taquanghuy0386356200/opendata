import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

Widget rowWidgetDetail(
  String title,
  String content, {
  Color colorContent = color667793,
}) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    crossAxisAlignment: CrossAxisAlignment.start,
    children: [
      Expanded(
        flex: 3,
        child: Text(
          title,
          style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
        ),
      ),
      spaceW14,
      Expanded(
        flex: 8,
        child: Text(
          content,
          style: XelaTextStyle.Xela14Regular.copyWith(color: colorContent),
        ),
      )
    ],
  );
}
