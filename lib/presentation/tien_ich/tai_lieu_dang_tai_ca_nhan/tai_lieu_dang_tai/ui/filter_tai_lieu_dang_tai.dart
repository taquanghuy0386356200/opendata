import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/tai_lieu_dang_tai/bloc/tai_lieu_dang_tai_cubit.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/widgets/category_widget/category_bottomshet_widget.dart';
import 'package:bnv_opendata/widgets/date_picker/custom_date_picker.dart';
import 'package:bnv_opendata/widgets/dropdown/cool_dropdown.dart';
import 'package:bnv_opendata/widgets/follow_keyboard/follow_keyboard.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class FilterTaiLieuDangTaiCaNhan extends StatefulWidget {
  const FilterTaiLieuDangTaiCaNhan({
    Key? key,
    required this.cubit,
  }) : super(key: key);
  final TaiLieuDangTaiCubit cubit;

  @override
  State<FilterTaiLieuDangTaiCaNhan> createState() =>
      _FilterTaiLieuDangTaiCaNhanState();
}

class _FilterTaiLieuDangTaiCaNhanState
    extends State<FilterTaiLieuDangTaiCaNhan> {
  final TextEditingController _controllerTuNgay = TextEditingController();
  final TextEditingController _controllerDenNgay = TextEditingController();

  final _key = GlobalKey<FormState>();
  bool isSelectedDate = false;
  bool isFirstChooseDate = true;

  @override
  void initState() {
    // _controllerTuNgay.text = DateTime.now().toStringDDMMYYYY;
    // _controllerDenNgay.text = DateTime.now().toStringDDMMYYYY;
    // widget.cubit.startDateFilter = DateTime(
    //   DateTime.now().year,
    //   DateTime.now().month,
    //   DateTime.now().day,
    //   0,
    // );
    // widget.cubit.endDateFilter = DateTime(
    //   DateTime.now().year,
    //   DateTime.now().month,
    //   DateTime.now().day,
    //   24,
    // );
    super.initState();
  }

  @override
  void dispose() {
    widget.cubit.resetParamApi(
    );
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        constraints: BoxConstraints(
          maxHeight: MediaQuery.of(context).size.height * 0.8,
        ),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: FollowKeyBoardWidget(
          child: SingleChildScrollView(
            child: Form(
              key: _key,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  spaceH20,
                  Text(
                    S.current.loai_file,
                    style: XelaTextStyle.Xela14Regular.copyWith(
                      color: color586B8B,
                    ),
                  ),
                  spaceH8,
                  CoolDropDown(
                    initData: widget.cubit.fileTypeParam,
                    useCustomHintColors: true,
                    placeHoder: S.current.tat_ca,
                    listData: widget.cubit.listFileType,
                    onChange: (index) {
                      widget.cubit.fileTypeParam =
                          '.${widget.cubit.listFileType[index]}';
                    },
                  ),
                  CategoryBottomShetWidget(
                    onchangeCategoryData: (value) {
                      widget.cubit.categoryID = '${value.id},';
                    },
                    onchangeSubCategoriesData: (value) {
                      if (value.id == 0) {
                        widget.cubit.subCategoryID = ',';
                      } else {
                        widget.cubit.subCategoryID = '${value.id},';
                      }
                    },
                  ),
                  spaceH20,
                  Text(
                    S.current.trang_thai,
                    style: XelaTextStyle.Xela14Regular.copyWith(
                      color: color586B8B,
                    ),
                  ),
                  spaceH8,
                  CoolDropDown(
                    initData: widget.cubit.valueDropDownTrangThai,
                    useCustomHintColors: true,
                    placeHoder: S.current.tat_ca,
                    listData:
                        widget.cubit.listStatusTaiLieuDangTai.values.toList(),
                    onChange: (vl) {
                      widget.cubit.statusParam = widget
                          .cubit.listStatusTaiLieuDangTai.keys
                          .toList()[vl];
                      widget.cubit.valueDropDownTrangThai = widget
                          .cubit.listStatusTaiLieuDangTai.values
                          .toList()[vl];
                    },
                  ),
                  spaceH20,
                  Text(
                    S.current.gui_duyet_tu_ngay,
                    style: XelaTextStyle.Xela14Regular.copyWith(
                      color: color586B8B,
                    ),
                  ),
                  spaceH8,
                  StreamBuilder<DateTime>(
                      stream: widget.cubit.tuNgayBHVSJ.stream,
                      builder: (context, snapshot) {
                        final dataTuNgay = snapshot.data ?? DateTime.now();
                        return StreamBuilder<DateTime>(
                            stream: widget.cubit.denNgayBHVSJ.stream,
                            builder: (context, snapshot) {
                              final dataDenNgay =
                                  snapshot.data ?? DateTime.now();
                              return SelectDatePicker(
                                hintText: 'DD/MM/YYYY',
                                controller: _controllerTuNgay,
                                validatorDateOnChange: (value) {
                                  return widget.cubit.validatorStartDate(
                                    currentValue: value,
                                    endDate: _controllerDenNgay.text,
                                  );
                                },
                                minDateCanSelect: DateTime(1970),
                                maxDateCanSelect: dataDenNgay,
                                initDate: dataTuNgay,
                                callBackSubmitted: (DateTime dateTime) {
                                  widget.cubit.startDateFilter = dateTime;
                                  _controllerTuNgay.text =
                                      dateTime.toStringDDMMYYYY;
                                  _key.currentState?.validate();
                                  widget.cubit.changeDateStartDateFeatEndDate(
                                    value: dateTime,
                                  );
                                },
                                callBackSelected: (DateTime dateTime) {
                                  // widget.cubit.startDateFilter = dateTime;
                                },
                                onChangeText: (String value) {
                                  widget.cubit.startDateFilter =
                                      value.convertStringToDate();
                                },
                              );
                            });
                      }),
                  spaceH20,
                  Text(
                    S.current.den_ngay,
                    style: XelaTextStyle.Xela14Regular.copyWith(
                      color: color586B8B,
                    ),
                  ),
                  spaceH8,
                  StreamBuilder<DateTime>(
                      stream: widget.cubit.denNgayBHVSJ.stream,
                      builder: (context, snapshot) {
                        final dataDenNgay = snapshot.data ?? DateTime.now();
                        return StreamBuilder<DateTime>(
                            stream: widget.cubit.tuNgayBHVSJ.stream,
                            builder: (context, snapshot) {
                              final dataTuNgay =
                                  snapshot.data ?? DateTime.now();
                              return SelectDatePicker(
                                hintText: 'DD/MM/YYYY',
                                controller: _controllerDenNgay,
                                validatorDateOnChange: (value) {
                                  return widget.cubit.validatorEndDate(
                                    currentValue: value,
                                    startDate: _controllerTuNgay.text,
                                  );
                                },
                                minDateCanSelect: dataTuNgay,
                                maxDateCanSelect: DateTime.now(),
                                initDate: dataDenNgay,
                                callBackSubmitted: (DateTime dateTime) {
                                  _controllerDenNgay.text =
                                      dateTime.toStringDDMMYYYY;
                                  isSelectedDate = true;
                                  _key.currentState?.validate();
                                  widget.cubit.changeDateStartDateFeatEndDate(
                                    value: dateTime,
                                    isStartDate: false,
                                  );
                                  widget.cubit.endDateFilter =
                                      dateTime.add(const Duration(hours: 24));
                                },
                                callBackSelected: (DateTime dateTime) {
                                  // widget.cubit.endDateFilter = dateTime;
                                },
                                onChangeText: (String value) {
                                  widget.cubit.endDateFilter =
                                      value.convertStringToDate();
                                },
                              );
                            });
                      }),
                  //custom lịch
                  spaceH20,
                  XelaButton(
                    onPressed: () {
                      if (widget.cubit.validateTime) {
                        Navigator.pop(context);
                        widget.cubit.getDanhSachTaiLieuDang(
                          isRefresh: true,
                          isRefreshDate: false,
                          isResetNormalParam: false,
                          keySearch: '_',
                        );
                      } else {}
                    },
                    text: S.current.tim_kiem,
                    leftIcon: SizedBox(
                      height: 24,
                      width: 24,
                      child: Image.asset(
                        ImageAssets.icSearch,
                        color: colorBgHome,
                      ),
                    ),
                    background: colorE26F2C,
                    textStyle: XelaTextStyle.Xela16Medium.copyWith(
                      color: Colors.white,
                    ),
                    autoResize: false,
                  ),
                  spaceH40,
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
