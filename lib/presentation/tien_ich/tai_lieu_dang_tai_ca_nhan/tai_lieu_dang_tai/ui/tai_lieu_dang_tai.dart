import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/domain/models/tien_ich/tai_lieu_dang_tai_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/chi_tiet_tai_lieu/ui/chi_tiet_tai_lieu.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/components/row_widget_detail.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/tai_lieu_dang_tai/bloc/tai_lieu_dang_tai_cubit.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/tai_lieu_dang_tai/ui/filter_tai_lieu_dang_tai.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/tai_lieu_dang_tai/ui/them_moi_tai_lieu.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/utils/string_ext.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/bottomShet/show_bottom_shet_custom.dart';
import 'package:bnv_opendata/widgets/nodata/nodata_widget.dart';
import 'package:bnv_opendata/widgets/search_popular/search_popular.dart';
import 'package:bnv_opendata/widgets/views/state_layout.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

class DanhSachTaiLieuDangTai extends StatefulWidget {
  const DanhSachTaiLieuDangTai({Key? key}) : super(key: key);

  @override
  State<DanhSachTaiLieuDangTai> createState() => _DanhSachTaiLieuDangTaiState();
}

class _DanhSachTaiLieuDangTaiState extends State<DanhSachTaiLieuDangTai> {
  final TextEditingController _controllerSearch = TextEditingController();
  late TaiLieuDangTaiCubit cubit;

  @override
  void initState() {
    super.initState();
    cubit = TaiLieuDangTaiCubit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isShowBack: true,
        title: S.current.tai_lieu_dang_tai,
        actions: [
          StreamBuilder<bool>(
              stream: cubit.showAddTaiLieuDangTai.stream,
              builder: (context, snapshot) {
                return (snapshot.data ?? false)
                    ? Padding(
                        padding: const EdgeInsets.only(right: 16),
                        child: GestureDetector(
                          onTap: () {
                            showBottomSheetCustom(
                              context,
                              title: S.current.them_moi_tai_lieu,
                              child: ThemMoiTaiLieuBTS(
                                cubit: cubit,
                              ),
                            ).then(
                              (value) => cubit.getDanhSachTaiLieuDang(
                                keySearch: _controllerSearch.text,
                                isRefresh: true,
                              ),
                            );
                          },
                          child: SizedBox(
                            height: 24,
                            width: 24,
                            child: Image.asset(
                              ImageAssets.icAddCircleOrange,
                            ),
                          ),
                        ),
                      )
                    : const SizedBox.shrink();
              })
        ],
      ),
      body: KeyboardDismisser(
        child: Container(
          margin: const EdgeInsets.only(top: 24),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: widgetSearch(),
              ),
              spaceH24,
              Expanded(
                child: StateStreamLayout(
                  stream: cubit.stateStream,
                  error: AppException('', S.current.something_went_wrong),
                  retry: () async {
                    cubit.resetParamApi();
                    _controllerSearch.text = '';
                    await cubit.getDanhSachTaiLieuDang();
                  },
                  textEmpty: '',
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 16),
                    child: NotificationListener<ScrollNotification>(
                      onNotification: (scrollInfo) {
                        if (cubit.canLoadMoreList &&
                            scrollInfo.metrics.pixels ==
                                scrollInfo.metrics.maxScrollExtent) {
                          cubit.loadMoreTaiLieuDangTai(
                            keySearch: _controllerSearch.text,
                          );
                        }
                        return true;
                      },
                      child: RefreshIndicator(
                        onRefresh: () async {
                          _controllerSearch.text = '';
                          await cubit.getDanhSachTaiLieuDang(
                            isRefresh: true,
                          );
                        },
                        child: StreamBuilder<List<TaiLieuDangTaiModel>>(
                          stream: cubit.danhSachTaiLieuDangTai.stream,
                          builder: (context, snapshot) {
                            final data = snapshot.data ?? [];
                            final stateStatus = cubit.stateLayout;
                            return data.isNotEmpty
                                ? ListView.builder(
                                    physics:
                                        const AlwaysScrollableScrollPhysics(),
                                    shrinkWrap: true,
                                    itemBuilder:
                                        (BuildContext context, int index) {
                                      return itemTaiLieuDangTai(data[index]);
                                    },
                                    itemCount: data.length,
                                  )
                                : (stateStatus == StateLayout.showLoading)
                                    ? const SizedBox()
                                    : SingleChildScrollView(
                                        physics:
                                            const AlwaysScrollableScrollPhysics(),
                                        child: SizedBox(
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .height *
                                              0.6,
                                          child: NodataWidget(
                                            text: (cubit.inSearching.value)
                                                ? S.current
                                                    .khong_tim_thay_ket_qua_phu_hop
                                                : S.current.chua_co_du_lieu,
                                          ),
                                        ),
                                      );
                          },
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget itemTaiLieuDangTai(TaiLieuDangTaiModel model) => GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ChiTietTaiLieuScreen(
                model: model,
              ),
            ),
          ).then((value) async {
            await cubit.getDanhSachTaiLieuDang(
              isRefresh: true,
            );
          });
        },
        child: Container(
          margin: const EdgeInsets.only(bottom: 16),
          padding: const EdgeInsets.all(16),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(6),
            color: colorBgHome,
            border: Border.all(color: colorE2E8F0),
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                model.title,
                style: XelaTextStyle.Xela16Medium.copyWith(
                  color: color364564,
                ),
              ),
              spaceH8,
              rowWidgetDetail(S.current.linh_vuc, model.categoryName),
              spaceH8,
              rowWidgetDetail(S.current.chuyen_muc, model.subcategoryName),
              spaceH8,
              rowWidgetDetail(
                S.current.dung_luong,
                model.fileSize.formatBytes(),
              ),
              spaceH8,
              rowWidgetDetail(
                S.current.file_du_lieu,
                cubit.getNameFile(model.fileDinhKem),
                colorContent: color20C997,
              ),
              spaceH8,
              rowWidgetDetail(
                  S.current.thoi_gian_gui_duyet,
                  '${model.createdAt.getDateTimeFromMil(
                    milisecondsTime: model.createdAt,
                  )} ${model.createdAt.getTimeFromMil(
                    milisecondsTime: model.createdAt,
                  )}'),
              spaceH8,
              widgetRowTrangThai(model.status)
            ],
          ),
        ),
      );

  Widget widgetSearch() {
    return WidgetSearchPopular(
      filterCallBack: () {
        showBottomSheetCustom(
          context,
          child: FilterTaiLieuDangTaiCaNhan(
            cubit: cubit,
          ),
          title: S.current.loc,
        );
      },
      searchController: _controllerSearch,
      onChange: (value) async {
        await Future.delayed(const Duration(milliseconds: 500));
        await cubit.getDanhSachTaiLieuDang(
          keySearch: _controllerSearch.text,
          isRefresh: true,
          isRefreshDate: false,
          isResetNormalParam: false,
        );
      },
      onClear: () async {
        _controllerSearch.clear();
        await cubit.getDanhSachTaiLieuDang(
          keySearch: _controllerSearch.text,
          isRefresh: true,
          isResetNormalParam: false,
          isRefreshDate: false,
        );
      },
    );
  }

  Widget widgetRowTrangThai(String status) {
    return Row(
      children: [
        Text(
          S.current.trang_thai,
          style: XelaTextStyle.Xela14Regular.copyWith(
            color: color667793,
          ),
        ),
        spaceW40,
        cubit.getStatusTaiLieuDangTai(status)
      ],
    );
  }
}
