import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/tai_lieu_dang_tai/bloc/tai_lieu_dang_tai_cubit.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/button/button_select_file/button_select_file.dart';
import 'package:bnv_opendata/widgets/dropdown/multi_choice/multichoi_tai_lieu_dang_tai.dart';
import 'package:bnv_opendata/widgets/follow_keyboard/follow_keyboard.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class ThemMoiTaiLieuBTS extends StatefulWidget {
  const ThemMoiTaiLieuBTS({Key? key, required this.cubit}) : super(key: key);
  final TaiLieuDangTaiCubit cubit;

  @override
  State<ThemMoiTaiLieuBTS> createState() => _ThemMoiTaiLieuBTSState();
}

class _ThemMoiTaiLieuBTSState extends State<ThemMoiTaiLieuBTS> {
  final _controllerTieuDe = TextEditingController();
  final _controllerMoTaTaiLieu = TextEditingController();
  final _key = GlobalKey<FormState>();
  final _keyMulti = GlobalKey<MultiChoiceTaiLieuDangTaiState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    widget.cubit.disposeThemMoiTaiLieu();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
        constraints:
            BoxConstraints(maxHeight: MediaQuery.of(context).size.height * 0.8),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: FollowKeyBoardWidget(
          child: SingleChildScrollView(
            child: Form(
              key: _key,
              child: StateStreamLayout(
                stream: widget.cubit.stateStream,
                error: AppException('', S.current.something_went_wrong),
                retry: () {},
                textEmpty: '',
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    spaceH20,

                    /// tiêu đề
                    RichText(
                      textScaleFactor: MediaQuery.of(context).textScaleFactor,
                      text: TextSpan(
                        text: S.current.tieu_de,
                        style: XelaTextStyle.Xela14Regular.copyWith(
                          color: color586B8B,
                        ),
                        children: [
                          TextSpan(
                            text: ' *',
                            style: XelaTextStyle.Xela16Regular.copyWith(
                              color: Colors.red,
                            ),
                          )
                        ],
                      ),
                    ),
                    spaceH8,

                    XelaTextFieldCustom(
                      validate: (value) {
                        if ((value ?? '').isEmpty) {
                          return S.current.vui_long_nhap_tieu_de;
                        } else {
                          return null;
                        }
                      },
                      onChange: (value) {
                        widget.cubit.request.title = value;
                      },
                      maxLength: 255,
                      textEditingController: _controllerTieuDe,
                      placeholder: S.current.nhap_ten_tep_tai_lieu,
                    ),

                    spaceH20,

                    MultiChoiceTaiLieuDangTai(
                      key: _keyMulti,
                      getCategoriesSelected: (value) {
                        widget.cubit.convertLinhVucToRequest(value);
                      },
                      getSubCategories: (value) {
                        widget.cubit.convertChuyenMucToRequest(value);
                      },
                    ),
                    spaceH20,

                    /// mô tả tài liệu
                    RichText(
                      textScaleFactor: MediaQuery.of(context).textScaleFactor,
                      text: TextSpan(
                        text: S.current.mo_ta_tai_lieu,
                        style: XelaTextStyle.Xela14Regular.copyWith(
                          color: color586B8B,
                        ),
                        children: [
                          TextSpan(
                            text: ' *',
                            style: XelaTextStyle.Xela16Regular.copyWith(
                              color: Colors.red,
                            ),
                          )
                        ],
                      ),
                    ),
                    spaceH8,
                    XelaTextFieldCustom(
                      maxLength: 500,
                      validate: (value) {
                        if ((value ?? '').trim().isEmpty) {
                          return S.current.vui_long_nhap_mo_ta_tai_lieu;
                        }
                        return null;
                      },
                      placeholder: S.current.nhap_mo_ta_tai_lieu,
                      textEditingController: _controllerMoTaTaiLieu,
                      keyboardType: TextInputType.multiline,
                      maxLine: 5,
                      isCanMultiLine: true,
                      onChange: (value) {
                        widget.cubit.request.description = value;
                      },
                    ),
                    spaceH20,

                    /// dinhKemFile(),
                    ButtonSelectFile(
                      onChange: (file) async {
                        widget.cubit.errorFileAddChiTietTailieu.sink.add('');
                        widget.cubit.fileThemMoiTaiLieu = file;
                        if (file.path.isNotEmpty) {
                          await widget.cubit.postFile(
                            widget.cubit.fileThemMoiTaiLieu,
                          );
                        }
                        widget.cubit.checkValidateHaveFile();
                      },
                    ),
                    _errorTextFile(
                      valueBHVSJ: widget.cubit.errorFileAddChiTietTailieu,
                    ),
                    spaceH20,
                    XelaButton(
                      onPressed: () {
                        _keyMulti.currentState?.cubit.checkAllValidate();
                        _keyMulti.currentState?.setState(() {

                        });
                        widget.cubit.checkValidateHaveFile();
                        if ((_key.currentState?.validate() ?? false) &&
                            widget.cubit.validateFile &&
                            widget.cubit.checkValidateThemMoiTaiLieu()) {
                          widget.cubit
                              .themSuaTaiLieu()
                              .then((value) => Navigator.pop(context));
                        } else {}
                      },
                      text: S.current.luu,
                      leftIcon: SizedBox(
                        height: 24,
                        width: 24,
                        child: Image.asset(
                          ImageAssets.icSaveAndSend,
                          color: colorBgHome,
                        ),
                      ),
                      background: colorE26F2C,
                      textStyle: XelaTextStyle.Xela16Medium.copyWith(
                        color: Colors.white,
                      ),
                      autoResize: false,
                    ),

                    spaceH40,
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _errorTextFile({required BehaviorSubject<String> valueBHVSJ}) {
    return StreamBuilder<String>(
      stream: valueBHVSJ.stream,
      builder: (context, snapshot) {
        return ((snapshot.data ?? '').isNotEmpty)
            ? Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 24, vertical: 8),
                child: Text(
                  snapshot.data ?? '',
                  style: XelaTextStyle.Xela14Regular.copyWith(
                    color: Colors.red,
                  ),
                ),
              )
            : const SizedBox.shrink();
      },
    );
  }

  Widget dinhKemFile() => Container(
        padding: const EdgeInsets.symmetric(
          vertical: 4,
          horizontal: 10,
        ),
        decoration: BoxDecoration(
          color: colorFEF2E2,
          borderRadius: BorderRadius.circular(4),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 24,
              width: 24,
              child: Image.asset(ImageAssets.icDinhFile),
            ),
            spaceW6,
            Text(
              S.current.tai_lieu_dinh_kem,
              style: XelaTextStyle.Xela14Medium.copyWith(
                color: colorE26F2C,
              ),
            )
          ],
        ),
      );
}
