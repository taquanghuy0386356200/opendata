import 'dart:io';

import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/data/request/them_sua_tai_lieu_request.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/domain/models/tien_ich/tai_lieu_dang_tai_model.dart';
import 'package:bnv_opendata/domain/repository/home/home_repository.dart';
import 'package:bnv_opendata/domain/repository/tien_ich/tien_ich_respository.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

// ignore: depend_on_referenced_packages
import 'package:rxdart/rxdart.dart';

part 'tai_lieu_dang_tai_state.dart';

class TaiLieuDangTaiCubit extends BaseCubit<TaiLieuDangTaiState> {
  TaiLieuDangTaiCubit() : super(TaiLieuDangTaiInitial()) {
    showContent();
    initApi();
  }

  static const String StatusReviewingFile = 'REVIEWING';
  static const String StatusDraftFile = 'DRAFT';

  Future<void> initApi() async {
    await getDanhSachTaiLieuDang();
  }

  Future<void> getCategories() async {
    final result = await _homeRepo.getListCategoryHome();
    result.when(
      success: (response) {
        listLinhVuc.sink.add(response);
      },
      error: (error) {
        listLinhVuc.sink.add([]);
      },
    );
  }

  HomeRepository get _homeRepo => Get.find();

  TienIchRepository get _repo => Get.find();
  BehaviorSubject<List<CategoryModel>> listLinhVuc = BehaviorSubject.seeded([]);
  BehaviorSubject<bool> donePickFile = BehaviorSubject();
  DateTime startDateFilter = DateTime(
    1970,
  );
  DateTime endDateFilter = DateTime(
    DateTime.now().year,
    DateTime.now().month,
    DateTime.now().day,
    24,
  );

  BehaviorSubject<String> errorFileAddChiTietTailieu =
      BehaviorSubject.seeded('');
  BehaviorSubject<String> errorLinhVucAddTaiLieu = BehaviorSubject.seeded('');
  BehaviorSubject<String> errorChuyenMucAddTaiLieu = BehaviorSubject.seeded('');
  final List<File> filePost = [];
  File fileThemMoiTaiLieu = File('');
  bool validateFile = false;
  bool validateLinhVucChuyenMuc = false;

  ThemSuaTaiLieuRequest request = ThemSuaTaiLieuRequest(
    description: '',
    subcategoryID: '',
    subcategoryName: '',
    categoryID: '',
    categoryName: '',
  );

  //default can null so set description = ''

  void checkValidateHaveFile() {
    if (fileThemMoiTaiLieu.path.isEmpty) {
      errorFileAddChiTietTailieu.add(S.current.vui_long_tai_file);
      validateFile = false;
    } else {
      errorFileAddChiTietTailieu.add('');
      validateFile = true;
    }
  }

  void convertLinhVucToRequest(List<CategoryModel> value) {
    if (value.length == 1) {
      request.categoryName = '${value[0].name};';
      request.categoryID = '${value[0].id},';
    } else {
      final listCategoryId =
          value.map((e) => e.id.toString()).toList().join(',');
      request.categoryID = '$listCategoryId,';
      final listNameCategory = value.map((e) => e.name).join(';');
      request.categoryName = '$listNameCategory;';
    }
  }

  void convertChuyenMucToRequest(List<SubCategoriesModel> value) {
    if (value.length == 1) {
      request.subcategoryName = '${value[0].name};';
      request.subcategoryID = '${value[0].id},';
    } else {
      final listCategoryId =
          '${value.map((e) => e.id.toString()).toList().join(',')},';
      request.subcategoryID = listCategoryId;
      final listNameCategory = '${value.map((e) => e.name).join(';')};';
      request.subcategoryName = listNameCategory;
    }
  }

  List<int> listLinhVucCoChuyenMuc = [1226, 1222];

  final BehaviorSubject<List<TaiLieuDangTaiModel>> danhSachTaiLieuDangTai =
      BehaviorSubject();

  bool loadMore = false;
  bool canLoadMoreList = true;
  int pageSize = 10;
  int pageIndex = 1;
  String categoryID = '';
  String subCategoryID = '';
  String statusParam = '';
  String valueDropDownTrangThai = '';
  String fileTypeParam = '';
  String nguoiDangTai = PrefsService.getNameUser();
  Map<String, String> listStatusTaiLieuDangTai = {
    'REVIEWING': S.current.cho_duyet,
    'APPROVED': S.current.da_duyet,
    'REJECTED': S.current.da_tra_lai,
  };

  List<String> listFileType = [
    'doc',
    'docx',
    'pdf',
    'png',
    'jpeg',
    'jpg',
    'xls',
    'xlsx',
    'heic',
  ];

  String getNameFile(String originalName) {
    final List<String> result = originalName.split('\\');
    return result.last;
  }

  void resetParamApi({
    bool isRefreshDateTime = true,
    bool isRefreshNormalParm = true,
  }) {
    if (isRefreshDateTime) {
      startDateFilter = DateTime(
        1970,
      );
      endDateFilter = DateTime(
        DateTime.now().year,
        DateTime.now().month,
        DateTime.now().day,
        24,
      );
    }
    if (isRefreshNormalParm) {
      categoryID = '';
      subCategoryID = '';
      statusParam = '';
      fileTypeParam = '';
    }
    valueDropDownTrangThai = '';
    pageSize = 10;
    pageIndex = 1;
    nguoiDangTai = PrefsService.getNameUser();
  }

  Future<void> loadMoreTaiLieuDangTai({String keySearch = ''}) async {
    if (loadMore == false) {
      pageIndex += 1;
      canLoadMoreList = true;
      loadMore = true;
      await getDanhSachTaiLieuDang(keySearch: keySearch);
    } else {
      //nothing
    }
  }

  bool checkValidateThemMoiTaiLieu() {
    if ((request.categoryID ?? '') == ',' ||
        ((request.subcategoryID ?? '') == ',')) {
      return false;
    } else {
      return true;
    }
  }

  Widget getStatusTaiLieuDangTai(String value) {
    switch (value) {
      case 'DRAFT':
        return Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 23,
            vertical: 3,
          ),
          decoration: BoxDecoration(
            color: colorE2E8F0,
            borderRadius: BorderRadius.circular(30),
          ),
          child: Text(
            S.current.nhap,
            style: XelaTextStyle.Xela12w700.copyWith(
              color: colorBgHome,
            ),
          ),
        );
      case 'REVIEWING':
        return Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 23,
            vertical: 3,
          ),
          decoration: BoxDecoration(
            color: colorFF9F43,
            borderRadius: BorderRadius.circular(30),
          ),
          child: Text(
            S.current.cho_duyet,
            style: XelaTextStyle.Xela12w700.copyWith(
              color: colorBgHome,
            ),
          ),
        );
      case 'APPROVED':
        return Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 23,
            vertical: 3,
          ),
          decoration: BoxDecoration(
            color: color20C997,
            borderRadius: BorderRadius.circular(30),
          ),
          child: Text(
            S.current.da_duyet,
            style: XelaTextStyle.Xela12w700.copyWith(
              color: colorBgHome,
            ),
          ),
        );
      default:
        return Container(
          padding: const EdgeInsets.symmetric(
            horizontal: 23,
            vertical: 3,
          ),
          decoration: BoxDecoration(
            color: colorFF4F50,
            borderRadius: BorderRadius.circular(30),
          ),
          child: Text(
            S.current.da_tra_lai,
            style: XelaTextStyle.Xela12w700.copyWith(
              color: colorBgHome,
            ),
          ),
        );
    }
  }

  int getSecondsSinceEpoch(int time) {
    return time ~/ Duration.millisecondsPerSecond;
  }

  //todo sau có phần authen thì fix lại nguoiDangTai
  Future<void> getDanhSachTaiLieuDang({
    String keySearch = '',
    bool isRefresh = false,
    bool isRefreshDate = true,
    bool isResetNormalParam = true,
  }) async {
    showLoading();
    if (keySearch == '_') {
      // ignore: parameter_assignments
      keySearch = '';
      inSearching.sink.add(true);
    } else {
      inSearching.sink.add(keySearch.isNotEmpty);
    }
    if (isRefresh) {
      resetParamApi(
        isRefreshDateTime: isRefreshDate,
        isRefreshNormalParm: isResetNormalParam,
      );
      danhSachTaiLieuDangTai.value.clear();
    }

    final result = await _repo.getListTaiLieuDangTai(
      pageSize,
      pageIndex,
      nguoiDangTai,
      keySearch,
      statusParam,
      fileTypeParam,
      categoryID: categoryID,
      subCategoryID: subCategoryID,
      //add 7 hours cause time be is time 0, vietNam +7
      fromDate: getSecondsSinceEpoch(
        startDateFilter.add(const Duration(hours: 7)).millisecondsSinceEpoch,
      ),
      toDate: getSecondsSinceEpoch(
        endDateFilter.add(const Duration(hours: 7)).millisecondsSinceEpoch,
      ),
    );
    result.when(
      success: (response) {
        if (danhSachTaiLieuDangTai.hasValue) {
          danhSachTaiLieuDangTai.sink
              .add(danhSachTaiLieuDangTai.value + response);
          canLoadMoreList = response.length >= pageSize;
          loadMore = false;
        } else {
          danhSachTaiLieuDangTai.sink.add(response);
        }
        showAddTaiLieuDangTai.sink.add(true);
        showContent();
      },
      error: (error) {
        showAddTaiLieuDangTai.sink.add(false);
        showError();
      },
    );
  }

  final BehaviorSubject<bool> showAddTaiLieuDangTai =
      BehaviorSubject.seeded(false);

  int idThemMoiTaiLieu = 0;
  int idSuaTaiLieu = 1;

  Future<void> themSuaTaiLieu({
    bool isDraft = false,
    bool isThemMoi = true,
  }) async {
    request.id = isThemMoi ? idThemMoiTaiLieu : idSuaTaiLieu;
    request.id = 0;
    request.status = isDraft ? StatusDraftFile : StatusReviewingFile;
    request.nguoiDangTai = PrefsService.getNameUser();
    showLoading();
    final result = await _repo.themSuaTaiLieu(request);
    result.when(
      success: (response) {
        showContent();
      },
      error: (error) {
        errorFileAddChiTietTailieu.sink.add('Lỗi tải tài liệu, thử lại');
        showError();
      },
    );
  }

  Future<void> postFile(File file) async {
    donePickFile.sink.add(true);
    filePost.clear();
    filePost.add(file);
    final result = await _repo.postFile(filePost);
    result.when(
      success: (response) {
        showContent();
        request.fileDinhKem = response.body;
        donePickFile.sink.add(false);
      },
      error: (error) {
        showContent();
        donePickFile.sink.add(false);
      },
    );
  }

  void disposeThemMoiTaiLieu() {
    request = ThemSuaTaiLieuRequest(
      description: '',
      subcategoryID: '',
      subcategoryName: '',
      categoryID: '',
      categoryName: '',
    );
    validateLinhVucChuyenMuc = false;
    validateFile = false;
    fileThemMoiTaiLieu = File('');
    errorFileAddChiTietTailieu.sink.add('');
    errorChuyenMucAddTaiLieu.sink.add('');
    errorLinhVucAddTaiLieu.sink.add('');
  }

  // void initParent() {
  //   startDateTuNgayBHSJ.sink.add(startDateFilter);
  //   endDateTuNgayBHSJ.sink.add(endDateFilter);
  // }

  //FilterTaiLieuDangTai
  final BehaviorSubject<DateTime> tuNgayBHVSJ =
      BehaviorSubject.seeded(DateTime.now());
  final BehaviorSubject<DateTime> denNgayBHVSJ =
      BehaviorSubject.seeded(DateTime.now());

  void changeDateStartDateFeatEndDate({
    bool isStartDate = true,
    required DateTime value,
  }) {
    if (isStartDate) {
      tuNgayBHVSJ.sink.add(value);
    } else {
      denNgayBHVSJ.sink.add(value);
    }
  }

  bool validateTime = true;
}

extension HandleDateTime on TaiLieuDangTaiCubit {
  bool validatorStartDate({
    required String currentValue,
    required String endDate,
  }) {
    final DateTime endDateConverted = endDate.convertStringToDate();
    final DateTime startDateCompare = currentValue.convertStringToDate();
    if (startDateCompare.isAfter(endDateConverted)) {
      validateTime = false;
      return false;
    } else {
      validateTime = true;
      startDateFilter = currentValue.convertStringToDate();
      return true;
    }
  }

  bool validatorEndDate({
    required String currentValue,
    required String startDate,
  }) {
    final DateTime startDateConverted = startDate.convertStringToDate();
    final DateTime endDateCompare = currentValue.convertStringToDate();
    if (endDateCompare.isBefore(startDateConverted)) {
      validateTime = false;
      return false;
    } else {
      validateTime = true;
      endDateFilter = currentValue.convertStringToDate();
      return true;
    }
  }
}
