import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:equatable/equatable.dart';

part 'danh_sach_tien_ich_state.dart';

class DanhSachTienIchCubit extends BaseCubit<DanhSachTienIchState> {
  DanhSachTienIchCubit() : super(DanhSachTienIchInitial()) {
    showContent();
    checkLoggedIn();
  }


  bool checkLoggedIn() {
    return PrefsService.getToken().isNotEmpty;
  }
}
