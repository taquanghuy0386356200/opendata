import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/authentication/login/bloc/derect_screen_extension.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/mobile/du_lieu_mo_screen.dart';
import 'package:bnv_opendata/presentation/tien_ich/danh_sach_tien_ich/bloc/danh_sach_tien_ich_cubit.dart';
import 'package:bnv_opendata/presentation/tien_ich/huong_dan/ui/mobile/huong_dan_screen.dart';
import 'package:bnv_opendata/presentation/tien_ich/lien_he/ui/phone/lien_he_screen.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/dialog/dialog_popular.dart';

import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class DanhSachTienIchScreen extends StatefulWidget {
  const DanhSachTienIchScreen({Key? key}) : super(key: key);

  @override
  State<DanhSachTienIchScreen> createState() => _DanhSachTienIchScreenState();
}

class _DanhSachTienIchScreenState extends State<DanhSachTienIchScreen>
    with AutomaticKeepAliveClientMixin<DanhSachTienIchScreen> {
  @override
  bool get wantKeepAlive => true;
  late DanhSachTienIchCubit cubit;

  @override
  void initState() {
    super.initState();
    cubit = DanhSachTienIchCubit();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        title: S.current.tien_ich,
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 24),
        padding: const EdgeInsets.symmetric(
          horizontal: 16,
        ),
        child: Column(
          children: [
            /// du lieu danh dau
            itemTienIch(
              callBack: () {
                if (cubit.checkLoggedIn()) {
                  goTo(
                    context,
                    const DuLieuMoScreen(
                      isDanhDauScreen: true,
                      showBack: true,
                    ),
                  );
                  return;
                }
                showPoppup(textDirect: NameScreenDirect.du_lieu_danh_dau);
              },
              title: S.current.du_lieu_danh_dau,
              image: ImageAssets.icDuLieuDanhDau,
            ),

            /// lien he
            itemTienIch(
              callBack: () {
                goTo(context, const LienHeScreen());
              },
              title: S.current.lien_he,
              image: ImageAssets.icLienHe,
            ),

            /// huong dan
            itemTienIch(
              callBack: () {
                goTo(context, const HuongDanScreen());
              },
              title: S.current.huong_dan,
              image: ImageAssets.icHuongDan,
            ),
          ],
        ),
      ),
    );
  }

  Widget itemTienIch({
    required void Function() callBack,
    required String title,
    required String image,
    Color? iconColor,
  }) {
    return GestureDetector(
      onTap: () => callBack(),
      child: Container(
        padding: const EdgeInsets.symmetric(
          vertical: 20,
          horizontal: 16,
        ),
        margin: const EdgeInsets.only(bottom: 12),
        decoration: BoxDecoration(
          color: colorBgHome,
          borderRadius: const BorderRadius.all(Radius.circular(8)),
          boxShadow: [
            BoxShadow(
              color: color6589CF14,
              offset: const Offset(2, 2),
              blurRadius: 8,
            ),
          ],
        ),
        child: Row(
          children: [
            Expanded(
              flex: 9,
              child: Row(
                children: [
                  SizedBox(
                    height: 20,
                    width: 20,
                    child: Image.asset(
                      image,
                      color: iconColor,
                    ),
                  ),
                  spaceW12,
                  Text(
                    title,
                    style: XelaTextStyle.Xela16Medium.copyWith(
                      color: color364564,
                    ),
                  )
                ],
              ),
            ),
            Expanded(
              child: SizedBox(
                height: 24,
                width: 24,
                child: Image.asset(ImageAssets.icArrowRight),
              ),
            )
          ],
        ),
      ),
    );
  }

  void showPoppup({required String textDirect}) => showDialogPopular(
        context,
        isDoubleBtn: true,
        imageContent: ImageAssets.icWarningOpenData,
        contentDialog: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20, bottom: 14),
              child: Text(
                S.current.thong_bao,
                style: XelaTextStyle.Xela18Medium.copyWith(
                  color: color364564,
                ),
              ),
            ),
            Text(
              S.current.vui_long_dang_nhap_de_su_dung_chuc_nang_nay,
              style: XelaTextStyle.Xela14Regular.copyWith(
                color: color667793,
              ),
              textAlign: TextAlign.center,
            )
          ],
        ),
        titleDefaultBtn: S.current.dang_nhap,
        titleLeftBtn: S.current.dong,
        leftPress: () {
          Navigator.of(context).pop();
        },
        defaultPress: () {
          gotoLoginAndDirectScreen(
            context,
            nameDirect: textDirect,
          );
        },
      );
}
