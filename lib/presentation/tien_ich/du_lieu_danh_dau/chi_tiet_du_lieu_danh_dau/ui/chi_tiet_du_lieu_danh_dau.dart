import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/tien_ich/du_lieu_danh_dau/bloc/du_lieu_danh_dau_cubit.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/components/row_widget_detail.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/button/double_buttom.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class ChiTietDuLieuDanhDauScreen extends StatefulWidget {
  const ChiTietDuLieuDanhDauScreen({
    Key? key,
    required this.cubit,
  }) : super(key: key);
  final DuLieuDanhDauCubit cubit;

  @override
  State<ChiTietDuLieuDanhDauScreen> createState() =>
      _ChiTietDuLieuDanhDauScreenState();
}

class _ChiTietDuLieuDanhDauScreenState
    extends State<ChiTietDuLieuDanhDauScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isShowBack: true,
        title: S.current.chi_tiet_du_lieu,
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 16),
            child: GestureDetector(
              onTap: () {
                widget.cubit.danhDauDuLieu(id: 123, nguoiDanhDau: 'huytq');
              },
              child: SizedBox(
                height: 20,
                width: 20,
                child: Image.asset(
                  ImageAssets.icUnPinOpenData,
                ),
              ),
            ),
          )
        ],
      ),
      body: StateStreamLayout(
        stream: widget.cubit.stateStream,
        error: AppException('', S.current.something_went_wrong),
        retry: () {},
        textEmpty: '',
        child: Stack(
          children: [
            Container(
              margin: const EdgeInsets.only(top: 24),
              padding: const EdgeInsets.symmetric(horizontal: 16),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Tra cứu điểm thi THPT',
                    style: XelaTextStyle.Xela24Medium.copyWith(
                      color: color364564,
                    ),
                  ),
                  spaceH24,
                  rowWidgetDetail(S.current.ngay_cap_nhat, '24/08/2022  08:41'),
                  spaceH10,
                  rowWidgetDetail(S.current.linh_vuc, '24/08/2022'),
                  spaceH10,
                  rowWidgetDetail(S.current.chuyen_muc, 'Giáo dục'),
                  spaceH10,
                  rowWidgetDetail(
                    S.current.mo_ta_du_lieu,
                    'Dữ liệu về điểm thi tốt nghiệp THPT tất cả'
                    ' các môn của tỉnh Lạng Sơn',
                  ),
                  spaceH10,
                  rowWidgetDetail(
                    S.current.file_dinh_kem,
                    'So_GDĐT_danang.xlsx',
                    colorContent: color20C997,
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Container(
                margin: const EdgeInsets.only(left: 16, right: 16, bottom: 44),
                child: DoubleButtonBottom(
                  titleRight: S.current.tai_du_lieu,
                  onClickRight: () {},
                  onClickLeft: () {},
                  titleLeft: S.current.xem_truoc,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
