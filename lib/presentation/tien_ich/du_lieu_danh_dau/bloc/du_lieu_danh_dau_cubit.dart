import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/domain/repository/tien_ich/tien_ich_respository.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

part 'du_lieu_danh_dau_state.dart';

class DuLieuDanhDauCubit extends BaseCubit<DuLieuDanhDauState> {
  DuLieuDanhDauCubit() : super(DuLieuDanhDauInitial()) {
    showContent();
  }

  TienIchRepository get _tienIch => Get.find();

  Future<void> danhDauDuLieu({
    required int id,
    required String nguoiDanhDau,
  }) async {
    showLoading();
    final result = await _tienIch.danhDauDuLieu(
      id,
      nguoiDanhDau,
    );
    result.when(
      success: (response) {
        showContent();
      },
      error: (error) {
        showContent();
      },
    );
  }
}
