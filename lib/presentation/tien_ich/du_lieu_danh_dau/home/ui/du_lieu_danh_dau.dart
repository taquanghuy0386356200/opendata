import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/tien_ich/du_lieu_danh_dau/bloc/du_lieu_danh_dau_cubit.dart';
import 'package:bnv_opendata/presentation/tien_ich/du_lieu_danh_dau/chi_tiet_du_lieu_danh_dau/ui/chi_tiet_du_lieu_danh_dau.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/components/row_widget_detail.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/bottomShet/show_bottom_shet_custom.dart';
import 'package:bnv_opendata/widgets/dropdown/cool_dropdown.dart';
import 'package:bnv_opendata/widgets/search_popular/search_popular.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class DuLieuDanhDauScreen extends StatefulWidget {
  const DuLieuDanhDauScreen({Key? key}) : super(key: key);

  @override
  State<DuLieuDanhDauScreen> createState() => _DuLieuDanhDauScreenState();
}

class _DuLieuDanhDauScreenState extends State<DuLieuDanhDauScreen> {
  final TextEditingController _searchController = TextEditingController();
  late DuLieuDanhDauCubit cubit;

  @override
  void initState() {
    super.initState();
    cubit = DuLieuDanhDauCubit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: BaseAppBar(
        isShowBack: true,
        title: S.current.du_lieu_danh_dau,
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 24),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          children: [
            WidgetSearchPopular(
              searchController: _searchController,
              onChange: (value) {},
              onClear: () {
                _searchController.clear();
              },
              filterCallBack: () {
                showBottomSheetCustom(
                  context,
                  child: filterDuLieuDanhDau(),
                  title: S.current.loc,
                );
              },
            ),
            spaceH24,
            Expanded(
              child: SingleChildScrollView(
                child: ListView.builder(
                  physics: const NeverScrollableScrollPhysics(),
                  shrinkWrap: true,
                  itemBuilder: (context, index) {
                    return itemDuLieuDanhDau(cubit);
                  },
                  itemCount: 10,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget itemDuLieuDanhDau(DuLieuDanhDauCubit cubit) {
    return GestureDetector(
      onTap: () {
        goTo(
          context,
          ChiTietDuLieuDanhDauScreen(
            cubit: cubit,
          ),
        );
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 16),
        padding: const EdgeInsets.all(16),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(6),
          color: colorBgHome,
          border: Border.all(color: colorE2E8F0),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              'Tra cứu điểm thi tốt nghiệp THPT',
              style: XelaTextStyle.Xela16Medium.copyWith(
                color: color364564,
              ),
            ),
            spaceH8,
            rowWidgetDetail(S.current.thoi_gian_cap_nhat, '25/07/2022   08:41'),
            spaceH8,
            rowWidgetDetail(S.current.linh_vuc, 'Cải cách hành chính'),
            spaceH8,
            rowWidgetDetail(S.current.chuyen_muc, 'Giáo dục'),
            spaceH8,
            rowWidgetDetail(
              S.current.mo_ta,
              'Tra cứu điểm thi tốt nghiệp THPT 2022 tại Đà Nẵng',
            ),
          ],
        ),
      ),
    );
  }

  Widget filterDuLieuDanhDau() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          spaceH20,
          Text(
            S.current.linh_vuc,
            style: XelaTextStyle.Xela14Regular.copyWith(
              color: color586B8B,
            ),
          ),
          spaceH8,
          CoolDropDown(
            initData: '',
            useCustomHintColors: true,
            placeHoder: S.current.tat_ca,
            listData: [
              S.current.chuyen_muc,
              's',
            ],
            onChange: (vl) {},
          ),
          spaceH20,
          Text(
            S.current.chuyen_muc,
            style: XelaTextStyle.Xela14Regular.copyWith(
              color: color586B8B,
            ),
          ),
          spaceH8,
          CoolDropDown(
            initData: '',
            useCustomHintColors: true,
            placeHoder: S.current.tat_ca,
            listData: [
              S.current.chuyen_muc,
              's',
            ],
            onChange: (vl) {},
          ),
          spaceH20,
          XelaButton(
            onPressed: () {},
            text: S.current.tim_kiem,
            leftIcon: SizedBox(
              height: 24,
              width: 24,
              child: Image.asset(
                ImageAssets.icSearch,
                color: colorBgHome,
              ),
            ),
            background: colorE26F2C,
            textStyle: XelaTextStyle.Xela16Medium.copyWith(
              color: Colors.white,
            ),
            autoResize: false,
          ),
          spaceH40,
        ],
      ),
    );
  }
}
