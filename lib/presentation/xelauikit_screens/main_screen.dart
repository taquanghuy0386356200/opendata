import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/authentication/change_password/ui/change_password.dart';
import 'package:bnv_opendata/presentation/authentication/lost_password/ui/set_password.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/mobile/du_lieu_mo_screen.dart';
import 'package:bnv_opendata/presentation/home/ui/home.dart';
import 'package:bnv_opendata/presentation/thong_bao/ui/mobile/thong_bao_screen.dart';
import 'package:bnv_opendata/presentation/tien_ich/danh_sach_tien_ich/ui/danh_sach_tien_ich.dart';
import 'package:bnv_opendata/presentation/van_ban/ui/mobile/van_ban_screen.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/dialog/message_config.dart';
import 'package:bnv_opendata/widgets/dialog/show_toast.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

class MainScreen extends StatefulWidget {
  final int? setSelectScreen;
  final String? keySearch;
  final bool? filterDanhDau;
  final CategoryModel? dataCategoryFilter;

  const MainScreen({
    Key? key,
    this.setSelectScreen,
    this.keySearch,
    this.filterDanhDau,
    this.dataCategoryFilter,
  }) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  late int _selectedIndex;
  late String keySearch = '';
  late CategoryModel? dataCategoryFilter;

  @override
  void initState() {
    _selectedIndex = widget.setSelectScreen ?? 0;
    keySearch = widget.keySearch ?? '';
    dataCategoryFilter = widget.dataCategoryFilter;
    super.initState();
  }

  @override
  void didChangeDependencies() {
    MessageConfig.init(context);
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    resetData();
    super.dispose();
  }

  void onItemTapped(int index) {
    _selectedIndex = index;
    resetData();
    setState(() {});
  }

  void onSetKeySearch(String data) {
    keySearch = data;
  }

  void onSetFilter(CategoryModel data) {
    dataCategoryFilter = data;
  }

  void resetData() {
    keySearch = '';
    dataCategoryFilter = CategoryModel(0, '', '', []);
  }

  dynamic checkSelect({
    required int thisIndex,
    dynamic dataSelect,
    dynamic dataUnSelect,
  }) {
    if (thisIndex == _selectedIndex) {
      return dataSelect;
    }
    return dataUnSelect;
  }

  Widget imageUrl(String url) {
    return SizedBox(height: 20, width: 20, child: Image.asset(url));
  }

  List<BottomNavigationBarItem> listBottom() {
    final List<BottomNavigationBarItem> listResult = [];

    final List<dynamic> listSelect = [
      imageUrl(ImageAssets.icHomeSelect),
      imageUrl(ImageAssets.icOpenDataSelect),
      imageUrl(ImageAssets.icNotificationSelect),
      imageUrl(ImageAssets.icDocumentSelect),
      imageUrl(ImageAssets.icTienIchSelect),
    ];

    final List<Widget> listUnSelect = [
      imageUrl(ImageAssets.icHomeUnSelect),
      imageUrl(ImageAssets.icOpenDataUnSelect),
      imageUrl(ImageAssets.icNotificationUnSelect),
      imageUrl(ImageAssets.icDocumentUnSelect),
      imageUrl(ImageAssets.icTienIchUnSelect)
    ];

    final List<String> namesScreen = [
      S.current.trang_chu,
      S.current.du_lieu_mo,
      S.current.thong_bao,
      S.current.van_ban,
      S.current.tien_ich,
    ];

    for (int index = 0; index < namesScreen.length; index++) {
      listResult.add(
        BottomNavigationBarItem(
          backgroundColor: XelaColor.Gray12,
          icon: SizedBox(
            height: 20,
            width: 20,
            child: checkSelect(
              thisIndex: index,
              dataSelect: listSelect[index],
              dataUnSelect: listUnSelect[index],
            ),
          ),
          label: checkSelect(
            thisIndex: index,
            dataSelect: namesScreen[index],
            dataUnSelect: '',
          ),
        ),
      );
    }
    return listResult;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: MyInheritedWidget(
        index: _selectedIndex,
        setSomeValue: onItemTapped,
        keySearch: keySearch,
        setKeySearch: onSetKeySearch,
        dataFilter: dataCategoryFilter,
        setFilter: onSetFilter,
        child: Scaffold(
          body: Center(
            child: [
              const HomeScreen(),
              DuLieuMoScreen(
                key: UniqueKey(),
                keySearch: keySearch,
                dataCategoryFilter: dataCategoryFilter,
              ),
              const ThongBaoScreen(),
              const VanBanScreen(),
              const DanhSachTienIchScreen(),
            ].elementAt(_selectedIndex),
          ),
          bottomNavigationBar: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            backgroundColor: XelaColor.Gray12,
            selectedItemColor: XelaColor.Gray1,
            selectedFontSize: 12,
            items: listBottom(),
            currentIndex: _selectedIndex,
            onTap: onItemTapped,
          ),
        ),
      ),
    );
  }
}

class MyInheritedWidget extends InheritedWidget {
  final int index;
  final String keySearch;
  final CategoryModel? dataFilter;
  final Function(int) setSomeValue;
  final Function(String) setKeySearch;
  final Function(CategoryModel) setFilter;

  const MyInheritedWidget({
    Key? key,
    required this.index,
    required this.setSomeValue,
    required this.keySearch,
    required this.setKeySearch,
    required this.dataFilter,
    required this.setFilter,
    required Widget child,
  }) : super(key: key, child: child);

  static MyInheritedWidget? of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<MyInheritedWidget>();
  }

  @override
  bool updateShouldNotify(MyInheritedWidget oldWidget) {
    return index != oldWidget.index;
  }
}
