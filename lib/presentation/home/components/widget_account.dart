import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/domain/env/model/app_constants.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/authentication/login/ui/login_screen.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

enum StatusAuthentication {
  LOGGED_IN,
  NOT_LOGIN,
}

class WidgetAccountHome extends StatelessWidget {
  const WidgetAccountHome({
    Key? key,
    this.imgTemp,
    this.name,
    required this.statusAuthentication,
    this.callBackLoggedIn,
  }) : super(key: key);

  // final String imageNetwork;
  final String? imgTemp;
  final String? name;
  final StatusAuthentication statusAuthentication;
  final Function()? callBackLoggedIn;

  @override
  Widget build(BuildContext context) {
    return statusAuthentication == StatusAuthentication.LOGGED_IN
        ? widgetAccountLoggedIn()
        : widgetAccountNotLogin(context);
  }

  Widget widgetAccountLoggedIn() {
    bool nullAvatar = false;

    final String fakeImage =
        '${Get.find<AppConstants>().baseUrl}/${PrefsService.getAvatar()}';
    if (PrefsService.getAvatar().isEmpty) {
      nullAvatar = true;
    }

    return GestureDetector(
      onTap: () {
        callBackLoggedIn!();
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 24),
        constraints: const BoxConstraints(
          minHeight: 40,
          maxHeight: 50,
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: 40,
              width: 40,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                border: Border.all(width: 1.5, color: colorE2E8F0),
              ),
              child: ClipRRect(
                borderRadius: BorderRadius.circular(40),
                child: nullAvatar
                    ? Image.asset(ImageAssets.default_avatar)
                    : Image.network(
                        fakeImage,
                        fit: BoxFit.fill,
                        loadingBuilder: (
                          BuildContext context,
                          Widget child,
                          ImageChunkEvent? loadingProgress,
                        ) {
                          if (loadingProgress == null) return child;
                          return Center(
                            child: CircularProgressIndicator(
                              value: loadingProgress.expectedTotalBytes != null
                                  ? loadingProgress.cumulativeBytesLoaded /
                                      loadingProgress.expectedTotalBytes!
                                  : null,
                            ),
                          );
                        },
                      ),
              ),
            ),
            spaceW12,
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  '${S.current.xin_chao_home},',
                  style: XelaTextStyle.Xela14Regular.copyWith(
                    color: color667793,
                  ),
                ),
                spaceH3,
                Text(
                  PrefsService.getNameUser(),
                  style: XelaTextStyle.Xela16Medium.copyWith(
                    color: color364564,
                  ),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }

  Widget widgetAccountNotLogin(BuildContext context) => GestureDetector(
        onTap: () {
          goTo(
            context,
            const LoginScreen(),
          );
        },
        child: Container(
          margin: const EdgeInsets.only(
            bottom: 14,
          ),
          constraints: const BoxConstraints(
            minHeight: 40,
            minWidth: 130,
          ),
          padding: const EdgeInsets.symmetric(
            vertical: 8,
            horizontal: 12,
          ),
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(
              Radius.circular(4),
            ),
            color: colorE26F2C,
          ),
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: 24,
                width: 24,
                child: Image.asset(ImageAssets.icUserHome),
              ),
              spaceW6,
              Text(
                S.current.dang_nhap,
                style: XelaTextStyle.Xela14Medium.copyWith(color: colorBgHome),
              )
            ],
          ),
        ),
      );
}
