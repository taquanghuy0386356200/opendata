import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/domain/env/model/app_constants.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/presentation/home/bloc/home_cubit.dart';
import 'package:bnv_opendata/presentation/xelauikit_screens/main_screen.dart';
import 'package:bnv_opendata/utils/constants/app_constants.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/widgets/image_network/image_network.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ListLinhVucDuLieu extends StatelessWidget {
  const ListLinhVucDuLieu({Key? key, required this.cubit}) : super(key: key);
  final HomeCubit cubit;

  @override
  Widget build(BuildContext context) {
    final inheritedWidget = MyInheritedWidget.of(context);
    return Container(
      constraints: const BoxConstraints(
        maxHeight: 125,
      ),
      child: StreamBuilder<List<CategoryModel>>(
        stream: cubit.categories.stream,
        builder: (context, snapshot) {
          final data = snapshot.data ?? [];
          if(data.isEmpty){
            return const SizedBox.shrink();
          }
          return ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: data.length,
            itemBuilder: (context, index) {
              final item = data[index];
              return GestureDetector(
                onTap: () {
                  inheritedWidget?.setSomeValue(
                    MainScreens.DuLieuMoScreen,
                  );
                  inheritedWidget?.setFilter(item);
                },
                child: itemLinhVuc(
                  image: item.icon,
                  title: item.name,
                ),
              );
            },
          );
        },
      ),
    );
  }

  Widget itemLinhVuc({
    required String image,
    required String title,
  }) {
    return Container(
      width: 80,
      margin: const EdgeInsets.only(right: 10),
      child: Column(
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 10),
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(
                color: colorE2E8F0,
              ),
              borderRadius: const BorderRadius.all(
                Radius.circular(16),
              ),
            ),
            child: SizedBox(
              height: 32,
              width: 32,
              child: imageNetwork(
                image: Get.find<AppConstants>().baseUrl + image,
              ),
            ),
          ),
          Text(
            title,
            style: XelaTextStyle.Xela11Medium.copyWith(
              color: color667793,
            ),
            textAlign: TextAlign.center,
          )
        ],
      ),
    );
  }
}
