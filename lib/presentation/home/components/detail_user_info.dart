import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/components/row_widget_detail.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:flutter/material.dart';

class DetailUserInfo extends StatefulWidget {
  const DetailUserInfo({
    Key? key,
  }) : super(key: key);

  @override
  State<DetailUserInfo> createState() => _DetailUserInfoState();
}

class _DetailUserInfoState extends State<DetailUserInfo> {
  @override
  Widget build(BuildContext context) {
    String getEmail() {
      String result = '';
      if (PrefsService.getEmailUser().isNotEmpty) {
        if (PrefsService.getEmailUser().contains('@gmail.com')) {
          result = PrefsService.getEmailUser();
        } else {
          result = '${PrefsService.getEmailUser()}@gmail.com';
        }
      }
      return result;
    }

    return Scaffold(
      appBar: BaseAppBar(
        title: S.current.thong_tin_ca_nhan,
        isShowBack: true,
      ),
      body: Container(
        margin: const EdgeInsets.only(top: 12),
        padding: const EdgeInsets.symmetric(horizontal: 16),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            checkToShow(
              title: S.current.ho_va_ten,
              value: PrefsService.getNameUser(),
            ),
            checkToShow(
              title: S.current.email,
              value: getEmail(),
            ),
          ],
        ),
      ),
    );
  }

  Widget checkToShow({
    required String title,
    required String value,
  }) {
    // if (value.isEmpty) {
    //   return const SizedBox.shrink();
    // }
    return Padding(
      padding: const EdgeInsets.only(bottom: 14),
      child: rowWidgetDetail(
        title,
        value,
        colorContent: color3D5586,
      ),
    );
  }
}
