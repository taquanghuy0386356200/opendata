import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/domain/model/document_model.dart';
import 'package:bnv_opendata/domain/model/notification_model.dart';
import 'package:bnv_opendata/domain/repository/home/home_repository.dart';
import 'package:bnv_opendata/domain/repository/thong_bao/thong_bao_respository.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'home_state.dart';

class HomeCubit extends BaseCubit<HomeState> {
  HomeCubit() : super(HomeInitial()) {
    showContent();
    checkLoggedIn();
    initApiHome();
  }

  HomeRepository get _homeRepo => Get.find();

  ThongBaoRepository get _thongBaoRepo => Get.find();

  bool isLoggedIn = false;

  Future<void> initApiHome({bool isRefresh = false}) async {
    if (isRefresh) {
      categories.value.clear();
      documents.value.clear();
      listThongBao.value.clear();
    }
    await getCategories();
    await getDocuments();
    await getNotifications();
  }

  final BehaviorSubject<List<CategoryModel>> categories = BehaviorSubject();
  final BehaviorSubject<List<DocumentModel>> documents = BehaviorSubject();
  final BehaviorSubject<List<ThongBaoModel>> listThongBao = BehaviorSubject();

  Future<void> getCategories() async {
    showLoading();
    final result = await _homeRepo.getListCategoryHome();
    result.when(
      success: (response) {
        categories.sink.add(response);
        showContent();
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> getNotifications() async {
    showLoading();
    final result = await _thongBaoRepo.getListThongBaoMoiNhat(
      keySearch: '',
      pageSize: 3,
      pageIndex: 1,
    );
    result.when(
      success: (response) {
        listThongBao.sink.add(response);
        showContent();
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> getDocuments() async {
    showLoading();
    final result = await _thongBaoRepo.getListVanBan(
      keySearch: '',
      pageIndex: 1,
      pageSize: 3,
    );
    result.when(
      success: (response) {
        documents.sink.add(response);
        showContent();
      },
      error: (error) {
        showError();
      },
    );
  }

  void checkLoggedIn() {
    if (PrefsService.getToken().isNotEmpty) {
      isLoggedIn = true;
    } else {
      isLoggedIn = false;
    }
  }

  void logout() {
    PrefsService.clearAuthData();
    checkLoggedIn();
  }

  BehaviorSubject<bool> showMenuLoggedInBHVSJ = BehaviorSubject.seeded(false);

  void turnOffDropDown() {
    showMenuLoggedInBHVSJ.sink.add(false);
  }

  void turnOnMenuAcc() {
    showMenuLoggedInBHVSJ.sink.add(true);
  }

  bool showClearButton = false;

  String titleBanner =
      'Quy Định Cấp Giấy Phép Hoạt Động Đối Với Cơ Sở Khám Chữa Bệnh';
}

class BannerSlideModel {
  final String imageNetwork;
  final String title;
  final String date;

  BannerSlideModel(this.imageNetwork, this.title, this.date);
}
