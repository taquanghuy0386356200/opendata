part of 'login_cubit.dart';

abstract class LoginState {
  const LoginState();
}

class LoginInitial extends LoginState {}

class LoginError extends LoginState {
  final String error;

  const LoginError(this.error);
}

class LoginSuccess extends LoginState {
  final String token;

  const LoginSuccess(this.token);
}
