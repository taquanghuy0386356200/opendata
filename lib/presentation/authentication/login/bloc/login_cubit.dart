import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/data/request/login_ft_token_request.dart';
import 'package:bnv_opendata/data/request/xac_thuc_tai_khoan_request.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_domain_am_repo.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_repository.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:get/get.dart';
import 'package:rxdart/rxdart.dart';

part 'login_state.dart';

class LoginCubit extends BaseCubit<LoginState> {
  LoginCubit() : super(LoginInitial()) {
    showContent();
    final result = checkEnableButtonFirstTime();
    checkDisableButtonBHVSJ = BehaviorSubject.seeded(result);
  }

  bool haveNetwork = true;

  AuthenticationRepository get _authenRepo => Get.find();

  AuthenticationDomainAmRepo get _authenAMRepo => Get.find();

  bool hidePassword = true;
  bool showClearButton = false;
  bool haveBtnShowHidePw = false;

  late BehaviorSubject<bool> checkDisableButtonBHVSJ;

  bool checkEnableButtonFirstTime() {
    return PrefsService.getEmailUser().isNotEmpty;
  }

  void checkEnableButtonPopular({required bool value}) {
    checkDisableButtonBHVSJ.sink.add(value);
  }

  static const String GRAND_TYPE_GET_TOKEN = 'password';
  static const String GRAND_TYPE_REFRESH_TOKEN = 'refresh_token';

  BehaviorSubject<String> errorLogin = BehaviorSubject.seeded('');

  final LoginFeatTokenRequest requestLogin = LoginFeatTokenRequest();

  Future<void> loginAndSaveUserInfo({
    String grandType = GRAND_TYPE_GET_TOKEN,
    required String email,
    required String password,
  }) async {
    showLoading();
    bool exitExcuteFunc = false;
    String getDataBeforeMail(String mail) {
      return mail.substring(0, mail.indexOf('@'));
    }

    String tmpToken = '';
    String tmpRefreshToken = '';
    final XacThucTKRequest xacThucTKRequest = XacThucTKRequest();
    requestLogin.grant_type = grandType;
    requestLogin.username = getDataBeforeMail(email);
    requestLogin.password = password;
    final result = await _authenAMRepo.getTokenLogin(
      requestLogin,
    );

    result.when(
      success: (response) {
        tmpToken = response.accessToken ?? '';
        tmpRefreshToken = response.refreshToken ?? '';
        errorLogin.sink.add('');
      },
      error: (error) {
        exitExcuteFunc = true;
        tmpToken = '';
        tmpRefreshToken = '';
        errorLogin.sink.add(S.current.loi_dang_nhap_sai_tk_mk);
        showContent();
      },
    );

    if (exitExcuteFunc) return;

    xacThucTKRequest.token = tmpToken;
    final resultXacThucTK =
        await _authenRepo.xacThucTaiKhoan(request: xacThucTKRequest);
    resultXacThucTK.when(
      success: (response) {
        if (response.errorCode == 200) {
          emit(LoginSuccess(tmpToken));
          PrefsService.saveRefreshToken(
            tmpRefreshToken,
          );
          PrefsService.saveLoginUserName(email);
          PrefsService.saveNameUser(response.body?.fullName ?? '');
          PrefsService.savePasswordUser(password);
          PrefsService.saveAvatar(response.body?.avatar ?? '');
        } else {
          emit(LoginError(response.message ?? ''));
        }
        showContent();
      },
      error: (error) {
        emit(LoginError(S.current.su_co_xay_ra));
        showContent();
      },
    );
  }

  void checkShowIconEmailAndPass() {
    /// if textfild has data => show icon
    haveBtnShowHidePw = PrefsService.getPasswordUser().isNotEmpty;
    showClearButton = PrefsService.getEmailUser().isNotEmpty;
  }
}
