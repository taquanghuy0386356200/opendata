import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/mobile/du_lieu_mo_screen.dart';
import 'package:bnv_opendata/presentation/tien_ich/tai_lieu_dang_tai_ca_nhan/tai_lieu_dang_tai/ui/tai_lieu_dang_tai.dart';
import 'package:bnv_opendata/presentation/xelauikit_screens/main_screen.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:dartx/dartx.dart';
import 'package:flutter/material.dart';

enum ScreenDirect {
  MAIN_SCREEN,
  DETAIL_OPEN_DATA,
  TAI_LIEU_DANG_TAI,
  DU_LIEU_DANH_GIAU
}

extension GotoScreen on ScreenDirect {
  ScreenDirect nameToEnum() {
    switch (getNameScreen()) {
      case NameScreenDirect.main_screen:
        return ScreenDirect.MAIN_SCREEN;
      case NameScreenDirect.detail_open_data:
        return ScreenDirect.DETAIL_OPEN_DATA;
      case NameScreenDirect.tai_lieu_dang_tai:
        return ScreenDirect.TAI_LIEU_DANG_TAI;
      case NameScreenDirect.du_lieu_danh_dau:
        return ScreenDirect.DU_LIEU_DANH_GIAU;
    }
    return ScreenDirect.MAIN_SCREEN;
  }

  void gotoScreen(BuildContext context) {
    switch (nameToEnum()) {
      case ScreenDirect.MAIN_SCREEN:
        return pushAndReplacement(context, const MainScreen());
      case ScreenDirect.DETAIL_OPEN_DATA:
        return Navigator.pop(context);
      case ScreenDirect.TAI_LIEU_DANG_TAI:
        return pushAndReplacement(context, const DanhSachTaiLieuDangTai());
      case ScreenDirect.DU_LIEU_DANH_GIAU:
        return pushAndReplacement(
          context,
          const DuLieuMoScreen(
            isDanhDauScreen: true,
            showBack: true,
          ),
        );
    }
  }

  int getId() {
    /// khi luu teen ma can id thi lưu theo định dạng "name_file/id"
    final List<String> result = nameScreenFromLocal().split('/');
    return result.last.toInt();
  }

  String getNameScreen() {
    if (nameScreenFromLocal().contains('/')) {
      return nameScreenFromLocal()
          .substring(0, nameScreenFromLocal().indexOf('/'));
    }
    return nameScreenFromLocal();
  }

  String nameScreenFromLocal() => PrefsService.getNameScreen();
}

class NameScreenDirect {
  static const main_screen = 'MAIN_SCREEN';
  static const detail_open_data = 'DETAIL_OPEN_DATA';
  static const tai_lieu_dang_tai = 'TAI_LIEU_DANG_TAI';
  static const du_lieu_danh_dau = 'DU_LIEU_DANH_GIAU';
}
