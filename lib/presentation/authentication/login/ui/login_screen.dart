import 'dart:async';

import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/data/network/network_checker.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/authentication/login/bloc/derect_screen_extension.dart';
import 'package:bnv_opendata/presentation/authentication/login/bloc/login_cubit.dart';

import 'package:bnv_opendata/utils/constants/image_asset.dart';

import 'package:bnv_opendata/utils/string_ext.dart';
import 'package:bnv_opendata/widgets/button/button_box_shadow.dart';
import 'package:bnv_opendata/widgets/dialog/dialog_popular.dart';
import 'package:bnv_opendata/widgets/dialog/show_toast.dart';
import 'package:bnv_opendata/widgets/header_login_module/header_popular.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _keyForm = GlobalKey<FormState>();
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  final cubit = LoginCubit();
  late final FToast toast;
  ScreenDirect screenDirect = ScreenDirect.MAIN_SCREEN;

  @override
  void initState() {
    super.initState();
    if (PrefsService.getEmailUser().isNotEmpty) {
      if (PrefsService.getEmailUser().contains('@gmail.com')) {
        emailController.text = PrefsService.getEmailUser();
      } else {
        emailController.text = '${PrefsService.getEmailUser()}@gmail.com';
      }
    }

    // emailController.text = PrefsService.getEmailUser().isNotEmpty
    //     ? PrefsService.getEmailUser()
    //     : '';
    passwordController.text = PrefsService.getPasswordUser();
    toast = FToast();
    cubit.checkShowIconEmailAndPass();
    toast.init(context);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: StateStreamLayout(
        retry: () {
          cubit.showContent();
        },
        error: AppException('', S.current.something_went_wrong),
        stream: cubit.stateStream,
        textEmpty: '',
        child: BlocListener<LoginCubit, LoginState>(
          bloc: cubit,
          listener: (context, state) async {
            if (state is LoginSuccess) {
              await PrefsService.saveToken(state.token);
              toast.removeQueuedCustomToasts();
              toast.showToast(
                child: ShowToast(
                  icon: ImageAssets.icSuccess,
                  color: color20C997opacity,
                  text: S.current.dang_nhap_thanh_cong,
                  withOpacity: 0.4,
                ),
                gravity: ToastGravity.BOTTOM,
              );
              if (!mounted) return;
              screenDirect.gotoScreen(context);

              /// reset name screen after login success
              await PrefsService.clearNameScreen();
            } else if (state is LoginError) {
              unawaited(
                showDialogPopular(
                  context,
                  defaultPress: () {},
                  haveButton: false,
                  imageContent: ImageAssets.icWarningOrange,
                  timeShowDialog: 2,
                  contentDialog: Container(
                    margin: const EdgeInsets.only(top: 32),
                    child: Text(
                      state.error,
                      style: XelaTextStyle.Xela18Medium.copyWith(
                        color: color364564,
                      ),
                    ),
                  ),
                  titleDefaultBtn: '',
                ),
              );
            } else {}
          },
          child: KeyboardDismisser(
            child: SingleChildScrollView(
              child: Form(
                key: _keyForm,
                child: Column(
                  children: [
                    const HeaderLoginPopular(
                      image: ImageAssets.bgLogin,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            S.current.xin_chao,
                            style: XelaTextStyle.Xela32Bold.copyWith(
                              color: color364564,
                            ),
                          ),
                          spaceH12,
                          Text(
                            S.current.vui_long_dang_nhap_de_su_dung_ung_dung,
                            style: XelaTextStyle.Xela14Regular.copyWith(
                              color: colorA2AEBD,
                            ),
                          ),
                          spaceH40,

                          /// textfild email
                          XelaTextFieldCustom(
                            textEditingController: emailController,
                            placeholder: S.current.email,
                            borderDefaultColor: colorE2E8F0,
                            placeholderColor: color667793,
                            maxLength: 255,
                            rightIcon: cubit.showClearButton
                                ? iconlear()
                                : const SizedBox.shrink(),
                            leftIcon: iconEmail(),
                            onChange: (value) {
                              cubit.showClearButton = value.isNotEmpty;
                              setState(() {});
                              cubit.checkEnableButtonPopular(
                                value:
                                    _keyForm.currentState?.validate() ?? false,
                              );
                            },
                            validate: (value) {
                              if ((value ?? '').isNotEmpty) {
                                return (value ?? '').trim().validateEmail();
                              } else {
                                return (value ?? '').checkTruongNull(' email');
                              }
                            },
                          ),
                          spaceH16,

                          /// textfild password
                          XelaTextFieldCustom(
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                RegExp(r'\s'),
                              ),
                            ],
                            maxLine: 1,
                            maxLength: 255,
                            textEditingController: passwordController,
                            placeholder: S.current.mat_khau,
                            borderDefaultColor: colorE2E8F0,
                            secureField: cubit.hidePassword,
                            placeholderColor: color667793,
                            onChange: (value) {
                              setState(() {
                                cubit.haveBtnShowHidePw = value.isNotEmpty;
                              });

                              cubit.checkEnableButtonPopular(
                                value:
                                    _keyForm.currentState?.validate() ?? false,
                              );
                            },
                            validate: (value) {
                              if ((value ?? '').trim().isEmpty) {
                                return S.current.vui_long_nhap_mat_khau;
                              } else {
                                return null;
                              }
                            },
                            leftIcon: iconPassWork(),
                            rightIcon: cubit.haveBtnShowHidePw
                                ? iconHidePassWork()
                                : const SizedBox.shrink(),
                          ),
                          spaceH16,

                          ///errorLogin
                          StreamBuilder<String>(
                            stream: cubit.errorLogin.stream,
                            builder: (context, snapshot) {
                              final data = snapshot.data ?? '';
                              return data.isNotEmpty
                                  ? Container(
                                      margin: const EdgeInsets.only(bottom: 16),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Row(
                                          children: [
                                            Image.asset(
                                              ImageAssets.icErrorLogin,
                                              height: 16,
                                              width: 16,
                                            ),
                                            spaceW8,
                                            Text(
                                              data,
                                              style: XelaTextStyle.Xela12Regular
                                                  .copyWith(
                                                color: color586B8B,
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                    )
                                  : const SizedBox.shrink();
                            },
                          ),

                          StreamBuilder<bool>(
                            stream: cubit.checkDisableButtonBHVSJ,
                            builder: (context, snapshot) {
                              final data = snapshot.data ?? false;
                              return ButtonBoxShadow(
                                blurRadius: 10,
                                colorBoxShadow: colorE26F2C.withOpacity(0.25),
                                offSetY: 4,
                                offSetX: 0,
                                child: XelaButton(
                                  onPressed: () {
                                    if ((_keyForm.currentState?.validate() ??
                                            false) &&
                                        data) {
                                      CheckerNetwork.checkNetwork()
                                          .then((value) {
                                        if (value) {
                                          cubit.loginAndSaveUserInfo(
                                            email: emailController.text.trim(),
                                            password: passwordController.text,
                                          );
                                        } else {
                                          cubit.showError();
                                        }
                                        //   toast.removeQueuedCustomToasts();
                                        //   toast.showToast(
                                        //     child: const ShowToast(
                                        //       icon: ImageAssets.icWarning,
                                        //       color: Colors.deepOrangeAccent,
                                        //       text:
                                        //           'Vui lòng kiểm tra kết nối mạng và thử lại.',
                                        //       withOpacity: 0.6,
                                        //     ),
                                        //     gravity: ToastGravity.BOTTOM,
                                        //   );
                                        // }
                                      });
                                    }
                                  },
                                  text: S.current.dang_nhap,
                                  background:
                                      data ? colorE26F2C : colorE26F2C25,
                                  textStyle:
                                      XelaTextStyle.Xela16Medium.copyWith(
                                    color: Colors.white,
                                  ),
                                  autoResize: false,
                                ),
                              );
                            },
                          ),
                          spaceH20,
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget errorWhenLogin(String error) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        spaceH16,
        Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            SizedBox(
              height: 16,
              width: 16,
              child: Image.asset(ImageAssets.icWarning),
            ),
            spaceW8,
            Expanded(
              child: Text(
                error,
                style: XelaTextStyle.Xela14Regular.copyWith(
                  color: color586B8B,
                ),
              ),
            )
          ],
        )
      ],
    );
  }

  Widget iconHidePassWork() => GestureDetector(
        onTap: () {
          /// an hien passwork
          setState(() {
            cubit.hidePassword = !cubit.hidePassword;
          });
        },
        child: SizedBox(
          width: 20,
          height: 20,
          child: cubit.hidePassword
              ? Image.asset(ImageAssets.icEye)
              : Image.asset(ImageAssets.icHideEye),
        ),
      );

  Widget iconlear() => GestureDetector(
        onTap: () {
          setState(() {});
          cubit.checkEnableButtonPopular(
            value: false,
          );
          emailController.clear();
          cubit.showClearButton = false;
        },
        child: SizedBox(
          height: 20,
          width: 20,
          child: Image.asset(ImageAssets.icClear),
        ),
      );

  Widget iconPassWork() => Container(
        height: 20,
        width: 20,
        margin: const EdgeInsets.only(right: 12),
        child: Image.asset(
          ImageAssets.icPassword,
        ),
      );

  Widget iconEmail() => Container(
        height: 20,
        width: 20,
        margin: const EdgeInsets.only(right: 12),
        child: Image.asset(
          ImageAssets.icEmail,
        ),
      );
}
