import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/data/request/doi_mat_khau_request.dart';
import 'package:bnv_opendata/data/request/login_ft_token_request.dart';
import 'package:bnv_opendata/data/request/xac_thuc_tai_khoan_request.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_domain_am_repo.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_repository.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'change_password_state.dart';

class ChangePasswordCubit extends BaseCubit<ChangePasswordState> {
  ChangePasswordCubit() : super(ChangePasswordInitial()) {
    showContent();
  }

  bool hideNewPassword = true;
  bool hideRePassword = true;
  bool hideOldPassword = true;
  bool haveBtnShowHideOldPw = false;
  bool haveBtnShowHideNewPw = false;
  bool haveBtnShowHideReNewPw = false;

  bool statusChangePassword = false;

  AuthenticationDomainAmRepo get _authenAMRepo => Get.find();

  BehaviorSubject<bool> checkEnableButton = BehaviorSubject.seeded(false);

  void addEnableButton({required bool value}) {
    checkEnableButton.sink.add(value);
  }

  AuthenticationRepository get _repo => Get.find();

  final LoginFeatTokenRequest requestLogin = LoginFeatTokenRequest();

  static const String GRAND_TYPE_GET_TOKEN = 'password';

  Future<bool> loginAndSaveUserInfo({
    String grandType = GRAND_TYPE_GET_TOKEN,
    required String password,
  }) async {
    String tmpToken = '';
    String tmpRefreshToken = '';
    final XacThucTKRequest xacThucTKRequest = XacThucTKRequest();
    final String email = getEmail().substring(0, getEmail().length - 10);

    showLoading();

    requestLogin.grant_type = grandType;
    requestLogin.password = password;
    requestLogin.username = email;
    final result = await _authenAMRepo.getTokenLogin(
      requestLogin,
    );
    bool resultLogin = false;
    await result.when(
      success: (response) async {
        tmpToken = response.accessToken ?? '';
        tmpRefreshToken = response.refreshToken ?? '';
        await Future.wait([
          PrefsService.saveToken(response.accessToken ?? ''),
          PrefsService.saveRefreshToken(
            response.refreshToken ?? '',
          ),
          PrefsService.saveLoginUserName(email),
          PrefsService.savePasswordUser(password),
        ]);

        resultLogin = true;
      },
      error: (error) {
        tmpToken = '';
        tmpRefreshToken = '';
        resultLogin = false;
      },
    );

    xacThucTKRequest.token = tmpToken;
    final resultXacThucTK =
        await _repo.xacThucTaiKhoan(request: xacThucTKRequest);
    resultXacThucTK.when(
      success: (response) {
        if (response.errorCode == 200) {
          PrefsService.saveRefreshToken(
            tmpRefreshToken,
          );
          PrefsService.saveNameUser(response.body?.fullName ?? '');
          resultLogin = true;
        } else {
          resultLogin = false;
        }
        showContent();
      },
      error: (error) {
        resultLogin = false;
        showContent();
      },
    );

    return resultLogin;
  }

  String getEmail() {
    String result = '';
    if (PrefsService.getEmailUser().isNotEmpty) {
      if (PrefsService.getEmailUser().contains('@gmail.com')) {
        result = PrefsService.getEmailUser();
      } else {
        result = '${PrefsService.getEmailUser()}@gmail.com';
      }
    }
    return result;
  }

  Future<void> doiMatKhau({
    required String oldPassword,
    required String newPassword,
  }) async {
    showLoading();
    bool existFunc = false;
    final result = await _repo.doiMatKhau(
      request: DoiMatKhauRequest(
        oldPassword: oldPassword,
        newPassword: newPassword,
        confirmPassword: newPassword,
        // email: PrefsService.getEmailUser(),
        email: getEmail(),
      ),
    );
    result.when(
      success: (response) {
        if (response.errorCode != 200) {
          emit(
            ChangePasswordFail(
              errorWhenChangePW: response.message ?? '',
            ),
          );
          showContent();
          existFunc = true;
          return;
        }
      },
      error: (error) {
        showError();
        existFunc = true;
        return;
      },
    );
    if (existFunc) return;

    final resultLogin = await loginAndSaveUserInfo(
      password: newPassword,
    );
    if (resultLogin) {
      emit(ChangePasswordSuccess());
      await PrefsService.savePasswordUser(newPassword);
      showContent();
    } else {
      emit(
        ChangePasswordFail(
          errorWhenChangePW: S.current.su_co_xay_ra,
        ),
      );
    }
  }
}
