part of 'change_password_cubit.dart';

@immutable
abstract class ChangePasswordState {}

class ChangePasswordInitial extends ChangePasswordState {}

class ChangePasswordSuccess extends ChangePasswordState {}

class ChangePasswordFail extends ChangePasswordState {
  final String errorWhenChangePW;

  ChangePasswordFail({required this.errorWhenChangePW});
}
