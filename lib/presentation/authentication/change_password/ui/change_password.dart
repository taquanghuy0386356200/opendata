import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/authentication/change_password/bloc/change_password_cubit.dart';
import 'package:bnv_opendata/presentation/xelauikit_screens/main_screen.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/utils/string_ext.dart';
import 'package:bnv_opendata/widgets/button/button_box_shadow.dart';
import 'package:bnv_opendata/widgets/dialog/dialog_popular.dart';
import 'package:bnv_opendata/widgets/header_login_module/header_popular.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  State<ChangePasswordScreen> createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  final _oldPasswordController = TextEditingController();
  final _newPasswordController = TextEditingController();
  final _reNewPasswordController = TextEditingController();
  final _keyForm = GlobalKey<FormState>();
  final cubit = ChangePasswordCubit();

  // final _keyHome = GlobalKey<HomeScreenState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: StateStreamLayout(
        retry: () {
          closeScreen(context);
        },
        error: AppException('', S.current.something_went_wrong),
        stream: cubit.stateStream,
        textEmpty: '',
        child: BlocListener<ChangePasswordCubit, ChangePasswordState>(
          bloc: cubit,
          listener: (context, state) {
            if (state is ChangePasswordFail) {
              showDialogPopular(
                context,
                defaultPress: () {},
                haveButton: false,
                imageContent: ImageAssets.icWarningOrange,
                timeShowDialog: 2,
                contentDialog: Container(
                  margin: const EdgeInsets.only(top: 32),
                  child: Text(
                    state.errorWhenChangePW,
                    style: XelaTextStyle.Xela18Medium.copyWith(
                      color: color364564,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                titleDefaultBtn: '',
              );
            } else if (state is ChangePasswordSuccess) {
              showDialogPopular(
                context,
                defaultPress: () {},
                timeShowDialog: 2,
                haveButton: false,
                imageContent: ImageAssets.icChangePWSuccess,
                contentDialog: Container(
                  margin: const EdgeInsets.only(top: 32),
                  child: Text(
                    S.current.cap_nhat_mat_khau_thanh_cong,
                    style: XelaTextStyle.Xela18Medium.copyWith(
                      color: color364564,
                    ),
                  ),
                ),
                titleDefaultBtn: '',
              ).then(
                (value) => {
                  Navigator.pushAndRemoveUntil<dynamic>(
                    context,
                    MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) => const MainScreen(),
                    ),
                    (route) => false,
                  )
                },
              );
            } else {}
          },
          child: KeyboardDismisser(
            child: SingleChildScrollView(
              child: Form(
                key: _keyForm,
                child: Column(
                  children: [
                    const HeaderLoginPopular(
                      image: ImageAssets.bgChangePassWord,
                      heightPopular: 123,
                      widthPopular: 231,
                      haveBackBtn: true,
                    ),
                    spaceH50,
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            S.current.doi_mat_khau,
                            style: XelaTextStyle.Xela32Bold.copyWith(
                              color: color364564,
                            ),
                          ),
                          spaceH12,
                          Text(
                            S.current.vui_long_dat_lai_mat_khau_moi,
                            style: XelaTextStyle.Xela14Regular.copyWith(
                              color: colorA2AEBD,
                            ),
                          ),
                          spaceH40,
                          XelaTextFieldCustom(
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                RegExp(r'\s'),
                              ),
                            ],
                            maxLine: 1,
                            maxLength: 15,
                            textEditingController: _oldPasswordController,
                            placeholder: S.current.mat_khau_cu,
                            borderDefaultColor: colorE2E8F0,
                            secureField: cubit.hideOldPassword,
                            placeholderColor: color667793,
                            validate: (value) {
                              if ((value ?? '').trim().isEmpty) {
                                return S.current.vui_long_nhap_mat_khau_cu;
                              } else {
                                return null;
                              }
                            },
                            onChange: (value) {
                              setState(() {
                                cubit.haveBtnShowHideOldPw = value.isNotEmpty;
                              });

                              cubit.addEnableButton(
                                value:
                                    _keyForm.currentState?.validate() ?? false,
                              );
                            },
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icPassword,
                              ),
                            ),
                            rightIcon: cubit.haveBtnShowHideOldPw
                                ? GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        cubit.hideOldPassword =
                                            !cubit.hideOldPassword;
                                      });
                                    },
                                    child: SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: cubit.hideOldPassword
                                          ? Image.asset(ImageAssets.icEye)
                                          : Image.asset(ImageAssets.icHideEye),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                          ),
                          spaceH16,
                          XelaTextFieldCustom(
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                RegExp(r'\s'),
                              ),
                            ],
                            maxLength: 15,
                            maxLine: 1,
                            textEditingController: _newPasswordController,
                            placeholder: S.current.mat_khau_moi,
                            borderDefaultColor: colorE2E8F0,
                            secureField: cubit.hideNewPassword,
                            placeholderColor: color667793,
                            validate: (value) {
                              if ((value ?? '').isEmpty) {
                                return S.current.vui_long_nhap_mat_khau_moi;
                              } else {
                                return (value ?? '').checkPassWord();
                              }
                            },
                            onChange: (value) {
                              setState(() {
                                cubit.haveBtnShowHideNewPw = value.isNotEmpty;
                              });

                              cubit.addEnableButton(
                                value:
                                    _keyForm.currentState?.validate() ?? false,
                              );
                            },
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icPassword,
                              ),
                            ),
                            rightIcon: cubit.haveBtnShowHideNewPw
                                ? GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        cubit.hideNewPassword =
                                            !cubit.hideNewPassword;
                                      });
                                    },
                                    child: SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: cubit.hideNewPassword
                                          ? Image.asset(ImageAssets.icEye)
                                          : Image.asset(ImageAssets.icHideEye),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                          ),
                          spaceH16,
                          XelaTextFieldCustom(
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                RegExp(r'\s'),
                              ),
                            ],
                            maxLine: 1,
                            maxLength: 15,
                            textEditingController: _reNewPasswordController,
                            placeholder: S.current.nhap_lai_mat_khau,
                            borderDefaultColor: colorE2E8F0,
                            secureField: cubit.hideRePassword,
                            placeholderColor: color667793,
                            onChange: (value) {
                              setState(() {
                                cubit.haveBtnShowHideReNewPw = value.isNotEmpty;
                              });
                              cubit.addEnableButton(
                                value:
                                    _keyForm.currentState?.validate() ?? false,
                              );
                            },
                            validate: (value) {
                              if ((value ?? '').trim().isEmpty) {
                                return S.current.vui_long_nhap +
                                    S.current.lai_mat_khau;
                              } else if ((value ?? '') !=
                                  _newPasswordController.text) {
                                return S.current.mat_khau_khong_giong;
                              } else {
                                return null;
                              }
                            },
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icPassword,
                              ),
                            ),
                            rightIcon: cubit.haveBtnShowHideReNewPw
                                ? GestureDetector(
                                    onTap: () {
                                      setState(() {
                                        cubit.hideRePassword =
                                            !cubit.hideRePassword;
                                      });
                                    },
                                    child: SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: cubit.hideRePassword
                                          ? Image.asset(ImageAssets.icEye)
                                          : Image.asset(ImageAssets.icHideEye),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                          ),
                          spaceH20,
                          StreamBuilder<bool>(
                            stream: cubit.checkEnableButton.stream,
                            builder: (context, snapshot) {
                              final data = snapshot.data ?? false;
                              return ButtonBoxShadow(
                                blurRadius: 10,
                                colorBoxShadow: colorE26F2C.withOpacity(0.25),
                                offSetY: 4,
                                offSetX: 0,
                                child: XelaButton(
                                  onPressed: () async {
                                    if ((_keyForm.currentState?.validate() ??
                                            false) &&
                                        data) {
                                      await cubit.doiMatKhau(
                                        oldPassword:
                                            _oldPasswordController.text,
                                        newPassword:
                                            _reNewPasswordController.text,
                                      );
                                    }
                                  },
                                  text: S.current.xac_nhan,
                                  background:
                                      data ? colorE26F2C : colorE26F2C25,
                                  textStyle:
                                      XelaTextStyle.Xela16Medium.copyWith(
                                    color: Colors.white,
                                  ),
                                  autoResize: false,
                                ),
                              );
                            },
                          ),
                          spaceH30,
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
