import 'dart:convert';

import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/authentication/register/bloc/register_screen_cubit.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/utils/string_ext.dart';
import 'package:bnv_opendata/widgets/dialog/dialog_popular.dart';
import 'package:bnv_opendata/widgets/header_login_module/header_popular.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final _keyForm = GlobalKey<FormState>();
  late RegisterScreenCubit cubit;
  TextEditingController fullNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController capchaController = TextEditingController();
  TextEditingController rePasswordController = TextEditingController();

  @override
  void initState() {
    super.initState();
    cubit = RegisterScreenCubit();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: StateStreamLayout(
        retry: () {
          closeScreen(context);
        },
        error: AppException('', S.current.something_went_wrong),
        stream: cubit.stateStream,
        textEmpty: '',
        child: BlocListener<RegisterScreenCubit, RegisterScreenState>(
          bloc: cubit,
          listener: (context, state) {
            if (state is RegisterScreenSuccess) {
              showDialogPopular(
                context,
                defaultPress: () {
                  closeScreen(context);
                  closeScreen(context);
                },
                imageContent: ImageAssets.icTickOrange,
                contentDialog: Column(
                  children: [
                    Text(
                      S.current.thong_bao,
                      style: XelaTextStyle.Xela18Medium.copyWith(
                        color: color364564,
                      ),
                    ),
                    spaceH14,
                    Text(
                      S.current.cam_on_ban_da_dang_ky,
                      style: XelaTextStyle.Xela14Regular.copyWith(
                        color: color667793,
                      ),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
                titleDefaultBtn: S.current.dong,
              );
            } else if (state is RegisterScreenFail) {
              showDialogPopular(
                context,
                defaultPress: () {},
                haveButton: false,
                imageContent: ImageAssets.icWarningOrange,
                timeShowDialog: 2,
                contentDialog: Container(
                  margin: const EdgeInsets.only(top: 32),
                  child: Text(
                    state.errorMessage,
                    style: XelaTextStyle.Xela18Medium.copyWith(
                      color: color364564,
                    ),
                  ),
                ),
                titleDefaultBtn: '',
              );
            }
          },
          child: KeyboardDismisser(
            child: SingleChildScrollView(
              child: Form(
                key: _keyForm,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const HeaderLoginPopular(
                      haveBackBtn: true,
                      image: ImageAssets.bgRegister,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            S.current.xin_chao,
                            style: XelaTextStyle.Xela24Bold.copyWith(
                              color: color364564,
                            ),
                          ),
                          spaceH12,
                          Text(
                            S.current.dang_ky_tai_khoan_de_truy_cap_du_lieu,
                            style: XelaTextStyle.Xela14Regular.copyWith(
                              color: colorA2AEBD,
                            ),
                          ),
                          spaceH40,
                          XelaTextFieldCustom(
                            textEditingController: fullNameController,
                            placeholder: S.current.ho_va_ten,
                            borderDefaultColor: colorE2E8F0,
                            maxLine: 1,
                            placeholderColor: color667793,
                            maxLength: 255,
                            rightIcon: cubit.showClearFullNameButton
                                ? GestureDetector(
                                    onTap: () {
                                      setState(() {});
                                      fullNameController.clear();
                                      cubit.showClearFullNameButton = false;
                                    },
                                    child: SizedBox(
                                      height: 20,
                                      width: 20,
                                      child: Image.asset(ImageAssets.icClear),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icUser,
                              ),
                            ),
                            onChange: (value) {
                              if (value.isEmpty) {
                                cubit.showClearFullNameButton = false;
                                setState(() {});
                              } else {
                                cubit.showClearFullNameButton = true;
                                setState(() {});
                              }
                            },
                            validate: (value) {
                              return (value ?? '')
                                  .checkTruongNull(' họ và tên');
                            },
                          ),
                          spaceH16,
                          XelaTextFieldCustom(
                            textEditingController: emailController,
                            placeholder: S.current.email,
                            maxLine: 1,
                            borderDefaultColor: colorE2E8F0,
                            placeholderColor: color667793,
                            maxLength: 255,
                            rightIcon: cubit.showClearEmailButton
                                ? GestureDetector(
                                    onTap: () {
                                      setState(() {});
                                      emailController.clear();
                                      cubit.showClearEmailButton = false;
                                    },
                                    child: SizedBox(
                                      height: 20,
                                      width: 20,
                                      child: Image.asset(ImageAssets.icClear),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icEmail,
                              ),
                            ),
                            onChange: (value) {
                              if (value.isEmpty) {
                                cubit.showClearEmailButton = false;
                                setState(() {});
                              } else {
                                cubit.showClearEmailButton = true;
                                setState(() {});
                              }
                            },
                            validate: (value) {
                              if ((value ?? '').isNotEmpty) {
                                return (value ?? '').trim().validateEmail();
                              } else {
                                return (value ?? '').checkTruongNull(' email');
                              }
                            },
                          ),
                          spaceH16,
                          Stack(
                            children: [
                              XelaTextFieldCustom(
                                inputFormatters: [
                                  FilteringTextInputFormatter.deny(
                                    RegExp(r'\s'),
                                  ),
                                ],
                                textEditingController: capchaController,
                                placeholder: S.current.ma_capcha,
                                maxLine: 1,
                                borderDefaultColor: colorE2E8F0,
                                placeholderColor: color667793,
                                maxLength: 255,
                                rightIcon: GestureDetector(
                                  onTap: () {
                                    cubit.getCapcha();
                                  },
                                  child: SizedBox(
                                    height: 20,
                                    width: 20,
                                    child:
                                        Image.asset(ImageAssets.icChangeCapcha),
                                  ),
                                ),
                                leftIcon: Container(
                                  height: 20,
                                  width: 20,
                                  margin: const EdgeInsets.only(right: 12),
                                  child: Image.asset(
                                    ImageAssets.icCapcha,
                                  ),
                                ),
                                onChange: (value) {},
                                validate: (value) {
                                  if ((value ?? '').isEmpty) {
                                    return S.current.vui_long_nhap_ma_capcha;
                                  } else {
                                    return null;
                                  }
                                },
                              ),
                              StreamBuilder<String>(
                                stream: cubit.capchaImageBHVSJ.stream,
                                builder: (context, snapshot) {
                                  final data = snapshot.data ?? '';
                                  return data.isNotEmpty
                                      ? Positioned(
                                          right: 40,
                                          top: 8,
                                          child: imageFromBase64String(data),
                                        )
                                      : const SizedBox.shrink();
                                },
                              ),
                            ],
                          ),
                          spaceH16,
                          XelaTextFieldCustom(
                            maxLine: 1,
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                RegExp(r'\s'),
                              ),
                            ],
                            textEditingController: passwordController,
                            placeholder: S.current.mat_khau,
                            borderDefaultColor: colorE2E8F0,
                            maxLength: 15,
                            secureField: cubit.hidePassword,
                            placeholderColor: color667793,
                            onChange: (value) {
                              if (value.isEmpty) {
                                cubit.haveBtnShowHidePw = false;
                                setState(() {});
                              } else {
                                cubit.haveBtnShowHidePw = true;
                                setState(() {});
                              }
                            },
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icPassword,
                              ),
                            ),
                            rightIcon: cubit.haveBtnShowHidePw
                                ? GestureDetector(
                                    onTap: () {
                                      if (cubit.hidePassword) {
                                        cubit.hidePassword = false;
                                        setState(() {});
                                      } else {
                                        cubit.hidePassword = true;
                                        setState(() {});
                                      }
                                    },
                                    child: SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: cubit.hidePassword
                                          ? Image.asset(ImageAssets.icEye)
                                          : Image.asset(ImageAssets.icHideEye),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            validate: (value) {
                              return (value ?? '').checkPassWord();
                            },
                          ),
                          spaceH16,
                          XelaTextFieldCustom(
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                RegExp(r'\s'),
                              ),
                            ],
                            maxLine: 1,
                            textEditingController: rePasswordController,
                            placeholder: S.current.nhap_lai_mat_khau,
                            borderDefaultColor: colorE2E8F0,
                            secureField: cubit.hideRePassword,
                            maxLength: 15,
                            placeholderColor: color667793,
                            onChange: (value) {
                              if (value.isEmpty) {
                                cubit.haveBtnShowHideRePw = false;
                                setState(() {});
                              } else {
                                cubit.haveBtnShowHideRePw = true;
                                setState(() {});
                              }
                            },
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icPassword,
                              ),
                            ),
                            rightIcon: cubit.haveBtnShowHideRePw
                                ? GestureDetector(
                                    onTap: () {
                                      if (cubit.hideRePassword) {
                                        cubit.hideRePassword = false;
                                        setState(() {});
                                      } else {
                                        cubit.hideRePassword = true;
                                        setState(() {});
                                      }
                                    },
                                    child: SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: cubit.hideRePassword
                                          ? Image.asset(ImageAssets.icEye)
                                          : Image.asset(ImageAssets.icHideEye),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            validate: (value) {
                              if ((value ?? '').trim().isEmpty) {
                                return S.current.vui_long_nhap +
                                    S.current.lai_mat_khau;
                              } else if ((value ?? '') !=
                                  passwordController.text) {
                                return S.current.mat_khau_khong_giong;
                              } else {
                                return null;
                              }
                            },
                          ),
                          spaceH20,
                          DecoratedBox(
                            decoration: BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: colorE26F2C.withOpacity(0.25),
                                  offset: const Offset(
                                    0,
                                    4,
                                  ),
                                  blurRadius: 10,
                                )
                              ],
                            ),
                            child: XelaButton(
                              onPressed: () async {
                                if (_keyForm.currentState?.validate() ??
                                    false) {
                                  await showDialogPopular(context,
                                      defaultPress: () async {
                                        closeScreen(context);
                                        await cubit.postToRegister(
                                          password: passwordController.text,
                                          fullName:
                                              fullNameController.text.trim(),
                                          email: emailController.text.trim(),
                                          capchaCode:
                                              capchaController.text.trim(),
                                        );
                                      },
                                      isDoubleBtn: true,
                                      leftPress: () {
                                        closeScreen(context);
                                      },
                                      imageContent: ImageAssets.icTickOrange,
                                      contentDialog: Column(
                                        children: [
                                          Text(
                                            S.current.xac_thuc_tk,
                                            style: XelaTextStyle.Xela18Medium
                                                .copyWith(
                                              color: color364564,
                                            ),
                                          ),
                                          spaceH14,
                                          Text(
                                            S.current.bac_xac_thuc_dang_ky_tk,
                                            style: XelaTextStyle.Xela14Regular
                                                .copyWith(
                                              color: color667793,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          spaceH14,
                                          rowWidgetDetail(
                                            S.current.ho_va_ten,
                                            fullNameController.text.trim(),
                                          ),
                                          spaceH8,
                                          rowWidgetDetail(
                                            S.current.email,
                                            emailController.text.trim(),
                                          ),
                                        ],
                                      ),
                                      titleDefaultBtn: S.current.xac_thuc,
                                      titleLeftBtn: S.current.huy);
                                } else {}
                              },
                              text: S.current.dang_ky,
                              background: colorE26F2C,
                              textStyle: XelaTextStyle.Xela16Medium.copyWith(
                                color: Colors.white,
                              ),
                              autoResize: false,
                            ),
                          ),
                          spaceH20,
                          XelaButton(
                            onPressed: () {
                              closeScreen(context);
                            },
                            background: Colors.transparent,
                            text: S.current.dang_nhap,
                            textStyle: XelaTextStyle.Xela16Medium.copyWith(
                              color: colorE26F2C,
                            ),
                            autoResize: false,
                          ),
                          spaceH30,
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget rowWidgetDetail(
    String title,
    String content,
  ) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Expanded(
          flex: 3,
          child: Text(
            title,
            style: XelaTextStyle.Xela14Regular.copyWith(color: color667793),
          ),
        ),
        spaceW14,
        Expanded(
          flex: 8,
          child: Text(
            content,
            style: XelaTextStyle.Xela14Medium.copyWith(color: color364564),
          ),
        )
      ],
    );
  }

  Image imageFromBase64String(String base64String) {
    return Image.memory(
      base64Decode(base64String),
      fit: BoxFit.fill,
    );
  }
}
