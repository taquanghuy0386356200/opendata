part of 'register_screen_cubit.dart';

@immutable
abstract class RegisterScreenState {
  const RegisterScreenState();
}

class RegisterScreenInitial extends RegisterScreenState {

}

class RegisterScreenFail extends RegisterScreenState {
  final String errorMessage;

  const RegisterScreenFail(this.errorMessage);

}

class RegisterScreenSuccess extends RegisterScreenState {

}
