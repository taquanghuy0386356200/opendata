import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_repository.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'register_screen_state.dart';

class RegisterScreenCubit extends BaseCubit<RegisterScreenState> {
  RegisterScreenCubit() : super(RegisterScreenInitial()) {
    showContent();
    getCapcha();
  }

  bool hidePassword = true;
  bool hideRePassword = true;
  bool showClearFullNameButton = false;
  bool showClearEmailButton = false;
  bool haveBtnShowHidePw = false;
  bool haveBtnShowHideRePw = false;

  AuthenticationRepository get _authenRepo => Get.find();

  BehaviorSubject<String> capchaImageBHVSJ = BehaviorSubject.seeded('');

  String capChatImage = '';
  String capChatID = '';

  Future<void> getCapcha() async {
    showLoading();
    final result = await _authenRepo.getCapchaCode();
    result.when(
      success: (response) {
        capChatID = response.body?.id ?? '';
        capChatImage = response.body?.capcha ?? '';
        capchaImageBHVSJ.sink.add(capChatImage);
        showContent();
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> postToRegister({
    required String email,
    required String fullName,
    required String password,
    required String capchaCode,
  }) async {
    showLoading();
    final result = await _authenRepo.register(
      email: email,
      fullName: fullName,
      password: password,
      capchaCode: capchaCode,
      capchaID: capChatID,
    );
    result.when(
      success: (response) {
        if (response.errorCode == 200) {
          emit(RegisterScreenSuccess());
        } else {
          emit(
            RegisterScreenFail(response.message ?? ''),
          );
        }
        showContent();
      },
      error: (error) {
        showError();
      },
    );
  }
}
