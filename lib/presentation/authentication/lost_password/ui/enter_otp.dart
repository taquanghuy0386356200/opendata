import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/authentication/lost_password/bloc/lost_password_cubit.dart';
import 'package:bnv_opendata/presentation/authentication/lost_password/ui/set_password.dart';
import 'package:bnv_opendata/utils/app_utils.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/widgets/button/button_box_shadow.dart';
import 'package:bnv_opendata/widgets/dialog/dialog_popular.dart';
import 'package:bnv_opendata/widgets/header_login_module/header_popular.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

class EnterOTP extends StatefulWidget {
  EnterOTP({
    Key? key,
    required this.cubit,
  }) : super(key: key);
  LostPasswordCubit cubit;

  @override
  State<EnterOTP> createState() => _EnterOTPState();
}

class _EnterOTPState extends State<EnterOTP> {
  final TextEditingController _otpController = TextEditingController();
  final _keyForm = GlobalKey<FormState>();
  late LostPasswordCubit cubit;

  @override
  void initState() {
    super.initState();
    cubit = widget.cubit;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: StateStreamLayout(
        retry: () {
          closeScreen(context);
          closeScreen(context);
        },
        error: AppException('', S.current.something_went_wrong),
        stream: cubit.stateStream,
        textEmpty: '',
        child: BlocListener<LostPasswordCubit, LostPasswordState>(
          bloc: cubit,
          listener: (context, state) {
            if (state is ConfirmOTPFail) {
              showDialogPopular(
                context,
                defaultPress: () {},
                haveButton: false,
                imageContent: ImageAssets.icWarningOrange,
                timeShowDialog: 2,
                contentDialog: Container(
                  margin: const EdgeInsets.only(top: 32),
                  child: Text(
                    state.error ?? '',
                    style: XelaTextStyle.Xela18Medium.copyWith(
                      color: color364564,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                titleDefaultBtn: '',
              );
            } else if (state is ConfirmOTPSuccess) {
              pushAndReplacement(context, SetPasswordScreen(cubit: cubit));
            }
          },
          child: KeyboardDismisser(
            child: SingleChildScrollView(
              child: Form(
                key: _keyForm,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const HeaderLoginPopular(
                      image: ImageAssets.bgCreatePassWord,
                      haveBackBtn: true,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            S.current.nhap_ma_xac_nhan,
                            style: XelaTextStyle.Xela32Bold.copyWith(
                              color: color364564,
                            ),
                          ),
                          spaceH12,
                          Text(
                            S.current.vui_long_nhap_ma_xac_nhan,
                            style: XelaTextStyle.Xela14Regular.copyWith(
                              color: colorA2AEBD,
                            ),
                          ),
                          spaceH40,
                          XelaTextFieldCustom(
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                RegExp(r'\s'),
                              ),
                            ],
                            textEditingController: _otpController,
                            placeholder: S.current.nhap_ma_xac_nhan,
                            borderDefaultColor: colorE2E8F0,
                            placeholderColor: color667793,
                            maxLength: 6,
                            rightIcon: cubit.showClearButton
                                ? GestureDetector(
                                    onTap: () {
                                      setState(() {});
                                      _otpController.clear();
                                      cubit.checkEnableButtonOTP(
                                        value: false,
                                      );
                                      cubit.showClearButton = false;
                                    },
                                    child: SizedBox(
                                      height: 20,
                                      width: 20,
                                      child: Image.asset(ImageAssets.icClear),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icOTP,
                              ),
                            ),
                            onChange: (value) {
                              if (value.isEmpty) {
                                cubit.showClearButtonOTP = false;
                                cubit.checkEnableButtonOTP(
                                  value: false,
                                );
                                setState(() {});
                              } else {
                                cubit.showClearButtonOTP = true;
                                setState(() {});
                              }
                              if (_keyForm.currentState?.validate() ?? false) {
                                cubit.checkEnableButtonOTP(
                                  value: true,
                                );
                              } else {
                                cubit.checkEnableButtonOTP(
                                  value: false,
                                );
                              }
                            },
                            validate: (value) {
                              if ((value ?? '').isEmpty) {
                                return S.current.vui_long_nhap_ma_xac_nhan;
                              } else if (!isValidOTP(value ?? '')) {
                                return S
                                    .current.ma_xac_nhan_khong_dung_dinh_dang;
                              }
                              return null;
                            },
                          ),
                          spaceH20,
                          StreamBuilder<bool>(
                            initialData: false,
                            stream: cubit.checkDisableButtonOTP.stream,
                            builder: (context, snapshot) {
                              final data = snapshot.data ?? false;
                              return ButtonBoxShadow(
                                blurRadius: 10,
                                colorBoxShadow: colorE26F2C.withOpacity(0.25),
                                offSetY: 4,
                                offSetX: 0,
                                child: XelaButton(
                                  onPressed: () async {
                                    if ((_keyForm.currentState?.validate() ??
                                            false) &&
                                        data) {
                                      await cubit.handleOTP(
                                        otpID: cubit.otpId,
                                        otp: _otpController.text.trim(),
                                      );
                                    } else {}
                                  },
                                  text: S.current.tiep_tuc,
                                  background:
                                      data ? colorE26F2C : colorE26F2C25,
                                  textStyle:
                                      XelaTextStyle.Xela16Medium.copyWith(
                                    color: Colors.white,
                                  ),
                                  autoResize: false,
                                ),
                              );
                            },
                          ),
                          spaceH30,
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
