import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/authentication/lost_password/bloc/lost_password_cubit.dart';
import 'package:bnv_opendata/presentation/xelauikit_screens/main_screen.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/utils/string_ext.dart';
import 'package:bnv_opendata/widgets/button/button_box_shadow.dart';
import 'package:bnv_opendata/widgets/dialog/dialog_popular.dart';
import 'package:bnv_opendata/widgets/header_login_module/header_popular.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

class SetPasswordScreen extends StatefulWidget {
  const SetPasswordScreen({
    Key? key,
    required this.cubit,
  }) : super(key: key);
  final LostPasswordCubit cubit;

  @override
  State<SetPasswordScreen> createState() => _SetPasswordScreenState();
}

class _SetPasswordScreenState extends State<SetPasswordScreen> {
  final _newPasswordController = TextEditingController();
  final _reEnterPasswordController = TextEditingController();
  final _keyForm = GlobalKey<FormState>();
  late LostPasswordCubit cubit;

  @override
  void initState() {
    super.initState();
    cubit = widget.cubit;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: StateStreamLayout(
        retry: () {
          closeScreen(context);
          closeScreen(context);
        },
        error: AppException('', S.current.something_went_wrong),
        stream: cubit.stateStream,
        textEmpty: '',
        child: BlocListener<LostPasswordCubit, LostPasswordState>(
          bloc: widget.cubit,
          listener: (context, state) {
            if (state is ChangePasswordSuccess) {
              showDialogPopular(
                context,
                defaultPress: () {},
                haveButton: false,
                timeShowDialog: 2,
                imageContent: ImageAssets.icChangePWSuccess,
                contentDialog: Text(
                  S.current.cap_nhat_mat_khau_thanh_cong,
                  style: XelaTextStyle.Xela18Medium.copyWith(
                    color: color364564,
                  ),
                  textAlign: TextAlign.center,
                ),
                titleDefaultBtn: '',
              ).then(
                (value) => {
                  Navigator.pushAndRemoveUntil<dynamic>(
                    context,
                    MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) => const MainScreen(),
                    ),
                    (route) => false,
                    //if you want to disable back feature set to false
                  )
                },
              );
            } else if (state is ChangePasswordFail) {
              showDialogPopular(
                context,
                defaultPress: () {},
                haveButton: false,
                imageContent: ImageAssets.icWarningOrange,
                timeShowDialog: 2,
                contentDialog: Container(
                  margin: const EdgeInsets.only(top: 32),
                  child: Text(
                    state.error ?? '',
                    style: XelaTextStyle.Xela18Medium.copyWith(
                      color: color364564,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                titleDefaultBtn: '',
              );
            }
          },
          child: KeyboardDismisser(
            child: SingleChildScrollView(
              child: Form(
                key: _keyForm,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const HeaderLoginPopular(
                      image: ImageAssets.bgCreatePassWord,
                      haveBackBtn: true,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            S.current.thiet_lap_mat_khau,
                            style: XelaTextStyle.Xela32Bold.copyWith(
                              color: color364564,
                            ),
                          ),
                          spaceH12,
                          Text(
                            S.current.vui_long_dat_lai_mat_khau,
                            style: XelaTextStyle.Xela14Regular.copyWith(
                              color: colorA2AEBD,
                            ),
                          ),
                          spaceH40,
                          XelaTextFieldCustom(
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                RegExp(r'\s'),
                              ),
                            ],
                            maxLength: 15,
                            maxLine: 1,
                            textEditingController: _newPasswordController,
                            placeholder: S.current.mat_khau_moi,
                            borderDefaultColor: colorE2E8F0,
                            secureField: cubit.hidePassword,
                            placeholderColor: color667793,
                            onChange: (value) {
                              if (value.isEmpty) {
                                cubit.haveBtnShowHidePw = false;
                                setState(() {});
                              } else {
                                cubit.haveBtnShowHidePw = true;
                                setState(() {});
                              }
                              if (_keyForm.currentState?.validate() ?? false) {
                                cubit.checkEnableButtonChangePW(value: true);
                              } else {
                                cubit.checkEnableButtonChangePW(value: false);
                              }
                            },
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icPassword,
                              ),
                            ),
                            rightIcon: cubit.haveBtnShowHidePw
                                ? GestureDetector(
                                    onTap: () {
                                      if (cubit.hidePassword) {
                                        cubit.hidePassword = false;
                                        setState(() {});
                                      } else {
                                        cubit.hidePassword = true;
                                        setState(() {});
                                      }
                                    },
                                    child: SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: cubit.hidePassword
                                          ? Image.asset(ImageAssets.icEye)
                                          : Image.asset(ImageAssets.icHideEye),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            validate: (value) {
                              if ((value ?? '').isEmpty) {
                                return S.current.vui_long_nhap_mat_khau_moi;
                              } else {
                                return (value ?? '').checkPassWord();
                              }
                            },
                          ),
                          spaceH16,
                          XelaTextFieldCustom(
                            inputFormatters: [
                              FilteringTextInputFormatter.deny(
                                RegExp(r'\s'),
                              ),
                            ],
                            maxLine: 1,
                            maxLength: 15,
                            textEditingController: _reEnterPasswordController,
                            placeholder: S.current.nhap_lai_mat_khau,
                            borderDefaultColor: colorE2E8F0,
                            secureField: cubit.hideRePassword,
                            placeholderColor: color667793,
                            onChange: (value) {
                              if (value.isEmpty) {
                                cubit.haveBtnShowHideRePw = false;
                                setState(() {});
                              } else {
                                cubit.haveBtnShowHideRePw = true;
                                setState(() {});
                              }
                              if (_keyForm.currentState?.validate() ?? false) {
                                cubit.checkEnableButtonChangePW(value: true);
                              } else {
                                cubit.checkEnableButtonChangePW(value: false);
                              }
                            },
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icPassword,
                              ),
                            ),
                            rightIcon: cubit.haveBtnShowHideRePw
                                ? GestureDetector(
                                    onTap: () {
                                      if (cubit.hideRePassword) {
                                        cubit.hideRePassword = false;
                                        setState(() {});
                                      } else {
                                        cubit.hideRePassword = true;
                                        setState(() {});
                                      }
                                    },
                                    child: SizedBox(
                                      width: 20,
                                      height: 20,
                                      child: cubit.hideRePassword
                                          ? Image.asset(ImageAssets.icEye)
                                          : Image.asset(ImageAssets.icHideEye),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            validate: (value) {
                              if ((value ?? '').trim().isEmpty) {
                                return S.current.vui_long_nhap_lai_mk_moi;
                              } else if ((value ?? '') !=
                                  _newPasswordController.text) {
                                return S.current.mat_khau_khong_giong;
                              } else {
                                return null;
                              }
                            },
                          ),
                          spaceH20,
                          StreamBuilder<bool>(
                            initialData: false,
                            stream: cubit.checkDisableButtonChangePW,
                            builder: (context, snapshot) {
                              final data = snapshot.data ?? false;
                              return ButtonBoxShadow(
                                blurRadius: 10,
                                colorBoxShadow: colorE26F2C.withOpacity(0.25),
                                offSetY: 4,
                                offSetX: 0,
                                child: XelaButton(
                                  onPressed: () async {
                                    if ((_keyForm.currentState?.validate() ??
                                            false) &&
                                        data) {
                                      await widget.cubit.changePassword(
                                        newPassword:
                                            _newPasswordController.text,
                                        confirmPassword:
                                            _reEnterPasswordController.text,
                                      );
                                    } else {}
                                  },
                                  text: S.current.dat_lai_mat_khau,
                                  background:
                                      data ? colorE26F2C : colorE26F2C25,
                                  textStyle:
                                      XelaTextStyle.Xela16Medium.copyWith(
                                    color: Colors.white,
                                  ),
                                  autoResize: false,
                                ),
                              );
                            },
                          ),
                          spaceH30,
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
