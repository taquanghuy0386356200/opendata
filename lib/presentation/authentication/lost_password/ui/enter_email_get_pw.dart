import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/authentication/lost_password/bloc/lost_password_cubit.dart';
import 'package:bnv_opendata/presentation/authentication/lost_password/ui/enter_otp.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/utils/string_ext.dart';
import 'package:bnv_opendata/widgets/button/button_box_shadow.dart';
import 'package:bnv_opendata/widgets/dialog/dialog_popular.dart';
import 'package:bnv_opendata/widgets/header_login_module/header_popular.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';

class LostPasswordScreen extends StatefulWidget {
  const LostPasswordScreen({Key? key}) : super(key: key);

  @override
  State<LostPasswordScreen> createState() => _LostPasswordScreenState();
}

class _LostPasswordScreenState extends State<LostPasswordScreen> {
  final TextEditingController _emailController = TextEditingController();
  late LostPasswordCubit cubit;
  final _keyForm = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    cubit = LostPasswordCubit();

    if (PrefsService.getEmailUser().isNotEmpty) {
      if (PrefsService.getEmailUser().contains('@gmail.com')) {
        _emailController.text = PrefsService.getEmailUser();
      } else {
        _emailController.text = '${PrefsService.getEmailUser()}@gmail.com';
      }
    }
    // _emailController.text = PrefsService.getEmailUser();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: StateStreamLayout(
        retry: () {
          closeScreen(context);
        },
        error: AppException('', S.current.something_went_wrong),
        stream: cubit.stateStream,
        textEmpty: '',
        child: BlocListener<LostPasswordCubit, LostPasswordState>(
          bloc: cubit,
          listener: (context, state) {
            if (state is ConfirmEmailSuccess) {
              showDialogPopular(
                context,
                defaultPress: () {
                  Navigator.pop(context);
                },
                imageContent: ImageAssets.icSearchOrange,
                contentDialog: Column(
                  children: [
                    Text(
                      S.current.thong_bao,
                      style: XelaTextStyle.Xela18Medium.copyWith(
                        color: color364564,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    spaceH14,
                    Text(
                      S.current.vui_long_kiem_tra_email,
                      style: XelaTextStyle.Xela14Regular.copyWith(
                        color: color667793,
                      ),
                      textAlign: TextAlign.center,
                    )
                  ],
                ),
                titleDefaultBtn: S.current.dong,
                haveButton: true,
              ).then(
                (_) => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => EnterOTP(
                      cubit: cubit,
                    ),
                  ),
                ),
              );
            } else if (state is ConfirmEmailError) {
              showDialogPopular(
                context,
                defaultPress: () {},
                haveButton: false,
                imageContent: ImageAssets.icWarningOrange,
                timeShowDialog: 2,
                contentDialog: Container(
                  margin: const EdgeInsets.only(top: 32),
                  child: Text(
                    state.error ?? '',
                    style: XelaTextStyle.Xela18Medium.copyWith(
                      color: color364564,
                    ),
                    textAlign: TextAlign.center,
                  ),
                ),
                titleDefaultBtn: '',
              );
            }
          },
          child: KeyboardDismisser(
            child: SingleChildScrollView(
              child: Form(
                key: _keyForm,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    const HeaderLoginPopular(
                      image: ImageAssets.bgLostPassWord,
                      haveBackBtn: true,
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            S.current.quen_mat_khau,
                            style: XelaTextStyle.Xela32Bold.copyWith(
                              color: color364564,
                            ),
                          ),
                          spaceH12,
                          Text(
                            S.current.vui_long_nhap_email_de_xac_minh_tk,
                            style: XelaTextStyle.Xela14Regular.copyWith(
                              color: colorA2AEBD,
                            ),
                          ),
                          spaceH40,
                          XelaTextFieldCustom(
                            textEditingController: _emailController,
                            placeholder: S.current.email,
                            borderDefaultColor: colorE2E8F0,
                            placeholderColor: color667793,
                            maxLength: 255,
                            rightIcon: cubit.showClearButton
                                ? GestureDetector(
                                    onTap: () {
                                      setState(() {});
                                      _emailController.clear();
                                      cubit.checkEnableButtonPopular(
                                        value: false,
                                      );
                                      cubit.showClearButton = false;
                                    },
                                    child: SizedBox(
                                      height: 20,
                                      width: 20,
                                      child: Image.asset(ImageAssets.icClear),
                                    ),
                                  )
                                : const SizedBox.shrink(),
                            leftIcon: Container(
                              height: 20,
                              width: 20,
                              margin: const EdgeInsets.only(right: 12),
                              child: Image.asset(
                                ImageAssets.icEmail,
                              ),
                            ),
                            onChange: (value) {
                              if (value.isEmpty) {
                                cubit.showClearButton = false;
                                cubit.checkEnableButtonPopular(
                                  value: false,
                                );
                                setState(() {});
                              } else {
                                cubit.showClearButton = true;
                                setState(() {});
                              }
                              if (_keyForm.currentState?.validate() ?? false) {
                                cubit.checkEnableButtonPopular(
                                  value: true,
                                );
                              } else {
                                cubit.checkEnableButtonPopular(
                                  value: false,
                                );
                              }
                            },
                            validate: (value) {
                              if ((value ?? '').isNotEmpty) {
                                return (value ?? '').trim().validateEmail();
                              } else {
                                return (value ?? '')
                                    .trim()
                                    .checkTruongNull(' email');
                              }
                            },
                          ),
                          spaceH20,
                          StreamBuilder<bool>(
                            stream: cubit.checkDisableButtonBHVSJ.stream,
                            builder: (context, snapshot) {
                              final data = snapshot.data ?? false;
                              return ButtonBoxShadow(
                                blurRadius: 10,
                                colorBoxShadow: colorE26F2C.withOpacity(0.25),
                                offSetY: 4,
                                offSetX: 0,
                                child: XelaButton(
                                  onPressed: () async {
                                    if ((_keyForm.currentState?.validate() ??
                                            false) &&
                                        data) {
                                      await cubit.confirmEmailToResetPW(
                                        email: _emailController.text.trim(),
                                      );
                                    } else {}
                                  },
                                  text: S.current.xac_nhan_tk,
                                  background:
                                      data ? colorE26F2C : colorE26F2C25,
                                  textStyle:
                                      XelaTextStyle.Xela16Medium.copyWith(
                                    color: Colors.white,
                                  ),
                                  autoResize: false,
                                ),
                              );
                            },
                          ),
                          spaceH30,
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
