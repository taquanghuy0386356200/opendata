import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/data/request/login_ft_token_request.dart';
import 'package:bnv_opendata/data/request/otp_request.dart';
import 'package:bnv_opendata/data/request/register_request.dart';
import 'package:bnv_opendata/data/request/xac_thuc_tai_khoan_request.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_domain_am_repo.dart';
import 'package:bnv_opendata/domain/repository/authentication/authentication_repository.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

import '../../../../generated/l10n.dart';

part 'lost_password_state.dart';

class LostPasswordCubit extends BaseCubit<LostPasswordState> {
  LostPasswordCubit() : super(LostPasswordInitial()) {
    showContent();
    final result = checkEnableButtonFirstTime();
    checkDisableButtonBHVSJ = BehaviorSubject.seeded(result);
  }

  AuthenticationDomainAmRepo get _authenAMRepo => Get.find();

  late BehaviorSubject<bool> checkDisableButtonBHVSJ;
  final BehaviorSubject<bool> checkDisableButtonChangePW = BehaviorSubject();
  final BehaviorSubject<bool> checkDisableButtonOTP = BehaviorSubject();

  AuthenticationRepository get _authenRepo => Get.find();

  bool showClearButton = false;
  bool showClearButtonOTP = false;
  bool hidePassword = true;
  bool hideRePassword = true;
  bool haveBtnShowHidePw = false;
  bool haveBtnShowHideRePw = false;

  void checkEnableButtonPopular({required bool value}) {
    checkDisableButtonBHVSJ.sink.add(value);
  }

  void checkEnableButtonChangePW({required bool value}) {
    checkDisableButtonChangePW.sink.add(value);
  }

  void checkEnableButtonOTP({required bool value}) {
    checkDisableButtonOTP.sink.add(value);
  }

  bool checkEnableButtonFirstTime() {
    if (PrefsService.getEmailUser().isNotEmpty) {
      return true;
    }
    return false;
  }

  String emailUser = '';
  String tokenResetPW = '';

  final LoginFeatTokenRequest requestLogin = LoginFeatTokenRequest();

  static const String GRAND_TYPE_GET_TOKEN = 'password';

  Future<bool> loginAndSaveUserInfo({
    String grandType = GRAND_TYPE_GET_TOKEN,
    String email = 'admin',
    required String password,
  }) async {
    showLoading();
    String tmpToken = '';
    String tmpRefreshToken = '';
    final XacThucTKRequest xacThucTKRequest = XacThucTKRequest();
    requestLogin.grant_type = grandType;
    requestLogin.username = email;
    requestLogin.password = password;
    final result = await _authenAMRepo.getTokenLogin(
      requestLogin,
    );
    bool resultLogin = false;
    await result.when(
      success: (response) async {
        tmpToken = response.accessToken ?? '';
        tmpRefreshToken = response.refreshToken ?? '';

        await Future.wait([
          PrefsService.saveToken(response.accessToken ?? ''),
          PrefsService.saveRefreshToken(
            response.refreshToken ?? '',
          ),
        ]);
      },
      error: (error) {
        tmpToken = '';
        tmpRefreshToken = '';
        showContent();
      },
    );

    xacThucTKRequest.token = tmpToken;
    final resultXacThucTK =
        await _authenRepo.xacThucTaiKhoan(request: xacThucTKRequest);
    await resultXacThucTK.when(
      success: (response) async {
        if (response.errorCode == 200) {
          await Future.wait([
            PrefsService.saveLoginUserName(email),
            PrefsService.saveNameUser(response.body?.fullName ?? ''),
            PrefsService.savePasswordUser(password),
          ]);
          resultLogin = true;
        } else {
          resultLogin = false;
        }
        showContent();
      },
      error: (error) {
        resultLogin = false;
        showContent();
      },
    );

    return resultLogin;
  }

  Future<void> changePassword({
    required String newPassword,
    required String confirmPassword,
  }) async {
    showLoading();
    bool existFunc = false;
    final result = await _authenRepo.thietLapMatKhau(
      newPassword: newPassword,
      confirmPassword: confirmPassword,
      email: emailUser,
      otpId: otpId,
      token: tokenResetPW,
    );
    result.when(
      success: (response) {
        if (response.errorCode != 200) {
          emit(
            ChangePasswordFail(
              response.message,
            ),
          );
          showContent();
          existFunc = true;
          return;
        }
      },
      error: (error) {
        showError();
        existFunc = true;
        return;
      },
    );
    if (existFunc) return;

    final resultLogin = await loginAndSaveUserInfo(
      email: PrefsService.getEmailUser()
          .substring(0, PrefsService.getEmailUser().length - 10),
      password: confirmPassword,
    );
    if (resultLogin) {
      emit(ChangePasswordSuccess());
      showContent();
    } else {
      emit(ChangePasswordFail(S.current.su_co_xay_ra));
    }
  }

  Future<void> handleOTP({
    required int otpID,
    required String otp,
  }) async {
    showLoading();
    final result = await _authenRepo.handleOTP(
      request: OTPRequest(
        otpId: otpId,
        otp: otp,
      ),
    );
    result.when(
      success: (response) {
        if (response.errorCode == 200) {
          showContent();
          emailUser = response.body?.email ?? '';
          tokenResetPW = response.body?.token ?? '';
          emit(ConfirmOTPSuccess());
        } else {
          showContent();
          emit(ConfirmOTPFail(error: response.errorMessage));
        }
      },
      error: (error) {
        showError();
      },
    );
  }

  int otpId = 0;

  Future<void> confirmEmailToResetPW({required String email}) async {
    showLoading();
    final result = await _authenRepo.quenMatKhau(
      request: QuenMatKhauRequest(email),
    );
    result.when(
      success: (response) {
        if (response.errorCode == 200) {
          otpId = response.body?.id ?? 0;
          PrefsService.saveLoginUserName(email);
          emit(ConfirmEmailSuccess());
        } else {
          emit(ConfirmEmailError(error: response.message ?? ''));
        }
        showContent();
      },
      error: (error) {
        showError();
      },
    );
  }
}
