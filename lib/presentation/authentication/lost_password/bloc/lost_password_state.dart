part of 'lost_password_cubit.dart';

@immutable
abstract class LostPasswordState {}

class LostPasswordInitial extends LostPasswordState {}

class ConfirmEmailSuccess extends LostPasswordState {}

class ConfirmEmailError extends LostPasswordState {
  String? error;

  ConfirmEmailError({this.error});
}

class ConfirmOTPSuccess extends LostPasswordState {}

class ConfirmOTPFail extends LostPasswordState {
  String? error;

  ConfirmOTPFail({this.error});
}

class ChangePasswordSuccess extends LostPasswordState {}

class ChangePasswordFail extends LostPasswordState {
  String? error;

  ChangePasswordFail(this.error);
}
