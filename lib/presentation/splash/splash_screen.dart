import 'dart:async';

import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/domain/locals/prefs_service.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/xelauikit_screens/main_screen.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late final AnimationController _controller = AnimationController(
    duration: const Duration(seconds: 2),
    vsync: this,
  )..repeat();
  late final Animation<double> _animation = CurvedAnimation(
    parent: _controller,
    curve: Curves.easeIn,
  );

  @override
  void initState() {
    super.initState();
    navigateToLogin();
    PrefsService.clearNameScreen();
  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies
    super.didChangeDependencies();
  }

  Future<void> navigateToLogin() async {
    await Future.delayed(const Duration(milliseconds: 2000), () {});
    if (!mounted) return;
    unawaited(
      Navigator.pushReplacement(
        context,
        MaterialPageRoute(
          builder: (context) => const MainScreen(),
        ),
      ),
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.only(
          top: 232,
          left: 76,
          right: 76,
        ),
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage(ImageAssets.bgSplash),
            fit: BoxFit.cover,
          ),
        ),
        child: Center(
          child: FadeTransition(
            opacity: _animation,
            child: Column(
              children: [
                SizedBox(
                  width: 145,
                  height: 146,
                  child: Image.asset(ImageAssets.icQuocHuy),
                ),
                spaceH43,
                Text(
                  S.current.cong_tt_du_lieu_mo,
                  style: XelaTextStyle.Xela16Medium,
                ),
                spaceH4,
                Text(
                  S.current.bo_noi_vu,
                  style: XelaTextStyle.Xela32Medium,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
