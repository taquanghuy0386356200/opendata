import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/domain/locals/logger.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/get_ext.dart';
import 'package:bnv_opendata/widgets/html_edit/edit_html.dart';
import 'package:bnv_opendata/widgets/views/state_layout.dart';
import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebViewWithHtml extends StatefulWidget {
  final String htmlData;

  const WebViewWithHtml({
    Key? key,
    required this.htmlData,
  }) : super(key: key);

  @override
  State<WebViewWithHtml> createState() => _WebViewWithHtmlState();
}

class _WebViewWithHtmlState extends State<WebViewWithHtml> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();

  StateLayout _stateLayout = StateLayout.showLoading;

  @override
  void initState() {
    super.initState();
    if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if(widget.htmlData.isEmpty){
      return const SizedBox.shrink();
    }
    return StateFullLayout(
      stateLayout: _stateLayout,
      retry: () {
        reload(
          Uri.dataFromString(
            editHTMlViewDetail(widget.htmlData),
            mimeType: 'text/html',
            encoding: Encoding.getByName('utf-8'),
          ).toString(),
        );
      },
      error: AppException('', S.current.something_went_wrong),
      textEmpty: '',
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: WebView(
          initialUrl: Uri.dataFromString(
            editHTMlViewDetail(widget.htmlData),
            mimeType: 'text/html',
            encoding: Encoding.getByName('utf-8'),
          ).toString(),
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
          onProgress: (int progress) {
            logger.d('WebView is loading (progress : $progress%)');
          },
          javascriptChannels: const <JavascriptChannel>{},
          navigationDelegate: (NavigationRequest request) {
            return NavigationDecision.navigate;
          },
          onPageStarted: (String url) {
            showLoading();
          },
          onPageFinished: (String url) {
            hideLoading();
          },
          onWebResourceError: (error) {
            showError();
          },
          gestureNavigationEnabled: true,
        ),
      ),
    );
  }

  void showLoading() {
    if (_stateLayout != StateLayout.showLoading) {
      _stateLayout = StateLayout.showLoading;
    }
    setState(() {});
  }

  void hideLoading() {
    if (_stateLayout == StateLayout.showLoading) {
      _stateLayout = StateLayout.showContent;
    }
    setState(() {});
  }

  void showError() {
    _stateLayout = StateLayout.showError;
    setState(() {});
  }

  Future<void> reload(String url) async {
    final WebViewController controller = await _controller.future;
    await controller.loadUrl(url);
  }

  Future<void> backToPreScreen() async => finish();
}
