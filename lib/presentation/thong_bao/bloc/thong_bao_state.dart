part of 'thong_bao_cubit.dart';

@immutable
abstract class ThongBaoState extends BaseState {}

class ThongBaoInitial extends ThongBaoState {
  @override
  List<Object?> get props => [];
}
