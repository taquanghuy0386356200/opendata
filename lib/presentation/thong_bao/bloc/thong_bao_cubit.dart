import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/config/base/base_state.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/domain/repository/thong_bao/thong_bao_respository.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:bnv_opendata/utils/constants/app_constants.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'thong_bao_state.dart';

class ThongBaoCubit extends BaseCubit<BaseState> {
  ThongBaoCubit() : super(ThongBaoInitial()) {
    showContent();
  }

  ThongBaoRepository get _thongBaoRepo => Get.find();

  BehaviorSubject<List<ItemThongBaoModel>> listThongBaoSubject =
      BehaviorSubject();

  BehaviorSubject<ItemThongBaoModel> thongBaoSubject = BehaviorSubject();

  String keySearch = '';

  Future<void> getListThongBao() async {
    if (!getInLoadMore) {
      showLoading();
    }
    inSearching.sink.add(keySearch.isNotEmpty);
    final result = await _thongBaoRepo.getListThongBao(
      keySearch: keySearch,
      pageSize: ApiConstants.DEFAULT_PAGE_SIZE,
      pageIndex: loadMorePage,
    );
    result.when(
      success: (response) {
        showContent();
        if (loadMorePage == ApiConstants.PAGE_BEGIN) {
          if (response.listThongBao.isEmpty) {
            showContent();
            emit(const CompletedLoadMore(CompleteType.SUCCESS, posts: []));
          } else {
            showContent();
            emit(
              CompletedLoadMore(
                CompleteType.SUCCESS,
                posts: response.listThongBao,
              ),
            );
          }
        } else {
          showContent();
          emit(
            CompletedLoadMore(
              CompleteType.SUCCESS,
              posts: response.listThongBao,
            ),
          );
        }
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> getDetailThongBao(
    String? id,
  ) async {
    showLoading();
    final result = await _thongBaoRepo.getDetailThongBao(
      id: id,
    );
    result.when(
      success: (response) {
        showContent();
        thongBaoSubject.sink.add(response);
      },
      error: (error) {
        showError();
      },
    );
  }

  Future<void> onSearching(String value) async {
    canMoveToTop = true;
    keySearch = value;
    loadMorePage = ApiConstants.PAGE_BEGIN;
  }
}
