import 'package:bnv_opendata/domain/env/model/app_constants.dart';
import 'package:bnv_opendata/domain/model/notification_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/thong_bao/bloc/thong_bao_cubit.dart';
import 'package:bnv_opendata/presentation/thong_bao/ui/mobile/detail_thong_bao_screen.dart';
import 'package:bnv_opendata/presentation/thong_bao/ui/widget/cell_thong_bao.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/utils/screen_controller.dart';
import 'package:bnv_opendata/widgets/appbar/app_bar_search.dart';
import 'package:bnv_opendata/widgets/listview/loadmore_list.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class ThongBaoScreen extends StatefulWidget {
  final bool showBack;

  const ThongBaoScreen({
    Key? key,
    this.showBack = false,
  }) : super(key: key);

  @override
  State<ThongBaoScreen> createState() => _ThongBaoScreenState();
}

class _ThongBaoScreenState extends State<ThongBaoScreen>
    with AutomaticKeepAliveClientMixin<ThongBaoScreen> {
  @override
  bool get wantKeepAlive => true;
  ThongBaoCubit cubit = ThongBaoCubit();

  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AppBarSearch(
            title: S.current.thong_bao,
            onchange: (value) {
              cubit.onSearching(value).then((value) => cubit.getListThongBao());
            },
            isCanBack: widget.showBack,
            controller: _controller,
          ),
          Expanded(
            child: ListViewLoadMoreWidget(
              onReload: () {
              },
              cubit: cubit,
              isListView: true,
              callApi: (page) => cubit.getListThongBao(),
              viewItem: (value, index) {
                value as ThongBaoModel;
                return CellThongBao(
                  image: '${Get.find<AppConstants>().baseUrl}/${value.banner}',
                  title: value.title,
                  time: value.releaseAt.fomatFullDateFromMili(value.releaseAt),
                  ontap: () {
                    goTo(
                      context,
                      DetailThongBaoScreen(
                        id: value.id.toString(),
                      ),
                    );
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
