import 'package:bnv_opendata/config/themes/app_theme.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:flutter/material.dart';

class HeaderLoginPopular extends StatelessWidget {
  const HeaderLoginPopular({
    Key? key,
    required this.image,
    this.heightPopular = 337,
    this.widthPopular,
    this.haveBackBtn = false,
  }) : super(key: key);
  final String image;
  final double? heightPopular;
  final double? widthPopular;
  final bool haveBackBtn;

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: (widthPopular != null)
          ? Alignment.center
          : AlignmentDirectional.topStart,
      children: [
        Container(
          height: 268,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: AppTheme.getInstance().headerBgColor(),
            ),
          ),
        ),
        Container(
          height: heightPopular,
          width: widthPopular,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: AssetImage(
                image,
              ),
            ),
          ),
        ),
        if (haveBackBtn)
          Positioned(
            top: 54,
            left: 10,
            child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: SizedBox(
                height: 24,
                width: 24,
                child: Image.asset(ImageAssets.icBackBtn),
              ),
            ),
          )
        else
          const SizedBox.shrink()
      ],
    );
  }
}
