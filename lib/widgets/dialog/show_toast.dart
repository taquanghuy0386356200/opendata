import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void tungShowToast(
  BuildContext context, {
  required String text,
  Widget? child,
  String? icon,
  double? withOpacity,
  Color? color,
  int? delay,
}) {
  final scaffold = ScaffoldMessenger.of(context);
  scaffold.showSnackBar(
    SnackBar(
      elevation: 0,
      backgroundColor: Colors.transparent,
      duration: Duration(milliseconds: delay ?? 600),
      content: child ??
          ShowToast(
            text: text,
            icon: icon,
            withOpacity: withOpacity,
            color: color,
          ),
    ),
  );
}

class ShowToast extends StatelessWidget {
  final String text;
  final String? icon;
  final Color? color;
  final double? withOpacity;

  const ShowToast({
    Key? key,
    required this.text,
    this.icon,
    this.color,
    this.withOpacity,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(14),
      decoration: BoxDecoration(
        color: (color ?? Colors.red).withOpacity(withOpacity ?? 0.5),
        borderRadius: BorderRadius.circular(8),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            width: 20,
            height: 20,
            child: Image.asset(icon ?? ImageAssets.icWarning),
          ),
          spaceW12,
          Text(
            text,
            style: XelaTextStyle.Xela14Medium,
          )
        ],
      ),
    );
  }
}
