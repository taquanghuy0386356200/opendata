import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

Future<T?> showDialogPopular<T>(
  BuildContext context, {
  bool isDoubleBtn = false,
  required dynamic Function() defaultPress,
  required String imageContent,
  required Widget contentDialog,
  dynamic Function()? leftPress,
  required String titleDefaultBtn,
  int? timeShowDialog,
  String? titleLeftBtn,
  bool haveButton = true,
  double? heightIcon,
  double? widthIcon,
}) {
  return showDialog(
    barrierDismissible: false,
    context: context,
    builder: (dialogContext) {
      if (timeShowDialog != null) {
        Future.delayed(Duration(seconds: timeShowDialog), () {
          Navigator.of(context).pop(true);
        });
      } else {}
      return Dialog(
        backgroundColor: Colors.white,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)),
        ),
        clipBehavior: Clip.antiAlias,
        child: Container(
          padding: const EdgeInsets.symmetric(
            vertical: 40,
            horizontal: 24,
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(
                height: heightIcon ?? 56,
                width: widthIcon ?? 56,
                child: Image.asset(imageContent),
              ),
              contentDialog,
              spaceH24,
              if (isDoubleBtn)
                Row(
                  children: [
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          if (leftPress != null) {
                            // ignore: prefer_null_aware_method_calls
                            leftPress();
                          }
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 12),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(4),
                            ),
                            color: colorFEF2E2,
                          ),
                          child: Center(
                            child: Text(
                              titleLeftBtn ?? '',
                              style: XelaTextStyle.Xela14Medium.copyWith(
                                color: color364564,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    spaceW16,
                    Expanded(
                      child: GestureDetector(
                        onTap: () {
                          defaultPress();
                        },
                        child: Container(
                          padding: const EdgeInsets.symmetric(vertical: 12),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(4),
                            ),
                            color: colorE26F2C,
                          ),
                          child: Center(
                            child: Text(
                              titleDefaultBtn,
                              style: XelaTextStyle.Xela14Medium.copyWith(
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    )
                  ],
                )
              else if (haveButton)
                GestureDetector(
                  onTap: () {
                    defaultPress();
                  },
                  child: Container(
                    width: 139,
                    padding: const EdgeInsets.symmetric(
                      vertical: 12,
                    ),
                    decoration: const BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(4),
                      ),
                      color: Color(0xffFEF2E2),
                    ),
                    child: Center(
                      child: Text(
                        titleDefaultBtn,
                        style: XelaTextStyle.Xela14Medium.copyWith(
                          color: color364564,
                        ),
                      ),
                    ),
                  ),
                )
              else
                const SizedBox.shrink(),
            ],
          ),
        ),
      );
    },
  );
}
