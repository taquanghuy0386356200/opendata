import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/dialog/mess_dialog_pop_up.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';

enum MessState { error, success, customIcon }

class MessageConfig {
  static BuildContext? contextConfig;
  static OverlayEntry? _overlayEntry;

  static void init(BuildContext context) {
    if (contextConfig != null) {
      return;
    }
    contextConfig = context;
  }

  static Future<void> showDialogSetting({
    String? okBtnTxt,
    String? cancelBtnTxt,
  }) async {
    final Widget okButton = TextButton(
      child: Text(
        okBtnTxt ?? S.current.mo_cai_dat,
        style: XelaTextStyle.Xela14Medium,
      ),
      onPressed: () {
        Navigator.pop(contextConfig!);
        openAppSettings();
      },
    );

    final Widget cancelBtnText = TextButton(
      child: Text(
        cancelBtnTxt ?? S.current.bo_qua,
        style: XelaTextStyle.Xela14Medium,
      ),
      onPressed: () {
        Navigator.pop(contextConfig!);
      },
    );
    // set up the AlertDialog
    final AlertDialog alert = AlertDialog(
      title: Text(
        S.current.ban_can_mo_quyen_de_truy_cap_ung_dung,
        style: XelaTextStyle.Xela14Medium,
      ),
      actions: [okButton, cancelBtnText],
    );

    return showDialog(
      context: contextConfig!,
      builder: (_) {
        return alert;
      },
    );
  }

  static void show({
    String title = '',
    String title2 = '',
    bool? showTitle2 = false,
    FontWeight? fontWeight,
    double? fontSize,
    String urlIcon = '',
    MessState messState = MessState.success,
    Function()? onDismiss,
  }) {
    if (_overlayEntry != null) {
      return;
    }
    final OverlayState? overlayState = Overlay.of(contextConfig!);

    _overlayEntry = OverlayEntry(
      builder: (context) {
        return MessageDialogPopup(
          onDismiss: () {
            _overlayEntry?.remove();
            _overlayEntry = null;
            if (onDismiss != null) {
              onDismiss();
            }
          },
          urlIcon: _urlIcon(messState, urlIcon),
          title: title,
          messState: messState,
        );
      },
    );

    overlayState?.insert(_overlayEntry!);
  }

  static String _urlIcon(MessState messState, String urlIcon) {
    switch (messState) {
      case MessState.error:
        return ImageAssets.icWarning;
      case MessState.success:
        return ImageAssets.icSuccess;
      case MessState.customIcon:
        return urlIcon;
    }
  }
}
