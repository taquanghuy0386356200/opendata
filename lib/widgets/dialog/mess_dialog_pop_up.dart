import 'dart:async';

import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/widgets/dialog/message_config.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class MessageDialogPopup extends StatefulWidget {
  final Function() onDismiss;
  final String urlIcon;
  final String title;
  final MessState messState;

  const MessageDialogPopup({
    Key? key,
    required this.onDismiss,
    this.urlIcon = '',
    this.title = '',
    required this.messState,
  }) : super(key: key);

  @override
  State<MessageDialogPopup> createState() => _MessageDialogPopupState();
}

class _MessageDialogPopupState extends State<MessageDialogPopup>
    with SingleTickerProviderStateMixin {
  late final AnimationController animationController;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 150),
    )..addStatusListener((status) async {
        if (status == AnimationStatus.completed) {
          await Future.delayed(const Duration(milliseconds: 500));
          unawaited(animationController.reverse());
        }
        if (status == AnimationStatus.dismissed) {
          widget.onDismiss();
        }
      });
    if (mounted) {
      animationController.forward();
    }
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        animationController.reverse();
      },
      child: Scaffold(
        backgroundColor: Colors.black38,
        body: Center(
          child: AnimatedBuilder(
            animation: animationController,
            builder: (context, _) => Opacity(
              opacity: animationController.value,
              child: Transform(
                transform: Matrix4.identity()
                  ..scale(animationController.value, animationController.value),
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 50),
                  padding:
                      const EdgeInsets.symmetric(horizontal: 24, vertical: 30),
                  constraints: const BoxConstraints(minWidth: 300),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: const BorderRadius.all(
                      Radius.circular(20),
                    ),
                    boxShadow: [
                      BoxShadow(
                        color: widget.messState == MessState.error
                            ? colorFF4F50
                            : color20C997,
                        blurRadius: 30,
                        offset: const Offset(0, 5), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Image.asset(
                        widget.urlIcon,
                        width: 50,
                        height: 50,
                      ),
                      spaceH15,
                      Text(
                        widget.title,
                        textAlign: TextAlign.center,
                        style: XelaTextStyle.Xela18Medium,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
