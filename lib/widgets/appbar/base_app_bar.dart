import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class BaseAppBar extends StatelessWidget with PreferredSizeWidget {
  final bool isShowBack;
  @override
  final Size preferredSize;
  final Widget? leadingIcon;
  final String? title;
  final Color? backGroundColor;
  final List<Widget>? actions;
  final PreferredSizeWidget? tabbar;
  final bool? refreshData;
  final Widget? widgetTitle;

  BaseAppBar({
    Key? key,
    this.title,
    this.leadingIcon,
    this.actions,
    this.tabbar,
    this.backGroundColor,
    this.isShowBack = false,
    this.refreshData,
    this.widgetTitle,
  })  : preferredSize = const Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    Widget leadingWidget() {
      if (leadingIcon != null) {
        return leadingIcon ?? const SizedBox();
      }
      if (isShowBack) {
        return GestureDetector(
          onTap: () {
            Navigator.pop(context, refreshData);
          },
          child: const Icon(
            Icons.arrow_back_ios_new,
            color: colorA2AEBD,
            size: 22,
          ),
        );
      }
      return const SizedBox();
    }

    return AppBar(
      systemOverlayStyle: const SystemUiOverlayStyle(
        statusBarBrightness: Brightness.light,
      ),
      bottomOpacity: 0.0,
      elevation: 0,
      shadowColor: XelaColor.Gray12,
      bottom: tabbar,
      automaticallyImplyLeading: false,
      title: widgetTitle ??
          Text(
            title ?? '',
            style: XelaTextStyle.Xela18Medium.copyWith(color: color364564),
          ),
      centerTitle: true,
      leading: leadingWidget(),
      actions: actions,
    );
  }
}
