import 'dart:async';

import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/custom_search_opendata_widget.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/appbar/base_app_bar.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class AppBarSearch extends StatefulWidget {
  final String title;
  final Function(String) onchange;
  final bool isCanBack;
  final String? hintText;
  final TextEditingController controller;
  final bool isTypeOther;

  const AppBarSearch({
    Key? key,
    required this.title,
    required this.onchange,
    this.isCanBack = false,
    this.hintText,
    required this.controller,
    this.isTypeOther = false,
  }) : super(key: key);

  @override
  State<AppBarSearch> createState() => _AppBarSearchState();
}

class _AppBarSearchState extends State<AppBarSearch> {
  final BehaviorSubject<bool> isSearch = BehaviorSubject.seeded(false);

  String dataSearch = '';
  late FocusNode myFocusNode;
  Timer? _debounce;

  @override
  void initState() {
    myFocusNode = FocusNode();
    super.initState();
  }

  @override
  void dispose() {
    myFocusNode.dispose();
    dataSearch = '';
    _debounce?.cancel();
    super.dispose();
    isSearch.close();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
      stream: isSearch.stream,
      builder: (context, snapshot) {
        final data = snapshot.data;
        if (data ?? false) {
          return Container(
            margin: const EdgeInsets.only(bottom: 16),
            child: AppBar(
              elevation: 0,
              actions: [
                spaceW16,
                searchWidget(),
                GestureDetector(
                  onTap: () {
                    isSearch.sink.add(false);
                    if (dataSearch.isNotEmpty) {
                      widget.onchange('');
                    }
                  },
                  child: Container(
                    color: Colors.transparent,
                    padding: const EdgeInsets.only(left: 8),
                    child: Center(
                      child: Text(
                        S.current.cancel,
                        style: XelaTextStyle.Xela14Medium.copyWith(
                          color: color667793,
                        ),
                      ),
                    ),
                  ),
                ),
                spaceW16
              ],
            ),
          );
        }
        return BaseAppBar(
          widgetTitle: widget.isTypeOther
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    if (widget.title.isNotEmpty) ...[
                      Text(
                        widget.title,
                        style: XelaTextStyle.Xela18Medium.copyWith(
                          color: color364564,
                        ),
                      ),
                      spaceW16
                    ],
                    iconSearch()
                  ],
                )
              : null,
          title: widget.title,
          isShowBack: widget.isCanBack,
          actions: [if (!widget.isTypeOther) iconSearch()],
        );
      },
    );
  }

  void _onSearchChanged({required Function() fun, int? timeDelay}) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(Duration(milliseconds: timeDelay ?? 500), () {
      fun();
    });
  }

  Widget searchWidget() => CustomSearchOpenData(
        hintText: widget.hintText ?? S.current.tim_kiem,
        textChange: (value) {
          _onSearchChanged(
            fun: () {
              widget.onchange(value.trim());
              dataSearch = value;
            },
          );
        },
        controller: widget.controller,
      );

  Widget iconSearch() => GestureDetector(
        onTap: () {
          isSearch.sink.add(true);
        },
        child: Container(
          height: 17,
          width: 17,
          margin: const EdgeInsets.only(right: 20),
          child: Image.asset(ImageAssets.icSearchThongBao),
        ),
      );
}
