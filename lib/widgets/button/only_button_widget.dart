import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

Widget onlyButtonWidget({
  required Function() onTap,
  required String title,
  Color? colors,
  String image = '',
  Color? textColors,
}) {
  return GestureDetector(
    onTap: () {
      onTap();
    },
    child: Container(
      height: 44,
      width: double.infinity,
      padding: const EdgeInsets.symmetric(horizontal: 16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(8),
        color: colors ?? colorE26F2C,
      ),
      child: Center(
        child: (image.isNotEmpty)
            ? Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  SizedBox(
                    height: 24,
                    width: 24,
                    child: Image.asset(image),
                  ),
                  spaceW6,
                  Expanded(
                    child: Text(
                      title,
                      style: XelaTextStyle.Xela16Medium.copyWith(
                        color: textColors ?? colorE26F2C,
                      ),
                    ),
                  )
                ],
              )
            : Text(
                title,
                style: XelaTextStyle.Xela16Medium.copyWith(color: textColors),
              ),
      ),
    ),
  );
}
