import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/widgets/button/only_button_widget.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class DoubleButtonBottom extends StatelessWidget {
  final String titleLeft;
  final String titleRight;
  final String? imageLeft;
  final String? imageRight;
  final Function() onClickLeft;
  final Function() onClickRight;

  const DoubleButtonBottom({
    Key? key,
    required this.titleLeft,
    required this.titleRight,
    required this.onClickLeft,
    required this.onClickRight,
    this.imageLeft,
    this.imageRight,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: onlyButtonWidget(
            onTap: () {
              onClickLeft();
            },
            title: titleLeft,
            image: imageLeft ?? '',
            colors: colorFEF2E2,
            textColors: color364564,
          ),
        ),
        const SizedBox(width: 16.0),
        Expanded(
          child: onlyButtonWidget(
            onTap: () {
              onClickRight();
            },
            title: titleRight,
            image: imageRight ?? '',
            colors: colorE26F2C,
            textColors: colorFFFFFF,
          ),
        ),
      ],
    );
  }
}
