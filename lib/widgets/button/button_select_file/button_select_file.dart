import 'dart:async';
import 'dart:io';
import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/constants/app_constants.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/string_ext.dart';
import 'package:bnv_opendata/widgets/button/button_select_file/butt_select_file_cubit.dart';
import 'package:bnv_opendata/widgets/dialog/show_toast.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:permission_handler/permission_handler.dart';

class ButtonSelectFile extends StatefulWidget {
  const ButtonSelectFile({
    Key? key,
    this.isChooseMultiFiles = false,
    this.allowedExtensionFiles = const [
      FileExtensions.DOC,
      FileExtensions.DOCX,
      FileExtensions.JPEG,
      FileExtensions.JPG,
      FileExtensions.PDF,
      FileExtensions.PNG,
      FileExtensions.XLSX,
      FileExtensions.HEIC,
    ],
    this.initFileFromApi = '',
    this.initFileSizeApi,
    required this.onChange,
    this.deleteFile,
  }) : super(key: key);

  final bool isChooseMultiFiles;
  final List<String> allowedExtensionFiles;
  final Function(File fileSelected) onChange;
  final Function()? deleteFile;
  final String initFileFromApi;
  final String? initFileSizeApi;

  @override
  State<ButtonSelectFile> createState() => _ButtonSelectFileState();
}

class _ButtonSelectFileState extends State<ButtonSelectFile> {
  final cubit = ButtonSelectFileCubit();
  late final FToast toast;
  Directory? pathTmp;

  @override
  void initState() {
    super.initState();
    toast = FToast();
    cubit.initFileFromApi.add(widget.initFileFromApi);
    toast.init(context);
  }

  void showToast({required String message}) {
    toast.removeQueuedCustomToasts();
    toast.showToast(
      child: ShowToast(
        text: message,
        withOpacity: 0.4,
      ),
      gravity: ToastGravity.TOP_RIGHT,
    );
  }

  Future<bool> handleFilePermission() async {
    final permission =
        Platform.isAndroid ? await Permission.storage.request() : true;
    if (permission == PermissionStatus.permanentlyDenied) {
      return false;
    } else {
      return true;
    }
  }

  Future<void> handleFileClicked() async {
    final FilePickerResult? result = await FilePicker.platform.pickFiles(
      allowedExtensions: widget.allowedExtensionFiles,
      type: FileType.custom,
    );
    if (result == null) {
      return;
    }

    File newFile = File('');
    newFile = File(result.files.single.path ?? '');
    if (cubit.isOverSize(file: newFile)) {
      toast.removeQueuedCustomToasts();
      showToast(
        message: S.current.dung_luong_toi_da_30,
      );
      return;
    }
    cubit.selectedFile = newFile;
    widget.onChange(cubit.selectedFile);
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return (cubit.initFileFromApi.value.isNotEmpty)
        ? itemFile(
            nameFile: widget.initFileFromApi,
            onDelete: () {
              cubit.initFileFromApi.sink.add('');
              cubit.selectedFile = File('');
              widget.onChange(cubit.selectedFile);
              setState(() {});
            },
            fileSize: widget.initFileSizeApi ?? '',
          )
        : (cubit.selectedFile.path.isEmpty)
            ? GestureDetector(
                onTap: () async {
                  final permission = await handleFilePermission();
                  if (permission) {
                    unawaited(handleFileClicked());
                  } else {
                    await openAppSettings();
                    // await MessageConfig.showDialogSetting();
                  }
                },
                child: Container(
                  padding: const EdgeInsets.symmetric(
                    vertical: 4,
                    horizontal: 10,
                  ),
                  decoration: BoxDecoration(
                    color: colorFEF2E2,
                    borderRadius: BorderRadius.circular(4),
                  ),
                  child: Row(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      SizedBox(
                        height: 24,
                        width: 24,
                        child: Image.asset(ImageAssets.icDinhFile),
                      ),
                      spaceW6,
                      Text(
                        S.current.tai_lieu_dinh_kem,
                        style: XelaTextStyle.Xela14Medium.copyWith(
                          color: colorE26F2C,
                        ),
                      )
                    ],
                  ),
                ),
              )
            : itemFile(
                nameFile: cubit.selectedFile.path.convertNameFile(),
                onDelete: () {
                  cubit.selectedFile = File('');
                  widget.onChange(cubit.selectedFile);
                  setState(() {});
                },
                fileSize: cubit.getFileSize(cubit.selectedFile),
              );
  }

  Widget itemFile({
    required String nameFile,
    required String fileSize,
    required dynamic Function() onDelete,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          S.current.mo_ta_tai_lieu,
          style: XelaTextStyle.Xela14Regular.copyWith(
            color: color667793,
          ),
        ),
        InkWell(
          onTap: () {
            onDelete();
          },
          child: Container(
            margin: const EdgeInsets.only(
              top: 8,
            ),
            decoration: BoxDecoration(borderRadius: BorderRadius.circular(6)),
            child: Row(
              children: [
                Text(
                  nameFile.handleStringTooLong(),
                  style: XelaTextStyle.Xela14Regular.copyWith(
                    color: color20C997,
                  ),
                ),
                Text(
                  ' ($fileSize)',
                  style: XelaTextStyle.Xela14Regular.copyWith(
                    color: color20C997,
                  ),
                ),
                spaceW8,
                SizedBox(
                  height: 20,
                  width: 20,
                  child: Image.asset(
                    ImageAssets.icClearFile,
                  ),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
