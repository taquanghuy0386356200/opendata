import 'dart:io';
import 'dart:math';

import 'package:bnv_opendata/utils/constants/app_constants.dart';
import 'package:rxdart/rxdart.dart';

class ButtonSelectFileCubit {
  File selectedFile = File('');

  BehaviorSubject<String> initFileFromApi = BehaviorSubject();

  bool isOverSize({required File file}) {
    return file.lengthSync() > MaxSizeFile.MAX_SIZE_30MB;
  }

  String getFileSize(File file) {
    final int bytes = file.lengthSync();
    if (bytes <= 0) return '0 B';
    return bytes < 100000
        ? '${(bytes / pow(1024, 1)).toStringAsFixed(2)} ${'kB'}'
        : '${(bytes / pow(1024, 2)).toStringAsFixed(2)} ${'Mb'}';
  }
}
