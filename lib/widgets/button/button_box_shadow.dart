import 'package:flutter/material.dart';

class ButtonBoxShadow extends StatelessWidget {
  const ButtonBoxShadow({
    Key? key,
    required this.colorBoxShadow,
    required this.offSetX,
    required this.offSetY,
    required this.blurRadius,
    required this.child,
  }) : super(key: key);
  final Color colorBoxShadow;
  final double offSetX;
  final double offSetY;
  final double blurRadius;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            color: colorBoxShadow,
            offset: Offset(offSetX, offSetY),
            blurRadius: blurRadius,
          ),
        ],
      ),
      child: child,
    );
  }
}
