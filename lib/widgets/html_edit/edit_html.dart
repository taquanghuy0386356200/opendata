import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/domain/models/open_data_model/detail_open_data_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';

String editHTMlViewDetail(String content) {
  return '''
 <div style="padding-left: 20; padding-right: 20; padding-top: 20">$content</div>   
    
<style>
    body {
        font-size: 250%
    }
</style>   
      ''';
}

String viewHtmlDetailOpendata({required BodyDetailOpenDataModel data}) {
  String fileDinhKem() {
    if (data.getNameFile().isEmpty) {
      return '';
    }
    return '''
    <div class="row">
            <p class="key_file">${S.current.file_dinh_kem}</p>
            <p class="value_file">${data.getNameFile()}</p>
        </div>
    ''';
  }

  return '''
<body>

    <div class="main_box">
        <div class="row">
            <p class="key">${S.current.tieu_de}</p>
            <p class="value">${data.title ?? ''}</p>
        </div>
        <div class="row">
            <p class="key">${S.current.thoi_gian_cap_nhat}</p>
            <p class="value">${(data.updatedAt ?? 0).fomatFullDateFromMili(data.updatedAt ?? 0)}</p>
        </div>
        <div class="row">
            <p class="key">${S.current.linh_vuc}</p>
            <p class="value">${data.categoryName ?? ''}</p>
        </div>
        <div class="row">
            <p class="key">${S.current.chuyen_muc}</p>
            <p class="value">${data.subcategoryName ?? ''}</p>
        </div>
        <div class="row">
            <p class="key">${S.current.mo_ta}</p>
            <p class="value">${data.description?.trim() ?? ''}</p>
        </div>
        ${fileDinhKem()}
        <div class="content">${data.content ?? ''}</div>
        <iframe src="${data.link}" frameborder="0"></iframe>
    </div>
</body>
<style>
    .main_box {
        padding: 20px;
    }

    .key {
        float: left;
        color: #667793;
        margin: 0;
    }

    .value {
        float: right;
        overflow: hidden;
        display: block;
        word-wrap: break-word;
        color: #364564;
        margin: 0;
    }

    body {
        font-size: 250%
    }

    .row {
        display: grid;
        grid-template-columns: 30% 70%;
        padding-bottom: 20px;
    }
    
    .key_file {
        float: left;
        color: #667793;
        margin: 0;
    }

    .value_file {
        float: right;
        overflow: hidden;
        display: block;
        word-wrap: break-word;
        color: #20C997;
        margin: 0;
    }
    .content {
        width: 100%;
    }
    img {
        width: 100%;
    }
    
    iframe {
        width: 100%;
        height: 100%;
        font-size: 250%;
    }
</style>
''';
}
