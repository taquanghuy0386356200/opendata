import 'package:flutter/material.dart';

class XelaTextStyle {
  XelaTextStyle._();

  static const fontFamily = 'Roboto';

  static const XelaTitle1 = TextStyle(
      fontSize: 60, fontWeight: FontWeight.w800, fontFamily: fontFamily);

  static const XelaTitle2 = TextStyle(
      fontSize: 48, fontWeight: FontWeight.w800, fontFamily: fontFamily);

  static const XelaTitle3 = TextStyle(
      fontSize: 34, fontWeight: FontWeight.w900, fontFamily: fontFamily);

  static const XelaHeadline = TextStyle(
      fontSize: 24, fontWeight: FontWeight.w800, fontFamily: fontFamily);

  static const XelaSubheadline = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w800, fontFamily: fontFamily);

  static const XelaBody = TextStyle(
      fontSize: 16, fontWeight: FontWeight.normal, fontFamily: fontFamily);

  static const XelaBodyBold = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w800, fontFamily: fontFamily);

  static const XelaSmallBody = TextStyle(
      fontSize: 14, fontWeight: FontWeight.normal, fontFamily: fontFamily);

  static const XelaSmallBodyBold = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w800, fontFamily: fontFamily);

  static const XelaCaption = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w700, fontFamily: fontFamily);

  static const XelaButtonLarge = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w700, fontFamily: fontFamily);

  static const Xela12w700 = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w700, fontFamily: fontFamily);

  static const XelaButtonMedium = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w800, fontFamily: fontFamily);

  static const XelaButtonSmall = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w800, fontFamily: fontFamily);

  static const Xela32Bold = TextStyle(
      fontSize: 32, fontWeight: FontWeight.w700, fontFamily: fontFamily);

  static const Xela28Bold = TextStyle(
      fontSize: 24, fontWeight: FontWeight.w700, fontFamily: fontFamily);

  static const Xela24Bold = TextStyle(
      fontSize: 24, fontWeight: FontWeight.w700, fontFamily: fontFamily);

  static const Xela18Bold = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w700, fontFamily: fontFamily);

  static const Xela14Regular = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w400, fontFamily: fontFamily);

  static const Xela13Regular = TextStyle(
      fontSize: 13, fontWeight: FontWeight.w400, fontFamily: fontFamily);

  static const Xela12Regular = TextStyle(
      fontSize: 12, fontWeight: FontWeight.w400, fontFamily: fontFamily);

  static const Xela16Regular = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w400, fontFamily: fontFamily);

  static const Xela14Medium = TextStyle(
      fontSize: 14, fontWeight: FontWeight.w500, fontFamily: fontFamily);

  static const Xela18Medium = TextStyle(
      fontSize: 18, fontWeight: FontWeight.w500, fontFamily: fontFamily);

  static const Xela24Medium = TextStyle(
      fontSize: 24, fontWeight: FontWeight.w500, fontFamily: fontFamily);

  static const Xela11Medium = TextStyle(
      fontSize: 11, fontWeight: FontWeight.w500, fontFamily: fontFamily);

  static const Xela16Medium = TextStyle(
      fontSize: 16, fontWeight: FontWeight.w500, fontFamily: fontFamily);

  static const Xela32Medium = TextStyle(
      fontSize: 32, fontWeight: FontWeight.w500, fontFamily: fontFamily);
}
