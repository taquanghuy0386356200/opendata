import 'package:bnv_opendata/domain/models/xela_accordion_models.dart';
import 'package:bnv_opendata/utils/xela_expanded_section.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class XelaAccordion extends StatefulWidget {
  final String title;
  bool isOpen;
  final Widget? openIcon;
  final Widget? closeIcon;
  final XelaAccordionIconPosition iconPosition;
  final Color openBackground;
  final Color closeBackground;
  final Color openTitleColor;
  final Color closeTitleColor;
  final Widget content;

  XelaAccordion({
    Key? key,
    required this.title,
    this.isOpen = false,
    this.openIcon,
    this.closeIcon,
    this.iconPosition = XelaAccordionIconPosition.RIGHT,
    this.openBackground = XelaColor.Gray12,
    this.closeBackground = XelaColor.Gray12,
    this.openTitleColor = XelaColor.Gray2,
    this.closeTitleColor = XelaColor.Gray2,
    required this.content,
  }) : super(key: key);

  @override
  _XelaAccordionState createState() => _XelaAccordionState();
}

class _XelaAccordionState extends State<XelaAccordion> {
  _XelaAccordionState();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DecoratedBox(
      decoration: BoxDecoration(
        color: widget.isOpen ? widget.openBackground : widget.closeBackground,
        borderRadius: BorderRadius.circular(12),
      ),
      child: Column(
        children: [
          InkWell(
            borderRadius: BorderRadius.circular(12),
            onTap: () {
              setState(() {
                widget.isOpen = !widget.isOpen;
              });
            },
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Row(
                children: [
                  if (widget.iconPosition == XelaAccordionIconPosition.LEFT &&
                      widget.openIcon != null &&
                      widget.closeIcon != null)
                    widget.isOpen ? widget.openIcon! : widget.closeIcon!
                  else
                    Container(),
                  if (widget.iconPosition == XelaAccordionIconPosition.LEFT &&
                      widget.openIcon != null &&
                      widget.closeIcon != null)
                    const SizedBox(width: 16)
                  else
                    Container(),
                  Text(
                    widget.title,
                    style: XelaTextStyle.XelaBodyBold.apply(
                      color: widget.isOpen
                          ? widget.openTitleColor
                          : widget.closeTitleColor,
                    ),
                  ),
                  const Spacer(),
                  if (widget.iconPosition == XelaAccordionIconPosition.RIGHT &&
                      widget.openIcon != null &&
                      widget.closeIcon != null)
                    widget.isOpen ? widget.openIcon! : widget.closeIcon!
                  else
                    Container(),
                ],
              ),
            ),
          ),
          XelaExpandedSection(
            expand: widget.isOpen,
            child: Padding(
              padding: const EdgeInsets.only(
                bottom: 16,
                left: 16,
                right: 16,
              ),
              child: widget.content,
            ),
          )
        ],
      ),
    );
  }
}
