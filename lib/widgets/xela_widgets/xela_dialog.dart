import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class XelaDialog extends StatefulWidget {
  final Widget? icon;
  final String? title;
  final String? description;
  final Widget? primaryButton;
  final Widget? secondaryButton;
  final Widget? closeButton;
  final bool buttonHorizontal;
  final Color background;
  final Color titleColor;
  final Color descriptionColor;

  const XelaDialog({
    Key? key,
    this.icon,
    this.title,
    this.description,
    this.primaryButton,
    this.secondaryButton,
    this.closeButton,
    this.buttonHorizontal = true,
    this.background = Colors.white,
    this.titleColor = XelaColor.Gray3,
    this.descriptionColor = XelaColor.Gray3,
  }) : super(key: key);

  @override
  _XelaDialogState createState() => _XelaDialogState();
}

class _XelaDialogState extends State<XelaDialog> {
  _XelaDialogState();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(24),
      decoration: BoxDecoration(
        color: widget.background,
        borderRadius: BorderRadius.circular(24),
      ),
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          if (widget.closeButton != null)
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                if (widget.closeButton != null)
                  widget.closeButton!
                else
                  Container(),
              ],
            )
          else
            Container(),
          Column(
            children: [
              SizedBox(
                height: widget.closeButton != null ? 24 : 0,
              ),
              if (widget.icon != null) widget.icon! else Container(),
              SizedBox(
                height: widget.icon != null ? 24 : 0,
              ),
              if (widget.title != null)
                Text(
                  widget.title!,
                  style: XelaTextStyle.XelaHeadline.apply(
                    color: widget.titleColor,
                  ),
                  textAlign: TextAlign.center,
                )
              else
                Container(),
              const SizedBox(
                height: 8,
              ),
              if (widget.description != null)
                Text(
                  widget.description!,
                  style: XelaTextStyle.XelaBody.apply(color: widget.titleColor),
                  textAlign: TextAlign.center,
                )
              else
                Container(),
              const SizedBox(
                height: 24,
              ),
              if (widget.buttonHorizontal)
                SizedBox(
                  width: double.infinity,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      if (widget.secondaryButton != null)
                        widget.secondaryButton!
                      else
                        Container(),
                      if (widget.primaryButton != null)
                        widget.primaryButton!
                      else
                        Container(),
                    ],
                  ),
                )
              else
                Column(
                  children: [
                    if (widget.primaryButton != null)
                      widget.primaryButton!
                    else
                      Container(),
                    SizedBox(
                      height: widget.secondaryButton != null ? 8 : 0,
                    ),
                    if (widget.secondaryButton != null)
                      widget.secondaryButton!
                    else
                      Container(),
                  ],
                )
            ],
          ),
        ],
      ),
    );
  }
}
