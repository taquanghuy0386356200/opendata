import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_user_avatar.dart';
import 'package:flutter/material.dart';

class XelaToast extends StatefulWidget {
  final String title;
  final String? description;
  final Widget? icon;
  final XelaUserAvatar? avatar;
  final Widget? rightButton;
  final Widget? firstActionButton;
  final Widget? secondActionButton;
  final bool autoresize;
  final Color background;
  final Color titleColor;
  final Color descriptionColor;

  const XelaToast({
    Key? key,
    required this.title,
    this.description,
    this.icon,
    this.avatar,
    this.rightButton,
    this.firstActionButton,
    this.secondActionButton,
    this.autoresize = false,
    this.background = Colors.white,
    this.titleColor = XelaColor.Gray2,
    this.descriptionColor = XelaColor.Gray6,
  }) : super(key: key);

  @override
  _XelaToastState createState() => _XelaToastState();
}

class _XelaToastState extends State<XelaToast> {
  _XelaToastState();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final child = Container(
      padding: const EdgeInsets.all(16),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18),
        color: widget.background,
      ),
      child: Row(
        crossAxisAlignment: widget.icon != null && widget.description != null
            ? CrossAxisAlignment.start
            : CrossAxisAlignment.center,
        children: [
          if (widget.icon != null)
            Container(
              alignment: Alignment.topCenter,
              padding: const EdgeInsets.only(right: 16),
              child: SizedBox(
                width: 24,
                height: 24,
                child: FittedBox(
                  child: widget.icon,
                ),
              ),
            )
          else
            Container(),
          if (widget.avatar != null) widget.avatar! else Container(),
          if (widget.avatar != null)
            const SizedBox(
              width: 16,
            )
          else
            Container(),
          Expanded(
            child: Row(
              children: [
                Expanded(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        widget.title,
                        style: XelaTextStyle.XelaBodyBold.apply(
                          color: widget.titleColor,
                        ),
                      ),
                      if (widget.description != null)
                        Text(
                          widget.description!,
                          style: XelaTextStyle.XelaSmallBody.apply(
                            color: widget.descriptionColor,
                          ),
                        )
                      else
                        Container(),
                      SizedBox(
                        height: widget.firstActionButton != null &&
                                widget.secondActionButton != null
                            ? 8
                            : 0,
                      ),
                      if (widget.firstActionButton != null &&
                          widget.secondActionButton != null)
                        Row(
                          children: [
                            if (widget.firstActionButton != null)
                              widget.firstActionButton!
                            else
                              Container(),
                            const SizedBox(
                              width: 18,
                            ),
                            if (widget.secondActionButton != null)
                              widget.secondActionButton!
                            else
                              Container(),
                          ],
                        )
                      else
                        Container(),
                    ],
                  ),
                ),
                SizedBox(
                  width: (widget.firstActionButton != null &&
                              widget.secondActionButton == null) ||
                          (widget.firstActionButton == null &&
                              widget.secondActionButton != null)
                      ? 16
                      : 0,
                ),
                if ((widget.firstActionButton != null &&
                        widget.secondActionButton == null) ||
                    (widget.firstActionButton == null &&
                        widget.secondActionButton != null))
                  widget.firstActionButton != null
                      ? widget.firstActionButton!
                      : widget.secondActionButton!
                else
                  Container(),
                SizedBox(
                  width: (widget.firstActionButton != null &&
                              widget.secondActionButton == null) ||
                          (widget.firstActionButton == null &&
                              widget.secondActionButton != null)
                      ? 16
                      : widget.rightButton != null
                          ? 8
                          : 0,
                ),
                if (widget.rightButton != null)
                  widget.rightButton!
                else
                  Container()
              ],
            ),
          ),
        ],
      ),
    );

    if (widget.autoresize) {
      return Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            padding: const EdgeInsets.all(16),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18),
              color: widget.background,
            ),
            child: Row(
              crossAxisAlignment: widget.icon != null
                  ? CrossAxisAlignment.start
                  : CrossAxisAlignment.center,
              children: [
                if (widget.icon != null)
                  Container(
                    alignment: Alignment.topCenter,
                    padding: const EdgeInsets.only(right: 16),
                    child: SizedBox(
                      width: 24,
                      height: 24,
                      child: FittedBox(
                        child: widget.icon,
                      ),
                    ),
                  )
                else
                  Container(),
                if (widget.avatar != null) widget.avatar! else Container(),
                if (widget.avatar != null)
                  const SizedBox(
                    width: 16,
                  )
                else
                  Container(),
                Row(
                  children: [
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.title,
                          style: XelaTextStyle.XelaBodyBold.apply(
                            color: widget.titleColor,
                          ),
                        ),
                        if (widget.description != null)
                          Text(
                            widget.description!,
                            style: XelaTextStyle.XelaSmallBody.apply(
                              color: widget.descriptionColor,
                            ),
                          )
                        else
                          Container(),
                        SizedBox(
                          height: widget.firstActionButton != null &&
                                  widget.secondActionButton != null
                              ? 8
                              : 0,
                        ),
                        if (widget.firstActionButton != null &&
                            widget.secondActionButton != null)
                          Wrap(
                            children: [
                              if (widget.firstActionButton != null)
                                widget.firstActionButton!
                              else
                                Container(),
                              const SizedBox(
                                width: 18,
                              ),
                              if (widget.secondActionButton != null)
                                widget.secondActionButton!
                              else
                                Container(),
                            ],
                          )
                        else
                          Container(),
                      ],
                    ),
                    SizedBox(
                      width: (widget.firstActionButton != null &&
                                  widget.secondActionButton == null) ||
                              (widget.firstActionButton == null &&
                                  widget.secondActionButton != null)
                          ? 16
                          : 0,
                    ),
                    if ((widget.firstActionButton != null &&
                            widget.secondActionButton == null) ||
                        (widget.firstActionButton == null &&
                            widget.secondActionButton != null))
                      widget.firstActionButton != null
                          ? widget.firstActionButton!
                          : widget.secondActionButton!
                    else
                      Container(),
                    SizedBox(
                      width: (widget.firstActionButton != null &&
                                  widget.secondActionButton == null) ||
                              (widget.firstActionButton == null &&
                                  widget.secondActionButton != null)
                          ? 16
                          : 0,
                    ),
                    if (widget.rightButton != null)
                      widget.rightButton!
                    else
                      Container()
                  ],
                ),
              ],
            ),
          )
        ],
      );
    }

    return child;
  }
}
