import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

class XelaSegmentedControl extends StatefulWidget {
  final List<XelaSegmentedControlItem> items;
  final int segmentedControlValue;
  final Function(XelaSegmentedControlItem)? onChange;
  final Color primaryBackground;
  final Color secondaryBackground;
  final Color primaryFontColor;
  final Color secondaryFontColor;
  final bool autoResize;

  const XelaSegmentedControl({
    Key? key,
    required this.items,
    this.segmentedControlValue = 0,
    this.onChange,
    this.primaryBackground = XelaColor.Blue6,
    this.secondaryBackground = XelaColor.Gray12,
    this.primaryFontColor = Colors.white,
    this.secondaryFontColor = XelaColor.Gray2,
    this.autoResize = false,
  }) : super(key: key);

  @override
  _XelaSegmentedControlState createState() => _XelaSegmentedControlState();
}

class _XelaSegmentedControlState extends State<XelaSegmentedControl>
    with SingleTickerProviderStateMixin {
  _XelaSegmentedControlState();

  int segmentedControlValue = 0;
  String? selectedItemId;

  @override
  void initState() {
    super.initState();
    segmentedControlValue = widget.segmentedControlValue;

    SchedulerBinding.instance.addPostFrameCallback(
      (_) => setState(() {
        selectedItemId = widget.items[segmentedControlValue].id;

        final RenderBox renderBox = keys[segmentedControlValue]
            .currentContext
            ?.findRenderObject() as RenderBox;
        final Offset childOffset = renderBox.localToGlobal(Offset.zero);
        final RenderBox parent =
            parentKey.currentContext?.findRenderObject() as RenderBox;
        final Offset childRelativeToParent = parent.globalToLocal(childOffset);

        width = renderBox.size.width;
        left = childRelativeToParent.dx;
      }),
    );
  }

  @override
  void dispose() {
    // Clean up the focus nodes
    // when the form is disposed
    keys.clear();
    super.dispose();
  }

  double itemWidth = 0;

  List<GlobalKey> keys = [];

  double left = 0;
  double width = 0;

  GlobalKey parentKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    keys.clear();
    final List<Widget> children = [];

    var i = 0;
    for (var item in widget.items) {
      final key = GlobalKey();
      keys.add(key);

      final child = InkWell(
        onTap: () {
          setState(() {
            segmentedControlValue = i;
            selectedItemId = item.id;

            final RenderBox renderBox =
                key.currentContext?.findRenderObject() as RenderBox;
            final Offset childOffset = renderBox.localToGlobal(Offset.zero);
            final RenderBox parent =
                parentKey.currentContext?.findRenderObject() as RenderBox;
            final Offset childRelativeToParent =
                parent.globalToLocal(childOffset);

            width = renderBox.size.width;
            left = childRelativeToParent.dx;
          });

          if (widget.onChange != null) {
            widget.onChange?.call(item);
          }
        },
        child: Container(
          padding: const EdgeInsets.all(16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              if (item.iconInactive != null && item.iconActive != null)
                Container(
                  padding: EdgeInsets.only(right: item.label != null ? 8 : 0),
                  alignment: Alignment.center,
                  width: item.label != null ? 24 : 16,
                  height: 16,
                  child: FittedBox(
                    fit: BoxFit.fill,
                    child: selectedItemId == item.id
                        ? item.iconActive
                        : item.iconInactive,
                  ),
                )
              else
                Container(),
              if (item.label != null)
                Text(
                  item.label!,
                  style: XelaTextStyle.XelaButtonMedium.apply(
                    color: selectedItemId == item.id
                        ? widget.primaryFontColor
                        : widget.secondaryFontColor,
                  ),
                )
              else
                Container(),
            ],
          ),
        ),
      );

      if (widget.autoResize) {
        children.add(Wrap(key: key, children: [child]));
      } else {
        children.add(Expanded(key: key, child: child));
      }

      i++;
    }

    return Column(
      key: parentKey,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          height: 48,
          decoration: BoxDecoration(
            color: widget.secondaryBackground,
            borderRadius: BorderRadius.circular(12),
          ),
          child: Stack(
            children: [
              AnimatedPositioned(
                left: left,
                duration: const Duration(milliseconds: 300),
                curve: Curves.fastOutSlowIn,
                child: AnimatedContainer(
                  duration: const Duration(milliseconds: 300),
                  height: 48,
                  width: width,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(12),
                    color: widget.primaryBackground,
                  ),
                  curve: Curves.fastOutSlowIn,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize:
                    widget.autoResize ? MainAxisSize.min : MainAxisSize.max,
                children: children,
              ),
            ],
          ),
        )
      ],
    );
  }
}

class XelaSegmentedControlItem {
  final String id;
  final String? label;
  final Widget? iconActive;
  final Widget? iconInactive;

  XelaSegmentedControlItem({
    required this.id,
    this.label,
    this.iconActive,
    this.iconInactive,
  });
}
