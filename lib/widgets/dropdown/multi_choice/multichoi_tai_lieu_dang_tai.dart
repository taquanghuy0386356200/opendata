import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/xela_buttom_custom.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/dropdown/multi_choice/multi_choice_cubit.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class MultiChoiceTaiLieuDangTai extends StatefulWidget {
  const MultiChoiceTaiLieuDangTai({
    Key? key,
    required this.getCategoriesSelected,
    required this.getSubCategories,
    this.categoryIDSuaTaiLieu,
    this.subCategoryIDSuaTaiLieu,
  }) : super(key: key);
  final Function(List<CategoryModel> categories) getCategoriesSelected;
  final Function(List<SubCategoriesModel> subcategories) getSubCategories;
  final String? categoryIDSuaTaiLieu;
  final String? subCategoryIDSuaTaiLieu;

  @override
  State<MultiChoiceTaiLieuDangTai> createState() =>
      MultiChoiceTaiLieuDangTaiState();
}

class MultiChoiceTaiLieuDangTaiState extends State<MultiChoiceTaiLieuDangTai> {
  late MultiChoiceCubit cubit;

  @override
  void initState() {
    super.initState();
    cubit = MultiChoiceCubit();
    if ((widget.categoryIDSuaTaiLieu ?? '').isNotEmpty &&
        (widget.subCategoryIDSuaTaiLieu ?? '').isNotEmpty) {
      cubit.getCategoriesList().then((value) => cubit.initDataSuaTaiLieu(
            widget.categoryIDSuaTaiLieu ?? '',
            widget.subCategoryIDSuaTaiLieu ?? '',
          ));
    } else {
      cubit.getCategoriesList().then((value) => null);
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Align(
          alignment: Alignment.centerLeft,
          child: RichText(
            textScaleFactor: MediaQuery.of(context).textScaleFactor,
            text: TextSpan(
              text: S.current.linh_vuc,
              style: XelaTextStyle.Xela14Regular.copyWith(
                color: color586B8B,
              ),
              children: [
                TextSpan(
                  text: ' *',
                  style: XelaTextStyle.Xela16Regular.copyWith(
                    color: Colors.red,
                  ),
                )
              ],
            ),
          ),
        ),
        spaceH8,
        InkWell(
          onTap: () {
            showModalBottomSheet(
              backgroundColor: Colors.transparent,
              context: context,
              builder: (_) {
                return Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    StreamBuilder<List<CategoryModel>>(
                      stream: cubit.categoriesList.stream,
                      builder: (context, snapshot) {
                        final data = snapshot.data ?? [];
                        return Container(
                          height: 352,
                          padding: const EdgeInsets.only(
                            bottom: 80,
                            left: 16,
                            right: 16,
                            top: 16,
                          ),
                          decoration: const BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(8),
                              topRight: Radius.circular(8),
                            ),
                            color: colorBgHome,
                          ),
                          child: ListView.builder(
                            itemBuilder: (context, index) {
                              return ItemCheckBox(
                                isChoosing: data[index].isChoosing,
                                onTap: (name, id, isChoosing) {
                                  cubit.changeSubCategory(id);
                                },
                                name: data[index].name,
                                id: data[index].id,
                              );
                            },
                            itemCount: data.length,
                            shrinkWrap: true,
                            // physics: const NeverScrollableScrollPhysics(),
                          ),
                        );
                      },
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 16,
                        vertical: 16,
                      ),
                      child: XelaButtonCustom(
                        text: S.current.xac_nhan,
                        autoResize: false,
                        onPressed: () {
                          cubit.checkAllValidate();
                          final _ = cubit.getSubcategoriesSelected();
                          final result = cubit.getCategorySelected();
                          widget.getCategoriesSelected(result);
                          Navigator.pop(context);
                        },
                      ),
                    )
                  ],
                );
              },
            ).whenComplete(() {
              cubit.checkAllValidate();
              final _ = cubit.getSubcategoriesSelected();
              final result = cubit.getCategorySelected();
              widget.getCategoriesSelected(result);
            });
          },
          child: Container(
            // height: 52,
            width: double.infinity,
            padding: const EdgeInsets.all(12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              border: Border.all(
                color: colorDBDFEF,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                StreamBuilder<String>(
                  stream: cubit.valueCategorySelected.stream,
                  builder: (context, snapshot) {
                    final data = snapshot.data ?? '';
                    return (data == S.current.chon_linh_vuc)
                        ? Expanded(
                            child: Text(
                              S.current.chon_linh_vuc,
                              maxLines: 1,
                              style: XelaTextStyle.Xela14Regular.copyWith(
                                overflow: TextOverflow.ellipsis,
                                color: XelaColor.Gray8,
                              ),
                            ),
                          )
                        : Expanded(
                            child: Text(
                              data,
                              maxLines: 1,
                              style: XelaTextStyle.Xela14Regular.copyWith(
                                overflow: TextOverflow.ellipsis,
                                color: color304261,
                              ),
                            ),
                          );
                  },
                ),
                SizedBox(
                  height: 24,
                  width: 24,
                  child: Image.asset(ImageAssets.icDropDownArror),
                )
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: StreamBuilder<bool>(
            stream: cubit.validateCategory.stream,
            builder: (context, snapshot) {
              final data = snapshot.data ?? true;
              return data
                  ? const SizedBox.shrink()
                  : Padding(
                      padding:
                          const EdgeInsets.only(left: 24, right: 24, top: 8),
                      child: Text(
                        S.current.vui_long_chon_linh_vuc,
                        style: XelaTextStyle.Xela14Regular.copyWith(
                          color: Colors.red,
                        ),
                      ),
                    );
            },
          ),
        ),
        spaceH20,
        Align(
          alignment: Alignment.centerLeft,
          child: RichText(
            textScaleFactor: MediaQuery.of(context).textScaleFactor,
            text: TextSpan(
              text: S.current.chuyen_muc,
              style: XelaTextStyle.Xela14Regular.copyWith(
                color: color586B8B,
              ),
              children: [
                TextSpan(
                  text: ' *',
                  style: XelaTextStyle.Xela16Regular.copyWith(
                    color: Colors.red,
                  ),
                )
              ],
            ),
          ),
        ),
        spaceH8,
        InkWell(
          onTap: () {
            showModalBottomSheet(
              backgroundColor: Colors.transparent,
              // isScrollControlled: false,
              context: context,
              builder: (_) {
                return Stack(
                  alignment: Alignment.bottomCenter,
                  children: [
                    Container(
                      height: 352,
                      padding: const EdgeInsets.only(
                        bottom: 80,
                        left: 16,
                        top: 16,
                        right: 16,
                      ),
                      decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(8),
                          topRight: Radius.circular(8),
                        ),
                        color: colorBgHome,
                      ),
                      child: ListView.builder(
                        itemBuilder: (context, index) {
                          return ItemCheckBox(
                            isChoosing:
                                cubit.listSubCategories[index].isChoosing,
                            onTap: (name, id, value) {
                              cubit.checkExistCategory(id, isSubCategory: true);
                            },
                            name: cubit.listSubCategories[index].name,
                            id: cubit.listSubCategories[index].id,
                          );
                        },
                        itemCount: cubit.listSubCategories.length,
                        shrinkWrap: true,
                        // physics: const NeverScrollableScrollPhysics(),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        vertical: 16,
                        horizontal: 16,
                      ),
                      child: XelaButtonCustom(
                        text: S.current.xac_nhan,
                        autoResize: false,
                        onPressed: () {
                          cubit.checkAllValidate();
                          final result = cubit.getSubcategoriesSelected();
                          widget.getSubCategories(result);
                          Navigator.pop(context);
                        },
                      ),
                    )
                  ],
                );
              },
            ).whenComplete(() {
              cubit.checkAllValidate();
              final result = cubit.getSubcategoriesSelected();
              widget.getSubCategories(result);
            });
          },
          child: Container(
            // height: 52,
            width: double.infinity,
            padding: const EdgeInsets.all(12),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(6),
              border: Border.all(
                color: colorDBDFEF,
              ),
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                StreamBuilder<String>(
                  stream: cubit.valueSubCategorySelected.stream,
                  builder: (context, snapshot) {
                    final data = snapshot.data ?? '';
                    return (data == S.current.chon_chuyen_muc)
                        ? Expanded(
                            child: Text(
                              S.current.chon_chuyen_muc,
                              maxLines: 1,
                              style: XelaTextStyle.Xela14Regular.copyWith(
                                overflow: TextOverflow.ellipsis,
                                color: XelaColor.Gray8,
                              ),
                            ),
                          )
                        : Expanded(
                      child: Text(
                              data,
                              maxLines: 1,
                              style: XelaTextStyle.Xela14Regular.copyWith(
                                overflow: TextOverflow.ellipsis,
                                color: color304261,
                              ),
                            ),
                          );
                  },
                ),
                SizedBox(
                  height: 24,
                  width: 24,
                  child: Image.asset(ImageAssets.icDropDownArror),
                )
              ],
            ),
          ),
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: StreamBuilder<bool>(
            stream: cubit.validateSubCategory.stream,
            builder: (context, snapshot) {
              final data = snapshot.data ?? true;
              return data
                  ? const SizedBox.shrink()
                  : Padding(
                      padding:
                          const EdgeInsets.only(left: 24, right: 24, top: 8),
                      child: Text(
                        S.current.vui_long_chon_chuyen_muc,
                        style: XelaTextStyle.Xela14Regular.copyWith(
                          color: Colors.red,
                        ),
                      ),
                    );
            },
          ),
        )
      ],
    );
  }
}

class ItemCheckBox extends StatefulWidget {
  const ItemCheckBox({
    Key? key,
    required this.name,
    required this.id,
    required this.isChoosing,
    required this.onTap,
  }) : super(key: key);
  final String name;
  final int id;

  final bool isChoosing;
  final Function(String name, int id, bool value) onTap;

  @override
  State<ItemCheckBox> createState() => _ItemCheckBoxState();
}

class _ItemCheckBoxState extends State<ItemCheckBox> {
  late bool isChoosing;

  @override
  void initState() {
    super.initState();
    isChoosing = widget.isChoosing;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        setState(() {
          isChoosing = !isChoosing;
        });
        widget.onTap(widget.name, widget.id, isChoosing);
      },
      child: Container(
        margin: const EdgeInsets.only(bottom: 8),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Text(
                widget.name,
                style: XelaTextStyle.Xela14Regular.copyWith(color: color586B8B),
              ),
            ),
            if (widget.id != -1)
              Container(
                height: 20,
                width: 20,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(
                    color: color304261,
                  ),
                ),
                child: isChoosing
                    ? Image.asset(ImageAssets.icCheck)
                    : const SizedBox.shrink(),
              )
            else
              const SizedBox.shrink()
          ],
        ),
      ),
    );
  }
}
