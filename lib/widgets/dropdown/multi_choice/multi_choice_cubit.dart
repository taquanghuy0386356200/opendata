import 'package:bloc/bloc.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/domain/repository/home/home_repository.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'multi_choice_state.dart';

class MultiChoiceCubit extends Cubit<MultiChoiceState> {
  MultiChoiceCubit() : super(MultiChoiceInitial());

  HomeRepository get _homeRepo => Get.find();

  List<SubCategoriesModel> listSubCategories = [];
  final BehaviorSubject<List<CategoryModel>> categoriesList = BehaviorSubject();

  final BehaviorSubject<String> valueCategorySelected =
      BehaviorSubject.seeded(S.current.chon_linh_vuc);
  final BehaviorSubject<String> valueSubCategorySelected =
      BehaviorSubject.seeded(S.current.chon_chuyen_muc);
  final BehaviorSubject<bool> validateCategory = BehaviorSubject.seeded(true);
  final BehaviorSubject<bool> validateSubCategory =
      BehaviorSubject.seeded(true);

  //todo còn 1 case sửa tài liệu phải check

  void checkIsFirstInit() {
    ///sẽ check có phải lần đầu tạo hay không
    ///nếu có thì add vào list cate như bth, hint chọn dữ liệu
    ///còn không thì init value cho 2 cái
  }

  Future<void> getCategoriesList() async {
    final result = await _homeRepo.getListCategoryHome();
    result.when(
      success: (response) {
        categoriesList.sink.add(response);
        listSubCategories.add(
          SubCategoriesModel(-1, S.current.khong_co_du_lieu),
        );
      },
      error: (error) {
        categoriesList.sink.add([]);
      },
    );
  }

  void initDataSuaTaiLieu(
    String categoriesIDString,
    String subCategoriesIDString,
  ) {
    final List<String> categoriesIDStringList = categoriesIDString.split(',');
    final List<String> subCategoriesIDStringList =
        subCategoriesIDString.split(',');

    final listIdCategoryEdit =
        categoriesIDStringList.map((e) => int.parse(e)).toList();
    final listIdSubCategoryEdit =
        subCategoriesIDStringList.map((e) => int.parse(e)).toList();

    for (final category in categoriesList.value) {
      for (final idCateEdit in listIdCategoryEdit) {
        if (category.id == idCateEdit) {
          category.isChoosing = true;
          break;
        }
      }
    }

    listSubCategories.clear();
    for (final e in categoriesList.value) {
      {
        if (e.isChoosing) {
          listSubCategories.addAll(e.subCategories ?? []);
        }
      }
    }

    for (final subCategory in listSubCategories) {
      for (final idSubCateEdit in listIdSubCategoryEdit) {
        if (subCategory.id == idSubCateEdit) {
          subCategory.isChoosing = true;
          break;
        }
      }
    }

    getCategorySelected();
    getSubcategoriesSelected();
  }

  void checkExistCategory(
    int id, {
    bool isSubCategory = false,
  }) {
    if (isSubCategory) {
      for (final element in listSubCategories) {
        if (element.id == id) {
          element.isChoosing = !element.isChoosing;
        }
      }
    } else {
      for (final element in categoriesList.value) {
        if (element.id == id) {
          element.isChoosing = !element.isChoosing;
        }
      }
    }
  }

  void changeSubCategory(int id) {
    listSubCategories.removeWhere((subCate) => subCate.id == -1);
    checkExistCategory(id);
    for (final element in categoriesList.value) {
      if (element.isChoosing) {
        listSubCategories.addAll(element.subCategories ?? []);
      } else {
        //removeSubcategoryUnchoose
        element.subCategories?.forEach((cate) {
          listSubCategories.removeWhere((subCate) => subCate.id == cate.id);
        });
      }
    }
    //checkDuplicateCategory
    final ids = listSubCategories.map((e) => e.id).toSet();
    listSubCategories.retainWhere((element) => ids.remove(element.id));
    //ifEmptyAddNoData
    if (listSubCategories.isEmpty) {
      listSubCategories.add(
        SubCategoriesModel(-1, S.current.khong_co_du_lieu),
      );
    }
  }

  List<CategoryModel> getCategorySelected() {
    final result = categoriesList.value.where((e) => e.isChoosing).toList();
    final List<String> valueSelectedShowUI = [];
    for (final element in result) {
      valueSelectedShowUI.add(element.name);
    }
    if (valueSelectedShowUI.isEmpty) {
      valueCategorySelected.sink.add(S.current.chon_linh_vuc);
    } else {
      valueCategorySelected.sink.add(valueSelectedShowUI.join(','));
    }
    return result;
  }

  List<SubCategoriesModel> getSubcategoriesSelected() {
    final result =
        listSubCategories.where((element) => element.isChoosing).toList();
    final List<String> valueSelectedShowUI = [];
    for (final element in result) {
      valueSelectedShowUI.add(element.name);
    }
    if (valueSelectedShowUI.isEmpty) {
      valueSubCategorySelected.sink.add(S.current.chon_chuyen_muc);
    } else {
      valueSubCategorySelected.sink.add(valueSelectedShowUI.join(','));
    }
    return result;
  }

  void checkAllValidate() {
    if (categoriesList.value.map((e) => e.isChoosing).contains(true)) {
      validateCategory.sink.add(true);
    } else {
      validateCategory.sink.add(false);
    }
    if (listSubCategories.map((e) => e.isChoosing).contains(true)) {
      validateSubCategory.sink.add(true);
    } else {
      validateSubCategory.sink.add(false);
    }
  }
}
