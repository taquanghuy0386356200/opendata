import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:cool_dropdown/cool_dropdown.dart';
import 'package:flutter/material.dart';

class CoolDropDown extends StatefulWidget {
  final String placeHoder;
  final String initData;
  final Function(int) onChange;
  final List<String> listData;
  final double? setWidth;
  final bool showSelectedDecoration;
  final bool useCustomHintColors;
  final bool isDisible;

  const CoolDropDown({
    Key? key,
    this.placeHoder = '',
    required this.onChange,
    required this.listData,
    required this.initData,
    this.setWidth,
    this.showSelectedDecoration = true,
    this.useCustomHintColors = false,
    this.isDisible = false,
  }) : super(key: key);

  @override
  _CoolDropDownState createState() => _CoolDropDownState();
}

class _CoolDropDownState extends State<CoolDropDown> {
  final List<Map<dynamic, dynamic>> pokemonsMap = [];
  int initIndex = -1;

  @override
  void initState() {
    for (var i = 0; i < widget.listData.length; i++) {
      pokemonsMap.add({
        'label': widget.listData[i],
        'value': widget.listData[i],
        'icon': const SizedBox(),
      });
    }
    initIndex = widget.listData.indexOf(widget.initData);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return CoolDropdown(
      defaultValue: initIndex < 0 ? null : pokemonsMap[initIndex],
      resultWidth: MediaQuery.of(context).size.width,
      dropdownWidth: dropdownSize(
        widget.setWidth ?? MediaQuery.of(context).size.width - 52,
      ),
      dropdownHeight: dropdownSize(checkHeight(widget.listData.length)),
      resultAlign: Alignment.center,
      dropdownList: pokemonsMap,
      onChange: (value) {
        widget.onChange(pokemonsMap.indexOf(value));
      },
      placeholder: widget.placeHoder,
      selectedItemTS: XelaTextStyle.Xela14Regular.copyWith(color: color304261),
      unselectedItemTS: XelaTextStyle.Xela14Regular.copyWith(
        color: XelaColor.Gray8,
      ),
      selectedItemBD: widget.showSelectedDecoration
          ? BoxDecoration(
              color: colorE26F2C,
              borderRadius: BorderRadius.circular(6),
            )
          : const BoxDecoration(),
      resultTS: XelaTextStyle.Xela14Regular.copyWith(color: color586B8B),
      placeholderTS: XelaTextStyle.Xela14Regular.copyWith(
        color: XelaColor.Gray8,
      ),
      resultBD: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: widget.isDisible
            ? colorDBDFEF.withOpacity(0.2)
            : Colors.transparent,
        border: Border.all(color: colorDBDFEF),
      ),
      dropdownBD: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: Colors.white,
        border: Border.all(
          color: widget.isDisible ? Colors.transparent : colorDBDFEF,
        ),
      ),
      isTriangle: false,
      resultIconRotationValue: widget.isDisible ? 0 : 0.5,
    );
  }

  double dropdownSize(double defaultValue) {
    if (widget.isDisible) {
      return 0;
    }
    return defaultValue;
  }

  double checkHeight(int data) {
    const double heightOfOne = 60;
    switch (data) {
      case 0:
        return heightOfOne - 30;
      case 1:
        return heightOfOne;
      case 2:
        return heightOfOne * 2;
      case 3:
        return heightOfOne * 3;
    }
    return heightOfOne * 3 + heightOfOne * 0.4;
  }
}
