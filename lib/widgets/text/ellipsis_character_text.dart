import 'package:flutter/material.dart';

class EllipsisDoubleLineText extends StatelessWidget {
  final String data;
  final TextStyle? style;
  static const double _defaultFontSize = 14;
  final StrutStyle? strutStyle;
  final TextAlign? textAlign;
  final TextDirection? textDirection;
  final Locale? locale;
  final bool? softWrap;
  final double? textScaleFactor;
  final String? semanticsLabel;
  final TextWidthBasis? textWidthBasis;
  final TextHeightBehavior? textHeightBehavior;
  final Color? selectionColor;
  final int maxLines;

  const EllipsisDoubleLineText(
    this.data, {
    Key? key,
    this.locale,
    this.maxLines = 2,
    this.semanticsLabel,
    this.selectionColor,
    this.softWrap,
    this.strutStyle,
    this.style,
    this.textAlign,
    this.textDirection,
    this.textHeightBehavior,
    this.textScaleFactor,
    this.textWidthBasis,
  }) : super(key: key);

  String _replaceString(
    int? maxLines,
    TextPainter textPainter, {
    double maxWidth = 0,
    double width = 0,
  }) {
    if (maxLines == null) return data;
    final textSlip = data.split(' ');
    String charaters = '';
    String newText = '';
    for (final element in textSlip) {
      final painter = TextPainter(
        text: TextSpan(text: '${charaters.trim()}...', style: style),
        textDirection: TextDirection.ltr,
        textHeightBehavior: textHeightBehavior,
        strutStyle: strutStyle,
        maxLines: 10,
      );

      painter.layout(maxWidth: maxWidth);
      final metrics = painter.computeLineMetrics();
      if (metrics.length > 2) {
        return '${newText.trim()}...';
      } else {
        newText = charaters;
      }

      charaters = '$charaters$element ';
    }

    return data;
  }

  String _loadData(
    BoxConstraints constraints,
    TextStyle style,
    double? textScale,
    int? maxLine,
  ) {
    final textPainter = TextPainter(
      text: TextSpan(text: data, style: style),
      textDirection: TextDirection.ltr,
      locale: locale ?? style.locale,
      textScaleFactor: textScale ?? 1,
      textHeightBehavior: textHeightBehavior,
      strutStyle: strutStyle,
      maxLines: 10,
      ellipsis: '...',
    );

    textPainter.layout(maxWidth: constraints.maxWidth);
    final lineText = textPainter.computeLineMetrics();
    if (lineText.length < maxLines) {
      return data;
    }
    final String newString = _replaceString(
      maxLine,
      textPainter,
      maxWidth: constraints.maxWidth,
      width: lineText[maxLines - 1].width,
    );
    return newString;
  }

  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        final defaultTextStyle = DefaultTextStyle.of(context);

        late TextStyle textStyle;
        if (style == null || style!.inherit) {
          textStyle = defaultTextStyle.style.merge(style);
        }
        if (textStyle.fontSize == null) {
          textStyle = textStyle.copyWith(
            fontSize: EllipsisDoubleLineText._defaultFontSize,
          );
        }

        final int maxLines = this.maxLines;

        final replaceCharacter = _loadData(constraints, textStyle, 1, maxLines);

        return Text(
          replaceCharacter,
          style: textStyle,
          overflow: TextOverflow.ellipsis,
          textHeightBehavior: textHeightBehavior,
          textAlign: textAlign,
          softWrap: softWrap,
          textDirection: textDirection,
          textWidthBasis: textWidthBasis,
          textScaleFactor: 1,
          locale: locale,
          semanticsLabel: semanticsLabel,
          strutStyle: strutStyle,
          maxLines: 10,
        );
      },
    );
  }
}
