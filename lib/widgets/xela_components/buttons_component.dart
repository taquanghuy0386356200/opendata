import 'package:bnv_opendata/domain/models/xela_button_models.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_button.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_divider.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_toggle.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class ButtonsComponent extends StatefulWidget {
  const ButtonsComponent({Key? key}) : super(key: key);

  @override
  _ButtonsComponentState createState() => _ButtonsComponentState();
}

class _ButtonsComponentState extends State<ButtonsComponent> {
  bool _isDark = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: _isDark ? XelaColor.Gray1 : Colors.white,
      body: Padding(
        padding: const EdgeInsets.only(top: kIsWeb ? 0 : 24),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 8),
              child: Row(
                children: [
                  RawMaterialButton(
                    elevation: 0,
                    focusElevation: 2,
                    highlightElevation: 0,
                    fillColor: Colors.transparent,
                    hoverElevation: 0,
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    constraints: const BoxConstraints(),
                    child: Padding(
                      padding: const EdgeInsets.all(16),
                      child: Icon(
                        Icons.arrow_back,
                        size: 20,
                        color: _isDark ? XelaColor.Gray11 : XelaColor.Gray2,
                      ),
                    ),
                  ),
                  Expanded(
                    child: Text(
                      'Buttons',
                      style: XelaTextStyle.XelaSubheadline.apply(
                        color: _isDark ? XelaColor.Gray11 : XelaColor.Gray2,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 16),
                    child: XelaToggle(
                      onToggle: (val) {
                        setState(() {
                          _isDark = val;
                        });
                      },
                      status: _isDark,
                      iconOn: const Icon(
                        Icons.nightlight_round,
                        size: 20,
                        color: XelaColor.Gray3,
                      ),
                      iconOff: const Icon(
                        Icons.nightlight_round,
                        size: 20,
                        color: XelaColor.Gray7,
                      ),
                      onBackground: XelaColor.Gray3,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: ListView(
                padding: EdgeInsets.zero,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Center(
                      child: Text(
                        'Usage Example',
                        style: XelaTextStyle.XelaCaption.apply(
                          color: _isDark ? XelaColor.Gray6 : XelaColor.Gray4,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaDivider(
                      color: _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.bookmark,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Bookmark',
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_4x4,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Grid',
                            background:
                                _isDark ? XelaColor.Pink5 : XelaColor.Pink3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.format_list_bulleted,
                              size: 20,
                              color: Colors.white,
                            ),
                            background: XelaColor.Gray3,
                          )
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.delete,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Delete item',
                            background:
                                _isDark ? XelaColor.Red6 : XelaColor.Red3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            rightIcon: Icon(
                              Icons.download,
                              size: 20,
                              color: _isDark ? Colors.black : Colors.white,
                            ),
                            text: 'Download file',
                            background: XelaColor.Green2,
                            foregroundColor:
                                _isDark ? Colors.black : Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            rightIcon: const Icon(
                              Icons.arrow_forward,
                              size: 20,
                              color: XelaColor.Purple3,
                            ),
                            text: 'Next',
                            type: XelaButtonType.SECONDARY,
                            background: Colors.transparent,
                            foregroundColor: XelaColor.Purple3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                          XelaButton(
                            onPressed: () {},
                            rightIcon: Icon(
                              Icons.more_horiz,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            text: 'More',
                            type: XelaButtonType.SECONDARY,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Center(
                      child: Text(
                        'Type: Primary Size: Large',
                        style: XelaTextStyle.XelaCaption.apply(
                          color: _isDark ? XelaColor.Gray6 : XelaColor.Gray4,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaDivider(
                      color: _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            text: 'Button',
                            size: XelaButtonSize.LARGE,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            rightIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Button',
                            size: XelaButtonSize.LARGE,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Button',
                            size: XelaButtonSize.LARGE,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            rightIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Button',
                            size: XelaButtonSize.LARGE,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            rightIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            size: XelaButtonSize.LARGE,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            size: XelaButtonSize.LARGE,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaButton(
                      onPressed: () {},
                      leftIcon: const Icon(
                        Icons.grid_view,
                        size: 20,
                        color: Colors.white,
                      ),
                      rightIcon: const Icon(
                        Icons.grid_view,
                        size: 20,
                        color: Colors.white,
                      ),
                      text: 'Autoresize Off',
                      size: XelaButtonSize.LARGE,
                      background: _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      autoResize: false,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Center(
                      child: Text(
                        'Type: Secondary Size: Large',
                        style: XelaTextStyle.XelaCaption.apply(
                          color: _isDark ? XelaColor.Gray6 : XelaColor.Gray4,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaDivider(
                      color: _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.LARGE,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            rightIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.LARGE,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.LARGE,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                          XelaButton(
                            onPressed: () {},
                            rightIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.LARGE,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            rightIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.LARGE,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.LARGE,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaButton(
                      onPressed: () {},
                      leftIcon: Icon(
                        Icons.grid_view,
                        size: 20,
                        color: _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      ),
                      rightIcon: Icon(
                        Icons.grid_view,
                        size: 20,
                        color: _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      ),
                      text: 'Autoresize Off',
                      type: XelaButtonType.SECONDARY,
                      size: XelaButtonSize.LARGE,
                      background: Colors.transparent,
                      foregroundColor:
                          _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      defaultBorderColor:
                          _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                      autoResize: false,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Center(
                      child: Text(
                        'Type: Primary Size: Medium',
                        style: XelaTextStyle.XelaCaption.apply(
                          color: _isDark ? XelaColor.Gray6 : XelaColor.Gray4,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaDivider(
                      color: _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            text: 'Button',
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            rightIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Button',
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Button',
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            rightIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Button',
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            rightIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaButton(
                      onPressed: () {},
                      leftIcon: const Icon(
                        Icons.grid_view,
                        size: 20,
                        color: Colors.white,
                      ),
                      rightIcon: const Icon(
                        Icons.grid_view,
                        size: 20,
                        color: Colors.white,
                      ),
                      text: 'Autoresize Off',
                      background: _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      autoResize: false,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Center(
                      child: Text(
                        'Type: Secondary Size: Medium',
                        style: XelaTextStyle.XelaCaption.apply(
                          color: _isDark ? XelaColor.Gray6 : XelaColor.Gray4,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaDivider(
                      color: _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            rightIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                          XelaButton(
                            onPressed: () {},
                            rightIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            rightIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            type: XelaButtonType.SECONDARY,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            type: XelaButtonType.SECONDARY,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaButton(
                      onPressed: () {},
                      leftIcon: Icon(
                        Icons.grid_view,
                        size: 20,
                        color: _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      ),
                      rightIcon: Icon(
                        Icons.grid_view,
                        size: 20,
                        color: _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      ),
                      text: 'Autoresize Off',
                      type: XelaButtonType.SECONDARY,
                      background: Colors.transparent,
                      foregroundColor:
                          _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      defaultBorderColor:
                          _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                      autoResize: false,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Center(
                      child: Text(
                        'Type: Primary Size: Small',
                        style: XelaTextStyle.XelaCaption.apply(
                          color: _isDark ? XelaColor.Gray6 : XelaColor.Gray4,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaDivider(
                      color: _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            text: 'Button',
                            size: XelaButtonSize.SMALL,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            rightIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Button',
                            size: XelaButtonSize.SMALL,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Button',
                            size: XelaButtonSize.SMALL,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            rightIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            text: 'Button',
                            size: XelaButtonSize.SMALL,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            rightIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            size: XelaButtonSize.SMALL,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: const Icon(
                              Icons.grid_view,
                              size: 20,
                              color: Colors.white,
                            ),
                            size: XelaButtonSize.SMALL,
                            background:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaButton(
                      onPressed: () {},
                      leftIcon: const Icon(
                        Icons.grid_view,
                        size: 20,
                        color: Colors.white,
                      ),
                      rightIcon: const Icon(
                        Icons.grid_view,
                        size: 20,
                        color: Colors.white,
                      ),
                      text: 'Autoresize Off',
                      size: XelaButtonSize.SMALL,
                      background: _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      autoResize: false,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 12),
                    child: Center(
                      child: Text(
                        'Type: Secondary Size: Small',
                        style: XelaTextStyle.XelaCaption.apply(
                          color: _isDark ? XelaColor.Gray6 : XelaColor.Gray4,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaDivider(
                      color: _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.SMALL,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            rightIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.SMALL,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.SMALL,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                          XelaButton(
                            onPressed: () {},
                            rightIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            text: 'Button',
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.SMALL,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            rightIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.SMALL,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                          XelaButton(
                            onPressed: () {},
                            leftIcon: Icon(
                              Icons.grid_view,
                              size: 20,
                              color:
                                  _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            ),
                            type: XelaButtonType.SECONDARY,
                            size: XelaButtonSize.SMALL,
                            background: Colors.transparent,
                            foregroundColor:
                                _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                            defaultBorderColor:
                                _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                          ),
                        ],
                      ),
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 24,
                      vertical: 12,
                    ),
                    child: XelaButton(
                      onPressed: () {},
                      leftIcon: Icon(
                        Icons.grid_view,
                        size: 20,
                        color: _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      ),
                      rightIcon: Icon(
                        Icons.grid_view,
                        size: 20,
                        color: _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      ),
                      text: 'Autoresize Off',
                      type: XelaButtonType.SECONDARY,
                      size: XelaButtonSize.SMALL,
                      background: Colors.transparent,
                      foregroundColor:
                          _isDark ? XelaColor.Blue5 : XelaColor.Blue3,
                      defaultBorderColor:
                          _isDark ? XelaColor.Gray3 : XelaColor.Gray11,
                      autoResize: false,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
