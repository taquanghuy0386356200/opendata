import 'dart:async';

import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/rxdart.dart';

class WidgetSearchPopular extends StatefulWidget {
  const WidgetSearchPopular({
    Key? key,
    required this.filterCallBack,
    required this.searchController,
    required this.onChange,
    required this.onClear,
  }) : super(key: key);
  final TextEditingController searchController;
  final Function(String value) onChange;
  final Function() onClear;
  final Function() filterCallBack;

  @override
  State<WidgetSearchPopular> createState() => _WidgetSearchPopularState();
}

class _WidgetSearchPopularState extends State<WidgetSearchPopular> {
  final showClearIcon = BehaviorSubject.seeded(false);
  Timer? _debounce;

  @override
  void initState() {
    showClearIcon.sink.add(widget.searchController.text.isNotEmpty);
    super.initState();
  }

  @override
  void dispose() {
    _debounce?.cancel();
    showClearIcon.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          flex: 8,
          child: SizedBox(
            child: XelaTextFieldCustom(
              maxLength: 255,
              maxLine: 1,
              textEditingController: widget.searchController,
              radiusBorder: 8,
              placeholder: S.current.tim_kiem,
              onChange: (value) {
                if (value.isNotEmpty) {
                  showClearIcon.sink.add(true);
                } else {
                  showClearIcon.sink.add(false);
                }

                _onSearchChanged(
                  fun: () {
                    widget.onChange(value);
                  },
                );
              },
              rightIcon: StreamBuilder<bool>(
                stream: showClearIcon.stream,
                builder: (context, snapshot) {
                  return snapshot.data ?? false
                      ? GestureDetector(
                          onTap: () {
                            showClearIcon.sink.add(false);
                            widget.searchController.clear();
                            widget.onClear();
                            widget.onChange(widget.searchController.text);
                          },
                          child: SizedBox(
                            height: 20,
                            width: 20,
                            child: Image.asset(ImageAssets.icClear),
                          ),
                        )
                      : const SizedBox.shrink();
                },
              ),
              leftIcon: Container(
                margin: const EdgeInsets.only(right: 12),
                height: 20,
                width: 20,
                child: Image.asset(ImageAssets.icSearchOpenData),
              ),
            ),
          ),
        ),
        spaceW6,
        GestureDetector(
          onTap: () {
            widget.filterCallBack();
          },
          child: Container(
            padding: const EdgeInsets.all(8),
            width: 50,
            height: 50,
            decoration: BoxDecoration(
              borderRadius: const BorderRadius.all(Radius.circular(8)),
              border: Border.all(
                color: colorE9F2FF,
              ),
            ),
            child: Image.asset(ImageAssets.iconFilterOpendata),
          ),
        )
      ],
    );
  }

  void _onSearchChanged({required Function() fun, int? timeDelay}) {
    if (_debounce?.isActive ?? false) _debounce?.cancel();
    _debounce = Timer(Duration(milliseconds: timeDelay ?? 500), () {
      fun();
    });
  }
}
