import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/utils/extensions/date_time_extension.dart';
import 'package:bnv_opendata/widgets/button/double_buttom.dart';
import 'package:bnv_opendata/widgets/text_form_field/xela_text_field_custom.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_date_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';

///if value in onchange is valid can get value

class SelectDatePicker extends StatelessWidget {
  const SelectDatePicker({
    Key? key,
    required this.controller,
    required this.validatorDateOnChange,
    this.initDate,
    this.minDateCanSelect,
    this.maxDateCanSelect,
    this.hintText,
    required this.onChangeText,
    required this.callBackSelected,
    required this.callBackSubmitted,
  }) : super(key: key);
  final TextEditingController controller;
  final DateTime? initDate;
  final DateTime? minDateCanSelect;
  final DateTime? maxDateCanSelect;
  final bool Function(String value) validatorDateOnChange;
  final Function(DateTime dateTime) callBackSelected;
  final Function(DateTime dateTime) callBackSubmitted;
  final Function(String value)? onChangeText;
  final String? hintText;

  @override
  Widget build(BuildContext context) {
    DateTime? selectedDay = (initDate != null) ? initDate : DateTime.now();
    return XelaTextFieldCustom(
      placeholder: hintText,
      placeholderColor: color667793,
      textEditingController: controller,
      onChange: (value) {
        if (isDate(value)) {
          selectedDay = value.convertStringToDate();
          // widget.cubit.startDateTuNgayBHSJ.sink.ad
          // d(widget.cubit.startDateFilter);
          onChangeText!(value);
        } else {}
      },
      inputFormatters: [
        const UpperCaseTextFormatter(),
        MaskTextInputFormatter(mask: '##/##/####'),
      ],
      keyboardType: TextInputType.phone,
      validate: (value) {
        if ((value ?? '').trim().isEmpty) {
          return S.current.vui_long_chon_ngay;
        }
        final components = (value ?? '').trim().split('/');
        if (components.length == 3) {
          final day = int.tryParse(components[0]);
          final month = int.tryParse(components[1]);
          final year = int.tryParse(components[2]);
          if (day != null && month != null && year != null) {
            final date = DateTime(year, month, day);
            if ((date.year == year && date.month == month && date.day == day) &&
                ((value ?? '').length == 10)) {
              return (validatorDateOnChange(value ?? '') && isDate(value ?? ''))
                  ? null
                  : S.current.chon_ngay_khong_hop_le;
            }
          }
        }
        return S.current.chon_ngay_khong_hop_le;
      },
      rightIcon: GestureDetector(
        onTap: () {
          showDialog(
            context: context,
            builder: (context) {
              return Dialog(
                backgroundColor: Colors.white,
                shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20)),
                ),
                clipBehavior: Clip.antiAlias,
                child: Padding(
                  padding: const EdgeInsets.all(16),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      XelaDatePicker(
                        selectedDate: selectedDay,
                        prevMonthIcon: const Icon(
                          Icons.chevron_left,
                          color: XelaColor.Gray3,
                        ),
                        nextMonthIcon: const Icon(
                          Icons.chevron_right,
                          color: XelaColor.Gray3,
                        ),
                        onTapDate: (value) {
                          selectedDay = value;
                          callBackSelected(value);
                        },
                        minDate: minDateCanSelect,
                        maxDate: maxDateCanSelect,
                        yearHeaderColor: XelaColor.Gray7,
                        selectedBackground: color364564,
                        selectedColor: colorE26F2C,
                        weekdayHeaderColor: XelaColor.Gray9,
                        dividerColor: XelaColor.Gray11,
                      ),
                      spaceH16,
                      DoubleButtonBottom(
                        titleLeft: S.current.dong,
                        titleRight: S.current.chon,
                        onClickLeft: () {
                          Navigator.pop(context);
                        },
                        onClickRight: () {
                          callBackSubmitted(selectedDay ??= DateTime.now());
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  ),
                ),
              );
            },
          );
        },
        child: Container(
          margin: const EdgeInsets.only(right: 5),
          height: 24,
          width: 24,
          child: Image.asset(ImageAssets.icCalender12),
        ),
      ),
    );
  }
}

class UpperCaseTextFormatter implements TextInputFormatter {
  const UpperCaseTextFormatter();

  @override
  TextEditingValue formatEditUpdate(
    TextEditingValue oldValue,
    TextEditingValue newValue,
  ) {
    return TextEditingValue(
      text: newValue.text.toUpperCase(),
      selection: newValue.selection,
    );
  }
}

bool isDate(
  String input,
) {
  try {
    DateFormat('dd/MM/yyyy').parseStrict(input);
    return true;
  } catch (e) {
    return false;
  }
}
