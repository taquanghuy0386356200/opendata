import 'package:bnv_opendata/config/base/rx.dart';
import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/domain/models/xela_textfield_models.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_color.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rxdart/rxdart.dart';

class XelaTextFieldCustom extends StatefulWidget {
  final String? placeholder;
  final String? value;
  final TextEditingController? textEditingController;
  final XelaTextFieldState state;
  final String? helperText;
  final int? maxLength;
  final Widget? leftIcon;
  final Widget? rightIcon;
  final bool disableAutoCorrection;
  final bool secureField;
  final int? maxLine;
  final Color background;
  final Color disabledBackground;
  final double radiusBorder;
  final Color placeholderColor;
  final Color textfieldColor;
  final Color disabledTextfieldColor;
  final Color borderDefaultColor;
  final Color borderDisabledColor;
  final Color borderErrorColor;
  final Color borderSuccessColor;
  final Color borderFocusColor;
  final Color defaultHelperTextColor;
  final Color disabledHelperTextColor;
  final Color errorHelperTextColor;
  final Color successHelperTextColor;
  final double paddingErrorText;
  final TextInputType keyboardType;
  final Function(String)? onChange;
  final Function(String?)? validate;
  final List<TextInputFormatter>? inputFormatters;
  final bool Function(String)? validatorPaste;
  final Function()? onTap;
  final double borderWidth;
  final EdgeInsets? contentPadding;
  final Function(String)? onFieldSubmitted;
  final bool isCanMultiLine;
  final double? minHeight;
  final double? maxHeight;

  const XelaTextFieldCustom({
    Key? key,
    this.placeholder,
    this.maxLength,
    this.inputFormatters,
    this.value,
    this.textEditingController,
    this.validatorPaste,
    this.state = XelaTextFieldState.DEFAULT,
    this.disableAutoCorrection = true,
    this.helperText,
    this.leftIcon,
    this.maxLine,
    this.isCanMultiLine = false,
    this.rightIcon,
    this.secureField = false,
    this.background = Colors.white,
    this.disabledBackground = XelaColor.Gray12,
    this.placeholderColor = XelaColor.Gray8,
    this.textfieldColor = color304261,
    this.disabledTextfieldColor = XelaColor.Gray8,
    this.borderDefaultColor = XelaColor.Gray11,
    this.borderDisabledColor = XelaColor.Gray8,
    this.borderErrorColor = XelaColor.Red3,
    this.borderSuccessColor = XelaColor.Green1,
    this.radiusBorder = 4,
    this.borderFocusColor = XelaColor.Blue5,
    this.defaultHelperTextColor = XelaColor.Gray8,
    this.disabledHelperTextColor = XelaColor.Gray8,
    this.errorHelperTextColor = XelaColor.Red3,
    this.successHelperTextColor = XelaColor.Green1,
    this.keyboardType = TextInputType.text,
    this.paddingErrorText = 20.0,
    this.onChange,
    this.validate,
    this.onTap,
    this.borderWidth = 1,
    this.contentPadding,
    this.onFieldSubmitted,
    this.minHeight,
    this.maxHeight,
  }) : super(key: key);

  @override
  _XelaTextFieldCustomState createState() => _XelaTextFieldCustomState();
}

class _XelaTextFieldCustomState extends State<XelaTextFieldCustom> {
  BehaviorSubject<String?> errTextSubject = BehaviorSubject();
  final ScrollController _scrollController = ScrollController();
  final keyTextField = GlobalKey();
  String? errText;

  // late TextSelectionControls _selectionControls;

  @override
  void initState() {
    super.initState();
    // if (Platform.isIOS) {
    //   WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
    //     if (_selectionControls is AppCupertinoTextSelectionControls) {
    //       (_selectionControls as AppCupertinoTextSelectionControls)
    //           .boxTextFeild = getOffsetTextFeild();
    //     }
    //   });
    //   _selectionControls = AppCupertinoTextSelectionControls(
    //     validatorPaste: (value) {
    //       if (widget.validatorPaste == null) {
    //         return true;
    //       } else {
    //         return widget.validatorPaste!(value);
    //       }
    //     },
    //   );
    // } else {
    //   WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
    //     if (_selectionControls is CustomMaterialTextSelectionControls) {
    //       (_selectionControls as CustomMaterialTextSelectionControls)
    //           .boxTextFeild = getOffsetTextFeild();
    //     }
    //   });
    //   _selectionControls = CustomMaterialTextSelectionControls(
    //     validatorPaste: (value) {
    //       if (widget.validatorPaste == null) {
    //         return true;
    //       } else {
    //         return widget.validatorPaste!(value);
    //       }
    //     },
    //   );
    // }
  }

  RenderBox? getOffsetTextFeild() =>
      keyTextField.currentContext?.findRenderObject() as RenderBox?;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          constraints: BoxConstraints(
            minHeight: widget.minHeight ?? 50,
            maxHeight: widget.maxHeight ?? 100,
          ),
          padding: widget.leftIcon != null
              ? const EdgeInsets.symmetric(horizontal: 12)
              : null,
          decoration: BoxDecoration(
            color: widget.state == XelaTextFieldState.DISABLED
                ? widget.disabledBackground
                : widget.background,
            border: Border.all(
              color: widget.state == XelaTextFieldState.DEFAULT
                  ? widget.borderDefaultColor
                  : widget.state == XelaTextFieldState.FOCUS
                      ? widget.borderFocusColor
                      : widget.state == XelaTextFieldState.ERROR
                          ? widget.borderErrorColor
                          : widget.state == XelaTextFieldState.SUCCESS
                              ? widget.borderSuccessColor
                              : widget.borderDisabledColor,
              width: widget.borderWidth,
            ),
            borderRadius: BorderRadius.circular(widget.radiusBorder),
          ),
          child: Focus(
            child: Scrollbar(
              child: TextFormField(
                onFieldSubmitted: (value) {
                  widget.onFieldSubmitted?.call(value);
                },
                onTap: widget.onTap,
                inputFormatters: widget.inputFormatters,
                maxLength: widget.maxLength,
                maxLines: widget.maxLine,
                keyboardType: widget.keyboardType,
                initialValue: widget.value,
                onChanged: (value) {
                  widget.onChange?.call(value);
                  errText = widget.validate?.call(value);
                  errTextSubject.wellAdd(errText);
                },
                controller: widget.textEditingController,
                style: XelaTextStyle.XelaButtonMedium.apply(
                  color: widget.state == XelaTextFieldState.DISABLED
                      ? widget.disabledTextfieldColor
                      : widget.textfieldColor,
                ).copyWith(fontWeight: FontWeight.w400),
                enabled: widget.state != XelaTextFieldState.DISABLED,
                cursorColor: widget.textfieldColor,
                decoration: InputDecoration(
                  counterText: '',
                  labelText: widget.placeholder,
                  labelStyle: XelaTextStyle.XelaSmallBody.apply(
                    color: color66779350,
                  ),
                  border: InputBorder.none,
                  floatingLabelStyle: XelaTextStyle.XelaSmallBody.apply(
                    color: Colors.transparent,
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                  ),
                  errorBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.transparent),
                  ),
                  hoverColor: Colors.transparent,
                  filled: true,
                  fillColor: widget.state == XelaTextFieldState.DISABLED
                      ? widget.disabledBackground
                      : widget.background,
                  prefixIcon: widget.leftIcon,
                  suffixIcon: widget.rightIcon,
                  prefixIconConstraints: const BoxConstraints(
                    maxWidth: 48,
                    maxHeight: 48,
                  ),
                  suffixIconConstraints: const BoxConstraints(
                    maxWidth: 48,
                    maxHeight: 48,
                  ),
                  errorStyle: const TextStyle(
                    fontSize: 0,
                    height: 0.7,
                  ),
                  contentPadding:
                      const EdgeInsets.symmetric(vertical: 10, horizontal: 16),
                ),
                obscuringCharacter: '*',
                autocorrect: !widget.disableAutoCorrection,
                obscureText: widget.secureField,
                validator: (value) {
                  errText = widget.validate?.call(value?.trim() ?? '');
                  errTextSubject.wellAdd(errText);
                  return errText;
                },
              ),
            ),
          ),
        ),
        StreamBuilder<String?>(
          stream: errTextSubject,
          builder: (context, snapshot) {
            final error = snapshot.data;
            if (error != null) {
              return Container(
                alignment: Alignment.centerLeft,
                padding: EdgeInsets.symmetric(
                  horizontal: widget.paddingErrorText,
                  vertical: 8,
                ),
                child: Text(
                  errText!,
                  style: XelaTextStyle.XelaSmallBody.apply(
                    color: widget.errorHelperTextColor,
                  ),
                ),
              );
            } else {
              return const SizedBox.shrink();
            }
          },
        ),
      ],
    );
  }
}
