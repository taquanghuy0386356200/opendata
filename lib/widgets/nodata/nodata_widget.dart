import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class NodataWidget extends StatelessWidget {
  final String? text;

  const NodataWidget({Key? key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          width: 162.19,
          height: 158,
          child: Image.asset(ImageAssets.imgKhongCoDuLieu),
        ),
        Text(
          text ?? S.current.chua_co_du_lieu,
          style: XelaTextStyle.Xela18Medium.copyWith(color: color364564),
        ),
      ],
    );
  }
}
