import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/config/resources/styles.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/material.dart';

class EmptyViewCustom extends StatelessWidget {
  const EmptyViewCustom({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(top: 160),
      child: Column(
        children: [
          SizedBox(
            height: 158,
            width: 163,
            child: Image.asset(ImageAssets.emptyData),
          ),
          spaceH2,
          Text(
            S.current.chua_co_du_lieu,
            style: XelaTextStyle.Xela18Medium.copyWith(
              color: color364564,
            ),
          )
        ],
      ),
    );
  }
}
