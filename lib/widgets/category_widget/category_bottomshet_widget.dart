import 'package:bnv_opendata/config/resources/color.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/widgets/category_widget/bloc/category_cubit.dart';
import 'package:bnv_opendata/widgets/dropdown/cool_dropdown.dart';
import 'package:bnv_opendata/widgets/xela_widgets/xela_text_style.dart';
import 'package:flutter/cupertino.dart';

import '../../config/resources/styles.dart';

class CategoryBottomShetWidget extends StatefulWidget {
  final CategoryModel? initCategory;
  final SubCategoriesModel? initSubCategories;
  final Function(CategoryModel) onchangeCategoryData;
  final Function(SubCategoriesModel) onchangeSubCategoriesData;
  final Widget? validateLinhVucWidget;
  final Widget? validateChuyenMucWidget;

  const CategoryBottomShetWidget({
    Key? key,
    this.initCategory,
    this.initSubCategories,
    this.validateLinhVucWidget,
    this.validateChuyenMucWidget,
    required this.onchangeCategoryData,
    required this.onchangeSubCategoriesData,
  }) : super(key: key);

  @override
  State<CategoryBottomShetWidget> createState() =>
      _CategoryBottomShetWidgetState();
}

class _CategoryBottomShetWidgetState extends State<CategoryBottomShetWidget> {
  final CategoryCubit cubit = CategoryCubit();

  @override
  void initState() {
    cubit.getCategoriesList().then(
          (value) => {
            cubit.addListToSubCategories(
              initCategory: widget.initCategory,
              initSubCategories: widget.initSubCategories,
            )
          },
        );
    cubit.checkCanSelectChuyenMuc(widget.initSubCategories);
    super.initState();
  }

  @override
  void dispose() {
    cubit.onReset();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<List<CategoryModel>>(
      stream: cubit.categoriesList.stream,
      builder: (context, snapshot) {
        final data = snapshot.data ?? [];
        data.insert(0, cubit.dataCategoryAddAll);
        return Column(
          children: [
            /// drop linh vuc
            dropDownFilter(
              initData: widget.initCategory?.name ?? '',
              text: S.current.linh_vuc,
              listData: data.map((e) => e.name).toSet().toList(),
              onChange: (value) {
                final dataChange = data[value];
                widget.onchangeCategoryData(dataChange);
                cubit.changeSubCategory(dataChange);
                if (cubit.checkDataCategory(dataChange)) {
                  widget.onchangeSubCategoriesData(
                    cubit.dataSubCategoriesAll,
                  );
                }
              },
            ),

            widget.validateLinhVucWidget ?? const SizedBox.shrink(),

            /// drop chuyen muc
            StreamBuilder<bool>(
              stream: cubit.isDisibleChuyenMuc.stream,
              builder: (context, snapshotBool) {
                final data = snapshotBool.data;
                return dropDownFilter(
                  initData: widget.initSubCategories?.name ?? '',
                  text: S.current.chuyen_muc,
                  listData: cubit.listSubCategories
                      .map((e) => e.name)
                      .toSet()
                      .toList(),
                  isDisible: data,
                  onChange: (value) {
                    widget.onchangeSubCategoriesData(
                      cubit.listSubCategories[value],
                    );
                    cubit.dataCategorySelecting =
                        cubit.listSubCategories[value];
                  },
                );
              },
            ),
            widget.validateChuyenMucWidget ?? const SizedBox.shrink(),
          ],
        );
      },
    );
  }

  Widget dropDownFilter({
    required String text,
    bool? isDisible,
    required List<String> listData,
    required Function(int) onChange,
    required String initData,
  }) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        spaceH20,
        Text(
          text,
          style: XelaTextStyle.Xela14Regular.copyWith(
            color: color586B8B,
          ),
        ),
        const SizedBox(
          height: 11,
        ),
        SizedBox(
          width: double.infinity,
          child: CoolDropDown(
            key: UniqueKey(),
            isDisible: isDisible ?? false,
            initData: initData,
            useCustomHintColors: true,
            placeHoder: S.current.tat_ca,
            listData: listData,
            onChange: (value) {
              onChange(value);
            },
          ),
        ),
      ],
    );
  }
}
