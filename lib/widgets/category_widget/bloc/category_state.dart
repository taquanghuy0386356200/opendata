part of 'category_cubit.dart';

@immutable
abstract class CategoryState extends BaseState {}

class CategoryInitial extends CategoryState {
  @override
  List<Object?> get props => [];
}
