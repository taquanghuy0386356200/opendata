import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/config/base/base_state.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';
import 'package:bnv_opendata/domain/models/open_data_model/open_data_model.dart';
import 'package:bnv_opendata/domain/repository/home/home_repository.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';
import 'package:rxdart/rxdart.dart';

part 'category_state.dart';

class CategoryCubit extends BaseCubit<BaseState> {
  CategoryCubit() : super(CategoryInitial()) {
    showContent();
  }

  HomeRepository get _homeRepo => Get.find();

  final dataCategoryAddAll = CategoryModel(0, S.current.tat_ca, '', []);
  final dataSubCategoriesAll = SubCategoriesModel(0, S.current.tat_ca);

  SubCategoriesModel dataCategorySelecting = SubCategoriesModel(0, '');

  List<SubCategoriesModel> listSubCategories = [];

  final BehaviorSubject<bool> isDisibleChuyenMuc = BehaviorSubject.seeded(true);

  BehaviorSubject<List<OpenDataModel>> listOpenDataSubject = BehaviorSubject();
  BehaviorSubject<OpenDataModel> openDataSubject = BehaviorSubject();
  final BehaviorSubject<List<CategoryModel>> categoriesList = BehaviorSubject();

  Future<void> getCategoriesList() async {
    showLoading();
    final result = await _homeRepo.getListCategoryHome();
    result.when(
      success: (response) {
        showContent();
        categoriesList.sink.add(response);
        checkListSubCategoriesFinal();
      },
      error: (error) {
        showError();
      },
    );
    showContent();
  }

  void changeSubCategory(CategoryModel data) {
    listSubCategories = data.subCategories ?? [];
    isDisibleChuyenMuc.sink.add((data.subCategories ?? []).isEmpty);
    checkListSubCategoriesFinal();
  }

  void addListToSubCategories({
    CategoryModel? initCategory,
    SubCategoriesModel? initSubCategories,
  }) {
    if ((initCategory?.subCategories ?? []).isNotEmpty) {
      checkListSubCategoriesFinal();
      listSubCategories =
          (initCategory ?? CategoryModel(0, '', '', [])).subCategories ?? [];
      isDisibleChuyenMuc.sink.add(false);
    }
  }

  void checkCanSelectChuyenMuc(SubCategoriesModel? data) {
    if ((data ?? SubCategoriesModel(0, '')).name.isNotEmpty) {
      isDisibleChuyenMuc.sink.add(false);
    }
  }

  void checkListSubCategoriesFinal() {
    if (!listSubCategories.contains(dataSubCategoriesAll)) {
      listSubCategories.insert(0, dataSubCategoriesAll);
    }
  }

  void onReset() {
    listSubCategories = [];
    dataCategorySelecting = SubCategoriesModel(0, '');
  }

  bool checkDataCategory(CategoryModel data) {
    /// trong list sub luon có "tất cả"
    if (data == dataCategoryAddAll ||
        [0, 1].contains((data.subCategories ?? []).length) ||
        !listSubCategories.contains(dataCategorySelecting)) {
      return true;
    }
    return false;
  }
}
