import 'package:bnv_opendata/config/base/base_cubit.dart';
import 'package:bnv_opendata/config/base/base_state.dart';
import 'package:bnv_opendata/data/exception/app_exception.dart';
import 'package:bnv_opendata/generated/l10n.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:bnv_opendata/utils/constants/app_constants.dart';
import 'package:bnv_opendata/widgets/dialog/loading_loadmore.dart';
import 'package:bnv_opendata/widgets/nodata/nodata_widget.dart';
import 'package:bnv_opendata/widgets/views/state_layout.dart';
import 'package:bnv_opendata/widgets/views/state_stream_layout.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ListViewLoadMoreWidget extends StatefulWidget {
  final BaseCubit<dynamic> cubit;
  final Function(int page) callApi;
  final Widget Function(dynamic, int?) viewItem;
  final bool isListView;
  final double? checkRatio;
  final double? crossAxisSpacing;
  final bool? sinkWap;
  final Function()? onReload;

  const ListViewLoadMoreWidget({
    Key? key,
    required this.cubit,
    required this.isListView,
    required this.callApi,
    required this.viewItem,
    this.checkRatio,
    this.crossAxisSpacing,
    this.sinkWap,
    this.onReload,
  }) : super(key: key);

  @override
  State<ListViewLoadMoreWidget> createState() => _ListViewLoadMoreWidgetState();
}

class _ListViewLoadMoreWidgetState extends State<ListViewLoadMoreWidget> {
  final ScrollController _controller = ScrollController();
  bool canShowNoData = false;

  Future<void> refreshPosts() async {
    if (!widget.cubit.loadMoreLoading) {
      widget.cubit.canMoveToTop = true;
      widget.onReload?.call();
      widget.cubit.loadMorePage = ApiConstants.PAGE_BEGIN;
      widget.cubit.loadMoreRefresh = true;
      widget.cubit.loadMoreLoading = true;
      await widget.callApi(widget.cubit.loadMorePage);
    }
  }

  Future<void> loadMorePosts() async {
    if (!widget.cubit.loadMoreLoading) {
      widget.cubit.canMoveToTop = false;
      widget.cubit.loadMorePage += ApiConstants.PAGE_BEGIN;
      widget.cubit.loadMoreRefresh = false;
      widget.cubit.loadMoreLoading = true;
      widget.cubit.loadMoreSink.add(widget.cubit.loadMoreLoading);
      await widget.callApi(widget.cubit.loadMorePage);
    }
  }

  Future<void> initData() async {
    widget.cubit.canMoveToTop = true;
    widget.cubit.loadMorePage = ApiConstants.PAGE_BEGIN;
    widget.cubit.loadMoreRefresh = true;
    widget.cubit.loadMoreLoading = true;
    
    await widget.callApi(widget.cubit.loadMorePage);
  }

  @override
  void initState() {
    initData().then((value) => canShowNoData = true);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  void didUpdateWidget(covariant ListViewLoadMoreWidget oldWidget) {
    super.didUpdateWidget(oldWidget);
  }

  @override
  void dispose() {
    widget.cubit.inSearching.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocConsumer(
      bloc: widget.cubit,
      listener: (ctx, state) {
        if (state is CompletedLoadMore) {
          if (state.completeType == CompleteType.SUCCESS) {
            if (widget.cubit.loadMoreRefresh ||
                widget.cubit.loadMorePage == ApiConstants.PAGE_BEGIN) {
              widget.cubit.loadMoreList.clear();
              if ((state.posts ?? []).isEmpty) {
                widget.cubit.showEmpty();
              } else {
                widget.cubit.showContent();
              }
            }
          } else {
            widget.cubit.loadMoreList.clear();
            widget.cubit.showError();
          }
          widget.cubit.loadMoreList.addAll(state.posts ?? []);
          widget.cubit.canLoadMore =
              (state.posts?.length ?? 0) >= ApiConstants.DEFAULT_PAGE_SIZE;
          widget.cubit.loadMoreLoading = false;
          widget.cubit.loadMoreSink.add(widget.cubit.loadMoreLoading);
          widget.cubit.loadMoreListController.add(widget.cubit.loadMoreList);
        }
      },
      builder: (BuildContext context, Object? state) {
        return StateStreamLayout(
          retry: () async {
            await initData();
          },
          error: AppException(
            S.current.error,
            S.current.something_went_wrong,
          ),
          textEmpty: noDataWidget(),
          stream: widget.cubit.stateStream,
          child: NotificationListener<ScrollNotification>(
            onNotification: (ScrollNotification scrollInfo) {
              if (widget.cubit.canLoadMore &&
                  scrollInfo.metrics.pixels ==
                      scrollInfo.metrics.maxScrollExtent) {
                loadMorePosts();
              }
              return true;
            },
            child: RefreshIndicator(
              onRefresh: () async {
                await refreshPosts();
              },
              child: SizedBox(
                height: MediaQuery.of(context).size.height,
                child: Stack(
                  children: [
                    StreamBuilder(
                      stream: widget.cubit.loadMoreListStream,
                      builder: (
                        BuildContext context,
                        AsyncSnapshot<List<dynamic>> snapshot,
                      ) {
                        if (widget.cubit.canMoveToTop &&
                            _controller.hasClients) {
                          _controller.animateTo(
                            0,
                            duration: const Duration(milliseconds: 500),
                            curve: Curves.easeOut,
                          );
                        }

                        if ((snapshot.data?.length ?? 0) != 0) {
                          return !widget.isListView
                              ? GridView.builder(
                                  physics:
                                      const AlwaysScrollableScrollPhysics(),
                                  controller: _controller,
                                  padding: const EdgeInsets.only(
                                    left: 16,
                                    right: 16,
                                    top: 16,
                                    bottom: 32,
                                  ),
                                  gridDelegate:
                                      SliverGridDelegateWithFixedCrossAxisCount(
                                    crossAxisCount: 2,
                                    mainAxisSpacing: 16,
                                    crossAxisSpacing:
                                        widget.crossAxisSpacing ?? 28,
                                    childAspectRatio:
                                        widget.checkRatio ?? 2 / 3,
                                  ),
                                  itemCount: snapshot.data?.length ?? 0,
                                  itemBuilder: (_, index) {
                                    return widget.viewItem(
                                      snapshot.data![index],
                                      index,
                                    );
                                  },
                                )
                              : ListView.builder(
                                  physics:
                                      const AlwaysScrollableScrollPhysics(),
                                  controller: _controller,
                                  padding: const EdgeInsets.only(
                                    left: 16,
                                    right: 16,
                                  ),
                                  shrinkWrap: widget.sinkWap ?? false,
                                  itemCount: snapshot.data?.length ?? 0,
                                  itemBuilder: (ctx, index) {
                                    return widget.viewItem(
                                      snapshot.data![index],
                                      index,
                                    );
                                  },
                                );
                        }
                        return canShowNoData
                            ? noDataWidget()
                            : const SizedBox();
                      },
                    ),
                    Positioned(
                      bottom: 5,
                      right: 16,
                      left: 16,
                      child: StreamBuilder<bool>(
                        stream: widget.cubit.loadMoreStream,
                        builder: (context, snapshot) {
                          return snapshot.data ?? false
                              ? const LoadingItem()
                              : const SizedBox();
                        },
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget noDataWidget() => RefreshIndicator(
        onRefresh: () async {
          await refreshPosts();
        },
        child: SingleChildScrollView(
          physics: const AlwaysScrollableScrollPhysics(),
          child: SizedBox(
            height: MediaQuery.of(context).size.height * 0.8,
            child: StreamBuilder<bool>(
              stream: widget.cubit.inSearching,
              builder: (context, snapshot) {
                final data = snapshot.data ?? false;
                return Center(
                  child: NodataWidget(
                    text: data
                        ? S.current.khong_tim_thay_ket_qua_phu_hop
                        : S.current.chua_co_du_lieu,
                  ),
                );
              },
            ),
          ),
        ),
      );
}
