import 'package:bnv_opendata/utils/constants/image_asset.dart';
import 'package:flutter/material.dart';

Widget imageNetwork({required String image}) {
  const String defaultImg =
      'https://www.inkling.com/wp-content/uploads/2021/06/SD-default-image.png';

  return Image.network(
    image,
    fit: BoxFit.fill,
    errorBuilder: (context, error, stackTrace) {
      return Image.asset(ImageAssets.default_img);
    },
    loadingBuilder: (
      BuildContext context,
      Widget child,
      ImageChunkEvent? loadingProgress,
    ) {
      if (loadingProgress == null) return child;
      return Center(
        child: CircularProgressIndicator(
          value: loadingProgress.expectedTotalBytes != null
              ? loadingProgress.cumulativeBytesLoaded /
                  loadingProgress.expectedTotalBytes!
              : null,
        ),
      );
    },
  );
}
