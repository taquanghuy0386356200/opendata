import 'package:json_annotation/json_annotation.dart';

part 'app_constants.g.dart';

@JsonSerializable()
class AppConstants {
  @JsonKey(name: 'type')
  String type;

  @JsonKey(name: 'base_url')
  String baseUrl;

  @JsonKey(name: 'base_url_wso2is')
  String baseUrlWso2is;

  @JsonKey(name: 'hard_token_api_login')
  String hardTokenAPILogin;

  AppConstants(
    this.type,
    this.baseUrl,
    this.baseUrlWso2is,
    this.hardTokenAPILogin,
  );

  factory AppConstants.fromJson(Map<String, dynamic> json) =>
      _$AppConstantsFromJson(json);
}
