class CategoryModel {
  int id;
  String name;
  String icon;
  List<SubCategoriesModel>? subCategories;
  bool isChoosing;

  CategoryModel(
    this.id,
    this.name,
    this.icon,
    this.subCategories, {
    this.isChoosing = false,
  });

  @override
  String toString() {
    return 'CategoryModel{id: $id, name: $name, icon: $icon, subCategories: $subCategories}';
  }
}

class SubCategoriesModel {
  int id;
  String name;
  bool isChoosing;

  SubCategoriesModel(
    this.id,
    this.name, {
    this.isChoosing = false,
  });

  @override
  String toString() {
    return 'SubCategoriesModel{id: $id, name: $name}';
  }
}
