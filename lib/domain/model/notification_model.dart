class ThongBaoTotalModel {
  int pageIndex;
  int pageSize;
  int totalItem;
  List<ThongBaoModel> listThongBao;

  ThongBaoTotalModel(
    this.pageIndex,
    this.pageSize,
    this.totalItem,
    this.listThongBao,
  );
}

class ThongBaoModel {
  int id;
  String title;
  String banner;
  bool isSaved;
  int releaseAt;
  int affectedAt;

  ThongBaoModel({
    required this.id,
    required this.title,
    required this.banner,
    required this.isSaved,
    required this.releaseAt,
    required this.affectedAt,
  });
}
