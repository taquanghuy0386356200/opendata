


class DocumentModel {
  int id;
  String title;
  String banner;
  int releaseAt;
  int affectedAt;

  DocumentModel(
    this.id,
    this.title,
    this.banner,
    this.releaseAt,
    this.affectedAt,
  );
}
