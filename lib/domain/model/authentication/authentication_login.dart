class AuthenticationLoginTotalModel {
  int? errorCode;
  String? errorMessage;
  AuthenticationLoginModel authenticationLoginModel;

  AuthenticationLoginTotalModel(
    this.errorCode,
    this.errorMessage,
    this.authenticationLoginModel,
  );
}

class AuthenticationLoginModel {
  String accessToken;
  String refreshToken;
  int? expiredTime;
  UserInfoModel userInfoModel;

  AuthenticationLoginModel(
    this.accessToken,
    this.refreshToken,
    this.expiredTime,
    this.userInfoModel,
  );
}

class UserInfoModel {
  String? email;
  String? fullName;
  String? avatar;

  UserInfoModel({
    this.email,
    this.fullName,
    this.avatar,
  });
}
