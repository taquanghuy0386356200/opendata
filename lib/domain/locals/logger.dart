import 'package:flutter/foundation.dart' as foundation;
import 'package:logger/logger.dart';

final logger = Logger(
  printer: foundation.kDebugMode
      ? PrettyPrinter()
      : null, // Use the default LogOutput (-> send everything to console)
);
