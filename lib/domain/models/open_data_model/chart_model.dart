import 'package:flutter/cupertino.dart';

class ChartDataModel {
  String? label;
  double? number;
  Color? color;
  List<double>? listdata;
  String? time;
  String? linhVuc;
  String? diaDiem;
  String? chiTieu;
  int? danhGia;

  ChartDataModel({
    this.label,
    this.number,
    this.color,
    this.listdata,
    this.time,
    this.linhVuc,
    this.diaDiem,
    this.chiTieu,
    this.danhGia,
  });

  String getDanhGia() {
    switch (danhGia) {
      case 1:
        return 'Hoàn thành';
      case 2:
        return 'Chưa hoàn thành';

    }
    return '';
  }

}
