import 'package:bnv_opendata/presentation/du_lieu_mo/ui/Widget/detail_opendata_chart/detail_opendata_mode_chart.dart';

class OpenDataModel {
  int? id;
  String? title;
  int? updatedAt;
  int? categoryId;
  String? categoryName;
  int? subCategoryId;
  String? subCategoryName;
  String? description;
  bool? danhDau;
  int? typeChart;
  String? fileDinhKem;

  OpenDataModel({
    this.id,
    this.title,
    this.updatedAt,
    this.categoryId,
    this.categoryName,
    this.subCategoryId,
    this.subCategoryName,
    this.description,
    this.danhDau,
    this.fileDinhKem
  });

  DetailOpenDataModeChart getChartType() {
    switch (typeChart) {
      case 1:
        return DetailOpenDataModeChart.FILE;
      case 2:
        return DetailOpenDataModeChart.PIE;
      case 3:
        return DetailOpenDataModeChart.COLUMN;
      case 4:
        return DetailOpenDataModeChart.LINE;
      case 5:
        return DetailOpenDataModeChart.LIST;
      case 6:
        return DetailOpenDataModeChart.HORIZONTAL_COLUMN;
      case 7:
        return DetailOpenDataModeChart.STASKED_BAR_CHART;
      case 8:
        return DetailOpenDataModeChart.STATISTICAL_REPORTS;
    }
    return DetailOpenDataModeChart.FILE;
  }

  String getNameFile() {
    final List<String> result = (fileDinhKem ?? '').split('\\');
    return result.last;
  }

  String getTypeFile() {
    final List<String> result = (fileDinhKem ?? '').split('.');
    return result.last;
  }
}

class DataDuLieuMoModel {
  List<OpenDataModel>? item;
  int? totalItem;
  int? pageSize;
  int? pageIndex;

  DataDuLieuMoModel({
    this.item,
    this.totalItem,
    this.pageSize,
    this.pageIndex,
  });
}
