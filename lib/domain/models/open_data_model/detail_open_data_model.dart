import 'dart:convert';

import 'package:dartx/dartx.dart';

class DetailOpenDataModel {
  BodyDetailOpenDataModel? body;
  int? errorCode;
  String? errorMessage;

  DetailOpenDataModel({this.body, this.errorCode, this.errorMessage});

  @override
  String toString() {
    return 'DetailOpenDataModel{body: $body, errorCode: $errorCode, errorMessage: $errorMessage}';
  }
}

class BodyDetailOpenDataModel {
  int? id;
  String? title;
  String? description;
  String? content;
  String? fileDinhKem;
  int? categoryID;
  String? categoryName;
  int? subcategoryID;
  String? subcategoryName;
  int? updatedAt;
  bool? danhDau;
  String? chartType;
  String? dataTable;
  String? link;
  DataTable? dataTableDeCode;

  BodyDetailOpenDataModel({
    this.id,
    this.title,
    this.description,
    this.content,
    this.fileDinhKem,
    this.categoryID,
    this.categoryName,
    this.subcategoryID,
    this.subcategoryName,
    this.updatedAt,
    this.danhDau,
    this.chartType,
    this.dataTable,
    this.link,
    this.dataTableDeCode,
  });

  String getNameFile() {
    final List<String> result = (fileDinhKem ?? '').split('\\');
    return result.last;
  }

  String getTypeFile() {
    final List<String> result = (fileDinhKem ?? '').split('.');
    return result.last;
  }

  List<Map<String, dynamic>> getListMaps() {
    final List<Map<String, dynamic>> listData = [];

    if ((dataTable ?? '').isNotEmpty) {
      try {
        final Map<String, dynamic> data = jsonDecode(dataTable ?? '{}');

        ///Get column name
        final column = data.getOrElse('columns', () => []) as List<dynamic>;

        ///Get list row data
        final row = data.getOrElse('rows', () => []) as List<dynamic>;

        /// chuyen ve list DataModel
        final dataObj = column.map((json) {
          json as Map<String, dynamic>;
          return Data.fromJson(
            json.getOrElse('columnName', () => ''),
            json.getOrElse('displayColumnName', () => ''),
          );
        }).toList();

        /// Parse data
        for (final e in row) {
          e as Map<String, dynamic>;
          final Map<String, dynamic> data = {};
          for (int i = 0; i < dataObj.length; i++) {
            data.putIfAbsent(
              dataObj[i].name,
                  () => e.getOrElse(dataObj[i].key, () => ''),
            );
          }
          listData.add(data);
        }
      } catch (e) {
        rethrow;
      }
    }
    return listData;
  }
}

class DataTable {
  List<Columns>? columns;
  List<Rows>? rows;
  int? rowCount;

  DataTable({this.columns, this.rows, this.rowCount});
}

class Columns {
  String? columnName;
  String? displayColumnName;
  bool? hasFilter;
  String? dataType;
  ColumnProps? columnProps;

  Columns({
    this.columnName,
    this.displayColumnName,
    this.hasFilter,
    this.dataType,
    this.columnProps,
  });
}

class Rows {
  String? kyhieu;
  String? loaivanban;
  String? trichyeu;
  String? nguoitao;
  String? strNgaytao;
  String? nguoixuly;
  String? strNgayxuly;
  String? tinhtrangxuly;
  String? trangthai;

  Rows({
    this.kyhieu,
    this.loaivanban,
    this.trichyeu,
    this.nguoitao,
    this.strNgaytao,
    this.nguoixuly,
    this.strNgayxuly,
    this.tinhtrangxuly,
    this.trangthai,
  });
}

class ColumnProps {
  String? id;

  ColumnProps({this.id});
}

class Data {
  String key;
  String name;
  String content;

  Data(this.key, this.name, this.content);

  factory Data.fromJson(String key, String name) {
    return Data(key, name, '');
  }
}
