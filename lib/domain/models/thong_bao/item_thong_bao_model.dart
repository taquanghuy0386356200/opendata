class ItemThongBaoModel {
  String? id;
  String? title;
  String? banner;
  bool? isSaved;
  int? releaseAt;
  int? affectedAt;
  String? content;
  int? createdAt;

  ItemThongBaoModel({
    this.id,
    this.title,
    this.banner,
    this.isSaved,
    this.releaseAt,
    this.content,
    this.affectedAt,
    this.createdAt,
  });
}

class DataDuThongBaoModel {
  List<ItemThongBaoModel>? item;
  int? totalItem;
  int? pageSize;
  int? pageIndex;

  DataDuThongBaoModel({
    this.item,
    this.totalItem,
    this.pageSize,
    this.pageIndex,
  });
}


