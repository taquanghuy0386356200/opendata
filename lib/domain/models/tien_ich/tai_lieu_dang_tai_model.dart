class TaiLieuDangTaiModel {
  int id;
  String title;
  String description;
  String nguoiDangTai;
  String fileDinhKem;
  int fileSize;
  String status;
  String categoryID;
  String categoryName;
  String subcategoryID;
  String subcategoryName;
  int createdAt;
  int thoiGianGuiDuyet;
  int thoiGianDuyet;
  String lyDoTraLai;
  String fileType;

  TaiLieuDangTaiModel(
    this.id,
    this.title,
    this.description,
    this.nguoiDangTai,
    this.fileDinhKem,
    this.fileSize,
    this.status,
    this.categoryID,
    this.categoryName,
    this.subcategoryID,
    this.subcategoryName,
    this.createdAt,
    this.fileType,
    this.thoiGianGuiDuyet,
    this.thoiGianDuyet,
    this.lyDoTraLai,
  );
}
