import 'dart:core';

class LienHeModel {
  double? latitude;
  double? longtitude;
  String? address;
  String? phoneNumber;
  String? faxNumber;
  String? email;
  String? title;


  LienHeModel({
    this.latitude,
    this.longtitude,
    this.address,
    this.phoneNumber,
    this.faxNumber,
    this.email,
    this.title,
  });
}
