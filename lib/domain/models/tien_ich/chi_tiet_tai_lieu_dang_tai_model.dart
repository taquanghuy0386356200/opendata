class ChiTietTaiLieuDangTaiModel {
  int id;
  String title;
  String description;
  String fileDinhKem;
  String fileName;
  int fileSize;
  String fileType;
  String status;
  int categoryID;
  String categoryName;
  int subcategoryID;
  String subCategoryName;
  int createAt;

  ChiTietTaiLieuDangTaiModel(
    this.id,
    this.title,
    this.description,
    this.fileDinhKem,
    this.fileName,
    this.fileSize,
    this.fileType,
    this.status,
    this.categoryID,
    this.categoryName,
    this.subcategoryID,
    this.subCategoryName,
    this.createAt,
  );
}
