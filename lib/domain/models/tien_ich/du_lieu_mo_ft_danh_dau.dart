import 'package:bnv_opendata/domain/model/category_model.dart';

class DuLieuMoFeatDanhDau {
  String id;
  String title;
  int updatedAt;
  CategoryModel category;
  String description;

  DuLieuMoFeatDanhDau(
    this.id,
    this.title,
    this.updatedAt,
    this.category,
    this.description,
  );
}
