import 'package:bnv_opendata/data/request/login_ft_token_request.dart';
import 'package:bnv_opendata/data/request/refreshTokenRequest.dart';
import 'package:bnv_opendata/data/response/authentication/get_token_login_response.dart';
import 'package:bnv_opendata/data/result/result.dart';

mixin AuthenticationDomainAmRepo {
  Future<Result<GetTokenLoginResponse>> getTokenLogin(
    LoginFeatTokenRequest request,
  );

  Future<Result<GetTokenLoginResponse>> refreshToken(
    RefreshTokenRequest request,
  );
}
