import 'package:bnv_opendata/data/request/doi_mat_khau_request.dart';
import 'package:bnv_opendata/data/request/otp_request.dart';
import 'package:bnv_opendata/data/request/register_request.dart';
import 'package:bnv_opendata/data/request/xac_thuc_tai_khoan_request.dart';
import 'package:bnv_opendata/data/response/authentication/capcha_response.dart';
import 'package:bnv_opendata/data/response/authentication/doi_mat_khau_response.dart';
import 'package:bnv_opendata/data/response/authentication/lost_pw_response.dart';
import 'package:bnv_opendata/data/response/authentication/otp_response.dart';
import 'package:bnv_opendata/data/response/authentication/register_reponse.dart';
import 'package:bnv_opendata/data/response/authentication/thiet_lap_mat_khau_response.dart';
import 'package:bnv_opendata/data/response/authentication/xac_thuc_tk_response.dart';
import 'package:bnv_opendata/data/result/result.dart';

mixin AuthenticationRepository {
  Future<Result<RegisterTotalReponse>> register({
    required String email,
    required String fullName,
    required String password,
    required String capchaCode,
    required String capchaID,
  });

  Future<Result<LostPasswordRespone>> quenMatKhau({
    required QuenMatKhauRequest request,
  });

  Future<Result<ThietLapMatKhauResponse>> thietLapMatKhau({
    required String newPassword,
    required String confirmPassword,
    required String email,
    required int otpId,
    required String token,
  });

  Future<Result<OTPResponse>> handleOTP({
    required OTPRequest request,
  });

  Future<Result<DoiMatKhauResponse>> doiMatKhau({
    required DoiMatKhauRequest request,
  });

  Future<Result<XacThucTKResponse>> xacThucTaiKhoan({
    required XacThucTKRequest request,
  });

  Future<Result<CapchaResponse>> getCapchaCode();
}
