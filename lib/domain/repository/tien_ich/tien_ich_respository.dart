import 'dart:io';

import 'package:bnv_opendata/data/request/gui_lien_he_request.dart';
import 'package:bnv_opendata/data/request/them_sua_tai_lieu_request.dart';
import 'package:bnv_opendata/data/response/tien_ich/danh_dau_du_lieu_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/post_file_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/them_sua_tai_lieu_response.dart';
import 'package:bnv_opendata/data/response/tien_ich/xoa_tai_lieu_response.dart';
import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';
import 'package:bnv_opendata/domain/models/tien_ich/chi_tiet_tai_lieu_dang_tai_model.dart';
import 'package:bnv_opendata/domain/models/tien_ich/lien_he_model.dart';
import 'package:bnv_opendata/domain/models/tien_ich/tai_lieu_dang_tai_model.dart';

mixin TienIchRepository {
  Future<Result<DataDuThongBaoModel>> getListHuongDan({
    String? keySearch,
    int? pageSize,
    int? pageIndex,
  });

  Future<Result<ItemThongBaoModel>> getDetailHuongDan({
    String? id,
  });

  Future<Result<LienHeModel>> getDetailLienHe();

  Future<Result<LienHeModel>> guiLienHe(
    GuiLienHeRequest guiLienHeRequest,
  );

  Future<Result<PostFileTotalResponse>> postFile(
    List<File> file,
  );

  Future<Result<ThemSuaTaiLieuTotalResponse>> themSuaTaiLieu(
    ThemSuaTaiLieuRequest request,
  );

  Future<Result<XoaTaiLieuResponse>> xoaTaiLieu(
    int idTailIEU,
  );

  Future<Result<DanhDauDuLieuResponse>> danhDauDuLieu(
    int id,
    String nguoiDanhDau,
  );

  Future<Result<DanhDauDuLieuResponse>> boDanhDauDuLieu(
    int id,
    String nguoiDanhDau,
  );

  Future<Result<List<TaiLieuDangTaiModel>>> getListTaiLieuDangTai(
    int pageSize,
    int pageIndex,
    String nguoiDangTai,
    String keySearch,
    String status,
    String fileType, {
    String categoryID = '',
    String subCategoryID = '',
    int fromDate = 0,
    int toDate = 0,
  });

  Future<Result<ChiTietTaiLieuDangTaiModel>> getChiTietTaiLieuDangTai(
      {required int id});
}
