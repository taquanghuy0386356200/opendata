import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';

mixin VanBanRepository {
  Future<Result<List<ItemThongBaoModel>>> getListVanBan({
    String? keySearch,
    int? pageSize,
    int? pageIndex,
  });

  Future<Result<ItemThongBaoModel>> getDetailVanBan(
      String id,
  );
}
