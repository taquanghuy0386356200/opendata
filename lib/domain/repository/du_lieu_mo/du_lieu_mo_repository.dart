import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/domain/models/open_data_model/detail_open_data_model.dart';
import 'package:bnv_opendata/domain/models/open_data_model/open_data_model.dart';

mixin DuLieuMoRepository {
  Future<Result<DataDuLieuMoModel>> getListOpenData({
    int? pageSize,
    int? pageIndex,
    String? keySearch,
    String nguoiDung = '',
    int? categoryID,
    int? subCategoryID,
    bool? bookmark,
  });

  Future<Result<DetailOpenDataModel>> getDtailOpenData(
    int id,
    String nguoiDung,
  );

  Future<Result<DataDuLieuMoModel>> getListBaiViet({
    int? pageSize,
    int? pageIndex,
    String? keySearch,
    String nguoiDung = '',
    int? categoryID,
    int? subCategoryID,
    bool? bookmark,
  });

  Future<Result<DetailOpenDataModel>> getDetailBaiViet(
    int id,
    String nguoiDung,
  );
}
