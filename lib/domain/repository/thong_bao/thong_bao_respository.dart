import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/domain/model/document_model.dart';
import 'package:bnv_opendata/domain/model/notification_model.dart';
import 'package:bnv_opendata/domain/models/thong_bao/item_thong_bao_model.dart';

mixin ThongBaoRepository {
  Future<Result<ThongBaoTotalModel>> getListThongBao({
    String? keySearch,
    int? pageSize,
    int? pageIndex,
  });

  Future<Result<List<DocumentModel>>> getListVanBan({
    String? keySearch,
    int? pageSize,
    int? pageIndex,
  });

  Future<Result<ItemThongBaoModel>> getDetailThongBao({
    String? id,
  });

  Future<Result<List<ThongBaoModel>>> getListThongBaoMoiNhat({
    String? keySearch,
    int? pageSize,
    int? pageIndex,
  });
}
