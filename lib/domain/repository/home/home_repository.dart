import 'package:bnv_opendata/data/result/result.dart';
import 'package:bnv_opendata/domain/model/category_model.dart';

mixin HomeRepository {
  Future<Result<List<CategoryModel>>> getListCategoryHome();
}
