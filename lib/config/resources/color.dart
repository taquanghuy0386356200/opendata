import 'package:flutter/material.dart';

///=========== Colors for default when didn't setup app theme ===============
///https://stackoverflow.com/a/17239853
const colorPrimary = Color(0xff0ABAB5);
const colorPrimaryTransparent = Color(0x720ABAB5);
const colorAccent = Color(0xffDCFFFE);
const colorSelected = Color(0xFFE0F2F1);
const blackColor = Color(0xFF303742);
const colorHeaderBG = [Color.fromRGBO(226, 111, 44, 0.2), Color(0xffffffff)];
const color364564 = Color(0xff364564);
const colorA2AEBD = Color(0xffA2AEBD);
const color667793 = Color(0xff667793);
Color color66779350 = const Color(0xff667793).withOpacity(0.5);
const color586B8B = Color(0xff586B8B);
const colorE2E8F0 = Color(0xffE2E8F0);
const colorE26F2C = Color(0xffE26F2C);
Color colorE26F2C25 = const Color(0xffE26F2C).withOpacity(0.25);
const colorFEF2E2 = Color(0xffFEF2E2);
const colorDBDFEF = Color(0xffDBDFEF);
const colorFFFFFF = Color(0xffFFFFFF);
const colorE2AEBD = Color(0xffA2AEBD);
Color colorDBDFEF50 = const Color(0xffDBDFEF).withOpacity(0.5);
const colorF9FAFF = Color(0xffF9FAFF);
const color20C997 = Color(0xff20C997);
Color color20C997opacity = const Color(0xff20C997).withOpacity(0.1);
Color color6589CF14 = const Color(0xff6589CF).withOpacity(0.14);
const colorE9F2FF = Color(0xffE9F2FF);
const colorBgHome = Colors.white;
const colorECEEF7 = Color(0xffECEEF7);
const colorFF9F43 = Color(0xffFF9F43);
const colorFF4F50 = Color(0xffFF4F50);
const color304261 = Color(0xff304261);
const color3D5586 = Color(0xFF3D5586);
const borderColor = Color(0xffDBDFEF);
const colorDDDDE6 = Color(0xffDDDDE6);

///=========== Using to make change app theme ================================
abstract class AppColor {
  Color primaryColor();

  Color accentColor();

  Color statusColor();

  Color mainColor();

  Color bgColor();

  Color dfTxtColor();

  Color secondTxtColor();

  Color dfBtnColor();

  Color dfBtnTxtColor();

  Color txtLightColor();

  Color sideBtnColor();

  Color disableColor();

  List<Color> headerBgColor();
}

class LightApp extends AppColor {
  @override
  Color primaryColor() {
    return colorPrimary;
  }

  @override
  Color accentColor() {
    return colorAccent;
  }

  @override
  Color statusColor() {
    return const Color(0xFFFCFCFC);
  }

  @override
  Color mainColor() {
    return const Color(0xFF30536F);
  }

  @override
  Color bgColor() {
    return const Color(0xFFFCFCFC);
  }

  @override
  Color dfBtnColor() {
    return const Color(0xFF324452);
  }

  @override
  Color dfBtnTxtColor() {
    return const Color(0xFFFFFFFF);
  }

  @override
  Color dfTxtColor() {
    return const Color(0xFF303742);
  }

  @override
  Color secondTxtColor() {
    return const Color(0xFF9097A3);
  }

  @override
  Color txtLightColor() {
    return Colors.white.withOpacity(0.85);
  }

  @override
  Color sideBtnColor() {
    return const Color(0xFFDCFFFE);
  }

  @override
  Color disableColor() {
    return const Color(0xFFA9B8BD);
  }

  @override
  List<Color> headerBgColor() {
    return colorHeaderBG;
  }
}

class DarkApp extends AppColor {
  @override
  Color primaryColor() {
    return Colors.black;
  }

  @override
  Color accentColor() {
    return Colors.black;
  }

  @override
  Color statusColor() {
    return Colors.black;
  }

  @override
  Color mainColor() {
    return Colors.black.withOpacity(0.8);
  }

  @override
  Color bgColor() {
    return Colors.black.withOpacity(0.8);
  }

  @override
  Color dfBtnColor() {
    return Colors.white.withOpacity(0.8);
  }

  @override
  Color dfBtnTxtColor() {
    return Colors.black.withOpacity(0.6);
  }

  @override
  Color dfTxtColor() {
    return Colors.white.withOpacity(0.6);
  }

  @override
  Color secondTxtColor() {
    return Colors.black.withOpacity(0.4);
  }

  @override
  Color txtLightColor() {
    return Colors.white.withOpacity(0.85);
  }

  @override
  Color sideBtnColor() {
    return const Color(0xFFA9B8BD);
  }

  @override
  Color disableColor() {
    return Colors.grey;
  }

  @override
  List<Color> headerBgColor() {
    return colorHeaderBG;
  }
}

///============ End setup app theme ======================================
