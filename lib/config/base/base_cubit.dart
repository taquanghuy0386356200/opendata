import 'package:bnv_opendata/config/base/rx.dart';
import 'package:bnv_opendata/utils/constants/api_constants.dart';
import 'package:bnv_opendata/widgets/views/state_layout.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:rxdart/rxdart.dart';

abstract class BaseCubit<BaseState> extends BlocBase<BaseState> {
  BaseCubit(BaseState initialState) : super(initialState);

  final BehaviorSubject<List<dynamic>> loadMoreListController =
      BehaviorSubject();

  Stream<List<dynamic>> get loadMoreListStream => loadMoreListController.stream;
  BehaviorSubject<bool> inSearching = BehaviorSubject.seeded(false);

  final List<dynamic> loadMoreList = [];
  int loadMorePage = ApiConstants.PAGE_BEGIN;
  bool canLoadMore = true;
  bool loadMoreRefresh = true;
  bool loadMoreLoading = false;
  bool canMoveToTop = true;

  int get loadMoreItemCount =>
      canLoadMore ? loadMoreList.length + 1 : loadMoreList.length;

  final BehaviorSubject<StateLayout> _state =
      BehaviorSubject<StateLayout>.seeded(StateLayout.showLoading);

  Stream<StateLayout> get stateStream => _state.stream;

  StateLayout get stateLayout => _state.stream.value;

  final _canLoadMoreSub = BehaviorSubject<bool>.seeded(false);

  Stream<bool> get loadMoreStream => _canLoadMoreSub.stream;

  Sink<bool> get loadMoreSink => _canLoadMoreSub.sink;

  bool get getInLoadMore => _canLoadMoreSub.valueOrNull ?? false;

  void updateStateError() {
    _state.sink.add(StateLayout.showError);
  }

  void showLoading() {
    _state.wellAdd(StateLayout.showLoading);
  }

  void showError() {
    _state.wellAdd(StateLayout.showError);
  }

  void showEmpty() {
    _state.wellAdd(StateLayout.showEmpty);
  }

  void showContent() {
    _state.wellAdd(StateLayout.showContent);
  }
}
